package;

enum _Reward {
  Reward0(i: _Item);
  Reward1(ib: _IslandBonus);
  Reward2;
  Reward3;
  Reward4(n: Int);
  Reward5;
  Reward6;
  Reward7;
  Reward8(gb: _GameBonus);
}
