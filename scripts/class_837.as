package
{
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.class_755;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_837 extends class_645
   {
       
      
      public var var_307:int;
      
      public var var_1098:int;
      
      public var var_577:Array;
      
      public var var_1147:Object;
      
      public var var_1145:Number;
      
      public var var_1146:Number;
      
      public var var_948:Number;
      
      public function class_837()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Function = null;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:* = null;
         var _loc5_:* = null;
         loop1:
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = function(param1:Object, param2:Object):int
               {
                  if(param1.var_631.var_245 < param2.var_631.var_245)
                  {
                     return -1;
                  }
                  return 1;
               };
               var_577.sort(_loc1_);
               _loc2_ = 0;
               _loc3_ = var_577;
               while(true)
               {
                  if(_loc2_ >= int(_loc3_.length))
                  {
                     break loop1;
                  }
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  _loc5_ = {
                     "L\x01":Number(var_948 + int(_loc4_.var_244) * var_1145),
                     "\x03\x01":Number(var_1146 + int(_loc4_.var_245) * var_1145)
                  };
                  _loc4_.var_631.method_229(_loc5_,0.5,null);
                  var_232.method_110(_loc4_.var_631.var_243);
               }
         }
         super.method_79();
      }
      
      public function name_55(param1:Object) : void
      {
         var _loc2_:int = int(param1.var_244);
         var _loc3_:int = int(param1.var_245);
         param1.var_244 = int(var_1147.var_244);
         param1.var_245 = int(var_1147.var_245);
         var_1147.var_244 = _loc2_;
         var_1147.var_245 = _loc3_;
      }
      
      public function method_241() : void
      {
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc1_:int = int(Number(2 + var_237[0] * 30));
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            _loc3_ = var_577[int(class_691.method_114(int(var_577.length)))];
            _loc4_ = Number(Number(Math.abs(int(var_1147.var_244) - int(_loc3_.var_244))) + Number(Math.abs(int(var_1147.var_245) - int(_loc3_.var_245))));
            if(_loc4_ == 1)
            {
               name_55(_loc3_);
               _loc2_++;
            }
         }
         if(method_228())
         {
            method_241();
            return;
         }
         var _loc5_:int = 0;
         var _loc6_:Array = var_577;
         while(_loc5_ < int(_loc6_.length))
         {
            _loc3_ = _loc6_[_loc5_];
            _loc5_++;
            _loc3_.var_631.var_244 = Number(var_948 + int(_loc3_.var_244) * var_1145);
            _loc3_.var_631.var_245 = Number(var_1146 + int(_loc3_.var_245) * var_1145);
         }
      }
      
      public function method_400(param1:Object) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:class_892 = null;
         var _loc10_:class_892 = null;
         if(Number(Number(Math.abs(int(var_1147.var_244) - int(param1.var_244))) + Number(Math.abs(int(var_1147.var_245) - int(param1.var_245)))) == 1)
         {
            name_55(param1);
            if(int(var_1147.var_244) == 0 && int(var_1147.var_245) == 0 && Boolean(method_228()))
            {
               _loc2_ = 0;
               _loc3_ = var_577;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  _loc4_.var_631.var_243.mouseEnabled = false;
                  _loc4_.var_631.var_243.mouseChildren = false;
                  new class_931(_loc4_.var_631.var_243,0.1);
                  _loc5_ = 150;
                  _loc6_ = 0;
                  while(_loc6_ < 3)
                  {
                     _loc6_++;
                     _loc7_ = _loc6_;
                     _loc8_ = Number(0.1 + Math.random() * 0.4);
                     _loc10_ = new class_892(var_232.method_84(class_899.__unprotect__("T3\f8"),class_645.var_209));
                     _loc10_.var_233 = 0.99;
                     _loc9_ = _loc10_;
                     _loc9_.var_244 = Number(200 + (Math.random() * 2 - 1) * _loc5_);
                     _loc9_.var_245 = Number(200 + (Math.random() * 2 - 1) * _loc5_);
                     _loc9_.var_278 = 0;
                     _loc9_.var_279 = 0;
                     _loc9_.var_243.gotoAndPlay(int(class_691.method_114(10)) + 1);
                     _loc9_.var_250 = Number(0.05 + Math.random() * 0.1);
                     _loc9_.var_251 = 16 + int(class_691.method_114(10));
                     _loc9_.method_119(Number(0.3 + Math.random() * 2));
                     _loc9_.method_118();
                     var_232.method_124(_loc9_.var_243,class_645.var_210);
                  }
               }
               method_81(true,20);
            }
         }
      }
      
      public function method_823(param1:class_656, param2:Object) : void
      {
         var var_40:Object = param2;
         var var_214:class_837 = this;
         param1.var_243.addEventListener(MouseEvent.CLICK,function(param1:*):void
         {
            var_214.method_400(var_40);
         });
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc4_:* = null;
         var_227 = [600 - param1 * 100];
         super.method_95(param1);
         var_307 = 320;
         var_1098 = 3;
         if(param1 > 1)
         {
            var_1098 = var_1098 + 1;
         }
         var_1147 = {
            "L\x01":0,
            "\x03\x01":0
         };
         method_117();
         method_241();
         var _loc2_:int = 0;
         var _loc3_:Array = var_577;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.var_631.method_118();
         }
      }
      
      public function method_228() : Boolean
      {
         var _loc3_:* = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_577;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(int(_loc3_.var_215) != int(Math.round(int(_loc3_.var_245) + int(_loc3_.var_244) * var_1098)))
            {
               return false;
            }
         }
         return true;
      }
      
      public function method_117() : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:class_656 = null;
         var _loc11_:class_656 = null;
         var _loc12_:MovieClip = null;
         var _loc13_:Number = NaN;
         var _loc14_:* = null;
         class_319 = var_232.method_84(class_899.__unprotect__("[\x0eW3\x02"),0);
         var_948 = (class_742.var_216 - var_307) * 0.5;
         var_1146 = (class_742.var_218 - var_307) * 0.5;
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("w*%\'\x03"),class_645.var_209);
         _loc1_.x = var_948;
         _loc1_.y = var_1146;
         _loc1_.gotoAndStop(1);
         var_577 = [];
         var_1145 = var_307 / var_1098;
         var _loc2_:int = 0;
         var _loc3_:int = int(class_691.method_114(3)) + 1;
         var _loc4_:int = 0;
         var _loc5_:int = var_1098;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = var_1098;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               if(int(var_1147.var_244) != _loc6_ || int(var_1147.var_245) != _loc9_)
               {
                  _loc11_ = new class_656(var_232.method_84(class_899.__unprotect__("\x1c\x06\bJ\x01"),class_645.var_209));
                  _loc10_ = _loc11_;
                  _loc10_.var_244 = Number(var_948 + _loc6_ * var_1145);
                  _loc10_.var_245 = Number(var_1146 + _loc9_ * var_1145);
                  _loc10_.var_243.scaleX = var_1145 * 0.01;
                  _loc10_.var_243.scaleY = var_1145 * 0.01;
                  _loc10_.method_118();
                  _loc12_ = new class_755(_loc10_.var_243.var_64).method_84(class_899.__unprotect__(",w\\\x03"),0);
                  _loc13_ = 100 / var_1145;
                  _loc12_.gotoAndStop(_loc3_);
                  _loc12_.scaleX = _loc13_;
                  _loc12_.scaleY = _loc13_;
                  _loc12_.x = -_loc6_ * var_1145 * _loc13_;
                  _loc12_.y = -_loc9_ * var_1145 * _loc13_;
                  _loc14_ = {
                     "gB\x01":_loc10_,
                     "L\x01":_loc6_,
                     "\x03\x01":_loc9_,
                     "\x1d\x0b\x01":_loc2_
                  };
                  var_577.push(_loc14_);
                  method_823(_loc10_,_loc14_);
               }
               _loc2_++;
            }
         }
         _loc12_ = var_232.method_84(class_899.__unprotect__("w*%\'\x03"),class_645.var_209 + 1);
         _loc12_.x = var_948;
         _loc12_.y = var_1146;
         _loc12_.gotoAndStop(2);
      }
   }
}
