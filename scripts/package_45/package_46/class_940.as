package package_45.package_46
{
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_13.class_719;
   import package_13.class_826;
   import package_24.class_656;
   import package_24.class_878;
   import package_32.class_899;
   
   public class class_940 extends class_656
   {
       
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_607:Number;
      
      public var var_1562:class_826;
      
      public var var_282:class_656;
      
      public var var_528:Number;
      
      public var var_267:class_719;
      
      public var var_17:Number;
      
      public var var_337:Number;
      
      public var var_336:Number;
      
      public function class_940(param1:class_719 = undefined, param2:class_826 = undefined, param3:Object = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_267 = param1;
         var_1562 = param2;
         var_282 = new class_656();
         var _loc4_:Number = 2;
         var_607 = _loc4_;
         var_17 = _loc4_;
         var_336 = 0;
         var_337 = 0;
         var_282.method_137(class_702.package_9.method_136("butterfly"));
         addChild(var_282);
         var_267.var_232.method_124(this,class_719.var_451);
         graphics.beginFill(0,0.2);
         graphics.drawEllipse(-1 * 2,-1,1 * 4,1 * 2);
         var _loc6_:* = var_1562.method_294();
         x = Number(_loc6_.var_244);
         y = Number(_loc6_.var_245);
         class_657.method_139(var_282,int(class_657.method_138(param3)));
         var_528 = 0;
         _loc4_ = var_528;
         var_282.scaleY = _loc4_;
         var_282.scaleX = _loc4_;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Array = null;
         super.method_79();
         if(int(class_691.method_114(20)) == 0)
         {
            var_607 = Number(2 + Math.random() * 14);
         }
         var_17 = Number(var_17 + (var_607 - var_17) * 0.1);
         var_282.y = -var_17;
         if(int(class_691.method_114(20)) == 0 && int(var_1562.var_541.length) > 0)
         {
            _loc1_ = var_1562.var_541.method_78();
            class_659.method_241(_loc1_);
            var_1562 = _loc1_[0];
         }
         var _loc2_:* = method_1171();
         var_278 = (_loc2_.var_244 - x) * 0.1;
         var_279 = (_loc2_.var_245 - y) * 0.1;
         x = Number(x + var_278);
         y = Number(y + var_279);
         if(int(class_691.method_114(20)) == 0)
         {
            var_336 = int(class_691.method_114(16)) - 8;
         }
         if(int(class_691.method_114(20)) == 0)
         {
            var_337 = int(class_691.method_114(16)) - 8;
         }
         if(var_528 < 1)
         {
            var_528 = Number(var_528 + 0.1);
         }
         var _loc3_:Number = var_528;
         var_282.scaleY = _loc3_;
         var_282.scaleX = _loc3_;
      }
      
      public function method_1171() : Object
      {
         var _loc1_:* = var_1562.method_294();
         return {
            "L\x01":Number(Number(_loc1_.var_244) + var_336),
            "\x03\x01":Number(Number(_loc1_.var_245) + var_337)
         };
      }
   }
}
