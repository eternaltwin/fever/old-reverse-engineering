package
{
   import flash.display.MovieClip;
   
   public dynamic class class_33 extends MovieClip
   {
       
      
      public var _bomb:class_113;
      
      public var _mask:MovieClip;
      
      public var var_25:MovieClip;
      
      public var _monster:MovieClip;
      
      public var _spark:MovieClip;
      
      public var _p0:MovieClip;
      
      public var _p2:MovieClip;
      
      public var _p3:MovieClip;
      
      public var _p4:MovieClip;
      
      public var _p5:MovieClip;
      
      public var _p6:MovieClip;
      
      public var _p7:MovieClip;
      
      public function class_33()
      {
         super();
         addFrameScript(0,method_1,5,method_4);
      }
      
      public function method_4() : *
      {
         stop();
      }
      
      public function method_1() : *
      {
         stop();
      }
   }
}
