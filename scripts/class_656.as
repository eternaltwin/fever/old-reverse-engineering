package
{
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public dynamic class class_656
   {
      
      public static var var_226:Array = [];
       
      
      public var var_245:Number;
      
      public var var_244:Number;
      
      public var var_344:Number;
      
      public var var_243:MovieClip;
      
      public function class_656(param1:MovieClip = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_243 = param1;
         var _loc2_:* = var_243;
         _loc2_.name_24 = this;
         var_243["obj"] = this;
         class_656.var_226.push(this);
         var_244 = var_243.x;
         var_245 = var_243.y;
         if(var_243.x == 0 && var_243.y == 0)
         {
            var_243.x = -100;
            var_243.y = -100;
            var_244 = 0;
            var_245 = 0;
         }
         var_344 = 1;
      }
      
      public function method_118() : void
      {
         var_243.x = var_244;
         var_243.y = var_245;
      }
      
      public function method_79() : void
      {
         method_118();
      }
      
      public function method_229(param1:Object, param2:Number, param3:Object = undefined, param4:Object = undefined) : void
      {
         if(param3 == null)
         {
            param3 = 1 / 0;
         }
         var _loc5_:Number = param1.var_244 - var_244;
         var _loc6_:Number = param1.var_245 - var_245;
         var _loc7_:Number = Number(class_663.method_99(-param3,_loc5_ * param2,param3));
         var _loc8_:Number = Number(class_663.method_99(-param3,_loc6_ * param2,param3));
         var_244 = Number(var_244 + _loc7_);
         var_245 = Number(var_245 + (param4 == null?_loc8_:Number(var_245 + _loc8_) >= param4?Number(0):_loc8_));
      }
      
      public function method_119(param1:Number) : void
      {
         var_344 = param1;
         var_243.scaleX = param1;
         var_243.scaleY = param1;
      }
      
      public function method_230(param1:int = 1) : void
      {
         var_243.x = int(var_244 / param1) * param1;
         var_243.y = int(var_245 / param1) * param1;
      }
      
      public function method_89() : void
      {
         if(var_243 != null && var_243.parent != null)
         {
            var_243.parent.removeChild(var_243);
         }
         class_656.var_226.method_116(this);
      }
      
      public function method_115(param1:Object) : Number
      {
         var _loc2_:Number = param1.var_244 - var_244;
         var _loc3_:Number = param1.var_245 - var_245;
         return Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)));
      }
      
      public function method_231(param1:Object) : Number
      {
         var _loc2_:Number = param1.var_244 - var_244;
         var _loc3_:Number = param1.var_245 - var_245;
         return Number(Math.atan2(_loc3_,_loc2_));
      }
   }
}
