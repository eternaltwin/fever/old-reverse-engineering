package
{
   import flash.display.MovieClip;
   import flash.filters.DropShadowFilter;
   import package_10.class_805;
   import package_10.class_821;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_17.class_709;
   import package_17.class_718;
   import package_17.class_719;
   import package_17.class_800;
   import package_17.class_833;
   import package_17.class_863;
   import package_17.class_939;
   import package_20.package_21.class_893;
   import package_20.package_21.class_901;
   import package_32.class_899;
   
   public class class_751 extends class_645
   {
      
      public static var var_370:Boolean;
      
      public static var var_770:class_709;
      
      public static var var_771:int = 72;
       
      
      public var var_431:Number;
      
      public var var_251:Number;
      
      public var var_772:class_795;
      
      public var var_676:class_795;
      
      public var var_773:class_689;
      
      public var var_528:Number;
      
      public var var_777:class_755;
      
      public function class_751()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_508() : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:* = null;
         var _loc5_:Number = NaN;
         var _loc6_:int = 0;
         var _loc1_:int = 0;
         var _loc2_:* = var_772.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            _loc4_ = method_507(_loc1_);
            _loc5_ = int(_loc4_.var_245) - _loc3_.y;
            _loc6_ = 8;
            _loc3_.y = Number(_loc3_.y + Number(class_663.method_99(-_loc6_,_loc5_ * 0.4,_loc6_)));
            _loc1_++;
         }
      }
      
      public function method_509() : void
      {
         var _loc5_:class_833 = null;
         var _loc6_:class_939 = null;
         var _loc1_:* = method_100();
         if(Number(_loc1_.var_244) < class_751.var_771 + 32)
         {
            _loc1_.var_244 = 100;
         }
         if(Number(_loc1_.var_244) > class_742.var_216 - 32)
         {
            _loc1_.var_244 = class_742.var_216 - 32;
         }
         if(Number(_loc1_.var_245) < 32)
         {
            _loc1_.var_245 = 32;
         }
         if(Number(_loc1_.var_245) > class_742.var_218 - 80)
         {
            _loc1_.var_245 = class_742.var_218 - 80;
         }
         var_773.method_129(Number(_loc1_.var_244),Number(_loc1_.var_245));
         var _loc3_:Boolean = false;
         var _loc4_:class_821 = var_773.var_21.var_222.var_84;
         while(_loc4_ != null)
         {
            _loc5_ = _loc4_.var_223;
            _loc4_ = _loc4_.name_1;
            _loc6_ = _loc5_.var_774;
            while(_loc6_ != null)
            {
               if(_loc6_.var_775)
               {
                  _loc3_ = true;
               }
               _loc6_.var_775 = false;
               _loc6_ = _loc6_.name_1;
            }
         }
         var_773.var_243.alpha = !!_loc3_?0.5:Number(1);
         var_773.method_339(0,0);
         if(!_loc3_ && name_5)
         {
            method_219();
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:class_805 = null;
         var _loc3_:class_719 = null;
         var _loc5_:class_689 = null;
         super.method_79();
         package_13.name_3(1,5);
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_509();
               break;
            case 2:
               _loc1_ = var_251;
               var_251 = var_251 - 1;
               if(_loc1_ == 0)
               {
                  if(var_772.name_37 > 0)
                  {
                     method_510();
                     break;
                  }
                  method_511();
                  break;
               }
               break;
            case 3:
               _loc1_ = 0;
               _loc2_ = package_13.var_776.var_84;
               while(_loc2_ != null)
               {
                  _loc3_ = _loc2_.var_223;
                  _loc2_ = _loc2_.name_1;
                  _loc1_ = Number(_loc1_ + _loc3_.var_677);
               }
               if(_loc1_ < 0.15)
               {
                  var_251 = var_251 - 1;
               }
               else
               {
                  var_251 = var_251 - 0.05;
               }
               if(var_251 <= 0)
               {
                  method_81(true,10);
                  break;
               }
         }
         method_508();
         var _loc4_:* = var_676.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            if(_loc5_.var_245 > class_742.var_218)
            {
               method_81(false,10);
            }
         }
      }
      
      public function method_512() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:* = null;
         var _loc7_:MovieClip = null;
         var_772 = new class_795();
         var _loc1_:Array = [0,0,0,0,1,1,1,1,2,2,2,2];
         var _loc2_:int = 0;
         while(_loc2_ < 7)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = int(class_691.method_114(int(_loc1_.length)));
            _loc5_ = int(_loc1_[_loc4_]);
            _loc1_.splice(_loc4_,1);
            _loc6_ = method_507(_loc3_);
            _loc7_ = var_777.method_84(class_899.__unprotect__("o(!\x1b\x01"),1);
            _loc7_.x = int(_loc6_.var_244);
            _loc7_.y = int(_loc6_.var_245);
            _loc7_.var_215 = _loc5_;
            _loc7_.var_65 = 0;
            if(int(_loc7_.var_215) == 1 && int(class_691.method_114(2)) == 0)
            {
               _loc7_.var_65 = Number(Number(_loc7_.var_65) + 0.77);
            }
            if(var_237[0] > 0.5 && _loc3_ > 2 && int(_loc7_.var_215) == 2 && int(class_691.method_114(2)) == 0)
            {
               _loc7_.var_65 = Number(Number(_loc7_.var_65) + 3.14);
            }
            _loc7_.gotoAndStop(int(_loc7_.var_215) + 1);
            _loc7_.rotation = _loc7_.var_65 / 0.0174;
            var_772.method_124(_loc7_);
         }
      }
      
      public function method_510() : void
      {
         name_3 = 1;
         var _loc1_:MovieClip = var_772.name_12();
         var_773 = new class_689(_loc1_);
         var_773.var_536 = this;
         var _loc2_:* = method_100();
         var_773.var_538 = class_751.var_770;
         var_773.method_129(Number(_loc2_.var_244),Number(_loc2_.var_245));
         var_773.method_349(Number(_loc1_.var_65));
         var_773.var_21.var_663.var_778 = 1;
         var_773.var_21.var_663.var_779 = 0.4;
         switch(int(_loc1_.var_215))
         {
            case 0:
               var_773.method_347(44 * var_528,44 * var_528);
               break;
            case 1:
               var_773.method_347(16 * var_528,50 * var_528);
               var_773.method_347(50 * var_528,16 * var_528);
               break;
            case 2:
               var_773.method_344([[0,-30],[-15,-4],[-30,22],[0,22],[30,22],[15,-4]]);
         }
         _loc1_.alpha = 1;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - 100 * param1];
         super.method_95(param1);
         var_528 = 1;
         var_431 = 220 - param1 * 100;
         method_117();
      }
      
      public function method_507(param1:int) : Object
      {
         return {
            "L\x01":36,
            "\x03\x01":36 + param1 * 62
         };
      }
      
      public function method_511() : void
      {
         var_229 = [true];
         name_3 = 3;
         var_251 = 30;
      }
      
      public function method_219() : void
      {
         var_773.method_342(false);
         name_3 = 2;
         var_251 = 10;
         var_676.method_103(var_773);
      }
      
      public function method_117() : void
      {
         var _loc8_:MovieClip = null;
         var _loc10_:int = 0;
         var _loc1_:MovieClip = var_232.method_92(1);
         var_777 = new class_755(_loc1_);
         var _loc2_:DropShadowFilter = new DropShadowFilter();
         _loc2_.color = 0;
         _loc2_.distance = 4;
         _loc2_.angle = 135;
         _loc2_.strength = 0.2;
         _loc1_.filters = [_loc2_];
         method_91();
         var _loc3_:class_893 = new class_893(0,0,class_742.var_216,class_742.var_218);
         package_13 = new package_17.class_652(_loc3_,new class_901());
         var _loc4_:class_718 = package_13.var_661;
         _loc4_.var_244 = 0;
         _loc4_.var_245 = 0.3;
         var _loc5_:int = class_742.var_218 - 40;
         var _loc6_:class_863 = class_800.method_346(var_431,20,Number(class_751.var_771 + (class_742.var_216 - class_751.var_771 - var_431) * 0.5),_loc5_);
         package_13.method_513(_loc6_);
         var _loc7_:MovieClip = var_777.method_84(class_899.__unprotect__("\x14\x051b\x03"),1);
         _loc7_.x = Number(class_751.var_771 + (class_742.var_216 - class_751.var_771) * 0.5);
         _loc7_.y = _loc5_ + 10;
         _loc8_ = _loc7_.var_1;
         _loc8_.scaleX = (var_431 - 20) * 0.01;
         class_319 = var_232.method_84(class_899.__unprotect__("+\x1fDj\x02"),0);
         var _loc9_:int = 0;
         while(_loc9_ < 2)
         {
            _loc9_++;
            _loc10_ = _loc9_;
            _loc8_ = Reflect.field(_loc7_,"$b" + _loc10_);
            _loc8_.x = (_loc10_ * 2 - 1) * (var_431 - 40) * 0.5;
         }
         var_676 = new class_795();
         method_512();
         method_510();
      }
   }
}
