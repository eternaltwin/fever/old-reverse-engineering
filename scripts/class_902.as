package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.package_12.class_658;
   import package_17.class_709;
   import package_17.class_718;
   import package_17.class_809;
   import package_20.package_21.class_893;
   import package_20.package_21.class_901;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_902 extends class_645
   {
      
      public static var var_370:Boolean;
      
      public static var var_1266:int = 60;
      
      public static var var_1400:class_809;
      
      public static var var_1401:class_709;
      
      public static var var_1402:class_709;
       
      
      public var var_251:int;
      
      public var var_1405:class_795;
      
      public var var_1404:class_689;
      
      public var var_1406:class_795;
      
      public var var_1407:int;
      
      public function class_902()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_1052() : void
      {
         if(!name_5)
         {
            method_404();
         }
         if(var_1404 == null)
         {
            return;
         }
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - var_1404.var_1171;
         var _loc3_:Number = _loc1_.var_245 - var_1404.var_334;
         var _loc4_:Number = Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)));
         var _loc5_:Number = Number(Math.atan2(_loc3_,_loc2_));
         if(_loc4_ > class_902.var_1266)
         {
            _loc4_ = class_902.var_1266;
            _loc1_.var_244 = Number(Number(var_1404.var_1171) + Math.cos(_loc5_) * class_902.var_1266);
            _loc1_.var_245 = Number(Number(var_1404.var_334) + Math.sin(_loc5_) * class_902.var_1266);
         }
         var _loc6_:MovieClip = var_1404.method_327.var_1;
         _loc6_.scaleX = _loc4_ * 0.01;
         var_1404.method_327.rotation = _loc5_ / 0.0174;
         var_1404.method_129(Number(_loc1_.var_244),Number(_loc1_.var_245));
      }
      
      override public function method_79() : void
      {
         var _loc3_:class_689 = null;
         var _loc4_:MovieClip = null;
         var _loc6_:Boolean = false;
         package_13.name_3(1,2);
         method_76();
         method_1052();
         var _loc1_:int = var_251;
         var_251 = var_251 - 1;
         if(_loc1_ == 0)
         {
            method_1053();
            var_251 = 10;
         }
         var _loc2_:* = var_1405.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            _loc4_ = _loc3_.var_243.var_1;
            _loc4_.rotation = -_loc3_.var_243.rotation;
            if(int(_loc3_.name_139) > 0)
            {
               _loc3_.name_139 = int(_loc3_.name_139) - 1;
            }
            if(_loc3_.var_245 > class_742.var_218 + 10)
            {
               _loc3_.method_89();
               var_1405.method_116(_loc3_);
            }
         }
         var _loc5_:Boolean = true;
         _loc2_ = var_1406.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            if(Number(_loc3_.var_562) > 0 && var_220 == null)
            {
               _loc3_.var_562 = _loc3_.var_562 - 0.025;
            }
            _loc4_ = _loc3_.var_243.var_1;
            _loc4_.gotoAndStop(1 + int(Number(_loc3_.var_562)));
            _loc6_ = Number(_loc3_.var_562) >= 18;
            _loc3_.var_243.gotoAndStop(!!_loc6_?2:1);
            if(!_loc6_)
            {
               _loc5_ = false;
            }
         }
         if(_loc5_)
         {
            method_81(true,20);
         }
         super.method_79();
      }
      
      public function method_404() : void
      {
         var_1404 = null;
      }
      
      public function method_405(param1:class_689) : void
      {
         var _loc2_:MovieClip = null;
         var_1404 = param1;
         if(param1.method_327 == null)
         {
            param1.method_327 = var_232.method_84(class_899.__unprotect__("U\x03Hn"),0);
            _loc2_ = param1.method_327.var_1;
            _loc2_.scaleX = 0;
            class_658.var_146(param1.method_327,10,1,16777215);
            param1.method_327.blendMode = BlendMode.ADD;
            param1.method_327.x = Number(param1.var_1171);
            param1.method_327.y = Number(param1.var_334);
            param1.method_327.scaleY = 0.4;
         }
      }
      
      public function method_1053() : void
      {
         var _loc1_:class_689 = new class_689(var_232.method_84(class_899.__unprotect__("HSjI"),1));
         _loc1_.var_536 = this;
         _loc1_.var_21.var_663 = class_902.var_1400;
         _loc1_.method_129(Number(class_742.var_216 * 0.5 + 1),-10);
         _loc1_.method_349(0.1);
         _loc1_.name_139 = 0;
         _loc1_.method_345(6);
         var_1405.method_103(_loc1_);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [700 - 100 * param1];
         super.method_95(param1);
         var_251 = 0;
         var_1407 = 2 + int(param1 * 3);
         var_1405 = new class_795();
         method_117();
      }
      
      public function method_1054(param1:class_689, param2:class_689) : void
      {
         var _loc4_:MovieClip = null;
         if(var_1404 == param1)
         {
            return;
         }
         var _loc3_:class_689 = param2;
         if(int(_loc3_.name_139) == 0)
         {
            new class_931(param2.var_243);
            _loc3_.name_139 = 6;
            param1.var_562 = Number(Number(param1.var_562) + 2.5);
            if(Number(param1.var_562) > 30)
            {
               param1.var_562 = 24;
            }
            _loc4_ = param1.var_243.var_1;
            _loc4_.gotoAndStop(1 + int(Number(param1.var_562)));
         }
      }
      
      public function method_117() : void
      {
         var _loc8_:int = 0;
         var _loc9_:MovieClip = null;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("[?\x140"),0);
         method_91();
         var _loc1_:class_893 = new class_893(0,0,class_742.var_216,class_742.var_218);
         package_13 = new package_17.class_652(_loc1_,new class_901());
         var _loc2_:class_718 = package_13.var_661;
         _loc2_.var_244 = 0;
         _loc2_.var_245 = 0.3;
         var_1406 = new class_795();
         var _loc4_:Number = Number(class_742.var_216 * 0.5 + 30);
         var _loc5_:int = 135 - var_1407 * 10;
         var _loc6_:int = 0;
         var _loc7_:int = var_1407;
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc9_ = var_232.method_84(class_899.__unprotect__("|\x0fe\x07"),1);
            _loc9_.stop();
            var var_1408:Array = [new class_689(_loc9_)];
            var_1408[0].var_536 = this;
            _loc10_ = int(_loc4_ / 10) * 10;
            _loc11_ = _loc5_;
            do
            {
               _loc4_ = Number(_loc4_ + (Math.random() * 2 - 1) * 200);
            }
            while(_loc4_ < 40 || _loc4_ > class_742.var_216 - 40 || Number(Math.abs(_loc4_ - _loc10_)) < 30);
            
            _loc5_ = _loc5_ + (116 - var_1407 * 9);
            var_1408[0].var_538 = class_902.var_1402;
            var_1408[0].method_129(_loc10_,_loc11_);
            var_1408[0].method_345(15);
            var_1408[0].method_342(true);
            var var_214:Array = [this];
            var_1408[0].var_243.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_1408:Array = param2;
               return function(param1:*):void
               {
                  var_214[0].method_405(var_1408[0]);
               };
            }(var_214,var_1408));
            _loc12_ = var_1408[0].var_243.var_1;
            _loc12_.stop();
            var_1408[0].var_562 = 0;
            var_1408[0].var_1171 = _loc10_;
            var_1408[0].var_334 = _loc11_;
            method_104(var_1408[0],function():Function
            {
               return function(param1:Function, param2:class_689):Function
               {
                  var var_27:Function = param1;
                  var var_18:class_689 = param2;
                  return function():Function
                  {
                     return function(param1:class_689):void
                     {
                        return var_27(var_18,param1);
                     };
                  }();
               };
            }()(method_1054,var_1408[0]));
            var_1406.method_103(var_1408[0]);
            _loc12_ = var_232.method_84(class_899.__unprotect__("F,j\x18"),0);
            _loc12_.x = _loc10_;
            _loc12_.y = _loc11_;
            _loc12_.gotoAndStop(int(class_691.method_114(_loc12_.totalFrames)) + 1);
         }
         _loc9_ = var_232.method_84(class_899.__unprotect__("F*{\x1b\x03"),2);
      }
   }
}
