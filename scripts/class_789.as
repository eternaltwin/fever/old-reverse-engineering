package
{
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_739;
   
   public class class_789 extends class_645
   {
      
      public static var var_941:int = 16;
      
      public static var var_942:int = 20;
      
      public static var var_943:int = 8;
       
      
      public var var_602:Sprite;
      
      public var var_944:Array;
      
      public var var_288:MovieClip;
      
      public var var_783:Number;
      
      public var var_430:Number;
      
      public var name_47:Number;
      
      public var var_945:Number;
      
      public var var_470:Array;
      
      public var var_946:BitmapData;
      
      public function class_789()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_706() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:MovieClip = null;
         var _loc7_:Number = NaN;
         var _loc1_:Array = var_944.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc4_ = 0;
            _loc5_ = var_470;
            while(_loc4_ < int(_loc5_.length))
            {
               _loc6_ = _loc5_[_loc4_];
               _loc4_++;
               _loc7_ = Number(_loc3_.method_115({
                  "L\x01":_loc6_.x,
                  "\x03\x01":_loc6_.y
               }));
               if(_loc7_ < class_789.var_942)
               {
                  _loc6_.name_3 = 2;
                  _loc6_.var_37 = _loc3_.var_279 * 10;
                  _loc3_.var_279 = _loc3_.var_279 * -1;
                  _loc3_.var_251 = 20;
                  _loc3_.var_250 = 0.5;
                  _loc3_.var_580 = (Math.random() * 2 - 1) * 36;
                  var_944.method_116(_loc3_);
                  break;
               }
            }
         }
      }
      
      override public function method_79() : void
      {
         var_945 = var_945 - 0.05;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_707();
               if(int(class_691.method_114(int(var_783))) == 0 || int(var_470.length) == 0)
               {
                  method_708();
               }
               method_706();
               method_709();
            case 2:
               method_707();
               if(int(class_691.method_114(int(var_783))) == 0 || int(var_470.length) == 0)
               {
                  method_708();
               }
               method_706();
               method_709();
         }
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      override public function method_83() : void
      {
         var _loc1_:class_892 = null;
         var _loc2_:class_892 = null;
         var _loc3_:Number = NaN;
         if(name_3 == 2)
         {
            return;
         }
         if(var_945 < 0.5)
         {
            var_945 = 1;
            var_288.gotoAndPlay("shoot");
            _loc2_ = new class_892(var_232.method_84(class_899.__unprotect__("g*@\x07\x03"),class_645.var_209));
            _loc2_.var_233 = 0.99;
            _loc1_ = _loc2_;
            _loc1_.var_244 = Number(var_288.x + 28);
            _loc1_.var_245 = 12;
            _loc3_ = Number(_loc1_.method_231(method_100()));
            _loc1_.var_278 = Math.cos(_loc3_) * class_789.var_943;
            _loc1_.var_279 = Math.sin(_loc3_) * class_789.var_943 - 2;
            _loc1_.var_580 = 4 * (Math.random() * 2 - 1);
            _loc1_.var_250 = 0;
            _loc1_.method_118();
            _loc1_.var_243.gotoAndStop(int(class_691.method_114(_loc1_.var_243.totalFrames)) + 1);
            _loc1_.var_243.rotation = Math.random() * 360;
            var_944.push(_loc1_);
         }
      }
      
      public function method_707() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         if(name_3 == 2)
         {
            return;
         }
         var _loc1_:Number = method_100().var_244 - var_288.x;
         if(var_945 < 0)
         {
            _loc2_ = 4;
            _loc3_ = Number(class_663.method_99(-_loc2_,_loc1_ * 0.1,_loc2_));
            var_288.x = Number(var_288.x + _loc3_);
            var_430 = (var_430 + Number(Math.abs(_loc3_ * 0.5))) % 20;
            var_288.gotoAndStop(int(var_430) + 1);
         }
      }
      
      public function method_709() : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc1_:Array = var_470.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            switch(int(_loc3_.name_3))
            {
               case 0:
                  _loc3_.var_37 = Number(_loc3_.var_37) - 1;
                  if(Number(_loc3_.var_37) < 0)
                  {
                     _loc3_.var_37 = 6;
                     _loc3_.name_3 = 1;
                     _loc3_.play();
                     break;
                  }
                  break;
               case 1:
                  _loc3_.y = _loc3_.y - 2;
                  _loc3_.var_37 = Number(_loc3_.var_37) - 1;
                  if(Number(_loc3_.var_37) < 0)
                  {
                     _loc3_.var_37 = name_47;
                     _loc3_.name_3 = 0;
                     break;
                  }
                  break;
               case 2:
                  _loc3_.y = Number(_loc3_.y + 2);
                  _loc3_.var_37 = Number(_loc3_.var_37) - 1;
                  if(Number(_loc3_.var_37) < 0)
                  {
                     _loc3_.var_37 = 5;
                     _loc3_.name_3 = 0;
                     break;
                  }
            }
            method_390(_loc3_);
            if(_loc3_.y < 70)
            {
               method_691(_loc3_.x,_loc3_.y);
            }
            _loc4_ = 10;
            if(_loc3_.x < -_loc4_ || _loc3_.x > class_742.var_217)
            {
               _loc3_.var_37 = 5;
               _loc3_.name_3 = 2;
            }
         }
      }
      
      override public function method_89() : void
      {
         super.method_89();
         if(var_946 != null)
         {
            var_946.dispose();
         }
      }
      
      public function method_691(param1:Number, param2:Number) : void
      {
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:class_739 = null;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc19_:Number = NaN;
         var _loc20_:Matrix = null;
         var _loc21_:Number = NaN;
         var _loc24_:MovieClip = null;
         name_3 = 2;
         method_81(false,30);
         var _loc3_:BitmapData = new BitmapData(class_742.var_217,class_742.var_219,true,0);
         var _loc4_:Matrix = new Matrix();
         _loc3_.draw(var_602,_loc4_);
         var_946 = _loc3_;
         var _loc5_:int = int(Math.ceil(class_742.var_217 / class_789.var_941));
         var _loc6_:int = int(Math.ceil(class_742.var_219 / class_789.var_941));
         var _loc7_:Number = class_789.var_941 * 0.5;
         var _loc8_:Array = [];
         var _loc9_:int = 0;
         while(_loc9_ < _loc5_)
         {
            _loc9_++;
            _loc10_ = _loc9_;
            _loc11_ = 0;
            while(_loc11_ < _loc6_)
            {
               _loc11_++;
               _loc12_ = _loc11_;
               _loc13_ = new class_739(new Sprite());
               _loc13_.var_244 = (_loc10_ + 0.5) * class_789.var_941;
               _loc13_.var_245 = (_loc12_ + 0.5) * class_789.var_941;
               _loc13_.method_118();
               _loc13_.var_233 = 0.98;
               var_232.method_124(_loc13_.var_243,class_645.var_209);
               _loc14_ = _loc13_.var_244 - param1;
               _loc15_ = _loc13_.var_245 - param2;
               _loc16_ = Number(Math.atan2(_loc15_,_loc14_));
               _loc17_ = Number(Math.sqrt(Number(_loc14_ * _loc14_ + _loc15_ * _loc15_)));
               _loc18_ = Number(Math.abs(_loc17_ / 250));
               _loc19_ = Number(0.25 + (1 - _loc18_) * 1.5);
               _loc13_.var_278 = Math.cos(_loc16_) * _loc19_;
               _loc13_.var_279 = Math.sin(_loc16_) * _loc19_ - 1;
               _loc13_.var_580 = (Math.random() * 2 - 1) * 3;
               _loc13_.var_947 = 0.97;
               _loc13_.var_250 = Number(0.05 + Math.random() * 0.15);
               _loc13_.var_251 = 40 + int(class_691.method_114(20));
               _loc20_ = new Matrix();
               _loc20_.translate(-_loc13_.var_244,-_loc13_.var_245);
               _loc13_.var_243.graphics.beginBitmapFill(_loc3_,_loc20_);
               _loc21_ = class_789.var_941 * 0.5;
               _loc13_.var_243.graphics.drawRect(-_loc21_,-_loc21_,2 * _loc21_,2 * _loc21_);
               class_657.gfof(_loc13_.var_243,_loc18_ * 0.25,0);
               _loc8_.push(_loc13_);
            }
         }
         var_288.gotoAndStop("faller");
         var _loc22_:Array = [var_288];
         _loc9_ = 0;
         var _loc23_:Array = var_470;
         while(_loc9_ < int(_loc23_.length))
         {
            _loc24_ = _loc23_[_loc9_];
            _loc9_++;
            _loc22_.push(_loc24_);
         }
         _loc9_ = 0;
         while(_loc9_ < int(_loc22_.length))
         {
            _loc24_ = _loc22_[_loc9_];
            _loc9_++;
            _loc13_ = new class_739(_loc24_);
            _loc13_.var_580 = (Math.random() * 2 - 1) * 8;
            _loc13_.var_250 = Number(0.25 + Math.random() * 0.5);
            if(_loc24_ == var_288)
            {
               _loc13_.var_279 = _loc13_.var_279 - 6;
               _loc13_.var_580 = 3;
               _loc13_.var_250 = 0.5;
            }
         }
         var_602.parent.removeChild(var_602);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [Number(500 + param1 * 200)];
         super.method_95(param1);
         var_470 = [];
         var_944 = [];
         var_783 = 46 - param1 * 20;
         name_47 = 10;
         var_945 = 0;
         var_430 = 0;
         method_117();
         method_72();
      }
      
      public function method_708() : void
      {
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("\x12c\x03z\x02"),class_645.var_209 + 1);
         _loc1_.x = Number(20 + Math.random() * (class_742.var_217 - 2 * 20));
         _loc1_.y = class_742.var_219 + 30;
         _loc1_.name_3 = 0;
         _loc1_.var_37 = 0;
         _loc1_.name_3 = 0;
         _loc1_.var_580 = (Math.random() * 2 - 1) * 8;
         _loc1_.var_250 = Number(0.5 + Number(Math.random()));
         var_470.push(_loc1_);
      }
      
      public function method_390(param1:MovieClip) : void
      {
         var _loc4_:MovieClip = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Array = var_470;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = param1.x - _loc4_.x;
            _loc6_ = param1.y - _loc4_.y;
            _loc7_ = Number(Math.sqrt(Number(_loc5_ * _loc5_ + _loc6_ * _loc6_)));
            if(_loc7_ < class_789.var_942 * 2)
            {
               _loc8_ = (class_789.var_942 * 2 - _loc7_) * 0.5;
               _loc9_ = Number(Math.atan2(_loc6_,_loc5_));
               param1.x = Number(param1.x + Math.cos(_loc9_) * _loc8_);
               param1.y = Number(param1.y + Math.sin(_loc9_) * _loc8_);
               _loc4_.x = _loc4_.x - Math.cos(_loc9_) * _loc8_;
               _loc4_.y = _loc4_.y - Math.sin(_loc9_) * _loc8_;
            }
         }
         if(param1.x < -class_789.var_942 || param1.x > class_742.var_217 + class_789.var_942)
         {
            param1.x = Number(class_663.method_99(-class_789.var_942,param1.x,class_742.var_217 + class_789.var_942));
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\x1br\x16#\x03"),0);
         var_288 = var_232.method_84(class_899.__unprotect__("\x17\x03y\x1f\x03"),class_645.var_209);
         var_288.x = class_742.var_217 * 0.5;
         var_288.y = 68;
         var_288.stop();
         var_602 = var_232.method_84(class_899.__unprotect__("\x17\x18\x03\f\x01"),class_645.var_209);
      }
   }
}
