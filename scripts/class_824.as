package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_824 extends class_645
   {
       
      
      public var var_351:Number;
      
      public var var_1078:class_656;
      
      public var var_307:Number;
      
      public var var_1074:Number;
      
      public var var_1075:Number;
      
      public var var_531:class_656;
      
      public var var_1079:class_656;
      
      public var package_32:Number;
      
      public var var_1076:MovieClip;
      
      public var var_565:class_892;
      
      public var var_1077:Number;
      
      public var var_700:Number;
      
      public function class_824()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_776();
               var_700 = (var_700 + 10) % 628;
               _loc1_ = class_742.var_217 * 0.5;
               _loc2_ = class_742.var_219 - 20;
               _loc3_ = Number(_loc2_ + Math.sin(var_700 / 100) * 4);
               _loc4_ = _loc3_ - var_531.var_245;
               var_531.var_244 = Number(_loc1_ + Math.cos(var_700 / 100) * 60);
               var_531.var_245 = _loc3_;
               var_1074 = Number(100 + (var_531.var_245 - _loc2_) * 4);
               var_1075 = (var_1075 + Number(Math.abs(_loc4_ * 2))) % var_531.var_243.totalFrames;
               var_531.var_243.gotoAndStop(int(Math.round(var_1075)) + 1);
               var_531.var_243.alpha = Number(Math.min(Number(var_531.var_243.alpha + 0.02),1));
               var_531.var_243.scaleX = var_1074 * 0.01;
               var_531.var_243.scaleY = var_1074 * 0.01;
               if(var_227[0] < 100)
               {
                  method_727();
                  break;
               }
               if(var_227[0] < 140 && int(class_691.method_114(int(100))) == 0)
               {
                  method_727();
                  break;
               }
               break;
            case 2:
               method_776();
               if(var_565 != null)
               {
                  _loc1_ = Number(Math.atan2(var_565.var_279,var_565.var_278));
                  var_565.var_243.rotation = _loc1_ / 0.0174;
               }
               if(name_5)
               {
                  method_777();
               }
               if(var_565 != null && var_565.var_245 > Number(var_1076.y + var_1076.scaleY * 50))
               {
                  method_778(var_565.var_244,var_565.var_245);
                  var_565.method_89();
                  var_1076.parent.removeChild(var_1076);
                  var_565 = null;
                  break;
               }
               break;
            case 3:
               package_32 = Number(Math.min(Number(package_32 + 2),100));
               class_657.gfof(var_201,1 - package_32 * 0.01,16777215);
               if(package_32 > 40)
               {
                  method_81(var_1077 < var_307 * 0.3,20);
                  break;
               }
         }
      }
      
      public function method_778(param1:Number, param2:Number) : void
      {
         var _loc4_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("I\\k|\x02"),class_645.var_209));
         _loc4_.var_233 = 0.99;
         var _loc3_:class_892 = _loc4_;
         _loc3_.var_244 = param1;
         _loc3_.var_245 = param2;
         _loc3_.method_118();
         _loc3_.var_243.rotation = Math.random() * 10;
      }
      
      public function method_776() : void
      {
         var _loc2_:* = method_100();
         var _loc3_:Number = _loc2_.var_244 - var_1079.var_244;
         var _loc4_:Number = _loc2_.var_245 - var_1079.var_245;
         var _loc5_:Number = _loc3_ * 0.5 - var_1079.var_243.rotation;
         var_1079.var_244 = Number(var_1079.var_244 + _loc3_ * 0.4);
         var_1079.var_245 = Number(var_1079.var_245 + _loc4_ * 0.4);
         var_1079.var_243.rotation = Number(var_1079.var_243.rotation + _loc5_ * 0.4);
      }
      
      public function method_777() : void
      {
         name_3 = 3;
         package_32 = 0;
         class_657.gfof(var_201,1 - package_32 * 0.01,16777215);
         var _loc1_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("u^\x03e\x03"),class_645.var_209));
         var_1078 = _loc1_;
         var_1078.var_244 = var_1079.var_244;
         var_1078.var_245 = var_1079.var_245;
         var_1078.var_243.rotation = var_1079.var_243.rotation;
         var_1078.var_243.scaleX = var_307 * 0.01;
         var_1078.var_243.scaleY = var_307 * 0.01;
         var_1078.method_118();
         class_319.mask = var_1078.var_243;
         var_1079.var_243.gotoAndStop(2);
         var_232.method_110(var_1079.var_243);
         var_1077 = 9999;
         if(var_565 == null)
         {
            return;
         }
         var_1077 = Number(var_565.method_115(var_1079));
         var _loc2_:MovieClip = new class_755(class_319).method_84(class_899.__unprotect__("wU3\x03\x01"),1);
         _loc2_.x = var_565.var_243.x;
         _loc2_.y = var_565.var_243.y;
         _loc2_.rotation = var_565.var_243.rotation;
         _loc2_.scaleY = var_565.var_243.scaleY;
         _loc2_.gotoAndStop(class_691.method_97(var_565.var_243.currentFrame));
         var_565.method_89();
         var_1076.parent.removeChild(var_1076);
      }
      
      public function method_727() : void
      {
         name_3 = 2;
         method_778(var_531.var_244,var_531.var_245);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("wU3\x03\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_565 = _loc1_;
         var_565.var_250 = Number(0.3 + var_237[0] * 1.2);
         var_565.var_244 = var_531.var_244;
         var_565.var_245 = var_531.var_245;
         var _loc2_:Number = Number(class_742.var_217 * 0.5 + Math.random() * 60);
         var _loc3_:Number = Number(50 + (Math.random() * 2 - 1) * 10);
         var _loc4_:Number = _loc2_ - var_565.var_244;
         var _loc5_:Number = _loc3_ - var_565.var_245;
         var _loc6_:Number = Number(Math.atan2(_loc5_,_loc4_));
         var _loc7_:Number = Number(Number(9 + Math.random() * 3) + var_237[0] * 12);
         var_565.var_278 = Math.cos(_loc6_) * _loc7_;
         var_565.var_279 = Math.sin(_loc6_) * _loc7_;
         if(var_565.var_278 < 0)
         {
            var_565.var_243.scaleY = var_565.var_243.scaleY * -1;
         }
         var_531.method_89();
         var_565.method_118();
         var_1076 = var_232.method_84(class_899.__unprotect__("u^\x03e\x03"),class_645.var_209);
         var_1076.x = class_742.var_217 * 0.5;
         var_1076.y = var_565.var_245 * 0.5;
         var_1076.scaleX = class_742.var_217 * 0.01;
         var_1076.scaleY = var_565.var_245 * 0.01;
         var_565.var_243.mask = var_1076;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [200];
         super.method_95(param1);
         var_700 = int(class_691.method_114(628));
         var_351 = Number(4 + param1 * 15);
         var_1075 = 0;
         var_307 = 100 - param1 * 40;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("[\fU\x02\x02"),0);
         class_319 = var_232.method_84(class_899.__unprotect__("\x13RT\x0b\x02"),class_645.var_209);
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("Tju9\x02"),class_645.var_209));
         var_531 = _loc2_;
         var_531.var_243.alpha = 0;
         var_531.var_243.stop();
         var_531.method_118();
         _loc2_ = new class_656(var_232.method_84(class_899.__unprotect__("Pc\x19l"),class_645.var_209));
         var_1079 = _loc2_;
         var_1079.var_243.scaleX = var_307 * 0.01;
         var_1079.var_243.scaleY = var_307 * 0.01;
         var_1079.method_118();
         var_1079.var_243.stop();
      }
   }
}
