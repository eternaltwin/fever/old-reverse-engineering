package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_890 extends class_645
   {
      
      public static var var_1010:int = 6;
      
      public static var var_1371:int = 2;
      
      public static var var_350:int = 186;
      
      public static var var_965:int = 11;
       
      
      public var var_1372:Array;
      
      public var name_11:int;
      
      public var var_1373:Boolean;
      
      public var var_1145:Number;
      
      public var var_1374:Array;
      
      public var var_1375:MovieClip;
      
      public var class_318:class_892;
      
      public function class_890()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc3_:class_889 = null;
         super.method_79();
         method_720();
         var _loc1_:int = 0;
         var _loc2_:Array = var_1372;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.method_79();
         }
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_720() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:class_892 = null;
         var _loc1_:Number = method_100().var_244 - class_318.var_244;
         if(!var_1373 && class_318.var_245 > class_890.var_350 - class_890.var_965)
         {
            _loc2_ = int((class_318.var_244 - class_890.var_1010) / (Number(var_1145 + class_890.var_1371)));
            _loc3_ = var_1374[_loc2_];
            if(_loc3_ == null)
            {
               _loc4_ = 0;
               while(_loc4_ < 2)
               {
                  _loc4_++;
                  _loc5_ = _loc4_;
                  _loc6_ = _loc5_ * 2 - 1;
                  _loc2_ = int((class_318.var_244 + class_890.var_965 * _loc6_ - class_890.var_1010) / (Number(var_1145 + class_890.var_1371)));
                  _loc7_ = var_1374[_loc2_];
                  if(_loc7_ != null)
                  {
                     _loc3_ = _loc7_;
                     break;
                  }
               }
            }
            if(_loc3_ != null)
            {
               _loc3_.var_250 = 1;
               _loc3_.var_251 = 10;
               _loc3_.var_272 = 3;
               var_1374[_loc2_] = null;
               class_318.var_245 = class_890.var_350 - class_890.var_965;
               class_318.var_278 = Number(class_318.var_278 + _loc1_ * 0.04);
               class_318.var_279 = -20;
            }
            else
            {
               var_1373 = true;
               var_229 = [true];
            }
         }
         if(class_318.var_245 > class_742.var_219 + 20)
         {
            var_229 = [false];
            method_81(false);
         }
         class_318.var_278 = Number(class_318.var_278 + Number(class_663.method_99(-0.15,_loc1_ * 0.05,0.15)));
         if(class_318.var_244 < class_890.var_1010 + class_890.var_965 || class_318.var_244 > class_742.var_217 - (class_890.var_1010 + class_890.var_965))
         {
            class_318.var_244 = Number(class_663.method_99(class_890.var_1010 + class_890.var_965,class_318.var_244,class_742.var_217 - (class_890.var_1010 + class_890.var_965)));
            class_318.var_278 = class_318.var_278 * -1;
         }
         class_318.var_243.rotation = Number(class_318.var_243.rotation + class_318.var_278 * 4);
         class_318.var_243.x = class_318.var_244;
         class_318.var_243.y = class_318.var_245;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [220];
         super.method_95(param1);
         var_1373 = false;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:class_889 = null;
         var _loc6_:Number = NaN;
         var _loc7_:class_892 = null;
         var _loc8_:class_892 = null;
         var _loc9_:class_10 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("%6WA"),0);
         var _loc1_:int = 48;
         var_1372 = [];
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = Number(Math.pow(_loc3_ / _loc1_,2));
            _loc5_ = new class_889();
            _loc5_.var_334 = class_742.var_219 + _loc4_ * 30 - 20;
            _loc5_.y = _loc5_.var_334;
            _loc6_ = Number(0.75 + _loc4_ * 0.5);
            _loc5_.scaleY = _loc6_;
            _loc5_.scaleX = _loc6_;
            _loc5_.x = Math.random() * class_742.var_217;
            _loc5_.gotoAndPlay(int(class_691.method_114(60)) + 1);
            class_319.addChild(_loc5_);
            var_1372.push(_loc5_);
            class_657.gfof(_loc5_,(1 - _loc4_) * 0.5,16756655);
         }
         var_1374 = [];
         _loc1_ = int(16 - Math.min(var_237[0],1) * 9);
         var_1145 = (class_742.var_217 - (2 * class_890.var_1010 + class_890.var_1371 * (_loc1_ - 1))) / _loc1_;
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc8_ = new class_892(var_232.method_84(class_899.__unprotect__(",Ot)\x01"),class_645.var_209));
            _loc8_.var_233 = 0.99;
            _loc7_ = _loc8_;
            _loc7_.var_244 = Number(class_890.var_1010 + (var_1145 + class_890.var_1371) * _loc3_);
            _loc7_.var_245 = class_890.var_350;
            _loc7_.method_118();
            _loc9_ = _loc7_.var_243;
            _loc9_.var_8.scaleX = var_1145 * 0.01;
            var_1374.push(_loc7_);
         }
         _loc7_ = new class_892(var_232.method_84(class_899.__unprotect__("9OL\x05\x01"),class_645.var_209));
         _loc7_.var_233 = 0.99;
         class_318 = _loc7_;
         class_318.var_244 = class_742.var_217 * 0.5;
         class_318.var_245 = 30;
         class_318.method_118();
         class_318.var_250 = 1;
      }
   }
}
