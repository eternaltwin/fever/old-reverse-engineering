package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_848 extends class_645
   {
       
      
      public var var_251:Number;
      
      public var var_1200:class_892;
      
      public var var_635:int;
      
      public var var_66:Number;
      
      public var var_70:MovieClip;
      
      public var var_90:MovieClip;
      
      public var var_25:class_656;
      
      public var var_1202:MovieClip;
      
      public var var_1203:MovieClip;
      
      public var var_714:int;
      
      public var var_1201:class_656;
      
      public var var_18:Number;
      
      public var var_19:Number;
      
      public function class_848()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:* = null;
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         var _loc9_:* = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:* = null;
         var _loc15_:* = null;
         var _loc16_:* = null;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_105();
               _loc1_ = {
                  "O6EU\x01":var_66,
                  "^\'\x14^\x01":class_742.var_217 - var_66,
                  "O\x15;\x03\x02":20,
                  "~tj[\x01":class_742.var_217 * 0.5
               };
               _loc2_ = method_100();
               _loc3_ = {
                  "L\x01":Number(Math.min(Number(Math.max(Number(_loc1_.name_22),Number(_loc2_.var_244))),Number(_loc1_.var_340))),
                  "\x03\x01":Number(Math.min(Number(Math.max(int(_loc1_.name_23),Number(_loc2_.var_245))),Number(_loc1_.var_341)))
               };
               var_1200.method_358(_loc3_,0.1,3);
               if(name_5)
               {
                  name_3 = 2;
                  break;
               }
               break;
            case 2:
               method_105();
               _loc1_ = method_100();
               _loc4_ = Number(var_1201.method_231({
                  "L\x01":Number(_loc1_.var_244),
                  "\x03\x01":Number(Math.min(Number(_loc1_.var_245),class_742.var_219 * 0.7))
               }));
               _loc5_ = _loc4_ - var_19;
               while(_loc5_ > 3.14)
               {
                  _loc5_ = _loc5_ - 6.28;
               }
               while(_loc5_ < -3.14)
               {
                  _loc5_ = Number(_loc5_ + 6.28);
               }
               var_19 = Number(var_19 + _loc5_ * 0.2);
               var_70.rotation = var_19 / 0.0174;
               _loc6_ = Number(Math.min(Number(Math.max(1,12 - var_1200.var_245 * 0.1)),5.5));
               var_1202.scaleX = _loc6_ * 30 * 0.01;
               var_1202.scaleY = _loc6_ * 30 * 0.01;
               var_1203.graphics.clear();
               var_1203.graphics.lineStyle(_loc6_,16777215,50);
               _loc7_ = var_1201.var_244;
               _loc8_ = var_635;
               _loc2_ = method_871(_loc7_,_loc8_,var_19);
               _loc3_ = method_871(var_1200.var_244,Number(var_1200.var_245 + 4),var_18);
               _loc9_ = method_872(_loc2_,_loc3_);
               var_1203.graphics.moveTo(Number(_loc9_.var_244),Number(_loc9_.var_245));
               if(Number(var_1200.method_115(_loc9_)) < var_66)
               {
                  _loc10_ = var_19 - var_18;
                  _loc11_ = Number(Math.cos(_loc10_));
                  _loc12_ = Number(Math.sin(_loc10_));
                  _loc13_ = Number(Math.atan2(_loc12_,-_loc11_));
                  _loc14_ = method_871(Number(_loc9_.var_244),Number(_loc9_.var_245),_loc13_);
                  _loc15_ = method_871(0,var_714,0);
                  _loc16_ = method_872(_loc14_,_loc15_);
                  var_1203.graphics.moveTo(Number(_loc16_.var_244),Number(_loc16_.var_245));
                  var_1203.graphics.lineTo(Number(_loc9_.var_244),Number(_loc9_.var_245));
                  var_1202.x = Number(_loc16_.var_244);
                  var_1202.y = Number(_loc16_.var_245);
                  var_1202.visible = true;
                  if(Number(var_25.method_115(_loc16_)) < _loc6_ * 1.5)
                  {
                     var_25.var_10 = Number(Math.min(Number(Number(var_25.var_10) + 20),100));
                     var_25.var_683 = Number(var_25.var_683) - 1;
                     if(Number(var_25.var_683) < 0)
                     {
                        var_1203.graphics.clear();
                        name_3 = 3;
                        var_251 = 10;
                        var_25.var_243.gotoAndPlay("dead");
                        var_229 = [true];
                        var_1202.visible = false;
                     }
                  }
               }
               else
               {
                  _loc3_ = method_871(0,0,0);
                  _loc9_ = method_872(_loc2_,_loc3_);
                  var_1203.graphics.moveTo(Number(_loc9_.var_244),Number(_loc9_.var_245));
                  var_1202.visible = false;
               }
               var_1203.graphics.lineTo(var_1201.var_244,var_635);
               if(!name_5)
               {
                  var_1203.graphics.clear();
                  var_1202.visible = false;
                  name_3 = 1;
                  break;
               }
               break;
            case 3:
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(true);
                  break;
               }
         }
         var_25.var_10 = var_25.var_10 * 0.9;
         class_657.gfof(var_25.var_243,var_25.var_10 * 0.01,16777215);
      }
      
      public function method_105() : void
      {
         var_25.var_700 = (Number(var_25.var_700) + 30) % 628;
         var_25.var_244 = Number(var_25.var_244 + Math.max(0,Number(Math.cos(var_25.var_700 / 100))) * (0.5 + var_237[0]) * int(var_25.var_796));
         if(Number(Math.abs(var_25.var_244 - var_1201.var_244)) < 20)
         {
            method_81(false,30);
            var_1201.var_243.gotoAndStop("2");
            var_25.var_243.gotoAndPlay("youpi");
            name_3 = 4;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [650 - param1 * 250];
         super.method_95(param1);
         var_714 = class_742.var_219 - 8;
         var_66 = 30 - param1 * 20;
         var_635 = var_714 - 20;
         var_19 = 0;
         var_18 = (Math.random() * 2 - 1) * 0.5;
         method_117();
         method_72();
      }
      
      public function method_871(param1:Number, param2:Number, param3:Number) : Object
      {
         var _loc4_:Number = Math.sin(param3) / Math.cos(param3);
         var _loc5_:Number = param2 - _loc4_ * param1;
         return {
            "F\x01":_loc4_,
            "t\x01":_loc5_
         };
      }
      
      public function method_872(param1:Object, param2:Object) : Object
      {
         var _loc3_:Number = (param1.var_68 * param2.var_8 - param2.var_68 * param1.var_8) / (param1.var_68 - param2.var_68);
         var _loc4_:Number = (_loc3_ - param1.var_8) / param1.var_68;
         return {
            "L\x01":_loc4_,
            "\x03\x01":_loc3_
         };
      }
      
      public function method_117() : void
      {
         var_232.method_84(class_899.__unprotect__("0+u]\x01"),0);
         var_1203 = var_232.method_92(class_645.var_209);
         var _loc1_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("P]nx\x03"),class_645.var_209));
         var_1201 = _loc1_;
         var_1201.var_244 = class_742.var_217 * 0.5;
         var_1201.var_245 = var_714;
         var_1201.method_118();
         var_70 = var_1201.var_243.var_70;
         var_1202 = var_232.method_84(class_899.__unprotect__("?\x13c~"),class_645.var_209);
         var_1202.visible = false;
         _loc1_ = new class_656(var_232.method_84(class_899.__unprotect__("\ni!Q\x01"),class_645.var_209));
         var_25 = _loc1_;
         var_25.var_796 = int(class_691.method_114(2)) * 2 - 1;
         var_25.var_700 = 0;
         var_25.var_683 = 100;
         var_25.var_10 = 0;
         var_25.var_244 = class_742.var_217 * (-int(var_25.var_796) * 0.5 + 0.5);
         var_25.var_245 = var_714;
         var_25.var_243.scaleX = int(var_25.var_796);
         var_25.method_118();
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("R\x15\x05\x02"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var_1200 = _loc2_;
         var_1200.var_244 = class_742.var_217 * 0.5;
         var_1200.var_245 = class_742.var_217 * 0.25;
         var_90 = var_1200.var_243.var_90;
         var_90.scaleX = var_66 * 0.02;
         var_90.rotation = var_18 / 0.0174;
         var_1200.method_118();
         var_1200.var_233 = 0.75;
      }
   }
}
