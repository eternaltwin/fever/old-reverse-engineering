package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_784 extends class_645
   {
      
      public static var var_919:int = 80;
      
      public static var var_920:int = 100;
      
      public static var var_212:int = 197;
      
      public static var var_350:int = 216;
       
      
      public var var_250:Number;
      
      public var var_923:class_656;
      
      public var var_251:Object;
      
      public var var_531:MovieClip;
      
      public var var_924:Array;
      
      public var var_921:Array;
      
      public var var_487:Number;
      
      public var var_922:Object;
      
      public var var_288:class_656;
      
      public var var_430:Number;
      
      public var var_248:Number;
      
      public function class_784()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Array = null;
         var _loc7_:* = null;
         var _loc8_:* = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc1_:int = name_3;
         if(_loc1_ == -1)
         {
            _loc2_ = var_921[0];
            var_921 = [var_921[0] + 1];
            if(_loc2_ > 15)
            {
               name_3 = 1;
            }
         }
         else if(_loc1_ == 1)
         {
            _loc3_ = method_100().var_244 - var_288.var_244;
            _loc2_ = 4;
            _loc4_ = Number(class_663.method_99(-_loc2_,_loc3_ * 0.1,_loc2_));
            var_288.var_244 = Number(var_288.var_244 + _loc4_);
            var_430 = Number(var_430 + _loc4_ * 4);
            var_248 = var_248 - _loc4_ * 0.005;
            while(var_430 < 0)
            {
               var_430 = Number(var_430 + class_784.var_919);
            }
            while(var_430 > class_784.var_919)
            {
               var_430 = var_430 - class_784.var_919;
            }
            var_288.var_243.gotoAndStop(int(var_430) + 1);
            _loc5_ = 1;
            if(var_922 != null)
            {
               var_922 = var_922 - 1;
               if(var_922 < 0)
               {
                  _loc5_ = 1 - var_922 / class_784.var_920;
                  var_922 = null;
               }
            }
            var_250 = var_250 * 1.0003;
            var_248 = var_248 * (var_250 * _loc5_ + (1 - _loc5_));
            var_923.var_243.rotation = var_248 / 0.0174;
            var_923.var_244 = var_288.var_244;
            if(Number(Math.abs(var_248)) > 0.8 && !var_220)
            {
               method_691();
            }
            method_692(var_248 - 1.57);
            var_487 = var_248;
         }
         else if(_loc1_ == 2)
         {
            _loc2_ = 0;
            _loc6_ = var_924;
            while(_loc2_ < int(_loc6_.length))
            {
               _loc7_ = _loc6_[_loc2_];
               _loc2_++;
               _loc7_.var_279 = Number(Number(_loc7_.var_279) + 0.3);
               _loc3_ = 0.95;
               _loc7_.var_278 = _loc7_.var_278 * _loc3_;
               _loc7_.var_279 = _loc7_.var_279 * _loc3_;
               _loc7_.var_244 = Number(Number(_loc7_.var_244) + Number(_loc7_.var_278));
               _loc7_.var_245 = Number(Number(_loc7_.var_245) + Number(_loc7_.var_279));
               if(Number(_loc7_.var_245) > class_784.var_350)
               {
                  _loc7_.var_245 = class_784.var_350;
                  _loc7_.var_279 = _loc7_.var_279 * -0.9;
               }
            }
            _loc7_ = var_924[0];
            _loc8_ = var_924[1];
            _loc3_ = _loc7_.var_244 - _loc8_.var_244;
            _loc4_ = _loc7_.var_245 - _loc8_.var_245;
            _loc5_ = Number(Math.sqrt(Number(_loc3_ * _loc3_ + _loc4_ * _loc4_)));
            _loc9_ = class_784.var_212 - _loc5_;
            _loc10_ = Number(Math.atan2(_loc4_,_loc3_));
            _loc7_.var_244 = Number(Number(_loc7_.var_244) + Math.cos(_loc10_) * _loc9_ * 0.5);
            _loc7_.var_245 = Number(Number(_loc7_.var_245) + Math.sin(_loc10_) * _loc9_ * 0.5);
            _loc8_.var_244 = _loc8_.var_244 - Math.cos(_loc10_) * _loc9_ * 0.5;
            _loc8_.var_245 = _loc8_.var_245 - Math.sin(_loc10_) * _loc9_ * 0.5;
            var_923.var_244 = Number(_loc7_.var_244);
            var_923.var_245 = Number(_loc7_.var_245);
            var_923.var_243.rotation = _loc10_ / 0.0174 - 90;
            method_692(Number(_loc10_ + 3.14));
            var_251 = var_251 - 1;
            if(var_251 < 0)
            {
               var_229 = [false];
               method_81(false,10);
               var_251 = null;
            }
         }
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_692(param1:Number) : void
      {
         var_531.x = var_923.var_244;
         var_531.scaleX = Math.cos(param1) * class_784.var_212 * 0.01;
      }
      
      public function method_691() : void
      {
         var_229 = [true];
         name_3 = 2;
         var_924 = [];
         var_924.push({
            "L\x01":var_923.var_244,
            "\x03\x01":var_923.var_245,
            "\x02\x04\x02":0,
            "\'\x17\x01":0
         });
         var _loc1_:Number = var_248 - 1.57;
         var _loc2_:Number = Number(Number(var_924[0].var_244) + Math.cos(_loc1_) * class_784.var_212);
         var _loc3_:Number = Number(Number(var_924[0].var_245) + Math.sin(_loc1_) * class_784.var_212);
         _loc1_ = var_487 - 1.57;
         var _loc4_:Number = Number(Number(var_924[0].var_244) + Math.cos(_loc1_) * class_784.var_212);
         var _loc5_:Number = Number(Number(var_924[0].var_245) + Math.sin(_loc1_) * class_784.var_212);
         var_924.push({
            "L\x01":_loc2_,
            "\x03\x01":_loc3_,
            "\x02\x04\x02":_loc2_ - _loc4_,
            "\'\x17\x01":_loc3_ - _loc5_
         });
         var _loc6_:MovieClip = var_232.method_84(class_899.__unprotect__("\x0et4\x1c\x02"),class_645.var_209);
         _loc6_.x = var_288.var_244;
         _loc6_.y = var_288.var_245;
         var_288.method_89();
         var_251 = 20;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_921 = [0];
         var_227 = [Number(150 + param1 * 300)];
         super.method_95(param1);
         var_922 = class_784.var_920;
         var_430 = 0;
         var_248 = Math.random() * 0.01;
         method_117();
         var_250 = 1.03;
         method_72();
      }
      
      public function method_117() : void
      {
         var_232.method_84(class_899.__unprotect__("k\x19\x03,"),0);
         var_531 = var_232.method_84(class_899.__unprotect__("i;V\x05"),class_645.var_209);
         var_531.x = class_742.var_217 * 0.5;
         var_531.y = class_784.var_350 + 3;
         var_531.scaleX = 0;
         var _loc1_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("l\'\'h\x01"),class_645.var_209));
         var_923 = _loc1_;
         var_923.var_244 = class_742.var_217 * 0.5;
         var_923.var_245 = 197;
         var_923.method_118();
         _loc1_ = new class_656(var_232.method_84(class_899.__unprotect__("cb0/"),class_645.var_209));
         var_288 = _loc1_;
         var_288.var_244 = class_742.var_217 * 0.5;
         var_288.var_245 = 219;
         var_288.method_118();
         var_232.method_84(class_899.__unprotect__("t3\b\x0e\x03"),class_645.var_209);
         name_3 = -1;
      }
   }
}
