package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_753 extends class_645
   {
      
      public static var var_781:int = 8;
       
      
      public var var_251:Number;
      
      public var var_351:Number;
      
      public var var_782:Array;
      
      public var var_288:class_892;
      
      public var var_783:int;
      
      public function class_753()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_514() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:Boolean = false;
         var _loc5_:int = 0;
         var _loc6_:class_892 = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:MovieClip = null;
         var _loc1_:Array = var_782.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc4_ = _loc3_.var_245 > Number(class_742.var_219 + Number(_loc3_.var_66));
            _loc5_ = 0;
            while(_loc5_ < int(_loc1_.length))
            {
               _loc6_ = _loc1_[_loc5_];
               _loc5_++;
               if(_loc3_ != _loc6_)
               {
                  _loc7_ = Number(_loc3_.method_115(_loc6_));
                  if(_loc7_ < Number(Number(_loc3_.var_66) + Number(_loc6_.var_66)))
                  {
                     _loc8_ = (Number(_loc3_.var_66) + Number(_loc6_.var_66) - _loc7_) * 0.5;
                     _loc9_ = Number(_loc3_.method_231(_loc6_));
                     _loc3_.var_244 = _loc3_.var_244 - Math.cos(_loc9_) * _loc8_;
                     _loc3_.var_245 = _loc3_.var_245 - Math.sin(_loc9_) * _loc8_;
                     _loc6_.var_244 = Number(_loc6_.var_244 + Math.cos(_loc9_) * _loc8_);
                     _loc6_.var_245 = Number(_loc6_.var_245 + Math.sin(_loc9_) * _loc8_);
                  }
               }
            }
            _loc7_ = Number(_loc3_.method_115(var_288));
            if(_loc7_ < Number(Number(_loc3_.var_66) + class_753.var_781))
            {
               _loc5_ = 5;
               _loc8_ = Number(_loc3_.method_231(var_288));
               var_288.var_278 = Number(var_288.var_278 + Math.cos(_loc8_) * _loc5_);
               var_288.var_279 = Number(var_288.var_279 + Math.sin(_loc8_) * _loc5_);
               _loc10_ = var_232.method_84(class_899.__unprotect__("\x19k\'^\x02"),class_645.var_209);
               _loc10_.x = _loc3_.var_244;
               _loc10_.y = _loc3_.var_245;
               _loc9_ = _loc3_.var_243.scaleX * 1.5;
               _loc10_.scaleY = _loc9_;
               _loc10_.scaleX = _loc9_;
               _loc4_ = true;
            }
            if(_loc4_)
            {
               _loc3_.method_89();
               var_782.method_116(_loc3_);
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:class_20 = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_288.var_279 = Number(Math.max(var_288.var_279 - 0.1,-var_351));
               if(var_288.var_245 < 0)
               {
                  method_81(true,10);
               }
               _loc1_ = method_100().var_244 - var_288.var_244;
               var_288.var_244 = Number(var_288.var_244 + _loc1_ * 0.15);
               var_288.var_278 = var_288.var_278 * 0.95;
               var_288.var_243.rotation = _loc1_ * 0.6;
               _loc2_ = var_288.var_243;
               _loc2_.var_17.var_17.rotation = Number(_loc2_.var_17.var_17.rotation + Number(Math.max(10,-30 * var_288.var_279)));
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  var_251 = var_783;
                  method_515();
               }
               method_514();
         }
         super.method_79();
      }
      
      public function method_516(param1:class_892) : Boolean
      {
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Array = var_782;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(param1 != _loc4_)
            {
               _loc5_ = Number(param1.method_115(_loc4_));
               if(_loc5_ < Number(Number(param1.var_66) + Number(_loc4_.var_66)))
               {
                  return true;
               }
            }
         }
         return false;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_782 = [];
         var_351 = 1;
         var_783 = 16 - int(param1 * 10);
         var_251 = 0;
         method_117();
         method_72();
      }
      
      public function method_515() : void
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\x034\x1dG\x02"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_66 = Number(5 + Math.random() * (15 + var_237[0] * 0.1));
         do
         {
            _loc1_.var_66 = _loc1_.var_66 - 0.2;
            _loc1_.var_244 = Number(Number(_loc1_.var_66) + Math.random() * (class_742.var_217 - 2 * _loc1_.var_66));
         }
         while(method_516(_loc1_) || var_288.var_245 < 40 && Number(Math.abs(_loc1_.var_244 - var_288.var_244)) < 30);
         
         _loc1_.var_245 = -_loc1_.var_66;
         _loc1_.var_279 = Number(1 + Math.random() * 3);
         _loc1_.method_118();
         _loc1_.var_243.scaleX = _loc1_.var_66 * 0.02;
         _loc1_.var_243.scaleY = _loc1_.var_66 * 0.02;
         _loc1_.var_243.gotoAndStop(1 + int(Math.round(_loc1_.var_66 / 20)));
         var_782.push(_loc1_);
      }
      
      public function method_390(param1:*) : void
      {
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\x1c%oz\x01"),0);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("=_ww"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_288 = _loc1_;
         var_288.var_244 = class_742.var_217 * 0.5;
         var_288.var_245 = class_742.var_219;
         var_288.var_279 = -var_351;
         var_288.method_118();
      }
   }
}
