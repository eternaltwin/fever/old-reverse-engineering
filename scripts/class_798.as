package
{
   import package_11.class_844;
   import package_32.class_899;
   
   public class class_798 extends class_645
   {
       
      
      public function class_798(param1:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var _loc2_:class_844 = new class_844(param1);
         var _loc3_:Number = _loc2_.var_503 * 16807 % 2147483647;
         _loc2_.var_503 = _loc3_;
         int((int(_loc3_) & 1073741823) % 10007) / 10007;
         _loc3_ = _loc2_.var_503 * 16807 % 2147483647;
         _loc2_.var_503 = _loc3_;
         graphics.beginFill(int((int(_loc3_) & 1073741823) % 16777215));
         graphics.drawRect(0,0,class_645.var_211,class_645.var_212);
      }
      
      override public function method_79() : void
      {
         super.method_79();
         if(name_5)
         {
            method_81(true);
         }
      }
   }
}
