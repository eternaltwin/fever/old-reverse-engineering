package package_27.package_28
{
   import flash.utils.ByteArray;
   import package_32.class_899;
   
   public class class_855
   {
       
      
      public var var_861:Boolean;
      
      public function class_855()
      {
      }
      
      public function method_621(param1:Boolean) : Boolean
      {
         var_861 = param1;
         return param1;
      }
      
      public function method_901(param1:int) : String
      {
         var _loc3_:int = 0;
         var _loc2_:StringBuf = new StringBuf();
         while(true)
         {
            _loc3_ = int(method_774());
            if(_loc3_ == param1)
            {
               break;
            }
            _loc2_.b = _loc2_.b + String.fromCharCode(_loc3_);
         }
         return _loc2_.b;
      }
      
      public function method_902() : int
      {
         var _loc1_:int = int(method_774());
         var _loc2_:int = int(method_774());
         var _loc3_:int = int(method_774());
         var _loc4_:int = int(method_774());
         if((!!var_861?_loc1_:_loc4_) >= 64)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         return !!var_861?_loc4_ | _loc3_ << 8 | _loc2_ << 16 | _loc1_ << 24:_loc1_ | _loc2_ << 8 | _loc3_ << 16 | _loc4_ << 24;
      }
      
      public function method_903() : int
      {
         var _loc1_:int = int(method_774());
         var _loc2_:int = int(method_774());
         var _loc3_:int = int(method_774());
         return !!var_861?_loc3_ | _loc2_ << 8 | _loc1_ << 16:_loc1_ | _loc2_ << 8 | _loc3_ << 16;
      }
      
      public function method_904() : int
      {
         var _loc1_:int = int(method_774());
         var _loc2_:int = int(method_774());
         return !!var_861?_loc2_ | _loc1_ << 8:_loc1_ | _loc2_ << 8;
      }
      
      public function method_503(param1:int) : String
      {
         var _loc2_:Bytes = Bytes.alloc(param1);
         method_905(_loc2_,0,param1);
         return _loc2_.toString();
      }
      
      public function method_906() : String
      {
         var _loc3_:int = 0;
         var _loc4_:String = null;
         var _loc5_:class_781 = null;
         var _loc2_:StringBuf = new StringBuf();
         try
         {
            while(true)
            {
               _loc3_ = int(method_774());
               if(_loc3_ == 10)
               {
                  break;
               }
               _loc2_.b = _loc2_.b + String.fromCharCode(_loc3_);
            }
            _loc4_ = _loc2_.b;
            if(_loc4_.charCodeAt(_loc4_.length - 1) == 13)
            {
               _loc4_ = _loc4_.substr(0,-1);
            }
         }
         catch(:class_781;)
         {
            _loc5_ = ;
            _loc4_ = _loc2_.b;
            if(_loc4_.length == 0)
            {
               class_899.var_317 = new Error();
               throw _loc5_;
            }
         }
         return _loc4_;
      }
      
      public function method_907() : int
      {
         var _loc1_:int = int(method_774());
         if(_loc1_ >= 128)
         {
            return _loc1_ - 256;
         }
         return _loc1_;
      }
      
      public function method_908() : int
      {
         var _loc1_:int = int(method_774());
         var _loc2_:int = int(method_774());
         var _loc3_:int = int(method_774());
         var _loc4_:int = int(method_774());
         return !!var_861?(_loc1_ << 8 | _loc2_) << 16 | (_loc3_ << 8 | _loc4_):(_loc4_ << 8 | _loc3_) << 16 | (_loc2_ << 8 | _loc1_);
      }
      
      public function method_909() : int
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         if(var_861)
         {
            _loc4_ = int(method_774());
            _loc3_ = int(method_774());
            _loc2_ = int(method_774());
            _loc1_ = int(method_774());
         }
         else
         {
            _loc1_ = int(method_774());
            _loc2_ = int(method_774());
            _loc3_ = int(method_774());
            _loc4_ = int(method_774());
         }
         if((_loc4_ & 128) == 0 != ((_loc4_ & 64) == 0))
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         return _loc1_ | _loc2_ << 8 | _loc3_ << 16 | _loc4_ << 24;
      }
      
      public function method_910() : int
      {
         var _loc1_:int = int(method_774());
         var _loc2_:int = int(method_774());
         var _loc3_:int = int(method_774());
         var _loc4_:int = !!var_861?_loc3_ | _loc2_ << 8 | _loc1_ << 16:_loc1_ | _loc2_ << 8 | _loc3_ << 16;
         if((_loc4_ & 8388608) != 0)
         {
            return _loc4_ - 16777216;
         }
         return _loc4_;
      }
      
      public function method_911() : int
      {
         var _loc1_:int = int(method_774());
         var _loc2_:int = int(method_774());
         var _loc3_:int = !!var_861?_loc2_ | _loc1_ << 8:_loc1_ | _loc2_ << 8;
         if((_loc3_ & 32768) != 0)
         {
            return _loc3_ - 65536;
         }
         return _loc3_;
      }
      
      public function method_905(param1:Bytes, param2:int, param3:int) : void
      {
         var _loc4_:int = 0;
         while(param3 > 0)
         {
            _loc4_ = int(method_615(param1,param2,param3));
            param2 = param2 + _loc4_;
            param3 = param3 - _loc4_;
         }
      }
      
      public function method_912() : Number
      {
         class_899.var_317 = new Error();
         throw "Not implemented";
      }
      
      public function method_913() : Number
      {
         class_899.var_317 = new Error();
         throw "Not implemented";
      }
      
      public function method_615(param1:Bytes, param2:int, param3:int) : int
      {
         var _loc4_:int = param3;
         var _loc5_:ByteArray = param1.b;
         if(param2 < 0 || param3 < 0 || param2 + param3 > param1.length)
         {
            class_899.var_317 = new Error();
            throw class_780.var_768;
         }
         while(_loc4_ > 0)
         {
            _loc5_[param2] = int(method_774());
            param2++;
            _loc4_--;
         }
         return param3;
      }
      
      public function method_774() : int
      {
         class_899.var_317 = new Error();
         throw "Not implemented";
      }
      
      public function method_915(param1:Object = undefined) : Bytes
      {
         var _loc5_:int = 0;
         var _loc6_:class_781 = null;
         if(param1 == null)
         {
            param1 = 16384;
         }
         var _loc3_:Bytes = Bytes.alloc(param1);
         var _loc4_:class_916 = new class_916();
         while(true)
         {
            try
            {
               continue;
            }
            catch(:class_781;)
            {
               _loc6_ = ;
            }
            return _loc4_.method_914();
         }
         class_899.var_317 = new Error();
         throw class_780.var_862;
      }
      
      public function method_653(param1:int) : Bytes
      {
         var _loc4_:int = 0;
         var _loc2_:Bytes = Bytes.alloc(param1);
         var _loc3_:int = 0;
         while(param1 > 0)
         {
            _loc4_ = int(method_615(_loc2_,_loc3_,param1));
            if(_loc4_ == 0)
            {
               class_899.var_317 = new Error();
               throw class_780.var_862;
            }
            _loc3_ = _loc3_ + _loc4_;
            param1 = param1 - _loc4_;
         }
         return _loc2_;
      }
      
      public function method_624() : void
      {
      }
   }
}
