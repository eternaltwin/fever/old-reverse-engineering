package {
import flash.utils.describeType;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;
import flash.utils.getQualifiedSuperclassName;

import package_32.class_899;

public class Type {
  public function Type() {
  }

  public static function method_878(param1:Object):Class {
    var _loc2_:String = getQualifiedClassName(param1);
    if (_loc2_ == "null" || _loc2_ == "Object" || _loc2_ == "int" || _loc2_ == "Number" || _loc2_ == "Boolean") {
      return null;
    }
    if (param1.hasOwnProperty("prototype")) {
      return null;
    }
    var _loc3_:* = getDefinitionByName(_loc2_) as Class;
    if (_loc3_.__isenum) {
      return null;
    }
    return _loc3_;
  }

  public static function method_255(param1:*):Class {
    var _loc2_:String = getQualifiedClassName(param1);
    if (_loc2_ == "null" || _loc2_.substr(0, 8) == "builtin.") {
      return null;
    }
    if (param1.hasOwnProperty("prototype")) {
      return null;
    }
    var _loc3_:* = getDefinitionByName(_loc2_) as Class;
    if (!_loc3_.__isenum) {
      return null;
    }
    return _loc3_;
  }

  public static function method_879(param1:Class):Class {
    var _loc2_:String = getQualifiedSuperclassName(param1);
    if (_loc2_ == null || _loc2_ == "Object") {
      return null;
    }
    return getDefinitionByName(_loc2_) as Class;
  }

  public static function method_880(param1:Class):String {
    if (param1 == null) {
      return null;
    }
    var _loc2_:String = getQualifiedClassName(param1);
    var _loc3_:String = _loc2_;
    if (_loc3_ == "int") {
      return "Int";
    }
    if (_loc3_ == "Number") {
      return "Float";
    }
    if (_loc3_ == "Boolean") {
      return "Bool";
    }
    return _loc2_.split("::").join(".");
  }

  public static function method_881(param1:Class):String {
    return Type.method_880(param1);
  }

  public static function method_750(param1:String):Class {
    var _loc3_:Class = null;
    var _loc5_:String = null;
    try {
      _loc3_ = getDefinitionByName(param1) as Class;
      if (_loc3_.__isenum) {
        return null;
      }
      return _loc3_;
    } catch (_loc4_:*) {
      _loc5_ = param1;
      if (_loc5_ == "Int") {
        return int;
      }
      if (_loc5_ == "Float") {
        return Number;
      }
      return null;
    }
    if (_loc3_ == null || _loc3_.__name__ == null) {
      return null;
    }
    return _loc3_;
  }

  public static function method_749(param1:String):Class {
    var _loc3_:* = null;
    try {
      _loc3_ = getDefinitionByName(param1);
      if (!_loc3_.__isenum) {
        return null;
      }
      return _loc3_;
    } catch (_loc4_:*) {
      if (param1 == "Bool") {
        return Boolean;
      }
      return null;
    }
    if (_loc3_ == null || _loc3_.__ename__ == null) {
      return null;
    }
    return _loc3_;
  }

  public static function method_245(param1:Class, param2:Array):Object {
    switch (int(param2.length)) {
      case 0:
      §§push(new param1());
        break;
      case 1:
      §§push(new param1(param2[0]));
        break;
      case 2:
      §§push(new param1(param2[0], param2[1]));
        break;
      case 3:
      §§push(new param1(param2[0], param2[1], param2[2]));
        break;
      case 4:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3]));
        break;
      case 5:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4]));
        break;
      case 6:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5]));
        break;
      case 7:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6]));
        break;
      case 8:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7]));
        break;
      case 9:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7], param2[8]));
        break;
      case 10:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7], param2[8], param2[9]));
        break;
      case 11:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7], param2[8], param2[9], param2[10]));
        break;
      case 12:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7], param2[8], param2[9], param2[10], param2[11]));
        break;
      case 13:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7], param2[8], param2[9], param2[10], param2[11], param2[12]));
        break;
      case 14:
      §§push(new param1(param2[0], param2[1], param2[2], param2[3], param2[4], param2[5], param2[6], param2[7], param2[8], param2[9], param2[10], param2[11], param2[12], param2[13]));
    }
    return; §§pop();
  }

  public static function method_882(param1:Class):Object {
    var _loc3_:Object = null;
    try {
      class_899.var_239 = true;
      _loc3_ = new param1();
      class_899.var_239 = false;
      return _loc3_;
    } catch (_loc4_:*) {
      class_899.var_239 = false;
      class_899.var_317 = new Error();
      throw null;
    }
    return null;
  }

  public static function method_254(param1:Class, param2:String, param3:Array = undefined):Object {
    var _loc4_:Object = Reflect.field(param1, param2);
    if (_loc4_ == null) {
      class_899.var_317 = new Error();
      throw "No such constructor " + param2;
    }
    if (Reflect.method_739(_loc4_)) {
      if (param3 == null) {
        class_899.var_317 = new Error();
        throw "Constructor " + param2 + " need parameters";
      }
      return _loc4_.apply(param1, param3);
    }
    if (param3 != null && int(param3.length) != 0) {
      class_899.var_317 = new Error();
      throw "Constructor " + param2 + " does not need parameters";
    }
    return _loc4_;
  }

  public static function method_669(param1:Class, param2:int, param3:Array = undefined):Object {
    var _loc4_:String = param1.__constructs__[param2];
    if (_loc4_ == null) {
      class_899.var_317 = new Error();
      throw param2 + " is not a valid enum constructor index";
    }
    return Type.method_254(param1, _loc4_, param3);
  }

  public static function method_883(param1:*, param2:Boolean):Array {
    var _loc8_:int = 0;
    var _loc3_:Array = [];
    var _loc4_:XML = describeType(param1);
    if (param2) {
      _loc4_ = _loc4_.factory[0];
    }
    var _loc5_:XMLList = _loc4_.child("method");
    var _loc6_:int = 0;
    var _loc7_:int = int(_loc5_.length());
    while (_loc6_ < _loc7_) {
      _loc6_++;
      _loc8_ = _loc6_;
      _loc3_.push(class_691.method_97(_loc5_[_loc8_].attribute("name")));
    }
    var _loc9_:XMLList = _loc4_.child("variable");
    _loc6_ = 0;
    _loc7_ = int(_loc9_.length());
    while (_loc6_ < _loc7_) {
      _loc6_++;
      _loc8_ = _loc6_;
      _loc3_.push(class_691.method_97(_loc9_[_loc8_].attribute("name")));
    }
    var _loc10_:XMLList = _loc4_.child("accessor");
    _loc6_ = 0;
    _loc7_ = int(_loc10_.length());
    while (_loc6_ < _loc7_) {
      _loc6_++;
      _loc8_ = _loc6_;
      _loc3_.push(class_691.method_97(_loc10_[_loc8_].attribute("name")));
    }
    return _loc3_;
  }

  public static function method_884(param1:Class):Array {
    return Type.method_883(param1, true);
  }

  public static function method_738(param1:Class):Array {
    var _loc2_:Array = Type.method_883(param1, false);
    _loc2_.method_116("__construct__");
    _loc2_.method_116("prototype");
    return _loc2_;
  }

  public static function method_253(param1:Class):Array {
    var _loc2_:Array = param1.__constructs__;
    return _loc2_.method_78();
  }

  public static function method_887(param1:*):class_887 {
    var _loc5_:* = null;
    var _loc3_:String = getQualifiedClassName(param1);
    var _loc4_:String = _loc3_;
    if (_loc4_ == "null") {
      return class_887.var_1219;
    }
    if (_loc4_ == "void") {
      return class_887.var_1219;
    }
    if (_loc4_ == "int") {
      return class_887.var_1220;
    }
    if (_loc4_ == "Number") {
      if ((param1 < -268435456 || param1 >= 268435456) && int(param1) == param1) {
        return class_887.var_1220;
      }
      return class_887.var_1221;
    }
    if (_loc4_ == "Boolean") {
      return class_887.var_1222;
    }
    if (_loc4_ == "Object") {
      return class_887.var_1223;
    }
    if (_loc4_ == "Function") {
      return class_887.var_1224;
    }
    _loc5_ = null;
    try {
      _loc5_ = getDefinitionByName(_loc3_);
      if (param1.hasOwnProperty("prototype")) {
        return class_887.var_1223;
      }
      if (_loc5_.__isenum) {
        return class_887.method_885(_loc5_);
      }
      return class_887.method_886(_loc5_);
    } catch (_loc6_:*) {
      if (null as Error) {
        class_899.var_317 = null;
      }
      if (_loc3_ == "builtin.as$0::MethodClosure" || int(_loc3_.indexOf("-")) != -1) {
        return class_887.var_1224;
      }
      return _loc5_ == null ? class_887.var_1224 : class_887.method_886(_loc5_);
    }
    return null;
  }

  public static function method_888(param1:Object, param2:Object):Boolean {
    var _loc4_:Array = null;
    var _loc5_:Array = null;
    var _loc6_:int = 0;
    var _loc7_:int = 0;
    var _loc8_:int = 0;
    if (param1 == param2) {
      return true;
    }
    try {
      if (param1.var_295 != param2.var_295) {
        return false;
      }
      _loc4_ = param1.var_296;
      _loc5_ = param2.var_296;
      _loc6_ = 0;
      _loc7_ = int(_loc4_.length);
      while (_loc6_ < _loc7_) {
        _loc6_++;
        _loc8_ = _loc6_;
        if (!Type.method_888(_loc4_[_loc8_], _loc5_[_loc8_])) {
          return false;
        }
      }
    } catch (_loc9_:*) {
      return false;
    }
    return true;
  }

  public static function method_889(param1:*):String {
    return param1.var_294;
  }

  public static function method_890(param1:*):Array {
    return param1.var_296 == null ? [] : param1.var_296;
  }

  public static function method_891(param1:*):int {
    return param1.var_295;
  }

  public static function method_892(param1:Class):Array {
    var _loc5_:String = null;
    var _loc6_:Object = null;
    var _loc2_:Array = [];
    var _loc3_:Array = param1.__constructs__;
    var _loc4_:int = 0;
    while (_loc4_ < int(_loc3_.length)) {
      _loc5_ = _loc3_[_loc4_];
      _loc4_++;
      _loc6_ = Reflect.field(param1, _loc5_);
      if (!Reflect.method_739(_loc6_)) {
        _loc2_.push(_loc6_);
      }
    }
    return _loc2_;
  }
}
}
