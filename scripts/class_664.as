package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_664 extends class_645
   {
      
      public static var var_349:int = 40;
      
      public static var var_332:int = 150;
      
      public static var var_350:int = 200;
       
      
      public var var_351:Number;
      
      public var var_307:Number;
      
      public var var_352:Object;
      
      public var var_64:MovieClip;
      
      public var name_26:Number;
      
      public var var_17:MovieClip;
      
      public var var_39:MovieClip;
      
      public function class_664()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_257();
               var_64.name_21 = (Number(var_64.name_21) + var_351 * 0.6) % 628;
               var_64.x = Number(class_664.var_332 + (class_742.var_217 - class_664.var_332) * (Number(Math.cos(var_64.name_21 / 100)) + 1) * 0.5);
               break;
            case 2:
               method_257();
               var_64.y = Number(var_64.y + 50);
               if(var_64.y >= class_664.var_350)
               {
                  var_155();
                  break;
               }
               break;
            case 3:
               name_26 = name_26 * 0.95;
               var_17.y = Number(var_17.y + name_26);
               name_26 = Number(name_26 + 0.6);
               if(var_17.y > class_664.var_350)
               {
                  var_17.y = class_664.var_350;
                  name_26 = -name_26 * 0.8;
                  break;
               }
            case 4:
               name_26 = name_26 * 0.95;
               var_17.y = Number(var_17.y + name_26);
               name_26 = Number(name_26 + 0.6);
               if(var_17.y > class_664.var_350)
               {
                  var_17.y = class_664.var_350;
                  name_26 = -name_26 * 0.8;
                  break;
               }
         }
         if(var_352 != null)
         {
            var_352 = var_352 * 0.8;
            _loc1_ = (Math.random() * 2 - 1) * var_352;
            root.y = _loc1_;
            var_39.y = Number(class_664.var_350 + _loc1_ * 0.5);
         }
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(false);
      }
      
      override public function method_83() : void
      {
         if(name_3 != 1)
         {
            return;
         }
         name_3 = 2;
         var_64.gotoAndPlay("2");
      }
      
      public function method_257() : void
      {
         var_17.name_21 = (Number(var_17.name_21) + var_351) % 628;
         var _loc1_:Number = Number(Math.cos(var_17.name_21 / 100));
         var _loc2_:Number = Number(class_664.var_349 + (class_742.var_217 - class_664.var_349 * 2) * (_loc1_ + 1) * 0.5);
         var_17.scaleX = _loc2_ < var_17.x?-1:1;
         var_17.x = _loc2_;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [350];
         super.method_95(param1);
         var_351 = Number(10 + param1 * 20);
         method_117();
         method_72();
      }
      
      public function var_155() : void
      {
         var_64.y = class_664.var_350;
         var _loc1_:Number = var_17.x - (var_64.x - class_664.var_332 * 0.5);
         var_352 = 3;
         if(Number(Math.abs(_loc1_)) <= (class_664.var_332 * 0.5 + class_664.var_349) * 0.75)
         {
            name_3 = 4;
            var_17.scaleY = 0.2;
            method_81(false,20);
            return;
         }
         var_17.scaleX = _loc1_ > 0?-1:1;
         name_3 = 3;
         name_26 = -8;
         method_81(true,20);
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("6\x14\x1f\x02"),0);
         var_64 = class_319.var_64;
         var_17 = class_319.var_17;
         var_39 = class_319.var_39;
         var_17.name_21 = Math.random() * 628;
         var_64.name_21 = Math.random() * 628;
         var_64.var_64.width = var_307;
      }
   }
}
