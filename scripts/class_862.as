package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.filters.DropShadowFilter;
   import flash.geom.Matrix;
   import package_10.class_870;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_862 extends class_645
   {
      
      public static var var_1265:int = 30;
      
      public static var var_1266:int = 150;
      
      public static var var_765:int = 10;
       
      
      public var var_1267:int;
      
      public var var_1268:class_795;
      
      public var var_351:Number;
      
      public var var_10:Array;
      
      public var var_1188:class_892;
      
      public function class_862()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_959() : void
      {
         var _loc2_:MovieClip = null;
         var _loc3_:* = null;
         var _loc1_:* = var_1268.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            if(_loc2_.var_945 != null && _loc3_ <= 0)
            {
               _loc2_.var_945 = null;
            }
            _loc2_.var_65 = Number(class_663.method_112(Number(Number(_loc2_.var_65) + Number(_loc2_.name_88)),3.14));
            _loc2_.rotation = _loc2_.var_65 / 0.0174;
         }
      }
      
      public function method_962() : void
      {
         var _loc1_:MovieClip = null;
         var _loc2_:Number = NaN;
         var _loc3_:class_892 = null;
         var _loc4_:Number = NaN;
         var _loc5_:* = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         if(var_1188.name_127 != null)
         {
            _loc1_ = var_1188.name_127;
            _loc2_ = Number(class_663.method_112(Number(Number(var_1188.var_65) + Number(_loc1_.var_65)),3.14));
            var_1188.var_244 = Number(_loc1_.x + Math.cos(_loc2_) * _loc1_.var_66);
            var_1188.var_245 = Number(_loc1_.y + Math.sin(_loc2_) * _loc1_.var_66);
            var_1188.var_243.rotation = _loc2_ / 0.0174;
            var_1188.method_118();
         }
         else
         {
            var_1188.name_128 = Number(Math.max(0.07,var_1188.name_128 * 0.94));
            if(Number(Math.random()) < Number(var_1188.name_128))
            {
               _loc3_ = method_960();
               _loc2_ = Number(0.4 + Math.random() * 0.4);
               _loc3_.var_244 = Number(_loc3_.var_244 + (Math.random() * 2 - 1) * 3);
               _loc3_.var_245 = Number(_loc3_.var_245 + (Math.random() * 2 - 1) * 3);
               _loc3_.var_278 = var_1188.var_278 * _loc2_;
               _loc3_.var_279 = var_1188.var_279 * _loc2_;
               _loc3_.method_119(Number(Number(0.5 + Math.random() * 0.5) + var_1188.name_128 * 0.5));
               _loc3_.var_250 = Number(0.2 + Math.random() * 0.2);
            }
            _loc2_ = Number(Number(Math.atan2(var_1188.var_279,var_1188.var_278)) + 3.14);
            _loc4_ = class_663.method_113(_loc2_,6.28) / 6.28;
            var_1188.var_243.gotoAndStop(60 + int(_loc4_ * 40));
            _loc5_ = var_1268.method_73();
            while(_loc5_.method_74())
            {
               _loc1_ = _loc5_.name_1();
               _loc6_ = _loc1_.x - var_1188.var_244;
               _loc7_ = _loc1_.y - var_1188.var_245;
               if(Number(Math.sqrt(Number(_loc6_ * _loc6_ + _loc7_ * _loc7_))) < Number(Number(_loc1_.var_66) + 5))
               {
                  method_961(_loc1_);
                  break;
               }
            }
            _loc8_ = -20;
            if(var_1188.var_244 < _loc8_ || var_1188.var_244 > class_742.var_216 - _loc8_ * 2 || var_1188.var_245 > class_742.var_218 - _loc8_ * 2)
            {
               method_81(false);
            }
         }
      }
      
      override public function method_79() : void
      {
         method_959();
         method_962();
         super.method_79();
      }
      
      override public function method_83() : void
      {
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:class_892 = null;
         if(var_1188.name_127 == null || var_220)
         {
            return;
         }
         var_1188.name_127.var_945 = 5;
         var _loc1_:Number = Number(class_663.method_112(Number(Number(var_1188.var_65) + Number(var_1188.name_127.var_65)),3.14));
         var _loc3_:int = 0;
         while(_loc3_ < 4)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = Math.random() * 2 - 1;
            _loc6_ = Number(Number(var_1188.var_65) + _loc5_ * 0.8);
            _loc7_ = 8 - Math.abs(_loc5_) * 6;
            _loc8_ = _loc4_ / 4;
            _loc9_ = method_960();
            _loc9_.var_278 = Math.cos(_loc6_) * _loc7_;
            _loc9_.var_279 = Math.sin(_loc6_) * _loc7_;
            _loc9_.method_119(Number(0.5 + _loc8_));
            _loc9_.var_251 = 10 + int(class_691.method_114(30));
            _loc9_.var_250 = Number(0.2 + _loc8_ * 0.2);
         }
         var_1188.name_127 = null;
         var_1188.var_250 = 0.35;
         var_1188.var_278 = Math.cos(_loc1_) * class_862.var_765;
         var_1188.var_279 = Math.sin(_loc1_) * class_862.var_765;
         var_1188.var_243.rotation = 0;
         var_1188.name_128 = 1;
      }
      
      public function method_960() : class_892
      {
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\x18\'\x03Y\x01"),2));
         _loc1_.var_244 = var_1188.var_244;
         _loc1_.var_245 = var_1188.var_245;
         _loc1_.var_251 = 10 + int(class_691.method_114(10));
         _loc1_.var_272 = 0;
         return _loc1_;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [500 - 100 * param1];
         super.method_95(param1);
         var_351 = Number(0.04 + param1 * 0.1);
         var_1267 = 3 + int(param1 * 5);
         var_10 = [0];
         method_117();
      }
      
      public function method_961(param1:MovieClip) : void
      {
         if(param1.var_945 != null)
         {
            return;
         }
         var _loc2_:Number = param1.x - var_1188.var_244;
         var _loc3_:Number = param1.y - var_1188.var_245;
         var _loc4_:Number = Number(Number(Math.atan2(_loc3_,_loc2_)) + 3.14);
         var_1188.name_127 = param1;
         var_1188.var_65 = Number(class_663.method_112(_loc4_ - param1.var_65,3.14));
         var_1188.var_250 = 0;
         var_1188.var_278 = 0;
         var_1188.var_279 = 0;
         var_1188.var_243.gotoAndPlay("grab");
         if(param1.name_129.visible)
         {
            return;
         }
         param1.filters = [];
         param1.name_129.visible = true;
         var_10 = [var_10[0] + 1];
         if(var_10[0] == var_1267)
         {
            method_81(true,20);
         }
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Matrix = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Boolean = false;
         var _loc15_:* = null;
         var _loc16_:MovieClip = null;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc19_:Number = NaN;
         var _loc20_:DropShadowFilter = null;
         var _loc21_:MovieClip = null;
         var _loc22_:MovieClip = null;
         var _loc23_:Array = null;
         var _loc24_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("H\x14b("),0);
         var _loc1_:BitmapData = new BitmapData(class_742.var_216,class_742.var_218);
         class_319.addChild(new Bitmap(_loc1_));
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__("lN(\n\x01"),0);
         var _loc3_:int = 0;
         while(_loc3_ < 10)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = 0;
            while(_loc5_ < 10)
            {
               _loc5_++;
               _loc6_ = _loc5_;
               _loc7_ = new Matrix();
               _loc7_.translate(_loc4_ * 40,_loc6_ * 40);
               _loc2_.gotoAndStop(int(class_691.method_114(_loc2_.totalFrames)) + 1);
               _loc1_.draw(_loc2_,_loc7_);
            }
         }
         var _loc8_:Number = 35 - var_237[0] * 10;
         _loc3_ = 0;
         var_1268 = new class_795();
         while(var_1268.name_37 < var_1267)
         {
            _loc9_ = Number(Math.max(15,_loc8_ - _loc3_ * 0.1));
            _loc10_ = Number(_loc9_ + Math.random() * _loc9_);
            _loc11_ = Number(_loc10_ + 8);
            _loc12_ = Number(_loc11_ + Math.random() * (class_742.var_216 - _loc11_ * 2));
            _loc13_ = Number(_loc11_ + Math.random() * (class_742.var_218 - _loc11_ * 2));
            _loc14_ = var_1268.name_37 == 0;
            _loc15_ = var_1268.method_73();
            while(_loc15_.method_74())
            {
               _loc16_ = _loc15_.name_1();
               _loc17_ = _loc12_ - _loc16_.x;
               _loc18_ = _loc13_ - _loc16_.y;
               _loc19_ = Number(Math.sqrt(Number(_loc17_ * _loc17_ + _loc18_ * _loc18_))) - (Number(_loc10_ + Number(_loc16_.var_66)));
               if(_loc19_ < class_862.var_1265)
               {
                  _loc14_ = false;
                  break;
               }
               if(_loc19_ < class_862.var_1266)
               {
                  _loc14_ = true;
               }
            }
            _loc20_ = new DropShadowFilter();
            _loc20_.blurX = 2;
            _loc20_.blurY = 2;
            _loc20_.color = 0;
            _loc20_.strength = 0.2;
            _loc20_.distance = 5;
            if(_loc14_)
            {
               _loc16_ = var_232.method_84(class_899.__unprotect__("&G\x01X\x01"),2);
               _loc16_.x = _loc12_;
               _loc16_.y = _loc13_;
               _loc16_.var_66 = _loc10_;
               _loc17_ = _loc10_ * 0.02;
               _loc16_.scaleY = _loc17_;
               _loc16_.scaleX = _loc17_;
               _loc16_.name_88 = Number(var_351 + Math.random() * var_351);
               _loc16_.var_65 = Math.random() * 6.28;
               _loc16_.gotoAndStop(int(class_691.method_114(4)) + 1);
               var_1268.method_103(_loc16_);
               _loc16_.filters = [_loc20_];
               _loc16_.name_129 = var_232.method_84(class_899.__unprotect__("Ysf\\"),1);
               _loc21_ = var_232.method_84(class_899.__unprotect__(")}h2\x02"),2);
               _loc22_ = var_232.method_84(class_899.__unprotect__("\x0f\'\x196\x03"),1);
               _loc21_.gotoAndStop(_loc16_.currentFrame);
               _loc16_.name_129.gotoAndPlay(int(class_691.method_114(_loc16_.totalFrames)) + 1);
               _loc16_.name_129.visible = false;
               class_657.name_18(_loc16_.name_129,int(class_657.method_237(var_1268.name_37 / var_1267)));
               _loc23_ = [_loc16_.name_129,_loc21_,_loc22_];
               _loc4_ = 0;
               while(_loc4_ < int(_loc23_.length))
               {
                  _loc24_ = _loc23_[_loc4_];
                  _loc4_++;
                  _loc24_.x = _loc12_;
                  _loc24_.y = _loc13_;
                  _loc17_ = _loc16_.scaleX;
                  _loc24_.scaleY = _loc17_;
                  _loc24_.scaleX = _loc17_;
               }
            }
            _loc3_++;
            if(_loc3_ > 300)
            {
               class_870.name_17("echec",{
                  "\x06\fi2\x03":"Interwheel.hx",
                  "d\x0f^S\x03":112,
                  "v2z\x01":"Interwheel",
                  "d\x19o\f\x03":"attachElements"
               });
               break;
            }
         }
         var_1188 = new class_892(var_232.method_84(class_899.__unprotect__("\x1a\x102_\x01"),1));
         method_961(var_1268.name_36());
         method_962();
      }
   }
}
