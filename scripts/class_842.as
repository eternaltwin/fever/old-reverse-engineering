package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_842 extends class_645
   {
      
      public static var var_333:int = 12;
      
      public static var var_268:int = 21;
      
      public static var var_269:int = 21;
      
      public static var var_1148:int = -6;
      
      public static var var_1149:int = -6;
      
      public static var var_1177:int = 0;
      
      public static var var_1178:int = 1;
      
      public static var var_1179:int = 2;
      
      public static var var_1180:int = 3;
      
      public static var var_1181:int = 4;
      
      public static var var_264:Array = [{
         "L\x01":1,
         "\x03\x01":0
      },{
         "L\x01":0,
         "\x03\x01":1
      },{
         "L\x01":-1,
         "\x03\x01":0
      },{
         "L\x01":0,
         "\x03\x01":-1
      }];
      
      public static var var_1182:Boolean = true;
       
      
      public var var_1183:String;
      
      public var var_1184:Array;
      
      public var var_641:int;
      
      public var var_642:int;
      
      public var var_1186:int;
      
      public var var_496:Array;
      
      public var name_62:MovieClip;
      
      public var var_254:Number;
      
      public var var_1185:int;
      
      public var var_945:int;
      
      public var class_318:MovieClip;
      
      public function class_842()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc7_:MovieClip = null;
         var _loc8_:* = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         super.method_79();
         switch(name_3)
         {
            default:
            default:
               break;
            case 2:
               _loc1_ = method_100();
               _loc2_ = Number(_loc1_.var_244) - (Number(class_318.x + class_842.var_333 * 0.5));
               _loc3_ = Number(_loc1_.var_245) - (Number(class_318.y + class_842.var_333 * 0.5));
               _loc4_ = Number(Number(Math.atan2(_loc3_,_loc2_)) + 0.77);
               if(_loc4_ < 0)
               {
                  _loc4_ = Number(_loc4_ + 6.28);
               }
               _loc5_ = int(Math.floor(_loc4_ / 6.28 * 4));
               _loc7_ = Reflect.field(name_62,class_899.__unprotect__("t\x01"));
               _loc6_ = _loc7_;
               if(_loc6_ != null)
               {
                  _loc6_.gotoAndStop(_loc5_ + 1);
               }
               if(name_5)
               {
                  name_62.visible = false;
                  var_945 = _loc5_;
                  _loc8_ = class_842.var_264[var_945];
                  var_642 = var_642 - int(_loc8_.var_244);
                  var_641 = var_641 - int(_loc8_.var_245);
                  var_254 = 1;
                  name_3 = 3;
                  break;
               }
               break;
            case 3:
               var_254 = Number(var_254 + 0.66);
               _loc1_ = class_842.var_264[var_945];
               while(var_254 > 1)
               {
                  var_254 = var_254 - 1;
                  var_642 = var_642 + int(_loc1_.var_244);
                  var_641 = var_641 + int(_loc1_.var_245);
                  _loc5_ = int(var_496[var_642][var_641]);
                  if(_loc5_ == class_842.var_1181)
                  {
                     method_81(true,15);
                     class_318.var_40.name_63 = false;
                     name_3 = 4;
                     var_254 = 0;
                     var_229 = [true];
                     break;
                  }
                  if(_loc5_ == class_842.var_1179)
                  {
                     method_81(false,15);
                     class_318.var_40.name_63 = false;
                     name_3 = 4;
                     var_254 = 0;
                     break;
                  }
                  _loc9_ = var_642 + int(_loc1_.var_244);
                  _loc10_ = var_641 + int(_loc1_.var_245);
                  _loc11_ = int(var_496[_loc9_][_loc10_]);
                  if(_loc11_ == class_842.var_1180)
                  {
                     method_847();
                     var_254 = 0;
                     break;
                  }
               }
               class_318.x = Number(method_394(Number(var_642 + var_254 * int(_loc1_.var_244))));
               class_318.y = Number(method_395(Number(var_641 + var_254 * int(_loc1_.var_245))));
               break;
            case 4:
               _loc6_ = class_318.var_12;
               _loc6_.scaleX = _loc6_.scaleX * 0.85;
               _loc6_.scaleY = _loc6_.scaleX;
               _loc6_.y = Number(_loc6_.y + 0.35);
               if(_loc6_.scaleX < 0.1)
               {
                  _loc6_.parent.removeChild(_loc6_);
                  name_3 = 5;
                  break;
               }
         }
      }
      
      public function method_241(param1:Array) : *
      {
         var _loc3_:int = 0;
         var _loc2_:Array = [];
         while(int(param1.length) > 0)
         {
            _loc3_ = int(class_691.method_114(int(param1.length)));
            _loc2_.push(param1[_loc3_]);
            param1.splice(_loc3_,1);
         }
         return _loc2_;
      }
      
      public function method_847() : void
      {
         name_3 = 2;
         name_62.visible = true;
         name_62.x = Number(method_394(var_642));
         name_62.y = Number(method_395(var_641));
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_1183 = "";
         var_1185 = 4 + int(param1 * 12);
         var_1186 = 0;
         method_72();
         method_848();
         name_3 = 2;
         method_117();
      }
      
      public function name_41(param1:int, param2:int, param3:int, param4:int, param5:Array) : Object
      {
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:* = null;
         var _loc14_:Array = null;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc6_:* = class_842.var_264[param3];
         var _loc7_:int = 0;
         while(true)
         {
            param1 = param1 + int(_loc6_.var_244);
            param2 = param2 + int(_loc6_.var_245);
            _loc8_ = int(var_496[param1][param2]);
            _loc9_ = _loc8_;
            if(_loc9_ == class_842.var_1180)
            {
               break;
            }
            if(_loc9_ == class_842.var_1179)
            {
               return 999;
            }
            if(_loc9_ == class_842.var_1181)
            {
               return param4;
            }
            _loc7_++;
         }
         if(_loc7_ == 0)
         {
            return 999;
         }
         param4++;
         _loc10_ = param1 - int(_loc6_.var_244);
         _loc11_ = param2 - int(_loc6_.var_245);
         _loc12_ = 0;
         while(_loc12_ < int(param5.length))
         {
            _loc13_ = param5[_loc12_];
            _loc12_++;
            if(int(_loc13_.var_244) == _loc10_ && int(_loc13_.var_245) == _loc11_ && param4 >= int(_loc13_.var_93))
            {
               return 999;
            }
         }
         param5.push({
            "L\x01":_loc10_,
            "\x03\x01":_loc11_,
            "z\x01":param4
         });
         _loc14_ = [];
         _loc14_.push(int((param3 + 1) % 4));
         _loc14_.push(int((param3 + 3) % 4));
         _loc12_ = 999;
         _loc15_ = 0;
         while(_loc15_ < int(_loc14_.length))
         {
            _loc16_ = int(_loc14_[_loc15_]);
            _loc15_++;
            _loc12_ = int(Number(Math.min(_loc12_,name_41(_loc10_,_loc11_,_loc16_,param4,param5))));
         }
         return _loc12_;
      }
      
      public function method_395(param1:Number) : Number
      {
         return Number(class_842.var_1149 + param1 * class_842.var_333);
      }
      
      public function method_394(param1:Number) : Number
      {
         return Number(class_842.var_1148 + param1 * class_842.var_333);
      }
      
      public function method_402() : Boolean
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:Array = null;
         var _loc14_:* = null;
         if(int(var_1184.length) >= var_1185)
         {
            return true;
         }
         var _loc1_:* = var_1184[int(var_1184.length) - 1];
         var _loc2_:* = class_842.var_264[int(_loc1_.var_8)];
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(true)
         {
            _loc4_++;
            _loc5_ = int(_loc1_.var_244) + _loc4_ * int(_loc2_.var_244);
            _loc6_ = int(_loc1_.var_245) + _loc4_ * int(_loc2_.var_245);
            _loc7_ = int(var_496[_loc5_][_loc6_]);
            if(_loc7_ == class_842.var_1177)
            {
               _loc3_.push(_loc4_);
            }
            else if(_loc7_ != class_842.var_1178)
            {
               break;
            }
         }
         _loc3_ = method_241(_loc3_);
         while(int(_loc3_.length) > 0)
         {
            _loc8_ = _loc3_.pop();
            _loc5_ = int(_loc1_.var_244) + _loc8_ * int(_loc2_.var_244);
            _loc6_ = int(_loc1_.var_245) + _loc8_ * int(_loc2_.var_245);
            _loc7_ = 1;
            _loc9_ = int(_loc8_ + 1);
            while(_loc7_ < _loc9_)
            {
               _loc7_++;
               _loc10_ = _loc7_;
               _loc11_ = int(_loc1_.var_244) + _loc10_ * int(_loc2_.var_244);
               _loc12_ = int(_loc1_.var_245) + _loc10_ * int(_loc2_.var_245);
               var_496[_loc11_][_loc12_] = class_842.var_1178;
            }
            _loc13_ = [];
            _loc13_.push(int((int(_loc1_.var_8) + 1) % 4));
            _loc13_.push(int((int(_loc1_.var_8) + 3) % 4));
            _loc13_ = method_241(_loc13_);
            while(int(_loc13_.length) > 0)
            {
               _loc14_ = _loc13_.pop();
               _loc7_ = int((int(_loc14_ + 2)) % 4);
               _loc9_ = _loc5_ + int(class_842.var_264[_loc7_].var_244);
               _loc10_ = _loc6_ + int(class_842.var_264[_loc7_].var_245);
               _loc11_ = int(var_496[_loc9_][_loc10_]);
               if(_loc11_ == class_842.var_1177 || _loc11_ == class_842.var_1180)
               {
                  var_496[_loc9_][_loc10_] = class_842.var_1180;
                  var_1184.push({
                     "L\x01":_loc5_,
                     "\x03\x01":_loc6_,
                     "t\x01":_loc14_
                  });
                  if(method_402())
                  {
                     return true;
                  }
                  var_1184.pop();
                  var_496[_loc9_][_loc10_] = _loc11_;
               }
            }
            _loc7_ = 1;
            _loc9_ = int(_loc8_ + 1);
            while(_loc7_ < _loc9_)
            {
               _loc7_++;
               _loc10_ = _loc7_;
               _loc11_ = int(_loc1_.var_244) + _loc10_ * int(_loc2_.var_244);
               _loc12_ = int(_loc1_.var_245) + _loc10_ * int(_loc2_.var_245);
               var_496[_loc11_][_loc12_] = class_842.var_1177;
            }
         }
         return false;
      }
      
      public function method_848() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc1_:int = 0;
         while(_loc1_ < 5)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            method_225();
            _loc3_ = int(method_849());
            if(_loc3_ >= var_1185 * 0.6)
            {
               break;
            }
         }
      }
      
      public function method_225() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc9_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("~1\x18;"),0);
         var_496 = [];
         var _loc1_:int = 0;
         _loc2_ = class_842.var_268;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_496[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = class_842.var_269;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               var_496[_loc3_][_loc6_] = _loc3_ == 0 || _loc3_ == class_842.var_268 - 1 || _loc6_ == 0 || _loc6_ == class_842.var_269 - 1?class_842.var_1179:class_842.var_1177;
            }
         }
         _loc1_ = 4;
         _loc2_ = _loc1_ + int(class_691.method_114(class_842.var_268 - 2 * _loc1_));
         _loc3_ = _loc1_ + int(class_691.method_114(class_842.var_269 - 2 * _loc1_));
         var_496[_loc2_][_loc3_] = class_842.var_1181;
         var_1184 = [{
            "L\x01":_loc2_,
            "\x03\x01":_loc3_,
            "t\x01":int(class_691.method_114(4))
         }];
         method_402();
         _loc7_ = var_1184[int(var_1184.length) - 1];
         var_642 = int(_loc7_.var_244);
         var_641 = int(_loc7_.var_245);
         var _loc8_:Array = [];
         _loc1_ = 0;
         while(_loc1_ < 8)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = 4;
            _loc4_ = _loc3_ + int(class_691.method_114(class_842.var_268 - 2 * _loc3_));
            _loc5_ = _loc3_ + int(class_691.method_114(class_842.var_269 - 2 * _loc3_));
            _loc7_ = class_842.var_264[int(class_691.method_114(4))];
            do
            {
               _loc4_ = _loc4_ + int(_loc7_.var_244);
               _loc5_ = _loc5_ + int(_loc7_.var_245);
               _loc6_ = int(var_496[_loc4_][_loc5_]);
               if(_loc6_ == class_842.var_1177)
               {
                  var_496[_loc4_][_loc5_] = class_842.var_1178;
               }
            }
            while(_loc6_ != class_842.var_1179);
            
         }
         _loc1_ = int(Number(Math.min(var_237[0] * 20,8)));
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = 4;
            _loc5_ = _loc4_ + int(class_691.method_114(class_842.var_268 - 2 * _loc4_));
            _loc6_ = _loc4_ + int(class_691.method_114(class_842.var_269 - 2 * _loc4_));
            _loc7_ = class_842.var_264[int(class_691.method_114(4))];
            do
            {
               _loc5_ = _loc5_ + int(_loc7_.var_244);
               _loc6_ = _loc6_ + int(_loc7_.var_245);
               _loc9_ = int(var_496[_loc5_][_loc6_]);
               if(_loc9_ == class_842.var_1177)
               {
                  var_496[_loc5_][_loc6_] = class_842.var_1180;
                  _loc8_.push({
                     "L\x01":_loc5_,
                     "\x03\x01":_loc6_
                  });
               }
            }
            while(_loc9_ != class_842.var_1179);
            
         }
         _loc8_ = method_241(_loc8_);
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            if(int(_loc8_.length) == 0)
            {
               break;
            }
            _loc7_ = _loc8_.pop();
            var_496[int(_loc7_.var_244)][int(_loc7_.var_245)] = class_842.var_1177;
         }
         _loc1_ = int(Math.floor(var_237[0] * 10));
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = 1;
            _loc5_ = _loc4_ + int(class_691.method_114(class_842.var_268 - 2 * _loc4_));
            _loc6_ = _loc4_ + int(class_691.method_114(class_842.var_269 - 2 * _loc4_));
            if(int(var_496[_loc5_][_loc6_]) == class_842.var_1177)
            {
               var_496[_loc5_][_loc6_] = class_842.var_1180;
            }
         }
      }
      
      public function method_849() : int
      {
         var _loc3_:int = 0;
         var _loc1_:int = 100;
         var _loc2_:int = 0;
         while(_loc2_ < 4)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc1_ = int(Number(Math.min(_loc1_,name_41(var_642,var_641,_loc3_,2,[]))));
         }
         return _loc1_;
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:int = class_842.var_268;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = 0;
            _loc5_ = class_842.var_269;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               _loc7_ = int(var_496[_loc3_][_loc6_]);
               if(_loc7_ == class_842.var_1180 || _loc7_ == class_842.var_1181)
               {
                  _loc8_ = var_232.method_84(class_899.__unprotect__("A9\x11\x01\x01"),class_645.var_209);
                  _loc8_.x = Number(method_394(_loc3_));
                  _loc8_.y = Number(method_395(_loc6_));
                  _loc8_.gotoAndStop(_loc7_ == class_842.var_1180?"1":"2");
               }
            }
         }
         class_318 = var_232.method_84(class_899.__unprotect__("A9\x11\x01\x01"),class_645.var_209);
         class_318.x = Number(method_394(var_642));
         class_318.y = Number(method_395(var_641));
         class_318.gotoAndStop("3");
         name_62 = var_232.method_84(class_899.__unprotect__("A9\x11\x01\x01"),class_645.var_209);
         name_62.x = class_318.x;
         name_62.y = class_318.y;
         name_62.gotoAndStop("4");
      }
   }
}
