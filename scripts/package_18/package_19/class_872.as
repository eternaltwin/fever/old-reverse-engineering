package package_18.package_19
{
   import flash.net.URLRequest;
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_765;
   import package_13.class_826;
   import package_13.class_868;
   import package_24.class_656;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.App;
   import package_32.class_899;
   import package_9.class_869;
   
   public class class_872 extends class_710
   {
       
      
      public var var_107:class_367;
      
      public var var_282:class_656;
      
      public var package_31:Object;
      
      public function class_872(param1:class_719 = undefined, param2:class_826 = undefined, param3:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(param1,param2);
         var_252 = class_669.var_426;
         name_58 = true;
         param1.var_242.push(this);
         package_31 = class_885.var_308._monsters[param3];
         var_282 = new class_656();
         var_282.method_137(class_702.var_242.method_136(package_31._anim));
         var_282.var_198.method_140();
         addChild(var_282);
         var_107 = new class_367();
         var_107.x = x;
         var_107.y = y;
         param1.var_1311.addChild(var_107);
         var_107.scaleY = Number(3);
         var_107.scaleX = 3;
         var_282.y = int(package_31._oy);
         y = Number(y + 4);
      }
      
      override public function method_380() : Boolean
      {
         var _loc1_:URLRequest = null;
         if(!class_652.var_214.method_182())
         {
            class_652.var_214.method_135(true);
            return true;
         }
         if(class_765.var_214.method_551())
         {
            class_652.var_214.method_196(var_289);
         }
         else
         {
            class_868.var_214.method_386(class_808.var_1023);
            class_652.var_214.method_135(true);
            _loc1_ = new URLRequest(class_905.var_1310);
            App.method_201(_loc1_,"_self");
         }
         return true;
      }
      
      public function method_989() : void
      {
         var _loc1_:Array = null;
         var _loc2_:Array = null;
      }
      
      override public function method_89() : void
      {
         var_282.method_89();
         super.method_89();
         var_267.var_242.method_116(this);
      }
      
      override public function method_382() : Boolean
      {
         return true;
      }
      
      override public function method_207(param1:String = undefined) : void
      {
         if(param1 == null)
         {
            param1 = "_explode";
         }
         var _loc2_:class_656 = new class_656();
         _loc2_.x = Number(x + var_282.x);
         _loc2_.y = Number(y + var_282.y);
         var_267.var_232.method_124(_loc2_,class_719.var_451);
         _loc2_.method_137(class_702.var_242.method_136(package_31._anim + param1));
         _loc2_.var_198.var_258 = _loc2_.method_89;
         method_295();
      }
      
      public function method_295() : void
      {
         method_89();
         new class_869(var_267,var_107,var_289);
         var_267.method_990();
      }
      
      public function method_988() : void
      {
         var_282.var_198 = null;
         var_282.method_128(class_702.var_242.method_98(0,"blob_yellow"));
      }
   }
}
