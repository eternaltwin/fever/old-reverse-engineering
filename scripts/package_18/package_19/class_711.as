package package_18.package_19
{
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_765;
   import package_13.class_826;
   import package_13.class_868;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   
   public class class_711 extends class_710
   {
       
      
      public var var_618:class_738;
      
      public var var_265:int;
      
      public function class_711(param1:class_719 = undefined, param2:class_826 = undefined, param3:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_378 = class_719.var_620;
         super(param1,param2);
         var_252 = class_669.method_277(param3);
         var_265 = param3;
         var_618 = new class_738();
         var_618.method_128(class_702.package_13.method_98(0,"jump_stone"));
         addChild(var_618);
      }
      
      override public function method_381() : void
      {
         var_618.method_128(class_702.package_13.method_98(1,"jump_stone"));
      }
      
      override public function method_180() : void
      {
         var _loc1_:* = class_765.var_214.package_31;
         if(int(_loc1_._rainbows) > 0 || Boolean(var_267.method_384()))
         {
            class_652.var_214.var_288.method_385(var_265);
         }
         else
         {
            class_868.var_214.method_386(class_808.var_619);
         }
      }
      
      override public function method_383() : int
      {
         var _loc1_:Array = class_742.var_264[var_265];
         var _loc2_:* = class_911.method_127(var_267.var_319 + int(_loc1_[0]),var_267.var_318 + int(_loc1_[1]));
         var _loc3_:* = class_911.var_214.method_175(int(_loc2_.var_244),int(_loc2_.var_245));
         return Number(_loc3_.var_237) > Number(var_267.package_31.var_237)?3:-3;
      }
   }
}
