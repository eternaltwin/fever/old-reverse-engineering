package package_9
{
   import flash.display.Sprite;
   import package_11.package_12.class_657;
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_765;
   import package_13.class_777;
   import package_13.class_826;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_931;
   
   public class class_690 extends class_647
   {
       
      
      public var name_3:int;
      
      public var var_17:class_777;
      
      public function class_690()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_17 = class_652.var_214.var_288;
         var_17.var_282.method_290(class_702.var_288.method_289(["hero_sorcery","hero_sorcery_loop"]));
         name_3 = 0;
         var_254 = 0;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         super.method_79();
         switch(name_3)
         {
            case 0:
               var_254 = Number(Math.min(Number(var_254 + 0.025),1));
               if(var_254 == 1)
               {
                  name_3 = name_3 + 1;
                  var_254 = 0;
                  break;
               }
               break;
            case 1:
               var_254 = Number(Math.min(Number(var_254 + 0.05),1));
               _loc1_ = Number(Math.pow(var_254,0.5));
               class_657.name_18(class_652.var_214.var_109,0,int(255 * _loc1_));
               if(var_254 == 1)
               {
                  name_55();
                  break;
               }
               break;
            case 2:
               var_254 = Number(Math.min(Number(var_254 + 0.05),1));
               _loc1_ = 1 - Math.pow(var_254,2);
               class_657.name_18(class_652.var_214.var_109,0,int(255 * _loc1_));
               if(var_254 == 1)
               {
                  class_652.var_214.method_135(true);
                  method_89();
                  break;
               }
         }
      }
      
      public function name_55() : void
      {
         var _loc5_:class_826 = null;
         var _loc6_:Sprite = null;
         var _loc7_:class_931 = null;
         var _loc1_:* = class_765.var_214.package_31._savePoint;
         var _loc2_:class_719 = new class_719(int(_loc1_._x),int(_loc1_._y));
         _loc2_.method_142();
         class_652.var_214.var_232.method_124(_loc2_,class_652.var_287);
         class_652.var_214.var_267.method_89();
         class_652.var_214.var_267 = _loc2_;
         var_17.method_143(_loc2_);
         var _loc3_:int = 0;
         var _loc4_:Array = _loc2_.var_540.var_541;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc5_.package_19 == null)
            {
               var_17.method_130(_loc5_);
               break;
            }
         }
         var_17.method_107();
         new class_754();
         _loc4_ = [var_17.var_243,_loc2_.var_540.package_19];
         _loc3_ = 0;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc6_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc6_ != null)
            {
               _loc7_ = new class_931(_loc6_,0.01);
               _loc7_.var_146(2,4);
            }
         }
         name_3 = name_3 + 1;
         var_254 = 0;
      }
   }
}
