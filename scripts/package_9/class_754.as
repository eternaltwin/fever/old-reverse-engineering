package package_9
{
   import package_13.class_719;
   import package_13.class_777;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_754 extends class_647
   {
       
      
      public var var_251:int;
      
      public var var_17:class_777;
      
      public function class_754()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_17 = class_652.var_214.var_288;
         var_251 = 200;
      }
      
      override public function method_79() : void
      {
         var _loc2_:* = null as package_24.class_739;
         super.method_79();
         var _loc1_:int = 5 - int(Math.round(var_251 / 50));
         if(int(var_251 % _loc1_) == 0)
         {
            _loc2_ = new package_24.class_739();
            _loc2_.visible = false;
            _loc2_.name_20 = int(class_691.method_114(12));
            _loc2_.method_128(class_702.package_9.method_98(null,"spark_twinkle"));
            _loc2_.var_262 = var_17.var_243.x + int(class_691.method_114(11)) - 5;
            _loc2_.var_263 = var_17.var_243.y + int(class_691.method_114(11)) - 9;
            _loc2_.method_118();
            _loc2_.var_250 = -(0.05 + Math.random() * 0.1);
            _loc2_.var_251 = 10 + int(class_691.method_114(10));
            _loc2_.var_233 = 0.95;
            var_17.var_267.var_232.method_124(_loc2_,class_719.var_285);
         }
         var _loc3_:int = var_251;
         var_251 = var_251 - 1;
         if(_loc3_ == 0)
         {
            method_89();
         }
      }
   }
}
