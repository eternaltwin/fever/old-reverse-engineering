package package_9
{
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_13.class_719;
   import package_13.class_777;
   import package_13.class_868;
   import package_18.package_19.class_859;
   import package_24.class_656;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_912 extends class_647
   {
       
      
      public var var_279:Number;
      
      public var var_251:int;
      
      public var name_3:int;
      
      public var var_282:class_656;
      
      public function class_912(param1:_Reward = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var _loc2_:class_777 = class_652.var_214.var_288;
         var_282 = new class_656();
         var_282.method_128(class_859.method_874(param1));
         _loc2_.var_267.var_232.method_124(var_282,class_719.var_285);
         var_282.x = _loc2_.var_243.x;
         var_282.y = _loc2_.var_243.y;
         class_658.var_146(var_282,2,4,5570560);
         _loc2_.var_282.method_137(class_702.var_288.method_136("hero_happy_jump"),true);
         var_282.y = var_282.y - 5;
         var_279 = -4;
         name_3 = 0;
         var_251 = 0;
         class_652.var_214.method_135(false);
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         switch(name_3)
         {
            case 0:
               var_279 = var_279 * 0.85;
               var_282.y = Number(var_282.y + var_279);
               _loc1_ = var_251;
               var_251 = var_251 + 1;
               if(_loc1_ > 28)
               {
                  name_3 = name_3 + 1;
                  var_251 = 0;
                  break;
               }
               break;
            case 1:
               var_251 = var_251 + 1;
               class_657.name_18(var_282,0,var_251 * 10);
               _loc1_ = var_251;
               var_251 = var_251 + 1;
               if(_loc1_ > 20)
               {
                  var_282.filters = [];
                  var_282.method_137(class_702.package_9.method_136("bonus_vanish"));
                  var_282.var_198.var_258 = var_282.method_89;
                  method_89();
                  class_652.var_214.var_288.method_107();
                  class_652.var_214.method_135(true);
                  class_868.var_214.method_195();
                  break;
               }
         }
      }
   }
}
