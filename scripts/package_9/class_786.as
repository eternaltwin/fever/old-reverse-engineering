package package_9
{
   import flash.display.DisplayObject;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_786 extends class_647
   {
       
      
      public var var_933:Number;
      
      public var var_344:Number;
      
      public var var_631:DisplayObject;
      
      public var name_91:Boolean;
      
      public function class_786(param1:DisplayObject = undefined, param2:Boolean = true, param3:Number = 0.1, param4:Number = 1.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_344 = param4;
         name_91 = param2;
         var_933 = param3;
         var_631 = param1;
         var_254 = 0;
         param1.scaleY = Number(!!param2?Number(0):var_344);
         param1.scaleX = 0;
         if(param2)
         {
            param1.visible = true;
         }
      }
      
      override public function method_79() : void
      {
         var_254 = Number(Math.min(Number(var_254 + var_933),1));
         var _loc1_:Number = var_254;
         if(!name_91)
         {
            _loc1_ = 1 - _loc1_;
         }
         var _loc2_:Number = _loc1_ * var_344;
         var_631.scaleY = _loc2_;
         var_631.scaleX = _loc2_;
         if(var_254 == 1)
         {
            var_631.visible = name_91;
            method_89();
         }
      }
   }
}
