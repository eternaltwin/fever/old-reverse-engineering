package package_9
{
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   import package_11.package_12.class_657;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_747;
   import package_8.package_9.class_873;
   
   public class class_924 extends class_736
   {
      
      public static var var_941:int = 20;
       
      
      public var name_3:int;
      
      public var var_831:Array;
      
      public var package_29:class_756;
      
      public function class_924(param1:class_756 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         package_29 = param1;
         super();
         method_366();
         class_652.var_214.method_208();
         class_652.var_214.method_135(false);
         name_3 = 0;
         var_254 = 0;
         var_17.var_282.method_137(class_702.var_288.method_136("hero_hurt"),false);
         new class_873(var_17.var_282,4,0);
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc4_:class_739 = null;
         super.method_79();
         switch(name_3)
         {
            case 0:
               _loc1_ = var_254;
               var_254 = var_254 + 1;
               if(_loc1_ > 20)
               {
                  var_254 = 0;
                  name_3 = name_3 + 1;
                  var_17.var_282.method_137(class_702.var_288.method_136("hero_explode"),false);
                  break;
               }
               break;
            case 1:
               _loc1_ = var_254;
               var_254 = var_254 + 1;
               if(_loc1_ > 120)
               {
                  var_17.method_130(var_729.var_281);
                  var_17.method_107();
                  class_652.var_214.method_135(true);
                  new class_747(var_17.var_282,100,8,4);
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 2:
               if(int(var_831.length) == 0)
               {
                  method_89();
                  break;
               }
         }
         var _loc2_:Array = var_831.method_78();
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            _loc4_.method_79();
            if(_loc4_.var_245 > class_742.var_323 + class_924.var_941)
            {
               _loc4_.method_89();
               var_831.method_116(_loc4_);
            }
         }
      }
      
      public function method_366() : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:class_739 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Matrix = null;
         var _loc17_:Number = NaN;
         var _loc1_:BitmapData = new BitmapData(class_742.var_216,class_742.var_323,false,16711935);
         _loc1_.draw(package_29);
         var _loc2_:int = int(Math.ceil(class_742.var_216 / class_924.var_941));
         var _loc3_:int = int(Math.ceil(class_742.var_323 / class_924.var_941));
         var _loc4_:Number = class_924.var_941 * 0.5;
         var_831 = [];
         var _loc5_:int = 0;
         while(_loc5_ < _loc2_)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = 0;
            while(_loc7_ < _loc3_)
            {
               _loc7_++;
               _loc8_ = _loc7_;
               _loc9_ = new class_739();
               _loc9_.var_244 = (_loc6_ + 0.5) * class_924.var_941;
               _loc9_.var_245 = (_loc8_ + 0.5) * class_924.var_941;
               _loc9_.method_118();
               _loc9_.var_233 = 0.98;
               class_905.var_243.addChild(_loc9_.var_243);
               _loc10_ = _loc9_.var_244 - class_742.var_216 * 0.5;
               _loc11_ = _loc9_.var_245 - class_742.var_323 * 0.5;
               _loc12_ = Number(Math.atan2(_loc11_,_loc10_));
               _loc13_ = Number(Math.sqrt(Number(_loc10_ * _loc10_ + _loc11_ * _loc11_)));
               _loc14_ = Number(Math.abs(_loc13_ / 250));
               _loc15_ = Number(0.75 + (1 - _loc14_) * 4);
               _loc9_.var_278 = Math.cos(_loc12_) * _loc15_;
               _loc9_.var_279 = Math.sin(_loc12_) * _loc15_ - 1;
               _loc9_.var_580 = (Math.random() * 2 - 1) * 5;
               _loc9_.var_1518 = 0.97;
               _loc9_.var_250 = Number(0.05 + Math.random() * 0.15);
               _loc9_.var_251 = 40 + int(class_691.method_114(20));
               _loc16_ = new Matrix();
               _loc16_.translate(-_loc9_.var_244,-_loc9_.var_245);
               _loc9_.var_243.graphics.beginBitmapFill(_loc1_,_loc16_);
               _loc17_ = class_924.var_941 * 0.5;
               _loc9_.var_243.graphics.drawRect(-_loc17_,-_loc17_,2 * _loc17_,2 * _loc17_);
               class_657.gfof(_loc9_.var_243,_loc14_ * 0.25,0);
               var_831.push(_loc9_);
            }
         }
      }
   }
}
