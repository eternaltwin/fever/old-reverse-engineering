package package_9
{
   import flash.display.BlendMode;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.geom.Point;
   import package_11.package_12.class_657;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_879 extends class_647
   {
       
      
      public var name_3:int;
      
      public var var_780:Array;
      
      public var var_531:Sprite;
      
      public var var_139:Array;
      
      public var package_29:class_756;
      
      public function class_879(param1:class_756 = undefined)
      {
         var _loc4_:int = 0;
         var _loc5_:class_368 = null;
         var _loc6_:Number = NaN;
         if(class_899.var_239)
         {
            return;
         }
         package_29 = param1;
         super();
         var_139 = [];
         name_3 = 0;
         var_254 = 0;
         var_531 = new Sprite();
         var_531.blendMode = BlendMode.ADD;
         var _loc2_:Point = class_652.var_214.var_321.package_19.localToGlobal(new Point(0,0));
         var_780 = [];
         var _loc3_:int = 0;
         while(_loc3_ < 2)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = new class_368();
            var_780.push(_loc5_);
            _loc5_.x = _loc2_.x;
            _loc5_.y = _loc2_.y;
            _loc6_ = 3;
            _loc5_.scaleY = _loc6_;
            _loc5_.scaleX = _loc6_;
            _loc5_.gotoAndStop(_loc4_ + 1);
         }
         class_905.var_243.addChild(var_531);
         class_905.var_243.addChild(var_780[1]);
         class_905.var_243.addChild(param1);
         class_905.var_243.addChild(var_780[0]);
         param1.mask = var_780[0];
      }
      
      override public function method_79() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Sprite = null;
         var _loc4_:Array = null;
         var _loc5_:class_368 = null;
         var _loc6_:class_368 = null;
         var _loc7_:class_368 = null;
         super.method_79();
         var _loc1_:Array = var_139.method_78();
         _loc2_ = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.alpha = _loc3_.alpha - 0.1;
            if(_loc3_.alpha <= 0)
            {
               var_139.method_116(_loc3_);
               var_531.removeChild(_loc3_);
            }
         }
         switch(name_3)
         {
            case 0:
               _loc2_ = 0;
               _loc4_ = var_780;
               while(_loc2_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc2_];
                  _loc2_++;
                  _loc5_.scaleX = _loc5_.scaleX * 0.95;
                  _loc5_.scaleX = _loc5_.scaleX - 0.05;
                  _loc5_.scaleY = _loc5_.scaleX;
                  _loc5_.rotation = _loc5_.rotation - 5;
               }
               var_254 = (var_254 + 0.03) % 1;
               _loc5_ = var_780[0];
               _loc6_ = new class_368();
               var_531.addChild(_loc6_);
               _loc6_.x = _loc5_.x;
               _loc6_.y = _loc5_.y;
               _loc6_.rotation = _loc5_.rotation;
               _loc6_.scaleX = _loc5_.scaleX;
               _loc6_.scaleY = _loc5_.scaleY;
               class_657.gfof(_loc6_,1,int(class_657.method_138(var_254)));
               var_139.push(_loc6_);
               _loc7_ = var_780[1];
               _loc2_ = 12;
               _loc7_.width = Number(_loc5_.width + _loc2_);
               _loc7_.height = Number(_loc5_.height + _loc2_);
               if(var_780[0].scaleX < 0.1)
               {
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 1:
               if(int(var_139.length) == 0)
               {
                  method_89();
                  break;
               }
         }
      }
      
      override public function method_89() : void
      {
         var _loc3_:class_368 = null;
         package_29.mask = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_780;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            class_905.var_243.removeChild(_loc3_);
         }
         class_905.var_243.removeChild(var_531);
         super.method_89();
         class_652.var_214.method_208();
      }
   }
}
