package package_9
{
   import flash.display.Sprite;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_660;
   import package_13.class_719;
   import package_13.class_777;
   import package_13.class_868;
   import package_18.package_19.class_859;
   import package_24.class_656;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_851 extends class_647
   {
       
      
      public var var_279:Number;
      
      public var var_283:class_660;
      
      public var name_3:int;
      
      public var var_282:class_656;
      
      public var var_1137:Sprite;
      
      public var package_19:class_859;
      
      public function class_851(param1:class_859 = undefined, param2:class_750 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         package_19 = param1;
         super();
         var _loc3_:class_777 = class_652.var_214.var_288;
         var_282 = new class_656();
         var_282.method_128(param2);
         class_658.var_146(var_282,2,4,5570560);
         _loc3_.var_267.var_232.method_124(var_282,class_719.var_285);
         var_282.x = _loc3_.var_243.x;
         var_282.y = _loc3_.var_243.y - 8;
         var _loc4_:* = param1.var_289.method_294();
         var_283 = new class_660(var_282.x,var_282.y,Number(_loc4_.var_244),Number(_loc4_.var_245));
         var_254 = 0;
         name_3 = 0;
         class_652.var_214.method_135(false);
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Array = null;
         var _loc3_:Number = NaN;
         switch(name_3)
         {
            case 0:
               var_254 = Number(Math.min(Number(var_254 + 0.05),1));
               _loc1_ = var_283.method_127(var_254);
               _loc2_ = class_742.var_264[int([1,0,1,0][class_652.var_214.var_288.var_265])];
               _loc3_ = Math.sin(var_254 * 3.14) * 20;
               _loc1_.var_244 = Number(Number(_loc1_.var_244) + int(_loc2_[0]) * _loc3_);
               _loc1_.var_245 = Number(Number(_loc1_.var_245) + int(_loc2_[1]) * _loc3_);
               var_282.x = Number(_loc1_.var_244);
               var_282.y = Number(_loc1_.var_245);
               var_282.method_293();
               if(var_254 == 1)
               {
                  method_876();
                  break;
               }
               break;
            case 1:
               var_254 = Number(Math.min(Number(var_254 + 0.04),1));
               var_282.y = Number(var_282.y + var_279);
               var_279 = var_279 * 0.8;
               if(var_254 == 1)
               {
                  name_3 = name_3 + 1;
                  var_254 = 0;
                  break;
               }
               break;
            case 2:
               var_254 = Number(Math.min(Number(var_254 + 0.05),1));
               class_657.name_18(var_282,0,int(var_254 * 255));
               if(var_254 == 1)
               {
                  var_282.filters = [];
                  var_282.method_137(class_702.package_9.method_136("bonus_vanish"));
                  var_282.var_198.var_258 = var_282.method_89;
                  method_89();
                  class_652.var_214.method_135(true);
                  class_868.var_214.method_195();
                  break;
               }
         }
      }
      
      public function method_876() : void
      {
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:* = null as package_24.class_739;
         var_254 = 0;
         name_3 = name_3 + 1;
         var_279 = -2.5;
         var _loc3_:int = 0;
         while(_loc3_ < 16)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = _loc4_ / 16 * 6.28;
            _loc6_ = Number(0.5 + Math.random() * 5);
            _loc7_ = new package_24.class_739();
            _loc7_.method_137(class_702.package_9.method_136("spark_twinkle"));
            _loc7_.var_198.method_140();
            class_652.var_214.var_288.var_267.var_232.method_124(_loc7_,class_719.var_285);
            _loc7_.var_278 = Math.cos(_loc5_) * _loc6_;
            _loc7_.var_279 = Math.sin(_loc5_) * _loc6_;
            _loc7_.var_262 = Number(var_282.x + _loc7_.var_278 * 1);
            _loc7_.var_263 = Number(var_282.y + _loc7_.var_279 * 1);
            _loc7_.var_251 = 10 + int(class_691.method_114(10));
            _loc7_.var_233 = 0.8;
            _loc7_.method_118();
         }
         package_19.method_877();
         var_282.method_128(class_859.method_874(package_19.var_309));
         var_1137 = new Sprite();
         var_1137.x = var_282.x;
         var_1137.y = var_282.y;
         var_1137.graphics.beginFill(16711680);
         var_1137.graphics.drawRect(-8,-32,16,32);
         class_652.var_214.var_288.var_267.var_232.method_124(var_1137,class_719.var_285);
         var_282.mask = var_1137;
      }
   }
}
