package package_9
{
   import flash.display.Sprite;
   import package_13.class_719;
   import package_18.package_19.class_730;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_873;
   import package_8.package_9.class_931;
   
   public class class_835 extends class_647
   {
       
      
      public var var_1136:Number;
      
      public var var_1135:class_730;
      
      public var var_1137:Sprite;
      
      public function class_835(param1:class_730 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_1135 = param1;
         super();
         var_1135.var_94.y = 300;
         var_254 = 0;
         var_1136 = 0;
         var_1137 = new Sprite();
         var_1137.x = param1.x;
         var_1137.y = param1.y;
         var_1137.graphics.beginFill(16711680);
         var_1137.graphics.drawRect(-16,-32,32,36);
         class_652.var_214.var_267.var_232.method_124(var_1137,class_719.var_451);
         var_1135.mask = var_1137;
         new class_873(class_652.var_214.var_109,4,0,0.97);
      }
      
      override public function method_79() : void
      {
         var _loc2_:* = null as package_24.class_739;
         var _loc3_:class_931 = null;
         super.method_79();
         var_254 = Number(Math.min(Number(var_254 + 0.01),1));
         var _loc1_:Number = 1 - Math.pow(var_254,2.5);
         var_1135.var_94.y = int(_loc1_ * 22);
         var_1136 = Number(var_1136 + _loc1_);
         while(var_1136 > 0)
         {
            var_1136 = var_1136 - 1;
            _loc2_ = new package_24.class_739();
            _loc2_.method_128(class_702.package_9.method_98(int(class_691.method_114(7)),"dirt_stones"));
            _loc2_.var_262 = (Math.random() * 2 - 1) * 6;
            _loc2_.var_263 = -int(class_691.method_114(24));
            _loc2_.method_118();
            _loc2_.var_278 = (Math.random() * 2 - 1) * 0.5;
            _loc2_.var_279 = -(1 + Math.random() * 2);
            _loc2_.var_250 = Number(0.2 + Math.random() * 0.1);
            _loc2_.var_251 = 10 + int(class_691.method_114(20));
            _loc2_.var_233 = 0.95;
            var_1135.var_94.addChild(_loc2_);
         }
         if(var_254 == 1)
         {
            _loc3_ = new class_931(var_1135,0.05);
            _loc3_.var_146(8,4);
            method_89();
         }
      }
   }
}
