package package_9
{
   import package_11.package_12.class_657;
   import package_24.class_656;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_931;
   
   public class class_932 extends class_647
   {
       
      
      public var package_29:class_756;
      
      public var var_802:class_656;
      
      public function class_932(param1:class_756 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         package_29 = param1;
         super();
         var_802 = new class_656();
         var_802.method_483(0,0);
         var_802.method_137(class_702.var_572.method_136("medusa"),false);
         class_905.var_243.addChild(var_802);
         var_802.scaleY = Number(8);
         var_802.scaleX = 8;
         var_254 = 0;
      }
      
      override public function method_79() : void
      {
         var _loc2_:class_931 = null;
         super.method_79();
         var_254 = Number(Math.min(Number(var_254 + 0.05),1));
         var _loc1_:Number = Number(Math.pow(var_254,2));
         class_657.name_18(var_802,0,int(_loc1_ * 255));
         if(var_254 == 1)
         {
            var_802.method_89();
            method_89();
            class_652.var_214.method_208();
            _loc2_ = new class_931(class_652.var_214.var_109,0.05);
            _loc2_.method_123(2);
            _loc2_.method_1016();
         }
      }
   }
}
