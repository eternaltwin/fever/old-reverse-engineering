package
{
   import package_32.class_899;
   
   public class class_866 extends class_645
   {
       
      
      public var var_216:Number;
      
      public var var_218:Number;
      
      public var var_1282:class_621;
      
      public var var_1283:Number;
      
      public var var_1286:class_624;
      
      public var var_1284:int;
      
      public var var_1285:class_619;
      
      public function class_866()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var_1282.y = Number(var_1282.y + var_1283);
         if(Number(var_1282.y + var_1282.height / 2) >= 400)
         {
            var_1283 = var_1283 * -1;
         }
         if(var_1282.y - var_1282.height / 2 <= 0)
         {
            var_1283 = var_1283 * -1;
         }
         if(!!name_5 && var_1284 == 1 && var_1285.currentFrame <= 5)
         {
            var_1285.gotoAndPlay(2);
            var_1284 = 2;
         }
         if(!name_5 && var_1284 == 2)
         {
            var_1285.gotoAndPlay("boing");
            var_1284 = 1;
         }
         if(var_1285.currentFrame == 11 && Number(var_1282.y + var_1282.height / 2) > var_1285.y && var_1282.y - var_1282.height / 2 < var_1285.y)
         {
            var_1285.gotoAndPlay("tchak");
            var_1283 = 0;
            method_81(true,10);
         }
         if(var_1285.currentFrame == 1)
         {
            var_1285.y = mouseY;
         }
         super.method_79();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [200];
         super.method_95(param1);
         var_218 = 400;
         var_216 = 400;
         var_1286 = new class_624();
         addChild(var_1286);
         var_1282 = new class_621();
         addChild(var_1282);
         var_1282.scaleX = 1 - param1 / 1.8;
         var_1282.scaleY = var_1282.scaleX;
         var_1282.x = 380;
         var_1282.y = Number(200 + Math.random() * 100);
         var_1283 = int(class_691.method_114(2)) * 2 - 1;
         var_1283 = 50 * (param1 / 1.5);
         var_1285 = new class_619();
         addChild(var_1285);
         var_1285.x = 55;
         var_1285.y = 200;
         var_1284 = 1;
      }
   }
}
