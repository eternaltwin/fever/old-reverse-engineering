package
{
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.geom.Rectangle;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_810 extends class_645
   {
      
      public static var var_1042:int = 246;
      
      public static var var_1043:int = 390;
       
      
      public var var_1044:class_795;
      
      public var var_555:class_892;
      
      public var var_1046:Boolean;
      
      public var var_446:Array;
      
      public var var_363:MovieClip;
      
      public var var_1052:class_892;
      
      public var var_27:Array;
      
      public var var_160:class_795;
      
      public var var_777:class_755;
      
      public var var_1049:Array;
      
      public var var_1054:Array;
      
      public var var_1053:Array;
      
      public var var_1047:MovieClip;
      
      public var var_1048:Rectangle;
      
      public var var_1045:Array;
      
      public var var_1050:Array;
      
      public var var_1051:Array;
      
      public function class_810()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc3_:class_892 = null;
         var _loc4_:* = null;
         var _loc5_:MovieClip = null;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:int = 0;
         var _loc11_:MovieClip = null;
         var _loc12_:Boolean = false;
         var _loc13_:Rectangle = null;
         var _loc14_:class_892 = null;
         super.method_79();
         _loc1_ = 0;
         var _loc2_:Array = var_446;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.method_79();
         }
         _loc1_ = 0;
         _loc2_ = var_1045;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.var_245 > class_742.var_218 + 50)
            {
               _loc3_.method_89();
               if(_loc3_.var_243.parent != null)
               {
                  _loc3_.var_243.parent.removeChild(_loc3_.var_243);
               }
               var_1045.method_116(_loc3_);
               method_81(false,20);
            }
            else if(!_loc3_.name_97)
            {
               if(name_5)
               {
                  _loc4_ = method_100();
                  var_363.graphics.clear();
                  _loc5_ = Reflect.field(_loc3_.var_243,class_899.__unprotect__("#\x16\x16\x01"));
                  _loc5_.visible = true;
                  _loc6_ = Number(_loc3_.method_115(_loc4_));
                  _loc7_ = 10;
                  _loc8_ = (_loc6_ - _loc7_) / _loc7_;
                  _loc9_ = Number(_loc3_.method_231(_loc4_));
                  _loc10_ = 1;
                  _loc3_.name_98 = Math.cos(_loc9_) * _loc10_ * _loc8_;
                  _loc3_.name_99 = Math.sin(_loc9_) * _loc10_ * _loc8_;
                  var_363.graphics.lineStyle(1,16777215,70);
                  var_363.graphics.moveTo(Number(_loc4_.var_244),Number(_loc4_.var_245));
                  var_363.graphics.lineTo(_loc3_.var_244,_loc3_.var_245 - _loc3_.var_243.height / 2);
               }
               _loc3_.method_79();
            }
            else
            {
               if(_loc3_.name_100)
               {
                  if(!_loc3_.name_101)
                  {
                     var_363.graphics.clear();
                     var_1046 = false;
                     _loc3_.name_101 = true;
                  }
                  _loc5_ = Reflect.field(_loc3_.var_243,class_899.__unprotect__("#\x16\x16\x01"));
                  _loc5_.visible = false;
                  _loc11_ = _loc3_.var_243.var_1;
                  _loc5_ = _loc11_;
                  if(_loc5_.currentFrame != 2)
                  {
                     _loc5_.gotoAndStop(2);
                  }
                  _loc12_ = false;
                  _loc6_ = var_1047.y - var_1047.var_185.height;
                  _loc13_ = new Rectangle(_loc3_.var_243.x - 23,_loc3_.var_243.y,Number(_loc3_.name_102),1);
                  if(_loc13_.y >= var_1048.y)
                  {
                     if(_loc13_.x > var_1048.x)
                     {
                        if(_loc13_.right < var_1048.right)
                        {
                           _loc3_.var_278 = 0;
                           _loc3_.var_279 = 0;
                           _loc3_.var_250 = 0;
                           _loc3_.var_580 = 0;
                           _loc3_.name_103 = _loc3_.var_243.rotation;
                           _loc5_.gotoAndStop(1);
                           var_1047.name_104.push(_loc3_);
                           var_1047.y = Number(var_1047.y + 1);
                           var_1045.method_116(_loc3_);
                           continue;
                        }
                     }
                  }
               }
               if(_loc3_.name_100)
               {
                  _loc3_.method_79();
               }
               else
               {
                  _loc3_.var_244 = _loc3_.var_244 - var_1049[0];
                  _loc3_.name_28 = int(_loc3_.name_28) + int(var_1049[0]);
                  if(int(int(_loc3_.name_28) % 2) == 0)
                  {
                     _loc5_ = _loc3_.var_243.var_1;
                     _loc5_ = _loc3_.var_243.var_1;
                     _loc5_.gotoAndStop(_loc5_.currentFrame == 1?2:1);
                  }
                  if(_loc3_.var_244 < class_810.var_1042 && !_loc3_.name_105)
                  {
                     _loc3_.var_250 = _loc3_.var_243.currentFrame / 10;
                     _loc3_.var_580 = -2;
                     _loc3_.name_105 = true;
                     Reflect.method_740(_loc3_.var_243,"onRelease");
                     Reflect.method_740(_loc3_.var_243,"onReleaseOutside");
                  }
                  if(_loc3_.name_105 && _loc3_.var_245 > class_810.var_1043)
                  {
                     _loc3_.var_580 = 0;
                     if(_loc3_ != null)
                     {
                        _loc5_ = var_232.method_84(class_899.__unprotect__("4t6\x03\x03"),4);
                        _loc5_.gotoAndStop(int(class_691.method_114(_loc5_.totalFrames)) + 1);
                        _loc14_ = new class_892(_loc5_);
                        _loc14_.var_245 = class_810.var_1043 + 7;
                        _loc14_.var_244 = Number(_loc3_.var_244 + (int(class_691.method_114(2)) == 0?int(class_691.method_114(_loc3_.var_243.currentFrame)):-int(class_691.method_114(_loc3_.var_243.currentFrame))));
                        _loc14_.var_251 = 20;
                        _loc14_.var_344 = 100 * _loc3_.var_243.currentFrame * 0.01;
                        _loc14_.var_272 = 0;
                        var_446.push(_loc14_);
                     }
                  }
                  _loc3_.method_79();
               }
            }
         }
         _loc4_ = var_160.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc5_.x = Number(_loc5_.x + Number(_loc5_.var_351));
         }
         _loc4_ = var_1044.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc5_.var_343 = Number(Number(_loc5_.var_343) + 0.05);
            _loc5_.rotation = 1.5 * Math.sin(Number(_loc5_.var_343));
         }
         var_1047.var_343 = Number(var_1047.var_343 + 0.03);
         var_1047.rotation = Number(Math.sin(var_1047.var_343));
         _loc1_ = 0;
         _loc2_ = var_1047.name_104;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc5_ = _loc3_.var_243.var_1;
            _loc5_.gotoAndStop(1);
            if(_loc3_.var_245 < var_1047.y + _loc3_.var_243.height - 20)
            {
               _loc3_.var_245 = Number(_loc3_.var_245 + 3);
            }
            else
            {
               _loc5_ = _loc3_.var_243.var_1;
               _loc5_.gotoAndStop(3);
            }
            _loc3_.var_243.rotation = Number(var_1047.rotation + Number(_loc3_.name_103));
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = var_1050[0];
               var_1050 = [var_1050[0] - 1];
               if(_loc1_ <= 0)
               {
                  _loc5_ = var_232.method_84(class_899.__unprotect__("Et6+\x01"),2);
                  _loc5_.gotoAndStop(int(class_691.method_114(_loc5_.totalFrames)) + 1);
                  _loc11_ = _loc5_.var_1;
                  _loc11_.gotoAndStop(1);
                  _loc5_.x = Number(class_742.var_216 + _loc5_.width * 1.5);
                  _loc5_.y = 270;
                  var var_64:class_892 = new class_892(_loc5_);
                  var_64.method_118();
                  var_64.name_102 = _loc5_.width - 10;
                  var_64.name_97 = true;
                  var_64.name_100 = false;
                  var_64.name_28 = 0;
                  var_64.name_101 = false;
                  var var_214:class_810 = this;
                  var_64.var_243.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:*):void
                  {
                     var_214.method_741(var_64);
                  });
                  var_64.var_243.var_183.name_63 = false;
                  var_1045.push(var_64);
                  var_1050 = [var_1051[0]];
                  break;
               }
         }
         if(var_555 != null && !name_5)
         {
            method_742(var_555);
            var_555 = null;
         }
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_742(param1:class_892) : void
      {
         var _loc3_:MovieClip = null;
         if(Number(param1.name_98) >= 0 || Number(param1.name_99) >= 0)
         {
            var_363.graphics.clear();
            var_1046 = false;
            param1.name_101 = false;
            param1.name_97 = true;
            _loc3_ = Reflect.field(param1.var_243,class_899.__unprotect__("#\x16\x16\x01"));
            _loc3_.visible = false;
            return;
         }
         param1.name_100 = true;
         param1.name_97 = true;
         param1.var_278 = Number(param1.name_98);
         param1.var_279 = Number(param1.name_99);
         param1.var_250 = -param1.name_99 / 20;
      }
      
      public function method_741(param1:class_892) : void
      {
         if(var_1046)
         {
            return;
         }
         if(!param1.name_97)
         {
            return;
         }
         if(param1.name_100)
         {
            return;
         }
         if(param1.name_105)
         {
            return;
         }
         param1.name_97 = false;
         param1.var_278 = 0;
         param1.var_279 = 0;
         var _loc2_:MovieClip = param1.var_243.var_1;
         _loc2_.gotoAndStop(1);
         var_555 = param1;
         var_1046 = true;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         var_1051 = [int(60)];
         var_1049 = [Number(1 + param1)];
         var_1050 = [0];
         super.method_95(param1);
         var_1046 = false;
         var_27 = [(int(class_691.method_114(2)) == 0?-1:1) * 1.2];
         var_160 = new class_795();
         var_1044 = new class_795();
         var_1045 = [];
         var_446 = [];
         method_117();
         name_3 = 1;
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var _loc6_:Number = NaN;
         var _loc8_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("T\x1af_"),0);
         var _loc1_:int = int(class_691.method_114(6)) + 1;
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = 10 + int(class_691.method_114(40));
            _loc5_ = var_232.method_84(class_899.__unprotect__("\x01|kK\x03"),0);
            _loc5_.y = 30 + int(class_691.method_114(int(class_742.var_218 - 80)));
            _loc5_.x = int(class_691.method_114(int(class_742.var_216)));
            _loc5_.scaleY = _loc4_ * 0.01;
            _loc5_.scaleX = 1 - _loc4_ * 0.01;
            _loc6_ = var_27[0] * _loc4_ / 100;
            _loc5_.var_351 = _loc6_;
            _loc5_.alpha = 1 - (0.5 - _loc6_);
            var_160.method_103(_loc5_);
         }
         _loc2_ = 0;
         while(_loc2_ < 4)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc5_ = var_232.method_84(class_899.__unprotect__("\x01RrU\x02"),1);
            _loc5_.x = 200;
            _loc5_.y = 409;
            _loc5_.var_343 = int(class_691.method_114(100));
            _loc5_.alpha = _loc5_.var_343 * 0.01;
            var_1044.method_103(_loc5_);
         }
         _loc5_ = var_232.method_84(class_899.__unprotect__("KFk8"),1);
         _loc5_.x = 320;
         _loc5_.y = 294;
         var_1047 = var_232.method_84(class_899.__unprotect__("E2@q\x03"),3);
         var_1047.x = 124;
         var_1047.y = 365;
         var_1047.var_343 = int(class_691.method_114(100));
         _loc6_ = var_1047.var_185.width;
         var_1047.var_185.scaleX = Number(Math.min(1,1 - var_237[0] * 0.8));
         var_1047.var_84.x = var_1047.var_84.x - (_loc6_ - var_1047.var_185.width);
         var_1053 = [Number(var_1047.x + var_1047.var_185.x)];
         var_1054 = [Number(var_1047.x + var_1047.var_84.x)];
         var _loc7_:Number = var_1047.y;
         var_1048 = new Rectangle(Number(var_1047.x - 119 + Number(Math.abs(var_1047.var_185.x - var_1047.var_184.x))),_loc7_,Number(var_1047.var_185.width + 10),20);
         var_1047.name_104 = [];
         var_363 = var_232.method_92(2);
         _loc2_ = 0;
         while(_loc2_ < 5)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc8_ = var_232.method_84(class_899.__unprotect__("\x01RrU\x02"),4);
            _loc8_.x = 200;
            _loc8_.y = 409;
            _loc8_.var_343 = int(class_691.method_114(100));
            _loc8_.alpha = _loc8_.var_343 * 0.01;
            var_1044.method_103(_loc8_);
         }
      }
   }
}
