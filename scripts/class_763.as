package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_763 extends class_645
   {
       
      
      public var var_280:class_656;
      
      public var var_609:int;
      
      public var var_819:Number;
      
      public var var_351:Number;
      
      public var var_818:class_656;
      
      public var var_824:int;
      
      public var var_823:Object;
      
      public var var_821:Array;
      
      public var var_820:Array;
      
      public var var_825:int;
      
      public var var_822:Boolean;
      
      public var var_700:Number;
      
      public function class_763()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_570() : void
      {
         var_280.var_244 = Number(var_818.var_244 + Math.cos(var_700 / 100) * var_609);
         var_280.var_245 = Number(var_818.var_245 + Math.sin(var_700 / 100) * var_609);
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:* = null;
         var _loc5_:class_892 = null;
         var _loc6_:Number = NaN;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_819 = (var_819 + 10) % 628;
               _loc1_ = Number(var_351 + Math.cos(var_819 / 100) * var_351 * 0.5);
               var_700 = (var_700 + _loc1_) % 628;
               method_570();
               var_280.var_243.rotation = Number(var_280.var_243.rotation + 50 / _loc1_);
               _loc2_ = 0;
               _loc3_ = var_820;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  if(_loc4_.var_37 != null)
                  {
                     _loc4_.var_37 = _loc4_.var_37 - 1;
                     if(_loc4_.var_37 < 0)
                     {
                        method_571(_loc4_);
                     }
                  }
               }
               _loc3_ = var_821.method_78();
               _loc2_ = 0;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc5_ = _loc3_[_loc2_];
                  _loc2_++;
                  _loc6_ = Number(_loc5_.method_115(var_280));
                  if(_loc6_ < 10 && !var_822)
                  {
                     method_411(var_280.var_244,var_280.var_245);
                     method_81(true,20);
                     _loc5_.method_89();
                     var_280.method_89();
                     var_821.method_116(_loc5_);
                     var_822 = true;
                  }
               }
               var_823 = {
                  "L\x01":var_280.var_244,
                  "\x03\x01":var_280.var_245
               };
         }
      }
      
      public function method_571(param1:Object) : void
      {
         param1.var_631.mouseEnabled = true;
         param1.var_37 = null;
         param1.var_631.gotoAndPlay("2");
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_822 = false;
         var_825 = 0;
         var_351 = Number(4 + param1 * 10);
         var_819 = 0;
         var_700 = int(class_691.method_114(328));
         var_824 = 50;
         var_609 = 108;
         var_821 = [];
         method_117();
         method_72();
      }
      
      public function method_572(param1:Object) : void
      {
         param1.var_37 = Number(10 + var_237[0] * 100);
         param1.var_631.gotoAndStop("1");
         param1.var_631.mouseEnabled = false;
         var _loc3_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("@\x15]$"),class_645.var_209));
         _loc3_.var_233 = 0.99;
         var _loc2_:class_892 = _loc3_;
         var _loc4_:Number = Number(Math.cos(Number(param1.var_65)));
         var _loc5_:Number = Number(Math.sin(Number(param1.var_65)));
         var _loc6_:int = var_824 + 10;
         _loc2_.var_244 = Number(param1.var_631.x + _loc4_ * _loc6_);
         _loc2_.var_245 = Number(param1.var_631.y + _loc5_ * _loc6_);
         _loc2_.var_278 = _loc4_ * 6;
         _loc2_.var_279 = _loc5_ * 6;
         _loc2_.var_243.rotation = param1.var_65 / 0.01714;
         _loc2_.method_118();
         var_821.push(_loc2_);
         var_825 = var_825 + 1;
      }
      
      public function method_411(param1:Number, param2:Number) : void
      {
         var _loc6_:int = 0;
         var _loc7_:class_892 = null;
         var _loc8_:class_892 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:int = 0;
         var _loc3_:Number = Number(var_280.method_115(var_823));
         var _loc4_:Number = Number(var_280.method_231(var_823));
         var _loc5_:int = 0;
         while(_loc5_ < 12)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc8_ = new class_892(var_232.method_84(class_899.__unprotect__("f\x139Q"),class_645.var_209));
            _loc8_.var_233 = 0.99;
            _loc7_ = _loc8_;
            _loc9_ = int(class_691.method_114(628)) / 100;
            _loc10_ = Number(Math.cos(_loc9_));
            _loc11_ = Number(Math.sin(_loc9_));
            _loc12_ = Number(0.5 + int(class_691.method_114(30)) * 0.1);
            _loc13_ = 50 + int(class_691.method_114(100));
            _loc7_.var_244 = Number(param1 + _loc10_ * _loc12_ * 1.5);
            _loc7_.var_245 = Number(param2 + _loc11_ * _loc12_ * 1.5);
            _loc7_.var_278 = _loc10_ * _loc12_ - Math.cos(_loc4_) * _loc3_ * 0.15;
            _loc7_.var_279 = _loc11_ * _loc12_ - Math.sin(_loc4_) * _loc3_ * 0.15;
            _loc7_.var_580 = Math.random() * 20;
            _loc7_.method_118();
            _loc7_.var_243.gotoAndStop(int(class_691.method_114(_loc7_.var_243.totalFrames)) + 1);
            _loc7_.var_243.scaleX = _loc13_ * 0.01;
            _loc7_.var_243.scaleX = _loc13_ * 0.01;
         }
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:MovieClip = null;
         var _loc7_:Number = NaN;
         var _loc8_:class_656 = null;
         var _loc9_:int = 0;
         var _loc10_:class_656 = null;
         var _loc11_:Boolean = false;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("$\x11N\x06\x03"),0);
         class_319.cacheAsBitmap = true;
         var _loc1_:class_755 = new class_755(class_319);
         var _loc3_:int = 0;
         while(_loc3_ < 300)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = Number(Math.pow(_loc4_ / 300,4));
            _loc6_ = _loc1_.method_84(class_899.__unprotect__("\x07%#\x1d\x01"),0);
            _loc6_.x = Math.random() * class_742.var_217;
            _loc6_.y = Math.random() * class_742.var_219;
            _loc6_.alpha = Number(0.5 + _loc5_ * 0.5);
            _loc7_ = Number(0.5 + _loc5_ * 0.5);
            _loc6_.scaleY = _loc7_;
            _loc6_.scaleX = _loc7_;
            _loc6_.blendMode = BlendMode.ADD;
         }
         _loc8_ = new class_656(var_232.method_84(class_899.__unprotect__("\x0ei9\x15\x02"),class_645.var_209));
         var_818 = _loc8_;
         var_818.var_244 = class_742.var_217 * 0.5;
         var_818.var_245 = class_742.var_219 * 0.5;
         var_818.var_243.scaleX = var_824 * 0.02;
         var_818.var_243.scaleY = var_818.var_243.scaleX;
         var_818.method_118();
         _loc8_ = new class_656(var_232.method_84(class_899.__unprotect__("^cSV"),class_645.var_209));
         var_280 = _loc8_;
         method_570();
         var_280.method_118();
         var_820 = [];
         _loc3_ = int(Number(Math.max(1,int(Math.round(6 - var_237[0] * 5)))));
         _loc4_ = 0;
         while(_loc4_ < _loc3_)
         {
            _loc4_++;
            _loc9_ = _loc4_;
            _loc10_ = new class_656(var_232.method_84(class_899.__unprotect__("U4{g\x03"),class_645.var_209));
            _loc8_ = _loc10_;
            _loc8_.var_244 = var_818.var_244;
            _loc8_.var_245 = var_818.var_245;
            _loc5_ = 0;
            do
            {
               _loc5_ = int(class_691.method_114(628)) / 100;
               _loc11_ = true;
               _loc12_ = 0;
               _loc13_ = int(var_820.length);
               while(_loc12_ < _loc13_)
               {
                  _loc12_++;
                  _loc14_ = _loc12_;
                  if(Number(Math.abs(var_820[_loc14_].var_65 - _loc5_)) < 0.2)
                  {
                     _loc11_ = false;
                  }
               }
            }
            while(!_loc11_);
            
            _loc8_.var_243.rotation = _loc5_ / 0.0174;
            _loc8_.var_243.stop();
            _loc8_.method_118();
            var var_159:Array = [{
               "gB\x01":_loc8_.var_243,
               "@\x01":_loc5_,
               "j\x01":_loc9_ * 4
            }];
            var var_214:Array = [this];
            _loc8_.var_243.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_159:Array = param2;
               return function(param1:*):void
               {
                  var_214[0].method_572(var_159[0]);
               };
            }(var_214,var_159));
            var_820.push(var_159[0]);
         }
      }
   }
}
