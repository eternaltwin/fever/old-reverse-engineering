package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_743 extends class_645
   {
      
      public static var var_370:Boolean;
      
      public static var var_350:int;
      
      public static var var_749:Number = 0.5;
      
      public static var var_574:int = 20;
      
      public static var var_750:int = 10;
       
      
      public var var_751:Number;
      
      public var var_752:Array;
      
      public var var_753:MovieClip;
      
      public var var_754:MovieClip;
      
      public function class_743()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_490();
               _loc1_ = var_751;
               var_751 = var_751 + 1;
               if(_loc1_ > 15)
               {
                  method_491();
                  break;
               }
               break;
            case 2:
               method_491();
         }
         super.method_79();
      }
      
      public function method_492(param1:MovieClip) : Boolean
      {
         var _loc4_:MovieClip = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_752;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(Number(Math.abs(_loc4_.x - param1.x)) < 5)
            {
               return true;
            }
         }
         return false;
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_491() : void
      {
         var _loc4_:MovieClip = null;
         var _loc5_:Number = NaN;
         var _loc1_:Boolean = true;
         var _loc2_:int = 0;
         var _loc3_:Array = var_752;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            switch(int(_loc4_.name_3))
            {
               case 0:
                  _loc4_.var_37 = Number(_loc4_.var_37) - 1;
                  if(Number(_loc4_.var_37) < 0)
                  {
                     _loc4_.y = _loc4_.y - class_743.var_574 * class_743.var_749;
                     if(_loc4_.y < -100)
                     {
                        _loc5_ = class_742.var_217 * 0.5;
                        _loc4_.x = Number(_loc5_ + (_loc4_.x - _loc5_) / class_743.var_749);
                        _loc4_.scaleX = 1;
                        _loc4_.scaleY = 1;
                        _loc4_.name_3 = 1;
                        var_232.method_110(_loc4_);
                     }
                  }
                  _loc1_ = false;
                  continue;
               case 1:
                  _loc4_.y = Number(_loc4_.y + class_743.var_574);
                  if(_loc4_.y > class_743.var_350)
                  {
                     _loc4_.y = class_743.var_350;
                     _loc4_.name_3 = 2;
                     _loc4_.gotoAndStop("2");
                     _loc5_ = Number(Math.abs(_loc4_.x - var_754.x));
                     if(_loc5_ < class_743.var_750)
                     {
                        _loc4_.y = Number(_loc4_.y + _loc5_ * 0.75);
                        name_3 = 2;
                        var_754.y = Number(var_754.y + 4);
                        var_754.gotoAndPlay("2");
                        method_81(false,24);
                        _loc4_.gotoAndStop(3);
                     }
                     else
                     {
                        _loc4_.rotation = Number(5 + Math.random() * 2);
                     }
                  }
                  _loc1_ = false;
                  continue;
               case 2:
                  _loc4_.rotation = -_loc4_.rotation * 0.92;
                  if(Number(Math.abs(_loc4_.rotation)) > 2)
                  {
                     _loc1_ = false;
                  }

            }
         }
         if(_loc1_)
         {
            method_81(true);
         }
      }
      
      public function method_490() : void
      {
         var _loc6_:MovieClip = null;
         var _loc1_:Number = var_754.x;
         var _loc2_:Number = method_100().var_244 - var_754.x;
         var_754.x = Number(var_754.x + _loc2_ * 0.15);
         var_754.rotation = _loc2_ * 0.3;
         var _loc4_:int = 0;
         var _loc5_:Array = var_752;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            if(int(_loc6_.name_3) == 2)
            {
               if(_loc6_.x > var_754.x - class_743.var_750 && _loc6_.x - 1 < _loc1_ - class_743.var_750)
               {
                  var_754.x = Number(_loc6_.x + class_743.var_750);
               }
               if(_loc6_.x < Number(var_754.x + class_743.var_750) && Number(_loc6_.x + 1) > Number(_loc1_ + class_743.var_750))
               {
                  var_754.x = _loc6_.x - class_743.var_750;
               }
            }
         }
         var_754.x = Number(class_663.method_99(class_743.var_750,var_754.x,class_742.var_217 - class_743.var_750));
         var_753.x = var_754.x;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [Number(100 + param1 * 20)];
         super.method_95(param1);
         var_751 = 0;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc7_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("IKC\'\x01"),0);
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("b\t\r}"),class_645.var_209);
         var_752 = [];
         var _loc2_:Number = class_742.var_217 * (1 - class_743.var_749) * 0.5;
         var _loc3_:int = 3 + int(var_237[0] * 10);
         var _loc4_:int = 0;
         loop0:
         while(_loc4_ < _loc3_)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = var_232.method_84(class_899.__unprotect__("\x03/,0\x02"),class_645.var_209);
            _loc7_ = 0;
            while(true)
            {
               _loc6_.x = Number(_loc2_ + Math.random() * (class_742.var_217 - 2 * _loc2_));
               _loc7_++;
               if(_loc7_ == 200)
               {
                  break;
               }
               if(!method_492(_loc6_))
               {
                  _loc6_.y = class_742.var_219;
                  _loc6_.var_37 = _loc5_ * var_237[0] * 3;
                  _loc6_.name_3 = 0;
                  _loc6_.scaleX = class_743.var_749;
                  _loc6_.scaleY = -class_743.var_749;
                  _loc6_.stop();
                  var_752.push(_loc6_);
                  continue loop0;
               }
            }
            _loc6_.parent.removeChild(_loc6_);
            return;
         }
         var_232.method_110(_loc1_);
         var_753 = var_232.method_84(class_899.__unprotect__("\x03\\$(\x01"),class_645.var_209);
         var_753.x = class_742.var_217 * 0.5;
         var_753.y = class_743.var_350;
         var_754 = var_232.method_84(class_899.__unprotect__("\x04pen"),class_645.var_209);
         var_754.x = class_742.var_217 * 0.5;
         var_754.y = class_743.var_350 - 14;
         var_754.stop();
      }
   }
}
