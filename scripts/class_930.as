package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_930 extends class_645
   {
      
      public static var var_493:int = 11;
      
      public static var var_1520:int = 15;
      
      public static var var_1521:int = 13;
       
      
      public var var_351:Number;
      
      public var var_531:MovieClip;
      
      public var var_1523:Number;
      
      public var var_562:Number;
      
      public var var_1522:Number;
      
      public var var_640:Array;
      
      public var var_714:Number;
      
      public var var_488:Array;
      
      public var class_318:class_892;
      
      public function class_930()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_1124() : void
      {
         var _loc4_:MovieClip = null;
         var _loc5_:Number = NaN;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:class_892 = null;
         var _loc1_:Boolean = true;
         var _loc2_:Array = var_488.method_78();
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            _loc5_ = Number(class_318.method_115({
               "L\x01":_loc4_.x,
               "\x03\x01":_loc4_.y
            }));
            _loc4_.name_21 = (Number(_loc4_.name_21) + 40) % 628;
            _loc4_.var_67.rotation = Number(_loc4_.var_67.rotation + 1.5);
            _loc4_.var_66.rotation = _loc4_.var_66.rotation + 1;
            if(_loc4_.name_150)
            {
               _loc4_.scaleX = _loc4_.scaleX * 0.5;
               _loc4_.scaleY = _loc4_.scaleX;
               if(_loc4_.scaleX < 0.02)
               {
                  _loc4_.parent.removeChild(_loc4_);
                  var_488.method_116(_loc4_);
               }
            }
            else
            {
               if(_loc5_ < class_930.var_1520 + class_930.var_493)
               {
                  _loc4_.name_150 = true;
                  _loc4_.var_67.visible = false;
                  _loc6_ = 0;
                  while(_loc6_ < 8)
                  {
                     _loc6_++;
                     _loc7_ = _loc6_;
                     _loc9_ = new class_892(var_232.method_84(class_899.__unprotect__("\x15}QD\x01"),class_645.var_209));
                     _loc9_.var_233 = 0.99;
                     _loc8_ = _loc9_;
                     _loc8_.var_244 = _loc4_.x;
                     _loc8_.var_245 = _loc4_.y;
                     _loc8_.var_580 = Number(0.5 + Math.random() * 4);
                     _loc8_.var_272 = 3;
                     _loc8_.var_251 = 10 + int(class_691.method_114(10));
                     _loc8_.method_118();
                     _loc8_.var_243.rotation = Math.random() * 360;
                     _loc8_.var_243.scaleX = Number(0.6 + Math.random() * 0.8);
                  }
               }
               _loc1_ = false;
            }
         }
         if(_loc1_)
         {
            var_229 = [true];
         }
      }
      
      public function method_1125() : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         var _loc1_:Number = var_201.mouseX - class_318.var_244;
         var_351 = Number(var_351 + _loc1_ * 0.01 * var_1522);
         var_351 = var_351 * 0.9;
         class_318.var_243.rotation = Number(class_318.var_243.rotation + var_351 * 8);
         var_1522 = Number(Math.min((var_1522 + 0.003) * 1.008,1));
         if(Number(class_318.var_245 + class_930.var_493) > var_714)
         {
            class_318.var_245 = var_714 - class_930.var_493;
            class_318.var_279 = -var_562 * var_1522;
            class_318.var_278 = var_351;
         }
         if(class_318.var_244 < class_930.var_493 || class_318.var_244 > class_742.var_217 * 2 - class_930.var_493)
         {
            class_318.var_244 = Number(class_663.method_99(class_930.var_493,class_318.var_244,class_742.var_217 * 2 - class_930.var_493));
            class_318.var_278 = class_318.var_278 * -0.8;
         }
         var _loc3_:String = "1";
         if(var_1522 < 0.5)
         {
            _loc3_ = "2";
            if(int(class_691.method_114(int(20 * var_1522))) == 0)
            {
               _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("\b[]\n\x03"),class_645.var_209));
               _loc5_.var_233 = 0.99;
               _loc4_ = _loc5_;
               _loc4_.var_244 = Number(class_318.var_244 + (Math.random() * 2 - 1) * class_930.var_493 * 0.8);
               _loc4_.var_245 = Number(class_318.var_245 + (Math.random() * 2 - 1) * class_930.var_493 * 0.8);
               _loc4_.var_250 = -(0.1 + Math.random() * 0.1);
               _loc4_.var_278 = (Math.random() * 2 - 1) * 1.5;
               _loc4_.method_118();
               _loc4_.var_243.rotation = Math.random() * 360;
               _loc4_.var_233 = 1;
            }
         }
         class_318.var_243.gotoAndStop(_loc3_);
         var _loc7_:MovieClip = Reflect.field(class_318.var_243,class_899.__unprotect__("\x04*[)"));
         var _loc6_:MovieClip = _loc7_;
         if(_loc6_ != null)
         {
            _loc6_.rotation = -class_318.var_243.rotation;
         }
         class_318.var_243.x = class_318.var_244;
         class_318.var_243.y = class_318.var_245;
         var_531.x = class_318.var_244;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Array = null;
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:Number = NaN;
         var _loc5_:class_892 = null;
         var _loc6_:class_892 = null;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1125();
               method_1124();
               _loc1_ = var_640.method_78();
               _loc2_ = 0;
               while(_loc2_ < int(_loc1_.length))
               {
                  _loc3_ = _loc1_[_loc2_];
                  _loc2_++;
                  _loc4_ = Number(class_318.method_115({
                     "L\x01":_loc3_.x,
                     "\x03\x01":_loc3_.y
                  }));
                  if(_loc4_ < class_930.var_1521 + class_930.var_493)
                  {
                     var_1522 = 0.01;
                     _loc6_ = new class_892(var_232.method_84(class_899.__unprotect__("=@Kj\x01"),class_645.var_209));
                     _loc6_.var_233 = 0.99;
                     _loc5_ = _loc6_;
                     _loc5_.var_244 = _loc3_.x;
                     _loc5_.var_245 = _loc3_.y;
                     _loc5_.method_118();
                     _loc3_.parent.removeChild(_loc3_);
                     var_640.method_116(_loc3_);
                  }
               }
               if(int(var_488.length) == 0)
               {
                  method_81(true,10);
               }
               _loc4_ = class_663.method_99(-class_742.var_217,-(class_318.var_244 - class_742.var_217 * 0.5),0) - var_1523;
               var_1523 = Number(var_1523 + _loc4_ * 0.1);
               var_201.x = var_1523 * class_742.var_216 / class_742.var_217;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [500 - int(param1 * 100)];
         super.method_95(param1);
         var_714 = class_742.var_219 - 6;
         var_351 = 0;
         var_562 = 20;
         var_1522 = 1;
         var_1523 = 0;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__(";M74\x02"),0);
         var_488 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = var_232.method_84(class_899.__unprotect__("\fe;o"),class_645.var_209);
            while(_loc3_.x == 0 || Number(Math.abs(_loc3_.x - class_742.var_217 * 0.5)) < 30)
            {
               _loc3_.x = Number(class_930.var_1520 + Math.random() * (class_742.var_217 * 2 - class_930.var_1520 * 2));
            }
            _loc3_.y = Number(class_930.var_1520 + Math.random() * 100);
            _loc3_.scaleX = 0.75;
            _loc3_.scaleY = 0.75;
            _loc3_.name_21 = Math.random() * 628;
            _loc3_.name_150 = false;
            var_488.push(_loc3_);
         }
         var_640 = [];
         _loc1_ = int(Math.floor(var_237[0] * 7));
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc3_ = var_232.method_84(class_899.__unprotect__("S\b\x10\r"),class_645.var_209);
            while(_loc3_.x == 0 || Number(Math.abs(_loc3_.x - class_742.var_217 * 0.5)) < 30)
            {
               _loc3_.x = Number(class_930.var_1521 + Math.random() * (class_742.var_217 * 2 - class_930.var_1521 * 2));
            }
            _loc3_.y = Number(class_930.var_1521 + Math.random() * (class_742.var_219 - class_930.var_1521 * 3));
            var_640.push(_loc3_);
         }
         var_531 = var_232.method_84(class_899.__unprotect__("iw%\t\x02"),class_645.var_209);
         var_531.x = class_742.var_217 * 0.5;
         var_531.y = var_714;
         var _loc5_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("n;\x02_\x03"),class_645.var_209));
         _loc5_.var_233 = 0.99;
         class_318 = _loc5_;
         class_318.var_244 = class_742.var_217 * 0.5;
         class_318.var_245 = class_742.var_219 * 0.5;
         class_318.var_250 = 1;
         class_318.method_118();
         class_318.var_243.stop();
         class_318.var_233 = 1;
      }
   }
}
