package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_796 extends class_645
   {
       
      
      public var var_977:int;
      
      public var var_971:Object;
      
      public var var_978:int;
      
      public var var_979:Number;
      
      public var var_981:int;
      
      public var var_975:Object;
      
      public var var_142:int;
      
      public var var_714:int;
      
      public var var_974:class_892;
      
      public var var_973:Boolean;
      
      public var var_984:MovieClip;
      
      public var var_337:Number;
      
      public var var_336:Number;
      
      public var var_101:MovieClip;
      
      public var var_983:int;
      
      public var var_976:class_656;
      
      public var var_985:Object;
      
      public var var_980:Number;
      
      public var var_972:class_892;
      
      public var var_982:Number;
      
      public function class_796()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_721();
               method_722();
               method_723();
               var_971 = {
                  "L\x01":var_972.var_244,
                  "\x03\x01":var_972.var_245
               };
               break;
            case 2:
               method_721();
               method_722();
               if(var_973)
               {
                  var_974.var_244 = var_972.var_244;
                  var_974.var_245 = var_972.var_245;
                  _loc1_ = -90 - var_974.var_243.rotation;
                  var_974.var_580 = Number(var_974.var_580 + _loc1_ * 0.01);
               }
               else
               {
                  method_724();
                  if(var_974.var_250 != null)
                  {
                     _loc1_ = Number(Math.atan2(var_974.var_279,var_974.var_278));
                     var_974.var_243.rotation = _loc1_ / 0.0174;
                     method_725();
                  }
               }
               if(var_975 != null)
               {
                  var_975 = var_975 - 1;
                  if(var_975 < 0)
                  {
                     method_81(false,30);
                     var_975 = 0;
                     break;
                  }
                  break;
               }
         }
         var_976.var_243.x = var_976.var_244;
         var_976.var_243.y = var_976.var_245;
         var_972.var_243.x = var_972.var_244;
         var_972.var_243.y = var_972.var_245;
      }
      
      public function method_722() : void
      {
         var _loc17_:Number = NaN;
         var _loc18_:int = 0;
         var _loc19_:Number = NaN;
         var _loc1_:Number = -1;
         if(name_5)
         {
            _loc1_ = -2.7;
         }
         var _loc2_:Number = _loc1_ - var_980;
         var_980 = Number(var_980 + _loc2_ * 0.2);
         var_976.var_243.rotation = var_980 / 0.0174;
         var _loc3_:* = {
            "L\x01":var_201.mouseX,
            "\x03\x01":mouseY
         };
         var_976.method_229(_loc3_,0.5,null);
         var _loc4_:Number = Number(method_726());
         var_976.var_243.graphics.clear();
         var_976.var_243.graphics.lineStyle(3,9136176,100);
         var_976.var_243.graphics.moveTo(var_981,0);
         var _loc5_:Number = Number(Math.cos(var_982) * _loc4_ + var_981);
         var _loc6_:Number = Math.sin(var_982) * _loc4_;
         var_976.var_243.graphics.curveTo(Number(var_981 + _loc4_ * 0.8),0,_loc5_,_loc6_);
         var _loc7_:Number = Number(var_976.var_244 + Math.cos(var_980) * var_981);
         var _loc8_:Number = Number(var_976.var_245 + Math.sin(var_980) * var_981);
         var _loc9_:Number = Number(_loc7_ + Math.cos(Number(var_980 + var_982)) * (var_983 - Math.abs(var_982) * 10));
         var _loc10_:Number = Number(_loc8_ + Math.sin(Number(var_980 + var_982)) * (var_983 - Math.abs(var_982) * 10));
         var _loc11_:Number = _loc9_ - var_972.var_244;
         var _loc12_:Number = _loc10_ - var_972.var_245;
         var _loc13_:Number = Number(Math.sqrt(Number(_loc11_ * _loc11_ + _loc12_ * _loc12_)));
         var _loc14_:Number = Number(Math.atan2(_loc12_,_loc11_));
         var _loc15_:* = null;
         var _loc16_:* = null;
         if(_loc13_ > var_977)
         {
            _loc17_ = (_loc13_ - var_977) / var_977;
            _loc18_ = 20;
            _loc16_ = {
               "@\x01":_loc14_,
               "a\x01":_loc17_ * _loc18_
            };
            var_972.var_278 = Number(var_972.var_278 + Math.cos(_loc14_) * _loc17_ * _loc18_);
            var_972.var_279 = Number(var_972.var_279 + Math.sin(_loc14_) * _loc17_ * _loc18_);
            _loc19_ = 0.2;
            if(_loc17_ > _loc19_)
            {
               var_972.var_244 = _loc9_ - Math.cos(_loc14_) * var_977 * (1 + _loc19_);
               var_972.var_245 = _loc10_ - Math.sin(_loc14_) * var_977 * (1 + _loc19_);
            }
         }
         else
         {
            _loc15_ = (var_977 - _loc13_) * 0.5;
         }
         var_972.var_278 = var_972.var_278 * 0.95;
         var_972.var_279 = var_972.var_279 * 0.95;
         if(_loc16_ != null)
         {
            _loc17_ = _loc16_.var_65 - var_980;
            _loc19_ = Math.sin(Number(_loc17_ + 3.14)) * _loc16_.var_38;
            var_982 = Number(var_982 + _loc19_ * 0.02 * (var_972.var_250 + (!!var_973?2:0)));
         }
         var_982 = var_982 * 0.9;
         var_984.graphics.clear();
         var_984.graphics.lineStyle(1,16777215,100);
         var_984.graphics.moveTo(_loc9_,_loc10_);
         if(_loc15_ == null)
         {
            var_984.graphics.lineTo(var_972.var_244,var_972.var_245);
         }
         else
         {
            _loc17_ = (var_972.var_244 + _loc9_) * 0.5;
            _loc19_ = Number((var_972.var_245 + _loc10_) * 0.5 + _loc15_);
            var_984.graphics.curveTo(_loc17_,_loc19_,var_972.var_244,var_972.var_245);
         }
      }
      
      public function method_721() : void
      {
         var _loc2_:Number = Number(var_985.var_475);
         var _loc3_:Number = class_742.var_217 * var_985.var_948 - var_974.var_244;
         var _loc4_:Number = class_742.var_219 * 0.5 - var_974.var_245;
         var _loc5_:Number = _loc3_ / 0.6 - var_201.x;
         var _loc6_:Number = _loc4_ / 0.6 - var_201.y;
         var _loc7_:Number = Number(var_985.var_475);
         var_201.x = Number(var_201.x + _loc5_ * _loc7_);
         var_201.y = Number(var_201.y + _loc6_ * _loc7_);
         var_201.x = Number(class_663.method_99(Number(var_985.name_22),var_201.x,Number(var_985.var_340)));
         var_201.y = Number(class_663.method_99(Number(var_985.name_23),var_201.y,Number(var_985.var_341)));
      }
      
      public function method_727() : void
      {
         name_3 = 2;
         var _loc1_:Number = Number(var_974.method_231(var_972));
         var _loc2_:Number = Number(var_974.method_115(var_972));
         var _loc3_:Number = Number(16 + _loc2_ * 0.02);
         var_974.var_278 = Number(var_974.var_278 + Math.cos(_loc1_) * _loc3_);
         var_974.var_279 = Number(var_974.var_279 + Math.sin(_loc1_) * _loc3_);
         var_974.var_250 = 1;
         var_974.var_243.gotoAndPlay("jump");
         var_985.name_23 = -200;
         var_985.var_341 = 0;
         var_985.var_948 = 0.5;
         var_985.var_475 = 0.2;
         var_985.var_340 = -var_974.var_244;
         var_229 = [true];
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [360];
         super.method_95(param1);
         var_981 = 30;
         var_983 = 80;
         var_977 = 80;
         var_142 = 700;
         var_714 = class_742.var_219 - 10;
         var_980 = -1.57;
         var_982 = 0;
         var_978 = 1000;
         var_979 = var_978;
         var_973 = false;
         var_985 = {
            "O6EU\x01":-9999.9,
            "^\'\x14^\x01":9999.9,
            "O\x15;\x03\x02":0,
            "~tj[\x01":0,
            "\x13\x06\x01":0.1,
            "\x05\x10\x01":1
         };
         method_117();
         var_971 = {
            "L\x01":var_972.var_244,
            "\x03\x01":var_972.var_245
         };
         method_72();
      }
      
      public function method_726() : Number
      {
         return var_983 - Math.abs(var_982) * 15;
      }
      
      public function method_724() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         var _loc1_:int = var_714;
         if(var_974.var_244 > var_142)
         {
            _loc1_ = _loc1_ + 120;
         }
         if(var_974.var_245 > _loc1_)
         {
            var_974.var_245 = _loc1_;
            var_974.var_250 = null;
            var_974.var_278 = 0;
            var_974.var_279 = 0;
            if(_loc1_ == var_714)
            {
               var_974.var_243.gotoAndStop("1");
               var_974.var_243.rotation = 0;
               method_81(false,20);
            }
            else
            {
               method_81(true,20);
               var_974.var_243.gotoAndPlay("impact");
               _loc2_ = 0;
               while(_loc2_ < 20)
               {
                  _loc2_++;
                  _loc3_ = _loc2_;
                  _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("Q\x03b2"),class_645.var_209));
                  _loc5_.var_233 = 0.99;
                  _loc4_ = _loc5_;
                  _loc4_.var_244 = var_974.var_244;
                  _loc4_.var_245 = var_974.var_245;
                  _loc4_.var_278 = 5 * (Math.random() * 2 - 1);
                  _loc4_.var_279 = -(3 + Math.random() * 6);
                  _loc4_.method_119(Number(0.3 + Math.random() * 0.6));
                  _loc4_.var_250 = 0.5;
                  _loc4_.method_118();
               }
            }
         }
      }
      
      public function method_723() : void
      {
         var _loc4_:int = 0;
         var_979 = Number(Math.min(Number(var_979 + 2),1000));
         var _loc1_:Number = Number(var_974.method_115(var_972));
         var _loc2_:Number = Number(var_972.method_115(var_971));
         var _loc3_:Number = Math.max(0,180 - _loc1_) / 180;
         var_979 = var_979 - _loc3_ * _loc2_ * 8;
         if(var_979 < 0)
         {
            method_727();
         }
         else
         {
            _loc4_ = 20 - int(Math.round(var_979 / var_978 * 10));
            var_974.var_243.gotoAndStop(_loc4_);
         }
         var _loc5_:Number = Number(var_974.method_231(var_972));
         var _loc6_:* = var_974.var_243;
         _loc6_.var_17.var_17.var_40.var_38.var_244 = 1.8 * (1 - _loc3_) * Math.cos(_loc5_);
         _loc6_.var_17.var_17.var_40.var_38.var_245 = 1.8 * (1 - _loc3_) * Math.sin(_loc5_);
      }
      
      public function method_725() : void
      {
         var _loc1_:Number = Number(var_974.method_115(var_972));
         if(_loc1_ < 20)
         {
            var_973 = true;
            var_972.var_278 = Number(var_972.var_278 + var_974.var_278);
            var_972.var_279 = Number(var_972.var_279 + var_974.var_279);
            var_972.var_243.visible = false;
            var_974.var_250 = null;
            var_974.var_278 = 0;
            var_974.var_279 = 0;
            var_974.var_580 = 0;
            var_974.var_243.gotoAndStop("eat");
            var_985.var_475 = 0;
            var_975 = 12;
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\x11C;(\x03"),0);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("I5U6\x03"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_974 = _loc1_;
         var_974.var_244 = var_142 - (Number(50 + var_237[0] * 250));
         var_974.var_245 = var_714;
         var_974.method_118();
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__(",DEl\x02"),class_645.var_209));
         var_976 = _loc2_;
         var_976.var_244 = class_742.var_217 * 0.5;
         var_976.var_245 = class_742.var_219 * 0.5;
         var_976.var_243.rotation = var_980 / 0.0174;
         var_976.method_118();
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("BCs0\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_972 = _loc1_;
         var_972.var_244 = class_742.var_217 - 0.5;
         var_972.var_245 = class_742.var_219 - 0.5;
         var_972.var_250 = 0.7;
         var_972.method_118();
         var_984 = var_232.method_92(class_645.var_209);
         var_101 = var_232.method_84(class_899.__unprotect__("h\\\x11P"),class_645.var_209);
         var_101.y = var_714;
      }
   }
}
