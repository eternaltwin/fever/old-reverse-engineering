package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_674 extends class_645
   {
       
      
      public var var_280:class_656;
      
      public var var_251:Object;
      
      public var var_442:class_656;
      
      public var var_66:Array;
      
      public var var_441:class_656;
      
      public var var_440:Object;
      
      public function class_674()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = method_100();
               var_440.var_244 = Number(Number(var_440.var_244) + Number(var_440.var_278));
               var_440.var_245 = Number(Number(var_440.var_245) + Number(var_440.var_279));
               if(Number(var_440.var_244) < 0 || Number(var_440.var_244) > class_742.var_217)
               {
                  var_440.var_278 = var_440.var_278 * -1;
                  var_440.var_244 = Number(Math.min(Number(Math.max(0,Number(var_440.var_244))),class_742.var_217));
               }
               if(Number(var_440.var_245) < 0 || Number(var_440.var_245) > class_742.var_219)
               {
                  var_440.var_279 = var_440.var_279 * -1;
                  var_440.var_245 = Number(Math.min(Number(Math.max(0,Number(var_440.var_245))),class_742.var_219));
               }
               var_280.var_244 = (Number(_loc1_.var_244) + Number(var_440.var_244)) * 0.5;
               var_280.var_245 = (Number(_loc1_.var_245) + Number(var_440.var_245)) * 0.5;
               var_441.method_229(var_280,0.1,null);
               break;
            case 2:
               var_441.var_245 = Number(var_441.var_245 + 33);
               var_441.var_243.scaleX = var_441.var_243.scaleX * 0.85;
               var_441.var_243.scaleY = var_441.var_243.scaleX;
               var_441.var_243.rotation = var_441.var_243.rotation - 10;
               if(var_441.var_245 > class_742.var_219 + 300)
               {
                  var_441.method_89();
               }
               if(var_251 != null)
               {
                  var_251 = var_251 - 1;
                  if(var_251 < 0)
                  {
                     name_3 = 1;
                     var_280.var_243.visible = true;
                     method_284();
                     break;
                  }
                  break;
               }
         }
      }
      
      override public function method_83() : void
      {
         var _loc1_:class_11 = null;
         var _loc2_:MovieClip = null;
         super.method_83();
         if(name_3 == 1)
         {
            name_3 = 2;
            var_280.var_243.visible = false;
            _loc1_ = var_441.var_243;
            _loc1_.var_9.visible = false;
            _loc2_ = var_232.method_84(class_899.__unprotect__("7\x073\r\x03"),class_645.var_209);
            _loc2_.x = var_280.var_244;
            _loc2_.y = var_280.var_245;
            if(Number(var_280.method_115(var_442)) < int(var_66[int(Math.round(var_237[0] * 2))]))
            {
               method_81(true,15);
               var_251 = null;
            }
            else
            {
               var_251 = 32;
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [300];
         super.method_95(param1);
         var_66 = [30,20,10];
         var _loc2_:Number = Math.random() * 6.28;
         var _loc3_:Number = Number(5 + param1 * 13);
         var_440 = {
            "L\x01":Math.random() * class_742.var_217,
            "\x03\x01":Math.random() * class_742.var_219,
            "\x02\x04\x02":Math.cos(_loc2_) * _loc3_,
            "\'\x17\x01":Math.sin(_loc2_) * _loc3_
         };
         method_117();
         method_72();
      }
      
      public function method_284() : void
      {
         var _loc1_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("/\\M\x19\x03"),class_645.var_209));
         var_441 = _loc1_;
         var_441.var_244 = class_742.var_217 * 0.5;
         var_441.var_245 = class_742.var_219 * 0.75;
         var_441.method_118();
      }
      
      public function method_117() : void
      {
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:class_12 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("?2n3"),0);
         var _loc1_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("-4\\-"),class_645.var_209));
         var_442 = _loc1_;
         var_442.var_244 = class_742.var_217 * 0.5;
         var_442.var_245 = class_742.var_219 * 0.5;
         var _loc2_:int = int(Math.round(var_237[0] * 2)) + 1;
         if(_loc2_ > 3)
         {
            _loc2_ = 3;
         }
         var _loc3_:class_18 = var_442.var_243;
         _loc3_.var_15.gotoAndStop(_loc2_);
         var_442.method_118();
         _loc1_ = new class_656(var_232.method_84(class_899.__unprotect__("\x010SZ"),class_645.var_209));
         var_280 = _loc1_;
         var_280.var_244 = class_742.var_217 * 0.5;
         var_280.var_245 = class_742.var_219 * 2;
         var_280.method_118();
         var _loc5_:int = 0;
         while(_loc5_ < 140)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = _loc6_ / 140 * 6.28;
            _loc8_ = Number(72 + Math.pow(Number(Math.random()),2) * 30);
            _loc9_ = new class_12();
            class_319.addChild(_loc9_);
            _loc9_.x = Number(var_442.var_244 + Math.cos(_loc7_) * _loc8_);
            _loc9_.y = Number(var_442.var_245 + Math.sin(_loc7_) * _loc8_);
         }
         method_284();
      }
   }
}
