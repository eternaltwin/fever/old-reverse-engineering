package package_11
{
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.display.Shape;
   import package_32.App;
   import package_32.class_899;
   
   public class class_755
   {
       
      
      public var var_243:DisplayObjectContainer;
      
      public var var_784:Array;
      
      public var var_785:int;
      
      public function class_755(param1:DisplayObjectContainer = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_243 = param1;
         var_785 = var_243.numChildren;
         var_784 = [];
      }
      
      public function method_519(param1:int) : void
      {
         var _loc6_:int = 0;
         var _loc7_:DisplayObject = null;
         var _loc8_:Number = NaN;
         var _loc9_:int = 0;
         var _loc10_:DisplayObject = null;
         var _loc2_:Number = -99999999;
         var _loc3_:int = int(method_517(param1));
         var _loc4_:int = int(var_243.getChildIndex(method_518(param1)));
         var _loc5_:int = _loc3_;
         while(_loc5_ < _loc4_)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = var_243.getChildAt(_loc6_);
            _loc8_ = _loc7_.y;
            if(_loc8_ >= _loc2_)
            {
               _loc2_ = _loc8_;
            }
            else
            {
               _loc9_ = _loc6_ - 1;
               while(_loc9_ >= _loc3_)
               {
                  _loc10_ = var_243.getChildAt(_loc9_);
                  if(_loc10_.y <= _loc8_)
                  {
                     break;
                  }
                  _loc9_--;
               }
               var_243.addChildAt(_loc7_,_loc9_ + 1);
            }
         }
      }
      
      public function method_521(param1:DisplayObject) : void
      {
         var _loc2_:int = int(method_520(param1));
         var_243.addChildAt(param1,int(method_517(_loc2_)));
      }
      
      public function method_110(param1:DisplayObject) : void
      {
         var _loc2_:int = int(method_520(param1));
         var_243.addChildAt(param1,int(var_243.getChildIndex(method_518(_loc2_))) - 1);
      }
      
      public function method_518(param1:int) : DisplayObject
      {
         var _loc2_:DisplayObject = var_784[param1];
         if(_loc2_ != null)
         {
            return _loc2_;
         }
         _loc2_ = new Shape();
         _loc2_.visible = false;
         _loc2_.name = "Plan#" + param1;
         var_243.addChildAt(_loc2_,int(method_517(param1)));
         var_784[param1] = _loc2_;
         return _loc2_;
      }
      
      public function method_520(param1:DisplayObject) : int
      {
         var _loc5_:int = 0;
         var _loc6_:DisplayObject = null;
         var _loc2_:int = int(var_243.getChildIndex(param1));
         var _loc3_:int = 0;
         var _loc4_:int = int(var_784.length);
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = var_784[_loc5_];
            if(_loc6_ != null && int(var_243.getChildIndex(_loc6_)) > _loc2_)
            {
               return _loc5_;
            }
         }
         return 0;
      }
      
      public function method_522() : DisplayObjectContainer
      {
         return var_243;
      }
      
      public function method_517(param1:int) : int
      {
         var _loc3_:DisplayObject = null;
         var _loc2_:int = param1;
         while(true)
         {
            _loc2_--;
            if(_loc2_ < 0)
            {
               break;
            }
            _loc3_ = var_784[_loc2_];
            if(_loc3_ != null)
            {
               return int(var_243.getChildIndex(_loc3_)) + 1;
            }
         }
         return var_785;
      }
      
      public function method_92(param1:int) : MovieClip
      {
         var _loc2_:MovieClip = new MovieClip();
         var_243.addChildAt(_loc2_,int(var_243.getChildIndex(method_518(param1))));
         return _loc2_;
      }
      
      public function method_207() : void
      {
         var _loc1_:DisplayObject = null;
         while(var_243.numChildren > var_785)
         {
            _loc1_ = var_243.getChildAt(var_785);
            _loc1_.parent.removeChild(_loc1_);
         }
         var_784 = [];
      }
      
      public function name_72(param1:int) : void
      {
         var _loc2_:DisplayObject = method_518(param1);
         var _loc3_:int = int(method_517(param1));
         var _loc4_:int = int(var_243.getChildIndex(_loc2_)) - _loc3_;
         while(_loc4_ > 0)
         {
            var_243.removeChildAt(_loc3_);
            _loc4_--;
         }
      }
      
      public function method_84(param1:String, param2:int) : MovieClip
      {
         var _loc3_:MovieClip = App.method_523(param1);
         var_243.addChildAt(_loc3_,int(var_243.getChildIndex(method_518(param2))));
         return _loc3_;
      }
      
      public function method_523(param1:String, param2:int) : MovieClip
      {
         return method_84(class_899.__unprotect__(param1),param2);
      }
      
      public function method_124(param1:Object, param2:int) : Object
      {
         var _loc3_:DisplayObject = param1 as DisplayObject;
         if(_loc3_.parent != null)
         {
            _loc3_.parent.removeChild(_loc3_);
         }
         var_243.addChildAt(_loc3_,int(var_243.getChildIndex(method_518(param2))));
         return param1;
      }
   }
}
