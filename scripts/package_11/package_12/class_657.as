package package_11.package_12
{
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.geom.ColorTransform;
   
   public class class_657
   {
       
      
      public function class_657()
      {
      }
      
      public static function method_232(param1:int) : Object
      {
         return {
            "z\x01":param1 >> 16,
            "6\x01":param1 >> 8 & 255,
            "r\x01":param1 & 255
         };
      }
      
      public static function method_233(param1:Object) : int
      {
         return int(param1.var_93) << 16 | int(param1.var_39) << 8 | int(param1.var_12);
      }
      
      public static function method_234(param1:int) : Object
      {
         return {
            "@\x01":param1 >>> 24,
            "z\x01":param1 >> 16 & 255,
            "6\x01":param1 >> 8 & 255,
            "r\x01":param1 & 255
         };
      }
      
      public static function method_235(param1:Object) : int
      {
         return int(param1.var_65) << 24 | int(param1.var_93) << 16 | int(param1.var_39) << 8 | int(param1.var_12);
      }
      
      public static function gfof(param1:DisplayObject, param2:Number, param3:int, param4:Object = undefined) : void
      {
         if(param4 == null)
         {
            param4 = 0;
         }
         var _loc5_:* = class_657.method_232(param3);
         var _loc6_:Number = 1 - param2;
         var _loc7_:ColorTransform = new ColorTransform(_loc6_,_loc6_,_loc6_,1,int(Number(param2 * int(_loc5_.var_93) + param4)),int(Number(param2 * int(_loc5_.var_39) + param4)),int(Number(param2 * int(_loc5_.var_12) + param4)),0);
         param1.transform.colorTransform = _loc7_;
      }
      
      public static function name_18(param1:DisplayObject, param2:int, param3:int = -255) : void
      {
         var _loc4_:* = class_657.method_234(param2);
         var _loc5_:ColorTransform = new ColorTransform(1,1,1,1,int(int(_loc4_.var_93) + param3),int(int(_loc4_.var_39) + param3),int(int(_loc4_.var_12) + param3),0);
         param1.transform.colorTransform = _loc5_;
      }
      
      public static function method_139(param1:DisplayObject, param2:int, param3:int = -128) : void
      {
         var _loc4_:* = class_657.method_234(param2);
         var _loc7_:ColorTransform = new ColorTransform(1,1,1,1,int((int(_loc4_.var_93) + param3) * 2),int((int(_loc4_.var_39) + param3) * 2),int((int(_loc4_.var_12) + param3) * 2),0);
         param1.transform.colorTransform = _loc7_;
      }
      
      public static function method_236(param1:DisplayObject, param2:int, param3:int = -128) : void
      {
         var _loc4_:* = class_657.method_234(param2);
         var _loc7_:ColorTransform = new ColorTransform(2,2,2,2,int((int(_loc4_.var_93) + param3) * 1),int((int(_loc4_.var_39) + param3) * 1),int((int(_loc4_.var_12) + param3) * 1),0);
         param1.transform.colorTransform = _loc7_;
      }
      
      public static function method_237(param1:Object = undefined) : int
      {
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         if(param1 == null)
         {
            param1 = Number(Math.random());
         }
         var _loc3_:Array = [0,0,0];
         var _loc4_:Number = 1 / 3 * 2;
         var _loc5_:int = 0;
         while(_loc5_ < 3)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = Number(_loc4_ + _loc6_ * 2 * _loc4_);
            _loc8_ = Number(class_663.method_112(_loc7_ - param1,0.5));
            _loc3_[_loc6_] = Number(Math.min(1.5 - Math.abs(_loc8_) * 3,1));
         }
         return int(class_657.method_233({
            "z\x01":int(_loc3_[0] * 255),
            "6\x01":int(_loc3_[1] * 255),
            "r\x01":int(_loc3_[2] * 255)
         }));
      }
      
      public static function method_138(param1:Object = undefined) : int
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         if(param1 == null)
         {
            param1 = Number(Math.random());
         }
         var _loc2_:Number = 1 / 3;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < 3)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = Math.abs(Number(class_663.method_112(param1 - _loc2_ * _loc5_,0.5))) * 2;
            _loc3_.push(int(_loc6_ * 255));
         }
         return int(class_657.method_233({
            "z\x01":int(_loc3_[0]),
            "6\x01":int(_loc3_[1]),
            "r\x01":int(_loc3_[2])
         }));
      }
      
      public static function method_238(param1:int, param2:int, param3:Object = undefined) : int
      {
         if(param3 == null)
         {
            param3 = 0.5;
         }
         var _loc4_:* = class_657.method_232(param1);
         var _loc5_:* = class_657.method_232(param2);
         var _loc6_:* = {
            "z\x01":int(Number(int(_loc4_.var_93) * param3 + int(_loc5_.var_93) * (1 - param3))),
            "6\x01":int(Number(int(_loc4_.var_39) * param3 + int(_loc5_.var_39) * (1 - param3))),
            "r\x01":int(Number(int(_loc4_.var_12) * param3 + int(_loc5_.var_12) * (1 - param3)))
         };
         return int(class_657.method_233(_loc6_));
      }
      
      public static function method_239(param1:int, param2:int, param3:Object = undefined) : int
      {
         if(param3 == null)
         {
            param3 = 0.5;
         }
         var _loc4_:* = class_657.method_234(param1);
         var _loc5_:* = class_657.method_234(param2);
         var _loc6_:* = {
            "z\x01":int(Number(int(_loc4_.var_93) * param3 + int(_loc5_.var_93) * (1 - param3))),
            "6\x01":int(Number(int(_loc4_.var_39) * param3 + int(_loc5_.var_39) * (1 - param3))),
            "r\x01":int(Number(int(_loc4_.var_12) * param3 + int(_loc5_.var_12) * (1 - param3))),
            "@\x01":int(Number(int(_loc4_.var_65) * param3 + int(_loc5_.var_65) * (1 - param3)))
         };
         return int(class_657.method_235(_loc6_));
      }
      
      public static function method_240(param1:int, param2:Number = 0.5) : int
      {
         var _loc3_:* = class_657.method_232(param1);
         var _loc4_:Number = (int(_loc3_.var_93) + int(_loc3_.var_39) + int(_loc3_.var_12)) / 3;
         _loc3_.var_93 = int(Number(int(_loc3_.var_93) * (1 - param2) + _loc4_ * param2));
         _loc3_.var_39 = int(Number(int(_loc3_.var_39) * (1 - param2) + _loc4_ * param2));
         _loc3_.var_12 = int(Number(int(_loc3_.var_12) * (1 - param2) + _loc4_ * param2));
         return int(class_657.method_233(_loc3_));
      }
      
      public static function method_151(param1:int, param2:int) : int
      {
         var _loc3_:* = class_657.method_232(param1);
         _loc3_.var_93 = int(Number(class_663.method_99(0,int(_loc3_.var_93) + param2,255)));
         _loc3_.var_39 = int(Number(class_663.method_99(0,int(_loc3_.var_39) + param2,255)));
         _loc3_.var_12 = int(Number(class_663.method_99(0,int(_loc3_.var_12) + param2,255)));
         return int(class_657.method_233(_loc3_));
      }
      
      public static function method_241(param1:int, param2:int) : int
      {
         var _loc3_:* = class_657.method_232(param1);
         _loc3_.var_93 = int(Number(class_663.method_99(0,Number(int(_loc3_.var_93) + (Math.random() * 2 - 1) * param2),255)));
         _loc3_.var_39 = int(Number(class_663.method_99(0,Number(int(_loc3_.var_39) + (Math.random() * 2 - 1) * param2),255)));
         _loc3_.var_12 = int(Number(class_663.method_99(0,Number(int(_loc3_.var_12) + (Math.random() * 2 - 1) * param2),255)));
         return int(class_657.method_233(_loc3_));
      }
      
      public static function method_242(param1:int, param2:int, param3:int) : int
      {
         return (param1 << 16) + (param2 << 8) + param3;
      }
      
      public static function method_244(param1:int) : String
      {
         return "#" + class_761.method_243(param1);
      }
      
      public static function method_246(param1:Class, param2:int = 8) : Array
      {
         var _loc8_:int = 0;
         var _loc3_:Sprite = Type.method_245(param1,[]);
         var _loc4_:BitmapData = new BitmapData(int(_loc3_.width),int(_loc3_.height),false,16776960);
         _loc4_.draw(_loc3_);
         var _loc5_:Array = [];
         var _loc6_:int = param2 >> 1;
         var _loc7_:int = 0;
         while(_loc7_ < 10)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc5_.push(uint(_loc4_.getPixel(_loc8_ * param2 + _loc6_,_loc6_)));
         }
         _loc4_.dispose();
         return _loc5_;
      }
   }
}
