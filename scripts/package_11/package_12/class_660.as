package package_11.package_12
{
   import package_32.class_899;
   
   public class class_660
   {
       
      
      public var var_345:Number;
      
      public var var_346:Number;
      
      public var var_348:Number;
      
      public var var_347:Number;
      
      public var var_254:Number;
      
      public function class_660(param1:Object = undefined, param2:Object = undefined, param3:Object = undefined, param4:Object = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_346 = param1;
         var_345 = param2;
         var_347 = param3;
         var_348 = param4;
         var_254 = 0;
      }
      
      public function method_250(param1:Number) : Object
      {
         return {
            "\x02\x04\x02":(var_347 - var_346) * param1,
            "\'\x17\x01":(var_348 - var_345) * param1
         };
      }
      
      public function method_127(param1:Object = undefined) : Object
      {
         if(param1 == null)
         {
            param1 = var_254;
         }
         return {
            "L\x01":Number(var_346 + (var_347 - var_346) * param1),
            "\x03\x01":Number(var_345 + (var_348 - var_345) * param1)
         };
      }
      
      public function method_251(param1:Object = undefined, param2:Object = undefined, param3:Object = undefined) : Object
      {
         var _loc4_:Number = Number(class_663.method_112(var_347 - var_346,param2 * 0.5));
         var _loc5_:Number = Number(class_663.method_112(var_348 - var_345,param3 * 0.5));
         return {
            "L\x01":Number(class_663.method_113(Number(var_346 + _loc4_ * param1),param2)),
            "\x03\x01":Number(class_663.method_113(Number(var_345 + _loc5_ * param1),param3))
         };
      }
      
      public function method_115() : Number
      {
         var _loc1_:Number = var_347 - var_346;
         var _loc2_:Number = var_348 - var_345;
         return Number(Math.sqrt(Number(_loc1_ * _loc1_ + _loc2_ * _loc2_)));
      }
      
      public function method_212() : Number
      {
         var _loc1_:Number = var_347 - var_346;
         var _loc2_:Number = var_348 - var_345;
         return Number(Math.atan2(_loc2_,_loc1_));
      }
   }
}
