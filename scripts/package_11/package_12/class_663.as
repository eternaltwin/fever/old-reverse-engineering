package package_11.package_12
{
   import package_10.class_870;
   
   public class class_663
   {
       
      
      public function class_663()
      {
      }
      
      public static function method_99(param1:Number, param2:Number, param3:Number) : Number
      {
         return Number(Math.min(Number(Math.max(param1,param2)),param3));
      }
      
      public static function method_256(param1:int, param2:int, param3:int) : int
      {
         if(param2 < param1)
         {
            return param1;
         }
         if(param2 > param3)
         {
            return param3;
         }
         return param2;
      }
      
      public static function method_113(param1:Number, param2:Number) : Number
      {
         if(param2 == 0)
         {
            class_870.name_17("sMod ERROR! (" + param1 + "," + param2 + ")",{
               "\x06\fi2\x03":"Lib.hx",
               "d\x0f^S\x03":460,
               "v2z\x01":"mt.bumdum9.Num",
               "d\x19o\f\x03":"sMod"
            });
            return param1;
         }
         while(param1 >= param2)
         {
            param1 = param1 - param2;
         }
         while(param1 < 0)
         {
            param1 = Number(param1 + param2);
         }
         return param1;
      }
      
      public static function method_112(param1:Number, param2:Number) : Number
      {
         while(param1 > param2)
         {
            param1 = param1 - param2 * 2;
         }
         while(param1 < -param2)
         {
            param1 = Number(param1 + param2 * 2);
         }
         return param1;
      }
   }
}
