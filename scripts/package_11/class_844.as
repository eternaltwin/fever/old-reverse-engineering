package package_11
{
   import package_32.class_899;
   
   public class class_844
   {
       
      
      public var var_503:Number;
      
      public function class_844(param1:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_503 = (param1 < 0?-param1:param1) + 131;
      }
      
      public function method_114(param1:int) : int
      {
         var _loc2_:Number = var_503 * 16807 % 2147483647;
         var_503 = _loc2_;
         return int((int(_loc2_) & 1073741823) % param1);
      }
      
      public function name_71() : Number
      {
         var _loc1_:Number = var_503 * 16807 % 2147483647;
         var_503 = _loc1_;
         return int((int(_loc1_) & 1073741823) % 10007) / 10007;
      }
      
      public function _int() : int
      {
         var _loc1_:Number = var_503 * 16807 % 2147483647;
         var_503 = _loc1_;
         return int(_loc1_) & 1073741823;
      }
      
      public function method_852(param1:int, param2:int = 5) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < param2)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            param1 = param1 ^ param1 << 7 & 727393536;
            param1 = param1 ^ param1 << 15 & 462094336;
            param1 = param1 ^ param1 >>> 16;
            param1 = param1 & 1073741823;
            _loc5_ = 5381;
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 & 255);
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 >> 8 & 255);
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 >> 16 & 255);
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 >> 24);
            param1 = _loc5_ & 1073741823;
         }
         var_503 = (param1 & 536870911) + 131;
      }
      
      public function method_853() : int
      {
         return int(var_503) - 131;
      }
      
      public function method_417() : class_844
      {
         var _loc1_:class_844 = new class_844(0);
         _loc1_.var_503 = var_503;
         return _loc1_;
      }
      
      public function method_854(param1:int) : void
      {
         var_503 = int((var_503 + param1) % 2147483647) & 1073741823;
         if(var_503 == 0)
         {
            var_503 = param1 + 1;
         }
      }
   }
}
