package
{
   import package_32.class_899;
   
   public class class_691
   {
       
      
      public function class_691()
      {
      }
      
      public static function method_351(param1:*, param2:*) : Boolean
      {
         return Boolean(class_899.__instanceof(param1,param2));
      }
      
      public static function method_97(param1:*) : String
      {
         return class_899.__string_rec(param1,"");
      }
      
      public static function _int(param1:Number) : int
      {
         return int(param1);
      }
      
      public static function method_352(param1:String) : Object
      {
         var _loc2_:* = parseInt(param1);
         if(isNaN(_loc2_))
         {
            return null;
         }
         return _loc2_;
      }
      
      public static function method_353(param1:String) : Number
      {
         return parseFloat(param1);
      }
      
      public static function method_114(param1:int) : int
      {
         return int(Math.floor(Math.random() * param1));
      }
   }
}
