package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_712 extends class_645
   {
      
      public static var var_216:int = 240;
      
      public static var var_218:int = 240;
      
      public static var var_621:int = 10;
      
      public static var var_622:Number = 0.1;
      
      public static var var_623:Number = 0.2;
      
      public static var var_624:Number = 0.8;
      
      public static var var_625:int = 80;
      
      public static var var_626:int = 50;
      
      public static var var_627:Array = [47,33,19];
       
      
      public var var_577:Array;
      
      public var var_628:class_892;
      
      public var var_630:int;
      
      public var var_41:MovieClip;
      
      public function class_712()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Function = null;
         var _loc2_:Boolean = false;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_892 = null;
         var _loc6_:Boolean = false;
         switch(name_3)
         {
            default:
               break;
            case 1:
               EsMu();
               _loc1_ = function(param1:class_892, param2:class_892):int
               {
                  if(param1.var_245 > param2.var_245)
                  {
                     return 1;
                  }
                  if(param1.var_245 < param2.var_245)
                  {
                     return -1;
                  }
                  return 0;
               };
               var_577.sort(_loc1_);
               _loc2_ = true;
               _loc3_ = 0;
               _loc4_ = var_577;
               while(_loc3_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  method_387(_loc5_);
                  if(!!_loc2_ && _loc5_.var_245 > var_628.var_245)
                  {
                     var_232.method_110(var_628.var_243);
                  }
                  var_232.method_110(_loc5_.var_243);
               }
               _loc6_ = true;
               _loc3_ = 0;
               _loc4_ = var_577;
               while(_loc3_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  if(!_loc5_.name_59)
                  {
                     _loc6_ = false;
                     break;
                  }
               }
               if(_loc6_)
               {
                  method_81(true,20);
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_388(param1:MovieClip, param2:MovieClip) : void
      {
         var _loc3_:int = int(Reflect.field(param1,"_leg"));
         param1["p" + _loc3_] = param2;
         _loc3_++;
         param1["_leg"] = _loc3_;
      }
      
      override public function method_83() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_577;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(Number(var_628.method_115(_loc3_)) < class_712.var_626)
            {
               _loc3_.var_475 = 8;
            }
            _loc4_ = var_628.var_243.var_1;
            _loc4_.play();
            _loc4_ = var_628.var_243.var_1;
            _loc4_.rotation = var_628.var_65 / 0.0174;
         }
      }
      
      public function method_387(param1:class_892) : void
      {
         var _loc2_:Number = Number(param1.method_115(var_628));
         if(param1.name_59)
         {
            param1.var_629 = -1.57;
            param1.var_475 = 2;
         }
         else if(_loc2_ < class_712.var_625)
         {
            param1.var_629 = Number(var_628.method_231(param1));
            param1.var_475 = Number(Number(param1.var_475) + (1 - _loc2_ / class_712.var_625) * 0.5);
         }
         else if(int(class_691.method_114(80)) == 0 && Number(param1.var_475) < 0.8)
         {
            param1.var_629 = Math.random() * 6.28;
            param1.var_475 = class_712.var_624;
         }
         param1.name_60 = Number(Number(param1.name_60) + param1.var_475 * 30);
         param1.var_475 = param1.var_475 * 0.9;
         if(!param1.name_59)
         {
            method_389(param1);
         }
         var _loc3_:Number = param1.var_629 - param1.var_65;
         while(_loc3_ > 3.14)
         {
            _loc3_ = _loc3_ - 6.28;
         }
         while(_loc3_ < -3.14)
         {
            _loc3_ = Number(_loc3_ + 6.28);
         }
         param1.var_65 = Number(Number(param1.var_65) + _loc3_ * class_712.var_622);
         param1.var_278 = Math.cos(Number(param1.var_65)) * param1.var_475;
         param1.var_279 = Math.sin(Number(param1.var_65)) * param1.var_475;
         var _loc4_:Number = param1.var_65 / 6.28 - 0.25;
         while(_loc4_ < 0)
         {
            _loc4_ = Number(_loc4_ + 1);
         }
         while(_loc4_ > 1)
         {
            _loc4_ = _loc4_ - 1;
         }
         param1.var_243.gotoAndStop(1 + int(_loc4_ * 58));
         method_390(param1);
      }
      
      public function EsMu() : void
      {
         var _loc1_:* = method_100();
         var_628.var_629 = Number(var_628.method_231(_loc1_));
         var _loc2_:Number = var_628.var_629 - var_628.var_65;
         while(_loc2_ > 3.14)
         {
            _loc2_ = _loc2_ - 6.28;
         }
         while(_loc2_ < -3.14)
         {
            _loc2_ = Number(_loc2_ + 6.28);
         }
         var_628.var_65 = Number(Number(var_628.var_65) + _loc2_ * class_712.var_623);
         var _loc3_:Number = Number(class_663.method_99(0,(var_628.method_115(_loc1_) - 10) * 0.1,5));
         var_628.var_278 = Math.cos(Number(var_628.var_65)) * _loc3_;
         var_628.var_279 = Math.sin(Number(var_628.var_65)) * _loc3_;
         var _loc4_:Number = var_628.var_65 / 6.28 - 0.25;
         while(_loc4_ < 0)
         {
            _loc4_ = Number(_loc4_ + 1);
         }
         while(_loc4_ > 1)
         {
            _loc4_ = _loc4_ - 1;
         }
         var_628.var_243.gotoAndStop(1 + int(_loc4_ * 58));
      }
      
      public function method_391(param1:Number, param2:Number) : Boolean
      {
         var _loc3_:Boolean = param2 < 10 && Number(Math.abs(class_712.var_216 * 0.5 - param1)) > int(class_712.var_627[var_630]);
         return param1 < 0 || param1 > class_712.var_216 || param2 > class_712.var_218 || _loc3_;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [480];
         super.method_95(param1);
         var_630 = int(param1 * 2);
         if(var_630 > 2)
         {
            var_630 = 2;
         }
         method_117();
         method_72();
      }
      
      public function method_392(param1:class_892) : void
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:int = 0;
         var _loc10_:Number = NaN;
         var _loc2_:* = null;
         var _loc3_:Number = 30000;
         var _loc4_:int = 0;
         while(_loc4_ < 2)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = Number(param1.var_244 + Math.cos(Number(param1.var_629)) * 20);
            _loc7_ = Number(param1.var_245 + Math.sin(Number(param1.var_629)) * 20);
            _loc8_ = Number(param1.var_629);
            _loc9_ = _loc5_ * 2 - 1;
            while(method_391(_loc6_,_loc7_))
            {
               _loc8_ = Number(_loc8_ + 0.01 * _loc9_);
               _loc6_ = Number(param1.var_244 + Math.cos(_loc8_) * 20);
               _loc7_ = Number(param1.var_245 + Math.sin(_loc8_) * 20);
            }
            _loc10_ = _loc8_ - param1.var_629;
            while(_loc10_ > 3.14)
            {
               _loc10_ = _loc10_ - 6.28;
            }
            while(_loc10_ < -3.14)
            {
               _loc10_ = Number(_loc10_ + 6.28);
            }
            if(Number(Math.abs(_loc10_)) < Number(Math.abs(_loc3_)))
            {
               _loc3_ = _loc10_;
               _loc2_ = _loc8_;
            }
         }
         param1.var_629 = _loc2_;
      }
      
      public function method_390(param1:class_892) : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Array = var_577;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(param1 != _loc4_)
            {
               _loc5_ = Number(param1.method_115(_loc4_));
               if(_loc5_ < class_712.var_621 * 2)
               {
                  _loc6_ = (class_712.var_621 * 2 - _loc5_) * 0.5;
                  _loc7_ = Number(param1.method_231(_loc4_));
                  param1.var_244 = param1.var_244 - Math.cos(_loc7_) * _loc6_;
                  param1.var_245 = param1.var_245 - Math.sin(_loc7_) * _loc6_;
                  _loc4_.var_244 = Number(_loc4_.var_244 + Math.cos(_loc7_) * _loc6_);
                  _loc4_.var_245 = Number(_loc4_.var_245 + Math.sin(_loc7_) * _loc6_);
               }
            }
         }
      }
      
      public function method_389(param1:class_892) : void
      {
         var _loc2_:int = class_712.var_621;
         if(param1.var_244 < _loc2_ || param1.var_244 > class_712.var_216 - _loc2_)
         {
            param1.var_244 = Number(Math.min(Number(Math.max(_loc2_,param1.var_244)),class_712.var_216 - _loc2_));
            method_392(param1);
         }
         var _loc3_:Boolean = Number(Math.abs(class_712.var_216 * 0.5 - param1.var_244)) < int(class_712.var_627[var_630]);
         if(param1.var_245 < 10 + _loc2_)
         {
            if(!_loc3_)
            {
               param1.var_245 = Number(Math.max(10 + _loc2_,param1.var_245));
               method_392(param1);
            }
         }
         if(!!_loc3_ && param1.var_245 < 20 + _loc2_)
         {
            param1.name_59 = true;
         }
         if(param1.var_245 > class_712.var_218 - _loc2_)
         {
            param1.var_245 = Number(Math.min(param1.var_245,class_712.var_218 - _loc2_));
            method_392(param1);
         }
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:class_892 = null;
         var _loc6_:class_892 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\\Qi`\x03"),0);
         var_41 = var_232.method_84(class_899.__unprotect__("D\x1b;+\x03"),class_645.var_209);
         var_41.gotoAndStop(1 + var_630);
         var_577 = [];
         var _loc2_:int = int(Math.round(1 + var_237[0] * 15 - var_630 * 2));
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc6_ = new class_892(var_232.method_84(class_899.__unprotect__("Domd\x01"),class_645.var_209));
            _loc6_.var_233 = 0.99;
            _loc5_ = _loc6_;
            _loc5_.var_244 = 20 + int(class_691.method_114(class_712.var_216 - 2 * 20));
            _loc5_.var_245 = 20 + int(class_691.method_114(class_712.var_218 - 2 * 20));
            if(_loc4_ < 2)
            {
               _loc5_.var_245 = class_712.var_216 * 0.75;
            }
            _loc5_.var_65 = Math.random() * 6.28;
            _loc5_.var_629 = Number(_loc5_.var_65);
            _loc5_.var_475 = class_712.var_624;
            _loc5_.name_60 = Math.random() * 628;
            _loc5_.name_59 = false;
            _loc5_.method_118();
            _loc5_.var_243.stop();
            var_577.push(_loc5_);
            _loc5_.var_243["_leg"] = 0;
            _loc5_.var_243["_register"] = function(param1:Function, param2:MovieClip):Function
            {
               var var_27:Function = param1;
               var var_18:MovieClip = param2;
               return function(param1:MovieClip):void
               {
                  return var_27(var_18,param1);
               };
            }(method_388,_loc5_.var_243);
         }
         _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("6W~A\x03"),class_645.var_209));
         _loc5_.var_233 = 0.99;
         var_628 = _loc5_;
         var_628.var_244 = class_712.var_216 * 0.5;
         var_628.var_245 = class_712.var_218 * 0.5;
         var_628.var_65 = 0;
         var_628.var_629 = 0;
         var_628.name_60 = 0;
         var_628.var_243["_leg"] = 0;
         var_628.var_243["_register"] = function(param1:Function, param2:MovieClip):Function
         {
            var var_27:Function = param1;
            var var_18:MovieClip = param2;
            return function(param1:MovieClip):void
            {
               return var_27(var_18,param1);
            };
         }(method_388,var_628.var_243);
         var_628.method_118();
      }
   }
}
