package package_16
{
   import package_32.class_899;
   
   public class class_675
   {
       
      
      public var var_371:int;
      
      public var var_443:Array;
      
      public var var_376:int;
      
      public function class_675(param1:class_684 = undefined)
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_685 = null;
         var _loc9_:Array = null;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         if(class_899.var_239)
         {
            return;
         }
         var_371 = param1.var_371;
         var_376 = param1.var_376;
         var_443 = [];
         var _loc2_:int = 0;
         var _loc3_:int = var_371;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = var_376;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = param1.method_285(_loc4_,_loc7_);
               _loc9_ = [];
               _loc10_ = 0;
               while(_loc10_ < 4)
               {
                  _loc10_++;
                  _loc11_ = _loc10_;
                  _loc9_.push(_loc8_.method_286(_loc11_) != class_683.var_444);
               }
               var_443.push({
                  "}\t\t\x01":_loc8_.var_237,
                  "\x04);[":_loc9_
               });
            }
         }
      }
      
      public function method_285(param1:int, param2:int) : Object
      {
         return var_443[param1 * var_371 + param2];
      }
   }
}
