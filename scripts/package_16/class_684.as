package package_16
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.geom.Rectangle;
   import package_11.class_844;
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_684
   {
      
      public static var var_264:Array = [[1,0],[0,1],[-1,0],[0,-1]];
       
      
      public var var_498:Array;
      
      public var var_499:int;
      
      public var var_500:int;
      
      public var var_445:Array;
      
      public var var_371:int;
      
      public var var_390:Object;
      
      public var var_502:Number;
      
      public var var_503:class_844;
      
      public var var_443:Array;
      
      public var var_302:Boolean;
      
      public var name_49:Bitmap;
      
      public var var_504:Boolean;
      
      public var var_505:Boolean;
      
      public var var_506:int;
      
      public var var_376:int;
      
      public var var_496:Array;
      
      public var var_501:Number;
      
      public var var_303:Function;
      
      public function class_684(param1:int = 0, param2:int = 0, param3:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_371 = param1;
         var_376 = param2;
         var_501 = 2000;
         var_502 = 0;
         var_506 = 50;
         var_500 = 50;
         var_499 = 1;
         method_311(param3);
      }
      
      public function method_79() : void
      {
         var _loc1_:Number = Number(Date.name_16().getTime());
         while(var_303 != null)
         {
            var_303();
            if(Date.name_16().getTime() - _loc1_ > var_501)
            {
               break;
            }
         }
      }
      
      public function method_311(param1:int) : void
      {
         var_503 = new class_844(param1);
      }
      
      public function name_50() : void
      {
         method_312();
         while(!var_302)
         {
            method_79();
         }
      }
      
      public function name_1(param1:Number = 1.0) : class_685
      {
         var _loc2_:int = int((1 - param1) * (int(var_445.length) - 1));
         var _loc3_:int = int(var_445.length);
         var _loc5_:class_844 = var_503;
         var _loc6_:Number = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         var _loc4_:int = _loc2_ + int((int(_loc6_) & 1073741823) % (_loc3_ - _loc2_));
         var _loc7_:class_685 = var_445[_loc4_];
         var_445.splice(_loc4_,1);
         return _loc7_;
      }
      
      public function method_312() : void
      {
         var _loc2_:class_844 = null;
         var _loc3_:Number = NaN;
         method_313();
         var_302 = false;
         if(var_390 == null)
         {
            _loc2_ = var_503;
            _loc3_ = _loc2_.var_503 * 16807 % 2147483647;
            _loc2_.var_503 = _loc3_;
            _loc2_ = var_503;
            _loc3_ = _loc2_.var_503 * 16807 % 2147483647;
            _loc2_.var_503 = _loc3_;
            var_390 = {
               "L\x01":int((int(_loc3_) & 1073741823) % var_371),
               "\x03\x01":int((int(_loc3_) & 1073741823) % var_376)
            };
         }
         if(var_498 == null)
         {
            method_314();
         }
         method_315();
      }
      
      public function method_316() : void
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Boolean = false;
         var _loc8_:class_844 = null;
         var _loc9_:Number = NaN;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:class_686 = null;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:class_685 = null;
         var_498 = [];
         var _loc1_:int = 0;
         var _loc2_:int = var_499;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = Number(Math.sqrt(var_371 * var_371 + var_376 * var_376)) / (var_499 + 1);
            _loc5_ = 0;
            _loc6_ = 0;
            do
            {
               _loc7_ = true;
               _loc8_ = var_503;
               _loc9_ = _loc8_.var_503 * 16807 % 2147483647;
               _loc8_.var_503 = _loc9_;
               _loc5_ = int((int(_loc9_) & 1073741823) % var_371);
               _loc8_ = var_503;
               _loc9_ = _loc8_.var_503 * 16807 % 2147483647;
               _loc8_.var_503 = _loc9_;
               _loc6_ = int((int(_loc9_) & 1073741823) % var_376);
               _loc10_ = 0;
               _loc11_ = var_498;
               while(_loc10_ < int(_loc11_.length))
               {
                  _loc12_ = _loc11_[_loc10_];
                  _loc10_++;
                  _loc13_ = _loc12_.name_51.var_244 - _loc5_;
                  _loc14_ = _loc12_.name_51.var_245 - _loc6_;
                  if(Number(Math.sqrt(_loc13_ * _loc13_ + _loc14_ * _loc14_)) < _loc4_)
                  {
                     _loc7_ = false;
                     break;
                  }
               }
            }
            while(!_loc7_);
            
            _loc15_ = var_496[_loc5_][_loc6_];
            _loc12_ = new class_686(_loc15_);
            _loc12_.var_443.push(_loc15_);
            _loc12_.var_215 = _loc3_;
            _loc8_ = var_503;
            _loc9_ = _loc8_.var_503 * 16807 % 2147483647;
            _loc8_.var_503 = _loc9_;
            _loc12_.var_503 = new class_844(int((int(_loc9_) & 1073741823) % 99999));
            var_498.push(_loc12_);
         }
      }
      
      public function method_314() : void
      {
         var _loc7_:class_685 = null;
         var _loc1_:class_685 = var_496[0][0];
         var _loc2_:class_686 = new class_686(_loc1_);
         var _loc3_:class_844 = var_503;
         var _loc4_:Number = _loc3_.var_503 * 16807 % 2147483647;
         _loc3_.var_503 = _loc4_;
         _loc2_.var_503 = new class_844(int((int(_loc4_) & 1073741823) % 99999));
         _loc2_.var_215 = 0;
         var_498 = [_loc2_];
         var _loc5_:int = 0;
         var _loc6_:Array = var_443;
         while(_loc5_ < int(_loc6_.length))
         {
            _loc7_ = _loc6_[_loc5_];
            _loc5_++;
            _loc2_.var_443.push(_loc7_);
            _loc7_.var_455 = _loc2_;
         }
      }
      
      public function method_313() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:class_685 = null;
         var _loc9_:Array = null;
         var _loc10_:Array = null;
         var_496 = [];
         var_443 = [];
         var _loc1_:int = 0;
         _loc2_ = var_371;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_496[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = var_376;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               _loc7_ = new class_685(_loc3_,_loc6_);
               var_496[_loc3_][_loc6_] = _loc7_;
               var_443.push(_loc7_);
            }
         }
         _loc1_ = 0;
         var _loc8_:Array = var_443;
         while(_loc1_ < int(_loc8_.length))
         {
            _loc7_ = _loc8_[_loc1_];
            _loc1_++;
            _loc2_ = 0;
            _loc3_ = 0;
            _loc9_ = class_684.var_264;
            while(_loc3_ < int(_loc9_.length))
            {
               _loc10_ = _loc9_[_loc3_];
               _loc3_++;
               _loc4_ = _loc7_.var_244 + int(_loc10_[0]);
               _loc5_ = _loc7_.var_245 + int(_loc10_[1]);
               if(var_505)
               {
                  _loc4_ = int(Number(class_663.method_113(_loc4_,var_371)));
               }
               if(var_504)
               {
                  _loc5_ = int(Number(class_663.method_113(_loc5_,var_376)));
               }
               if(_loc4_ >= 0 && _loc4_ < var_371 && _loc5_ >= 0 && _loc5_ < var_376)
               {
                  _loc7_.var_507[_loc2_] = var_496[_loc4_][_loc5_];
               }
               _loc2_++;
            }
         }
      }
      
      public function method_315() : void
      {
         var _loc3_:class_685 = null;
         var _loc4_:class_686 = null;
         var_445 = [];
         var _loc1_:int = 0;
         var _loc2_:Array = var_443;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_508 = 0;
         }
         _loc1_ = 0;
         _loc2_ = var_498;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc1_];
            _loc1_++;
            method_317(_loc4_.name_51);
         }
         var_303 = method_318;
      }
      
      public function method_285(param1:int, param2:int) : class_685
      {
         return var_496[param1][param2];
      }
      
      public function method_319() : Bitmap
      {
         name_49 = new Bitmap();
         name_49.bitmapData = new BitmapData(var_371 * 2,var_376 * 2,false,0);
         return name_49;
      }
      
      public function method_321(param1:int, param2:Number = 0.0) : void
      {
         var _loc5_:class_686 = null;
         var_499 = param1;
         var_302 = false;
         method_313();
         method_316();
         var_303 = method_320;
         var _loc3_:int = 0;
         var _loc4_:Array = var_498;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc5_.var_509 = param2;
         }
      }
      
      public function method_323(param1:class_685) : void
      {
         var _loc4_:class_685 = null;
         method_322(param1);
         param1.var_508 = 1;
         var _loc2_:int = 0;
         var _loc3_:Array = param1.var_489;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.var_508 == 0)
            {
               _loc4_.var_237 = param1.var_237 + 1;
               var_445.push(_loc4_);
            }
            else
            {
               if(_loc4_.var_237 > param1.var_237 + 1)
               {
                  _loc4_.var_237 = param1.var_237 + 1;
                  var_445.push(_loc4_);
               }
               if(_loc4_.var_237 < param1.var_237 - 1)
               {
                  var_445.push(_loc4_);
               }
            }
         }
      }
      
      public function method_322(param1:class_685, param2:int = 0) : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:class_683 = null;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:class_685 = null;
         if(name_49 == null)
         {
            return;
         }
         var _loc3_:Number = Number(0.5 + param1.var_237 / 200);
         var _loc4_:int = int(class_657.method_237(_loc3_));
         if(param1.var_237 == 0)
         {
            _loc4_ = 16777215;
         }
         var _loc5_:int = _loc4_;
         switch(param2)
         {
            case 0:
               _loc7_ = 0;
               while(_loc7_ < 2)
               {
                  _loc7_++;
                  _loc8_ = _loc7_;
                  _loc9_ = param1.var_480[_loc8_];
                  _loc10_ = _loc4_;
                  switch(int(_loc9_.var_295))
                  {
                     case 0:
                        break;
                     case 1:
                        _loc10_ = 0;
                        _loc5_ = 0;
                  }
                  _loc11_ = class_684.var_264[_loc8_];
                  name_49.bitmapData.setPixel(param1.var_244 * 2 + int(_loc11_[0]),param1.var_245 * 2 + int(_loc11_[1]),_loc10_);
                  _loc12_ = param1.var_507[_loc8_];
                  if(_loc12_ == null || _loc12_.var_480[1 - _loc8_] == class_683.var_497)
                  {
                     _loc5_ = 0;
                  }
               }
               name_49.bitmapData.setPixel(param1.var_244 * 2 + 1,param1.var_245 * 2 + 1,_loc5_);
               name_49.bitmapData.setPixel(param1.var_244 * 2,param1.var_245 * 2,_loc4_);
               break;
            case 1:
               _loc7_ = 0;
               if(param1.var_455 != null)
               {
                  _loc7_ = int(class_657.method_237(param1.var_455.var_215 / var_499));
               }
               name_49.bitmapData.fillRect(new Rectangle(param1.var_244 * 2,param1.var_245 * 2,2,2),_loc7_);
         }
      }
      
      public function method_325(param1:int, param2:int) : void
      {
         var _loc5_:class_685 = null;
         var _loc3_:int = 0;
         var _loc4_:Array = var_443;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc5_.var_237 = 0;
            _loc5_.var_508 = 0;
         }
         var_445 = [];
         method_323(var_496[param1][param2]);
         var_303 = method_324;
      }
      
      public function method_317(param1:class_685) : void
      {
         var _loc5_:int = 0;
         var _loc6_:class_685 = null;
         var _loc7_:Boolean = false;
         var _loc8_:class_844 = null;
         var _loc9_:Number = NaN;
         param1.var_508 = 2;
         var _loc2_:Boolean = false;
         var _loc3_:Array = [0,1,2,3];
         class_659.method_241(_loc3_,var_503);
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc3_.length))
         {
            _loc5_ = int(_loc3_[_loc4_]);
            _loc4_++;
            _loc6_ = param1.var_507[_loc5_];
            if(_loc6_ != null)
            {
               switch(_loc6_.var_508)
               {
                  case 0:
                     _loc6_.var_508 = 1;
                     _loc6_.var_455 = param1.var_455;
                     var_445.push(_loc6_);
                     continue;
                  case 1:
                     continue;
                  case 2:
                     _loc7_ = false;
                     if(_loc6_.var_455 != param1.var_455)
                     {
                        _loc7_ = !param1.var_455.method_326(_loc6_.var_455) || int((int(_loc9_) & 1073741823) % var_500) == 0;
                     }
                     else if(!_loc2_)
                     {
                        _loc8_ = var_503;
                        _loc9_ = _loc8_.var_503 * 16807 % 2147483647;
                        _loc8_.var_503 = _loc9_;
                        _loc2_ = int((int(_loc9_) & 1073741823) % var_506) > 0;
                        _loc7_ = true;
                     }
                     if(_loc7_)
                     {
                        param1.method_327(_loc5_);
                        method_322(param1);
                        method_322(_loc6_);
                     }

               }
            }
            else
            {

            }
         }
      }
      
      public function method_320() : void
      {
         var _loc4_:class_686 = null;
         var _loc5_:class_685 = null;
         var _loc1_:Boolean = false;
         var _loc2_:int = 0;
         var _loc3_:Array = var_498;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(int(_loc4_.var_445.length) > 0)
            {
               _loc5_ = _loc4_.method_328();
               method_322(_loc5_,1);
               _loc1_ = true;
            }
         }
         if(!_loc1_)
         {
            var_302 = true;
            var_303 = null;
         }
      }
      
      public function method_324() : void
      {
         var _loc2_:int = 0;
         var _loc1_:int = 0;
         while(_loc1_ < 30)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            method_323(name_1());
            if(int(var_445.length) == 0)
            {
               var_303 = null;
               var_302 = true;
               break;
            }
         }
      }
      
      public function method_318() : void
      {
         var _loc2_:int = 0;
         var _loc1_:int = 0;
         while(_loc1_ < 30)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            method_317(name_1(1 - var_502));
            if(int(var_445.length) == 0)
            {
               var_303 = null;
               method_325(int(var_390.var_244),int(var_390.var_245));
               break;
            }
         }
      }
   }
}
