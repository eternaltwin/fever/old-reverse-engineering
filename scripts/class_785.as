package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.filters.GlowFilter;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_785 extends class_645
   {
      
      public static var var_925:int = 11;
      
      public static var var_926:int = 10;
       
      
      public var var_932:Array;
      
      public var var_104:class_892;
      
      public var var_928:MovieClip;
      
      public var var_927:Number;
      
      public var var_929:Number;
      
      public var var_581:Number;
      
      public var var_230:Bitmap;
      
      public var var_930:Array;
      
      public var var_931:class_755;
      
      public function class_785()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_693() : void
      {
         var_230.bitmapData.fillRect(var_230.bitmapData.rect,0);
         var _loc1_:Matrix = new Matrix();
         _loc1_.scale(var_927,var_927);
         var_230.bitmapData.draw(var_928,_loc1_);
         var _loc2_:GlowFilter = new GlowFilter();
         _loc2_.blurX = 2;
         _loc2_.blurY = 2;
         _loc2_.strength = var_929 * 4;
         _loc2_.color = 16777215;
         var_230.bitmapData.applyFilter(var_230.bitmapData,var_230.bitmapData.rect,new Point(0,0),_loc2_);
         var _loc3_:Number = Number(8 + var_929 * 8);
         var _loc4_:GlowFilter = new GlowFilter();
         _loc4_.blurX = _loc3_;
         _loc4_.blurY = _loc3_;
         _loc4_.strength = 1;
         _loc4_.color = 16777215;
         var_230.bitmapData.applyFilter(var_230.bitmapData,var_230.bitmapData.rect,new Point(0,0),_loc4_);
      }
      
      override public function method_79() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:MovieClip = null;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:class_892 = null;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:int = 0;
         super.method_79();
         var_929 = var_929 * 0.5;
         method_372();
         var _loc1_:int = 0;
         var _loc2_:Array = var_930;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.var_244 > Number(class_742.var_216 + Number(_loc3_.var_66)))
            {
               _loc3_.var_244 = _loc3_.var_244 - (Number(class_742.var_216 + _loc3_.var_66 * 2));
            }
            if(_loc3_.var_244 < -_loc3_.var_66)
            {
               _loc3_.var_244 = Number(_loc3_.var_244 + (Number(class_742.var_216 + _loc3_.var_66 * 2)));
            }
            if(_loc3_.var_245 > Number(class_742.var_218 + Number(_loc3_.var_66)))
            {
               _loc3_.var_245 = _loc3_.var_245 - (Number(class_742.var_218 + _loc3_.var_66 * 2));
            }
            if(_loc3_.var_245 < -_loc3_.var_66)
            {
               _loc3_.var_245 = Number(_loc3_.var_245 + (Number(class_742.var_218 + _loc3_.var_66 * 2)));
            }
            if(var_104 != null && Number(_loc3_.method_115(var_104)) < Number(8 + Number(_loc3_.var_66)))
            {
               _loc4_ = var_931.method_84(class_899.__unprotect__("\'O1;\x03"),1);
               _loc4_.x = var_104.var_244;
               _loc4_.y = var_104.var_245;
               var_104.method_89();
               var_104 = null;
               method_81(false,20);
            }
         }
         _loc2_ = var_930.method_78();
         var _loc5_:Array = var_932.method_78();
         _loc1_ = 0;
         while(_loc1_ < int(_loc5_.length))
         {
            _loc3_ = _loc5_[_loc1_];
            _loc1_++;
            _loc6_ = false;
            _loc7_ = 0;
            while(_loc7_ < int(_loc2_.length))
            {
               _loc8_ = _loc2_[_loc7_];
               _loc7_++;
               _loc9_ = _loc8_.var_244 - _loc3_.var_244;
               _loc10_ = _loc8_.var_245 - _loc3_.var_245;
               if(Number(Math.sqrt(Number(_loc9_ * _loc9_ + _loc10_ * _loc10_))) < Number(_loc8_.var_66))
               {
                  if(int(_loc8_.var_307) > 0)
                  {
                     _loc11_ = Number(Math.atan2(_loc8_.var_279,_loc8_.var_278));
                     _loc12_ = 0;
                     while(_loc12_ < 2)
                     {
                        _loc12_++;
                        _loc13_ = _loc12_;
                        _loc14_ = method_694(int(_loc8_.var_307) - 1);
                        _loc15_ = Number(_loc11_ + 1.57 * (_loc13_ * 2 - 1));
                        _loc16_ = _loc8_.var_66 * 0.5;
                        _loc14_.var_244 = Number(_loc8_.var_244 + Math.cos(_loc15_) * _loc16_);
                        _loc14_.var_245 = Number(_loc8_.var_245 + Math.sin(_loc15_) * _loc16_);
                        _loc17_ = Number(Math.sqrt(Number(_loc8_.var_278 * _loc8_.var_278 + _loc8_.var_279 * _loc8_.var_279)));
                        _loc14_.var_278 = Math.cos(_loc15_) * _loc17_;
                        _loc14_.var_279 = Math.sin(_loc15_) * _loc17_;
                        _loc14_.method_118();
                     }
                  }
                  _loc12_ = 8;
                  _loc13_ = 0;
                  while(_loc13_ < _loc12_)
                  {
                     _loc13_++;
                     _loc18_ = _loc13_;
                     _loc14_ = new class_892(var_931.method_84(class_899.__unprotect__("~\x10#s\x03"),1));
                     _loc11_ = (_loc18_ + Number(Math.random())) / _loc12_ * 6.28;
                     _loc15_ = Math.random() * 4;
                     _loc16_ = 2.5;
                     _loc14_.var_278 = Number(Math.cos(_loc11_) * _loc15_ + _loc8_.var_278 * 0.5);
                     _loc14_.var_279 = Number(Math.sin(_loc11_) * _loc15_ + _loc8_.var_279 * 0.5);
                     _loc14_.var_244 = Number(_loc8_.var_244 + _loc14_.var_278 * _loc16_);
                     _loc14_.var_245 = Number(_loc8_.var_245 + _loc14_.var_279 * _loc16_);
                     _loc14_.var_251 = 10 + int(class_691.method_114(20));
                     _loc14_.var_243.gotoAndStop(int(class_691.method_114(_loc14_.var_243.totalFrames)) + 1);
                     _loc14_.var_243.rotation = Math.random() * 360;
                     _loc14_.var_580 = (Math.random() * 2 - 1) * 12;
                     _loc14_.var_687 = 0.98;
                     _loc14_.var_233 = 0.97;
                     _loc14_.var_272 = 0;
                     _loc14_.method_118();
                  }
                  var_930.method_116(_loc8_);
                  _loc8_.method_89();
                  _loc6_ = true;
                  var_929 = 1;
                  break;
               }
            }
            _loc7_ = 10;
            if(_loc3_.var_244 > class_742.var_216 + _loc7_ || _loc3_.var_244 < -_loc7_ || _loc3_.var_245 > class_742.var_218 + _loc7_ || _loc3_.var_245 < -_loc7_ || _loc6_)
            {
               _loc3_.method_89();
               var_932.method_116(_loc3_);
            }
         }
         if(int(var_930.length) == 0)
         {
            method_81(true,20);
         }
         method_693();
      }
      
      public function method_372() : void
      {
         var _loc7_:MovieClip = null;
         var _loc8_:class_892 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:int = 0;
         if(var_104 == null)
         {
            return;
         }
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - var_104.var_244;
         var _loc3_:Number = _loc1_.var_245 - var_104.var_245;
         var _loc4_:Number = Number(Math.atan2(_loc3_,_loc2_));
         var _loc5_:Number = Number(class_663.method_112(_loc4_ - var_104.var_248,3.14));
         var_104.var_248 = Number(Number(var_104.var_248) + Number(class_663.method_99(-0.4,_loc5_ * 0.3,0.4)));
         var_104.var_243.rotation = var_104.var_248 / 0.0174;
         if(var_581 > 0)
         {
            var_581 = var_581 - 1;
         }
         _loc7_ = var_104.var_243.var_1;
         _loc7_.visible = false;
         if(!name_5)
         {
            if(var_581 == 0)
            {
               _loc8_ = new class_892(var_931.method_84(class_899.__unprotect__("\x0fv;U\x01"),1));
               _loc9_ = Number(Math.cos(Number(var_104.var_248)));
               _loc10_ = Number(Math.sin(Number(var_104.var_248)));
               _loc11_ = class_785.var_925;
               _loc8_.var_244 = Number(var_104.var_244 + _loc9_ * 5);
               _loc8_.var_245 = Number(var_104.var_245 + _loc10_ * 5);
               _loc8_.var_278 = Number(_loc9_ * _loc11_ + var_104.var_278);
               _loc8_.var_279 = Number(_loc10_ * _loc11_ + var_104.var_279);
               _loc8_.method_118();
               var_581 = class_785.var_926;
               var_932.push(_loc8_);
            }
         }
         else
         {
            _loc9_ = 0.5;
            if(Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_))) > 30)
            {
               _loc7_ = var_104.var_243.var_1;
               _loc7_.visible = true;
               var_104.var_278 = Number(var_104.var_278 + Math.cos(Number(var_104.var_248)) * _loc9_);
               var_104.var_279 = Number(var_104.var_279 + Math.sin(Number(var_104.var_248)) * _loc9_);
            }
         }
         _loc9_ = 0.96;
         var_104.var_278 = var_104.var_278 * _loc9_;
         var_104.var_279 = var_104.var_279 * _loc9_;
      }
      
      public function method_695() : void
      {
         var_928 = var_232.method_92(0);
         var_931 = new class_755(var_928);
         var_928.visible = false;
         var_927 = 0.5;
         var _loc1_:int = int(Math.ceil(class_742.var_216 * var_927));
         var _loc2_:int = int(Math.ceil(class_742.var_218 * var_927));
         var_230 = new Bitmap();
         var_230.bitmapData = new BitmapData(_loc1_,_loc2_,true,0);
         var _loc3_:MovieClip = var_232.method_92(0);
         _loc3_.addChild(var_230);
         _loc3_.scaleY = Number(2);
         _loc3_.scaleX = 2;
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var_227 = [400];
         super.method_95(param1);
         var_232.method_84(class_899.__unprotect__("\\/p!\x01"),0);
         method_695();
         var_929 = 0;
         var_104 = new class_892(var_931.method_84(class_899.__unprotect__("\'6%\x04"),1));
         var_104.var_244 = class_742.var_216 * 0.5;
         var_104.var_245 = class_742.var_218 * 0.5;
         var_104.var_248 = 0;
         var_104.method_118();
         var_581 = 0;
         var _loc2_:Number = Number(2 + param1 * 8);
         var_930 = [];
         while(_loc2_ >= 1)
         {
            _loc3_ = int(class_691.method_114(3));
            while(_loc2_ < Number(Math.pow(2,_loc3_)))
            {
               _loc3_--;
            }
            _loc2_ = _loc2_ - Math.pow(2,_loc3_);
            _loc4_ = method_694(_loc3_);
            _loc4_.var_244 = Math.random() * class_742.var_216;
            _loc4_.var_245 = Math.random() * class_742.var_218;
            _loc5_ = Number(Number(1 + param1) + Number(Math.random()));
            _loc6_ = Math.random() * 6.28;
            _loc4_.var_278 = Math.cos(_loc6_) * _loc5_;
            _loc4_.var_279 = Math.sin(_loc6_) * _loc5_;
            switch(int(class_691.method_114(4)))
            {
               case 0:
                  _loc4_.var_244 = Math.random() * (class_742.var_216 - 2 * _loc4_.var_66);
                  _loc4_.var_245 = -_loc4_.var_66;
                  continue;
               case 1:
                  _loc4_.var_244 = Math.random() * (class_742.var_216 - 2 * _loc4_.var_66);
                  _loc4_.var_245 = Number(class_742.var_218 + Number(_loc4_.var_66));
                  continue;
               case 2:
                  _loc4_.var_244 = -_loc4_.var_66;
                  _loc4_.var_245 = Math.random() * (class_742.var_218 - 2 * _loc4_.var_66);
                  continue;
               case 3:
                  _loc4_.var_244 = Number(class_742.var_216 + Number(_loc4_.var_66));
                  _loc4_.var_245 = Math.random() * (class_742.var_218 - 2 * _loc4_.var_66);

            }
         }
         var_932 = [];
         method_693();
      }
      
      public function method_694(param1:int) : class_892
      {
         var _loc2_:class_892 = new class_892(var_931.method_84(class_899.__unprotect__("\x14\x0fm@\x02"),1));
         _loc2_.var_307 = param1;
         _loc2_.var_66 = int([15,25,40][int(_loc2_.var_307)]);
         _loc2_.method_119(_loc2_.var_66 * 0.02);
         _loc2_.var_580 = (Math.random() * 2 - 1) * 3;
         var_930.push(_loc2_);
         return _loc2_;
      }
   }
}
