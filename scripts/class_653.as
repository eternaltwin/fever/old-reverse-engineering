package
{
   import flash.geom.Point;
   import package_32.class_899;
   
   public class class_653
   {
       
      
      public var var_330:Array;
      
      public function class_653(param1:Array = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(param1 != null)
         {
            var_330 = param1;
         }
         else
         {
            var_330 = [];
         }
      }
      
      public function method_210(param1:Number, param2:Number = 1.0, param3:Boolean = undefined) : Point
      {
         var _loc4_:Point = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Point = null;
         if(param3)
         {
            param2 = param2 >= 0?1:-1;
            _loc4_ = var_330[0];
            _loc5_ = 0;
            _loc6_ = int(var_330.length);
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = var_330[_loc7_];
               if(param2 > 0)
               {
                  if(_loc8_.x <= param1)
                  {
                     _loc4_ = _loc8_;
                     continue;
                  }
                  return _loc4_;
               }
               if(_loc8_.x >= param1)
               {
                  return _loc8_;
               }
               _loc4_ = _loc8_;
            }
            return null;
         }
         return null;
      }
      
      public function method_213(param1:Number, param2:Number, param3:Number) : Object
      {
         var _loc4_:Point = method_211(param1,param3,true);
         if(_loc4_ == null)
         {
            return null;
         }
         var _loc5_:Point = method_210(param1,param3,true);
         var _loc6_:Point = new Point(param1,param2);
         var _loc7_:Number = _loc4_.y - _loc5_.y;
         var _loc8_:Number = _loc4_.x - _loc5_.x;
         var _loc9_:Number = _loc7_ / _loc8_;
         var _loc10_:Number = _loc4_.y - _loc6_.y;
         var _loc11_:Number = Number(_loc5_.y + _loc9_ * (_loc6_.x - _loc5_.x));
         var _loc12_:Number = Number(method_212(_loc5_,_loc4_,param3));
         return {
            "\x03\x01":_loc11_,
            "jvD}\x01":_loc12_
         };
      }
      
      public function method_211(param1:Number, param2:Number = 1.0, param3:Boolean = undefined) : Point
      {
         var _loc4_:Point = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Point = null;
         if(param3)
         {
            param2 = param2 >= 0?1:-1;
            _loc4_ = null;
            _loc5_ = 0;
            _loc6_ = int(var_330.length);
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = var_330[_loc7_];
               if(param2 > 0)
               {
                  if(_loc8_.x >= param1)
                  {
                     return _loc8_;
                  }
               }
               else if(_loc8_.x <= param1)
               {
                  _loc4_ = _loc8_;
               }
            }
            return _loc4_;
         }
         return null;
      }
      
      public function method_212(param1:Point, param2:Point, param3:Number) : Number
      {
         param3 = param3 >= 0?1:-1;
         if(param3 > 0)
         {
            return Number(class_649.method_146(Number(Math.atan2(param2.y - param1.y,param2.x - param1.x))));
         }
         return Number(class_649.method_146(Number(Math.atan2(param1.y - param2.y,param1.x - param2.x))));
      }
      
      public function method_214(param1:Number, param2:Number) : void
      {
         var_330.push(new Point(param1,param2));
      }
   }
}
