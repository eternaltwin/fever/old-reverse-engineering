package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_32.class_899;
   
   public class class_745 extends class_645
   {
       
      
      public var var_758:class_656;
      
      public var iUkn:Array;
      
      public var var_658:Boolean;
      
      public var var_762:int;
      
      public var var_760:Number;
      
      public var var_761:Object;
      
      public var var_699:Array;
      
      public function class_745()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:MovieClip = null;
         loop1:
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(var_658)
               {
                  _loc1_ = method_100();
                  var_758.method_229({
                     "L\x01":Number(_loc1_.var_244),
                     "\x03\x01":Number(_loc1_.var_245)
                  },0.5,null);
               }
               _loc2_ = 0;
               _loc3_ = var_699;
               while(true)
               {
                  if(_loc2_ >= int(_loc3_.length))
                  {
                     break loop1;
                  }
                  _loc1_ = _loc3_[_loc2_];
                  _loc2_++;
                  if(_loc1_.var_759 != null)
                  {
                     _loc4_ = _loc1_.var_759.var_12;
                     if(_loc4_ != null && _loc1_.var_37 != null)
                     {
                        if(_loc4_.y > 0)
                        {
                           _loc4_.y = _loc4_.y * var_760;
                           if(_loc4_.y < 1)
                           {
                              _loc4_.y = 0;
                           }
                        }
                        else if(_loc1_.var_37 > 0)
                        {
                           _loc1_.var_37 = _loc1_.var_37 - 1;
                        }
                        else
                        {
                           _loc1_.var_37 = null;
                           _loc4_.mouseEnabled = false;
                        }
                     }
                     else
                     {
                        _loc4_.y = Number(_loc4_.y + (Number(20 + var_237[0] * 20)));
                        if(_loc4_.y > 100)
                        {
                           method_494(_loc1_);
                           method_495(_loc1_);
                        }
                     }
                  }
               }
         }
         if(var_761 != null && _loc1_ < 0)
         {
            var_761 = null;
            method_496();
         }
         super.method_79();
      }
      
      public function method_496() : void
      {
         if(var_220 != null)
         {
            return;
         }
         var_658 = true;
         var_758.var_243.visible = true;
         var_758.var_243.gotoAndPlay("2");
      }
      
      override public function method_83() : void
      {
         var_762 = var_762 + 1;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_762 = 0;
         var_760 = 0.3;
         var_658 = true;
         method_117();
         method_72();
      }
      
      public function method_494(param1:Object) : void
      {
         var _loc3_:MovieClip = Reflect.field(param1.var_759,class_899.__unprotect__("r\x01"));
         var _loc2_:MovieClip = _loc3_;
         _loc2_.visible = false;
         iUkn.push(param1.var_759);
         _loc2_.mouseEnabled = false;
         param1.var_759 = null;
      }
      
      public function method_495(param1:Object) : void
      {
         var _loc2_:int = int(class_691.method_114(int(iUkn.length)));
         var _loc3_:MovieClip = iUkn[_loc2_];
         iUkn.splice(_loc2_,1);
         var _loc4_:MovieClip = _loc3_.var_12;
         _loc4_.visible = true;
         _loc4_.y = 100;
         _loc4_.gotoAndStop(int(param1.var_430));
         _loc4_.mouseEnabled = true;
         param1.var_759 = _loc3_;
         param1.var_37 = int(class_691.method_114(int(Math.round(Number(15 + 80 * (1 - var_237[0]))))));
      }
      
      public function ZyaO(param1:MovieClip) : void
      {
         var _loc5_:* = null;
         var _loc6_:MovieClip = null;
         var _loc2_:* = null;
         var _loc3_:int = 0;
         var _loc4_:Array = var_699;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc5_.var_759 != null && _loc6_ == param1)
            {
               _loc2_ = _loc5_;
               break;
            }
         }
         if(var_658)
         {
            var_658 = false;
            var_758.var_244 = _loc2_.var_759.x;
            var_758.var_245 = _loc2_.var_759.y;
            var_758.var_243.visible = false;
            _loc6_ = Reflect.field(_loc2_.var_759,class_899.__unprotect__("\x0b\x01"));
            _loc6_.gotoAndPlay("2");
            _loc6_ = Reflect.field(_loc2_.var_759,class_899.__unprotect__("\x0b\x01"));
            _loc6_.visible = true;
            _loc5_ = _loc2_.var_759.var_12;
            method_494(_loc2_);
            var_699.method_116(_loc2_);
            if(int(var_699.length) == 0)
            {
               method_81(true,10);
            }
            var_761 = 8;
         }
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:MovieClip = null;
         var _loc5_:int = 0;
         var _loc6_:* = null;
         class_319 = var_232.method_84(class_899.__unprotect__("=hfs\x02"),0);
         class_319["_obj"] = this;
         iUkn = [];
         var _loc1_:int = 0;
         while(_loc1_ < 18)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = Reflect.field(class_319,"$t" + _loc2_);
            _loc4_ = Reflect.field(_loc3_,class_899.__unprotect__("\x0b\x01"));
            _loc4_.visible = false;
            _loc4_ = Reflect.field(_loc3_,class_899.__unprotect__("r\x01"));
            var var_12:Array = [_loc4_];
            var var_214:Array = [this];
            var_12[0].visible = false;
            var_12[0].addEventListener(MouseEvent.CLICK,function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_12:Array = param2;
               return function(param1:*):void
               {
                  var_214[0].ZyaO(var_12[0]);
               };
            }(var_214,var_12));
            iUkn.push(_loc3_);
         }
         var_699 = [];
         _loc1_ = 1 + int(var_237[0] * 8);
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc5_ = _loc2_;
            _loc6_ = {
               "Q29)\x01":null,
               "j\x01":null,
               "\x0eeo\x0b":int(class_691.method_114(8)) + 1
            };
            method_495(_loc6_);
            var_699.push(_loc6_);
         }
         var _loc7_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("\x05ULE\x02"),class_645.var_209));
         var_758 = _loc7_;
         var_758.var_244 = class_742.var_216 * 0.5;
         var_758.var_245 = class_742.var_218 * 0.5;
         var_758.method_118();
      }
   }
}
