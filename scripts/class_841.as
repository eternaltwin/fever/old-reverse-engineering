package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.Graphics;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.Matrix;
   import package_11.package_12.class_659;
   import package_11.package_12.class_663;
   import package_16.class_675;
   import package_16.class_684;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_883;
   import package_8.package_9.class_931;
   
   public class class_841 extends class_645
   {
       
      
      public var var_1173:Sprite;
      
      public var var_1174:BitmapData;
      
      public var name_112:Sprite;
      
      public var var_1098:int;
      
      public var var_1172:int;
      
      public var var_1175:Object;
      
      public var var_204:Array;
      
      public var var_641:int;
      
      public var var_642:int;
      
      public var name_36:Object;
      
      public var var_496:Array;
      
      public var name_13:Object;
      
      public var var_254:Number;
      
      public var var_1176:Array;
      
      public var var_94:Object;
      
      public var var_1103:Array;
      
      public function class_841()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_839() : void
      {
         var _loc6_:Number = NaN;
         var _loc1_:* = method_100();
         var _loc2_:int = var_1098 * 16;
         var _loc3_:Number = name_112.x;
         var _loc4_:Number = name_112.y;
         var _loc5_:int = 0;
         if(var_1172 == 0)
         {
            _loc6_ = _loc1_.var_244 - var_94.var_244;
            _loc5_ = int(Math.round(_loc6_ / 16));
            _loc3_ = _loc5_ * 16;
         }
         else
         {
            _loc6_ = _loc1_.var_245 - var_94.var_245;
            _loc5_ = int(Math.round(_loc6_ / 16));
            _loc4_ = _loc5_ * 16;
         }
         _loc6_ = Number(class_663.method_112(_loc3_ - name_112.x,_loc2_ * 0.5));
         var _loc7_:Number = Number(class_663.method_112(_loc4_ - name_112.y,_loc2_ * 0.5));
         name_112.x = Number(name_112.x + _loc6_ * 0.5);
         name_112.y = Number(name_112.y + _loc7_ * 0.5);
         name_112.x = Number(class_663.method_113(name_112.x,_loc2_));
         name_112.y = Number(class_663.method_113(name_112.y,_loc2_));
         if(!name_5)
         {
            var_1173.parent.removeChild(var_1173);
            var_1174.dispose();
            method_837(var_1172,_loc5_);
            if(method_825())
            {
               method_838();
            }
            else
            {
               name_3 = 1;
            }
         }
      }
      
      public function method_840() : void
      {
         var _loc6_:* = null;
         var _loc7_:* = null;
         var _loc8_:* = null;
         var _loc9_:class_738 = null;
         var _loc10_:class_883 = null;
         var_254 = Number(Math.min(Number(var_254 + 0.1),1));
         var _loc1_:int = 1;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Array = var_1103;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc7_ = var_204[int(_loc6_.name_1)];
            _loc8_ = method_127(Number(int(_loc6_.var_134.var_244) + (int(_loc7_.var_244) - int(_loc6_.var_134.var_244)) * var_254),Number(int(_loc6_.var_134.var_245) + (int(_loc7_.var_245) - int(_loc6_.var_134.var_245)) * var_254));
            _loc6_.var_618.x = Number(Number(_loc8_.var_244) + 8);
            _loc6_.var_618.y = Number(Number(_loc8_.var_245) + 8);
            _loc6_.var_618.method_293();
            if(var_254 == 1)
            {
               _loc6_.var_134 = _loc7_;
               _loc6_.name_1 = int(_loc6_.name_1) + _loc1_;
            }
            _loc2_ = _loc2_ + int(_loc6_.var_618.x * _loc1_);
            _loc3_ = _loc3_ + int(_loc6_.var_618.y * _loc1_);
            _loc1_ = _loc1_ * -1;
         }
         if(var_254 == 1)
         {
            var_254 = 0;
         }
         if(Number(Number(Math.abs(_loc2_)) + Number(Math.abs(_loc3_))) == 0)
         {
            name_3 = name_3 + 1;
            _loc9_ = var_1103[1].var_618;
            method_81(true,20);
            new class_931(_loc9_,0.05);
            _loc10_ = new class_883(8,24,0.05);
            var_232.method_124(_loc10_.var_243,4);
            _loc10_.method_129(_loc9_.x,_loc9_.y);
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               _loc1_ = method_100();
               _loc2_ = _loc1_.var_244 - var_94.var_244;
               _loc3_ = _loc1_.var_245 - var_94.var_245;
               if(Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_))) > 2)
               {
                  var_1172 = int(int(class_742.method_489(Number(Math.atan2(_loc3_,_loc2_)))) % 2);
                  method_841();
               }
               if(!name_5)
               {
                  name_3 = 1;
                  break;
               }
               break;
            case 3:
               method_839();
               break;
            case 4:
               method_840();
         }
      }
      
      public function method_842(param1:Object, param2:* = undefined) : void
      {
         if(name_3 != 1)
         {
            return;
         }
         var_1175 = param1;
         var_94 = method_100();
         name_3 = name_3 + 1;
      }
      
      public function method_837(param1:int, param2:int) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:* = null;
         var _loc8_:* = null;
         if(param1 == 0)
         {
            _loc3_ = 0;
            _loc4_ = var_1098;
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc6_ = var_496[_loc5_][int(var_1175.var_245)];
               _loc6_.var_244 = int(Number(class_663.method_113(int(_loc6_.var_244) + param2,var_1098)));
            }
         }
         else
         {
            _loc3_ = 0;
            _loc4_ = var_1098;
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc6_ = var_496[int(var_1175.var_244)][_loc5_];
               _loc6_.var_245 = int(Number(class_663.method_113(int(_loc6_.var_245) + param2,var_1098)));
            }
         }
         _loc3_ = 0;
         var _loc7_:Array = var_1176;
         while(_loc3_ < int(_loc7_.length))
         {
            _loc6_ = _loc7_[_loc3_];
            _loc3_++;
            var_496[int(_loc6_.var_244)][int(_loc6_.var_245)] = _loc6_;
            _loc8_ = method_127(int(_loc6_.var_244),int(_loc6_.var_245));
            _loc6_.var_618.x = Number(_loc8_.var_244);
            _loc6_.var_618.y = Number(_loc8_.var_245);
         }
      }
      
      public function method_843(param1:int, param2:int) : Object
      {
         var _loc3_:* = var_496[param1][param2];
         _loc3_.class_318 = new class_738();
         _loc3_.class_318.method_128(class_702.var_571.method_98(null,"laby_slide_ball"));
         _loc3_.class_318.y = Number(8);
         _loc3_.class_318.x = 8;
         _loc3_.var_618.addChild(_loc3_.class_318);
         return _loc3_;
      }
      
      public function method_782(param1:Object) : void
      {
         var _loc4_:* = null;
         var _loc2_:Array = method_711(param1);
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            if(int(_loc4_.var_294) == -1)
            {
               _loc4_.var_294 = int(param1.var_294) + 1;
               method_782(_loc4_);
            }
         }
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < var_1098 && param2 >= 0 && param2 < var_1098;
      }
      
      public function method_841() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Bitmap = null;
         var _loc1_:* = method_844(var_1172);
         if(_loc1_ != null)
         {
            new class_931(_loc1_.var_618,0.1,16711680);
            name_3 = 1;
            return;
         }
         name_3 = name_3 + 1;
         var_1173 = new Sprite();
         name_112 = new Sprite();
         if(var_1172 == 0)
         {
            var_1174 = new BitmapData(var_1098 * 16,16,false,16711680);
            _loc2_ = 0;
            while(_loc2_ < 2)
            {
               _loc2_++;
               _loc3_ = _loc2_;
               _loc4_ = new Bitmap(var_1174);
               _loc4_.x = -_loc3_ * var_1098 * 16;
               name_112.addChild(_loc4_);
            }
            var_1173.x = var_642;
            var_1173.y = var_1175.var_618.y;
         }
         else
         {
            var_1174 = new BitmapData(16,var_1098 * 16,false,16711680);
            _loc2_ = 0;
            while(_loc2_ < 2)
            {
               _loc2_++;
               _loc3_ = _loc2_;
               _loc4_ = new Bitmap(var_1174);
               _loc4_.y = -_loc3_ * var_1098 * 16;
               name_112.addChild(_loc4_);
            }
            var_1173.x = var_1175.var_618.x;
            var_1173.y = var_641;
         }
         var _loc5_:Matrix = new Matrix();
         _loc5_.translate(-var_1173.x,-var_1173.y);
         var_1174.draw(var_201,_loc5_);
         var _loc6_:Sprite = new Sprite();
         name_112.mask = _loc6_;
         _loc6_.graphics.beginFill(255);
         _loc6_.graphics.drawRect(0,0,var_1174.width,var_1174.height);
         var_1173.addChild(_loc6_);
         var_232.method_124(var_1173,2);
         var_1173.addChild(name_112);
      }
      
      public function method_838() : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         var _loc6_:class_738 = null;
         var_204 = [];
         name_3 = 4;
         var _loc1_:* = name_36;
         do
         {
            var_204.push(_loc1_);
            _loc2_ = method_711(_loc1_);
            _loc3_ = 0;
            while(_loc3_ < int(_loc2_.length))
            {
               _loc4_ = _loc2_[_loc3_];
               _loc3_++;
               if(int(_loc4_.var_294) < int(_loc1_.var_294))
               {
                  _loc1_ = _loc4_;
               }
            }
         }
         while(int(_loc1_.var_294) != 0);
         
         var_204.push(_loc1_);
         var_1103 = [];
         _loc2_ = [name_36,name_13];
         _loc3_ = 0;
         while(_loc3_ < 2)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc4_ = _loc2_[_loc5_];
            _loc6_ = _loc4_.class_318;
            var_232.method_124(_loc6_,2);
            _loc6_.x = Number(_loc4_.var_618.x + 8);
            _loc6_.y = Number(_loc4_.var_618.y + 8);
            var_1103.push({
               "\x01^\x01":_loc6_,
               "h\x10\x01":_loc4_,
               "?\x05\x10;\x01":_loc5_ * (int(var_204.length) - 1)
            });
         }
         var_254 = 1;
         var_229 = [true];
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:* = null;
         var _loc13_:class_738 = null;
         var _loc14_:* = null;
         var _loc15_:* = null;
         var _loc16_:Array = null;
         var_227 = [600];
         super.method_95(param1);
         var_1098 = 5;
         var_201.scaleY = Number(2);
         var_201.scaleX = 2;
         var_642 = int((class_742.var_216 * 0.5 - var_1098 * 16) * 0.5);
         var_641 = int((class_742.var_218 * 0.5 - var_1098 * 16) * 0.5);
         var _loc3_:class_684 = new class_684(var_1098,var_1098,int(class_691.method_114(400)));
         _loc3_.var_502 = 0;
         _loc3_.name_50();
         var _loc4_:class_675 = new class_675(_loc3_);
         var _loc5_:Graphics = var_201.graphics;
         _loc5_.beginFill(4465169);
         _loc5_.drawRect(0,0,class_742.var_216 >> 1,class_742.var_218 >> 1);
         _loc5_.endFill();
         _loc5_.beginFill(3346688);
         _loc5_.drawRect(var_642,var_641,(class_742.var_216 >> 1) - 2 * var_642,(class_742.var_218 >> 1) - 2 * var_641);
         _loc5_.endFill();
         var_496 = [];
         var_1176 = [];
         var _loc6_:int = 0;
         _loc7_ = var_1098;
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            var_496[_loc8_] = [];
            _loc9_ = 0;
            _loc10_ = var_1098;
            while(_loc9_ < _loc10_)
            {
               _loc9_++;
               _loc11_ = _loc9_;
               _loc12_ = _loc4_.method_285(_loc8_,_loc11_);
               _loc13_ = new class_738();
               _loc14_ = method_127(_loc8_,_loc11_);
               _loc13_.x = Number(_loc14_.var_244);
               _loc13_.y = Number(_loc14_.var_245);
               var_232.method_124(_loc13_,1);
               _loc15_ = {
                  "\x01^\x01":_loc13_,
                  "\x04);[":_loc12_.var_480,
                  "(SX$\x01":null,
                  "L\x01":_loc8_,
                  "\x03\x01":_loc11_,
                  "ejh\x01":0
               };
               var_496[_loc8_][_loc11_] = _loc15_;
               var_1176.push(_loc15_);
               _loc13_.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:Function, param2:Object):Function
               {
                  var var_27:Function = param1;
                  var var_18:Object = param2;
                  return function(param1:* = undefined):void
                  {
                     return var_27(var_18,param1);
                  };
               }(method_842,_loc15_));
            }
         }
         _loc6_ = int((1 - param1) * 24);
         while(_loc6_ > 0)
         {
            _loc12_ = var_1176[int(class_691.method_114(int(var_1176.length)))];
            _loc16_ = [0,1,2,4];
            class_659.method_241(_loc16_);
            _loc7_ = 0;
            while(_loc7_ < int(_loc16_.length))
            {
               _loc8_ = int(_loc16_[_loc7_]);
               _loc7_++;
               if(_loc12_.var_480[_loc8_])
               {
                  _loc12_.var_480[_loc8_] = false;
                  _loc6_--;
                  break;
               }
            }
         }
         _loc7_ = 0;
         _loc16_ = var_1176;
         while(_loc7_ < int(_loc16_.length))
         {
            _loc12_ = _loc16_[_loc7_];
            _loc7_++;
            method_845(_loc12_);
         }
         name_13 = method_843(0,0);
         name_36 = method_843(var_1098 - 1,var_1098 - 1);
         _loc7_ = 10;
         while(_loc7_ > 0)
         {
            var_1175 = var_1176[int(class_691.method_114(int(var_1176.length)))];
            _loc8_ = int(class_691.method_114(2));
            if(method_844(_loc8_) == null)
            {
               method_837(_loc8_,int(class_691.method_114(var_1098)));
               _loc7_--;
            }
            if(method_825())
            {
               _loc7_++;
            }
         }
      }
      
      public function method_127(param1:Number, param2:Number) : Object
      {
         return {
            "L\x01":Number(var_642 + param1 * 16),
            "\x03\x01":Number(var_641 + param2 * 16)
         };
      }
      
      public function method_711(param1:Object) : Array
      {
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         while(_loc3_ < 4)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            if(!param1.var_480[_loc4_])
            {
               _loc5_ = class_742.var_264[_loc4_];
               _loc6_ = int(param1.var_244) + int(_loc5_[0]);
               _loc7_ = int(param1.var_245) + int(_loc5_[1]);
               if(method_222(_loc6_,_loc7_))
               {
                  _loc8_ = var_496[_loc6_][_loc7_];
                  if(!_loc8_.var_480[int((_loc4_ + 2) % 4)])
                  {
                     _loc2_.push(_loc8_);
                  }
               }
            }
         }
         return _loc2_;
      }
      
      public function method_846(param1:int) : Array
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc2_:Array = [];
         if(param1 == 0)
         {
            _loc3_ = 0;
            _loc4_ = var_1098;
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc2_.push(var_496[_loc5_][int(var_1175.var_245)]);
            }
         }
         else
         {
            _loc3_ = 0;
            _loc4_ = var_1098;
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc2_.push(var_496[int(var_1175.var_244)][_loc5_]);
            }
         }
         return _loc2_;
      }
      
      public function method_844(param1:int) : Object
      {
         var _loc4_:* = null;
         var _loc2_:Array = method_846(param1);
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            if(_loc4_.class_318 != null)
            {
               return _loc4_;
            }
         }
         return null;
      }
      
      public function method_845(param1:Object) : void
      {
         var _loc4_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < 4)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            if(!param1.var_480[_loc4_])
            {
               _loc2_ = _loc2_ + int(Number(Math.pow(2,_loc4_)));
            }
         }
         param1.var_618.method_128(class_702.var_571.method_98(_loc2_,"laby_slide_tiles"),0,0);
      }
      
      public function method_825() : Boolean
      {
         var _loc3_:* = null;
         name_3 = 1;
         var _loc1_:int = 0;
         var _loc2_:Array = var_1176;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_294 = -1;
         }
         name_13.var_294 = 0;
         method_782(name_13);
         return int(name_36.var_294) >= 0;
      }
   }
}
