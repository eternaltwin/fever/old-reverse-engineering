package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.filters.DropShadowFilter;
   import flash.geom.Matrix;
   import package_11.package_12.class_657;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_762 extends class_645
   {
       
      
      public var var_813:MovieClip;
      
      public var var_486:MovieClip;
      
      public var var_351:int;
      
      public var var_815:Array;
      
      public var var_814:Array;
      
      public var var_363:MovieClip;
      
      public var var_462:int;
      
      public var name_28:int;
      
      public var var_230:BitmapData;
      
      public var var_816:int;
      
      public function class_762()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:int = 0;
         var _loc10_:Array = null;
         var _loc11_:MovieClip = null;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:class_892 = null;
         var _loc15_:int = 0;
         var _loc16_:DropShadowFilter = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = Number(method_100().var_244);
               _loc3_ = var_363.var_1;
               _loc2_ = _loc1_ - _loc3_.x;
               _loc3_ = var_363.var_1;
               _loc3_.x = _loc1_;
               _loc4_ = 0;
               _loc5_ = var_814;
               while(_loc4_ < int(_loc5_.length))
               {
                  _loc3_ = _loc5_[_loc4_];
                  _loc4_++;
                  _loc3_.name_75 = Number(Number(_loc3_.name_75) + _loc2_);
                  _loc6_ = Number(Math.atan2(_loc3_.name_76 - _loc3_.y,_loc3_.name_75 - _loc3_.x));
                  _loc7_ = Math.sin(_loc6_) * _loc3_.var_351;
                  _loc8_ = Math.cos(_loc6_) * _loc3_.var_351 * 4;
                  _loc3_.y = _loc3_.y - _loc7_;
                  _loc3_.x = _loc3_.x - _loc8_;
                  _loc3_.x = _loc3_.x;
                  _loc3_.y = _loc3_.y;
                  _loc3_.name_76 = _loc3_.name_76 - _loc7_;
                  method_152(_loc3_.x,_loc3_.y,_loc8_,_loc7_);
                  if(Number(_loc3_.var_351) < 0 && Number(_loc3_.y + _loc3_.height) < 0)
                  {
                     var_814.method_116(_loc3_);
                     _loc3_.parent.removeChild(_loc3_);
                  }
               }
               _loc4_ = 0;
               _loc5_ = var_815;
               while(_loc4_ < int(_loc5_.length))
               {
                  _loc3_ = _loc5_[_loc4_];
                  _loc4_++;
                  _loc3_.name_75 = _loc3_.name_75 - _loc2_;
                  _loc6_ = Number(Math.atan2(_loc3_.name_76 - _loc3_.y,_loc3_.name_75 - _loc3_.x));
                  _loc7_ = Math.sin(_loc6_) * _loc3_.var_351;
                  _loc8_ = Math.cos(_loc6_) * _loc3_.var_351 * 4;
                  _loc3_.y = Number(_loc3_.y + _loc7_);
                  _loc3_.x = Number(_loc3_.x + _loc8_);
                  _loc3_.x = _loc3_.x;
                  _loc3_.y = _loc3_.y;
                  _loc3_.name_76 = Number(Number(_loc3_.name_76) + _loc7_);
                  method_152(_loc3_.x,_loc3_.y,_loc8_,_loc7_,true);
                  if(Number(_loc3_.var_351) > 0 && Number(_loc3_.y + _loc3_.height) > class_742.var_218)
                  {
                     var_814.method_116(_loc3_);
                     if(_loc3_.parent != null)
                     {
                        _loc3_.parent.removeChild(_loc3_);
                     }
                  }
                  else if(!(_loc3_.y >= 380 || _loc3_.y <= 20))
                  {
                     _loc9_ = 0;
                     _loc10_ = var_814;
                     while(_loc9_ < int(_loc10_.length))
                     {
                        _loc11_ = _loc10_[_loc9_];
                        _loc9_++;
                        if(!(_loc11_.y >= 380 || _loc11_.y <= 20))
                        {
                           _loc12_ = _loc3_.x - _loc11_.x;
                           _loc13_ = _loc3_.y - _loc11_.y;
                           if(Number(Math.sqrt(Number(_loc12_ * _loc12_ + _loc13_ * _loc13_))) < _loc11_.width)
                           {
                              method_102(4);
                              new class_931(class_319);
                              var_462 = var_816;
                              method_262(_loc3_.x,_loc3_.y);
                              method_262(_loc11_.x,_loc11_.y);
                              _loc14_ = new class_892(int(class_691.method_114(2)) == 0?_loc11_:_loc3_);
                              _loc14_.var_817 = 2;
                              _loc14_.var_272 = 4;
                              _loc14_.var_251 = 6;
                              method_81(false,20);
                              name_3 = 3;
                           }
                        }
                     }
                  }
               }
               _loc4_ = var_462;
               var_462 = var_462 - 1;
               if(_loc4_ <= 0 && var_227[0] > 80)
               {
                  _loc4_ = 0;
                  _loc9_ = name_28;
                  while(_loc4_ < _loc9_)
                  {
                     _loc4_++;
                     _loc15_ = _loc4_;
                     _loc3_ = var_232.method_84(class_899.__unprotect__("mQ\'}\x02"),4);
                     _loc3_.gotoAndStop(1);
                     _loc3_.y = Number(class_742.var_218 + _loc3_.height);
                     _loc6_ = int(class_691.method_114(class_742.var_216));
                     _loc3_.name_75 = _loc6_;
                     _loc3_.x = _loc6_;
                     _loc3_.y = _loc3_.y;
                     _loc3_.x = _loc3_.x;
                     _loc3_.name_76 = 200;
                     _loc3_.var_351 = -var_351;
                     _loc16_ = new DropShadowFilter(2,45,0,90,2,2,1,2);
                     _loc3_.filters = [_loc16_];
                     var_814.push(_loc3_);
                     _loc11_ = var_232.method_84(class_899.__unprotect__("mQ\'}\x02"),4);
                     _loc11_.gotoAndStop(2);
                     _loc11_.y = -_loc11_.height;
                     _loc6_ = int(class_691.method_114(class_742.var_216));
                     _loc11_.name_75 = _loc6_;
                     _loc11_.x = _loc6_;
                     _loc11_.y = _loc11_.y;
                     _loc11_.x = _loc11_.x;
                     _loc11_.name_76 = 200;
                     _loc11_.var_351 = var_351;
                     _loc11_.filters = [_loc16_];
                     var_815.push(_loc11_);
                  }
                  var_462 = var_816;
                  break;
               }
         }
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [350];
         super.method_95(param1);
         var_816 = int(Math.ceil(Number(Math.max(10,70 - 25 * param1))));
         var_462 = 10;
         name_28 = 2;
         var_351 = 2;
         var_814 = [];
         var_815 = [];
         method_117();
      }
      
      public function method_152(param1:Number, param2:Number, param3:Number, param4:Number, param5:Boolean = false) : void
      {
         var _loc6_:int = !!param5?16075081:11653677;
         var _loc7_:MovieClip = var_232.method_92(1);
         _loc7_.graphics.lineStyle(2,_loc6_,50 + int(class_691.method_114(50)));
         _loc7_.graphics.moveTo(Number(param1 + param3),Number(param2 + param4));
         _loc7_.graphics.lineTo(param1,param2);
         var_230.draw(_loc7_,new Matrix(1,0,0,1,0,0));
         _loc7_.parent.removeChild(_loc7_);
      }
      
      public function method_262(param1:Number, param2:Number, param3:int = 20) : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:MovieClip = null;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:class_892 = null;
         var _loc5_:Number = 360 / param3;
         var _loc6_:int = 0;
         while(_loc6_ < 4)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc8_ = 0;
            while(_loc8_ < param3)
            {
               _loc8_++;
               _loc9_ = _loc8_;
               _loc10_ = var_232.method_92(3);
               class_649.method_149(_loc10_,16777215);
               class_657.name_18(_loc10_,int(class_691.method_114(2)) == 0?16075081:11653677);
               _loc11_ = _loc5_ * _loc9_;
               _loc10_.x = Number(param1 + class_649.method_148(_loc11_) * (2 * _loc7_));
               _loc10_.y = Number(param2 + class_649.name_9(_loc11_) * (2 * _loc7_));
               _loc12_ = Number(1 + Math.random() * 3);
               _loc10_.scaleY = _loc12_;
               _loc10_.scaleX = _loc12_;
               _loc13_ = new class_892(_loc10_);
               _loc13_.var_251 = 20;
               _loc13_.var_278 = class_649.method_148(_loc11_) * (8 / (_loc7_ + 1));
               _loc13_.var_279 = class_649.name_9(_loc11_) * (8 / (_loc7_ + 1));
            }
         }
      }
      
      public function method_117() : void
      {
         var_230 = new BitmapData(class_742.var_216,class_742.var_218,true,65280);
         class_319 = var_232.method_84(class_899.__unprotect__("ubvh\x01"),1);
         class_319.addChild(new Bitmap(var_230));
         var_813 = var_232.method_84(class_899.__unprotect__("B=\'D\x01"),3);
         var _loc1_:DropShadowFilter = new DropShadowFilter();
         var_813.filters = [_loc1_];
         var_486 = var_232.method_84(class_899.__unprotect__("C@h3\x01"),4);
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__("Uw|\x17\x01"),6);
         var_363 = var_232.method_84(class_899.__unprotect__("\\Bry"),5);
         var_363.filters = [_loc1_];
         var_363.y = 200;
      }
   }
}
