package
{
   import package_32.class_899;
   
   public class class_795
   {
       
      
      public var var_236:Array;
      
      public var name_37:int;
      
      public var var_17:Array;
      
      public function class_795()
      {
         if(class_899.var_239)
         {
            return;
         }
         name_37 = 0;
      }
      
      public function toString() : String
      {
         var _loc1_:StringBuf = new StringBuf();
         var _loc2_:Boolean = true;
         var _loc3_:Array = var_17;
         _loc1_.b = _loc1_.b + "{";
         while(_loc3_ != null)
         {
            if(_loc2_)
            {
               _loc2_ = false;
            }
            else
            {
               _loc1_.b = _loc1_.b + ", ";
            }
            _loc1_.b = _loc1_.b + class_691.method_97(_loc3_[0]);
            _loc3_ = _loc3_[1];
         }
         _loc1_.b = _loc1_.b + "}";
         return _loc1_.b;
      }
      
      public function method_116(param1:Object) : Boolean
      {
         var _loc2_:Array = null;
         var _loc3_:Array = var_17;
         while(_loc3_ != null)
         {
            if(_loc3_[0] == param1)
            {
               if(_loc2_ == null)
               {
                  var_17 = _loc3_[1];
               }
               else
               {
                  _loc2_[1] = _loc3_[1];
               }
               if(var_236 == _loc3_)
               {
                  var_236 = _loc2_;
               }
               name_37 = name_37 - 1;
               return true;
            }
            _loc2_ = _loc3_;
            _loc3_ = _loc3_[1];
         }
         return false;
      }
      
      public function method_103(param1:Object) : void
      {
         var _loc2_:Array = [param1,var_17];
         var_17 = _loc2_;
         if(var_236 == null)
         {
            var_236 = _loc2_;
         }
         name_37 = name_37 + 1;
      }
      
      public function name_12() : Object
      {
         if(var_17 == null)
         {
            return null;
         }
         var _loc1_:Object = var_17[0];
         var_17 = var_17[1];
         if(var_17 == null)
         {
            var_236 = null;
         }
         name_37 = name_37 - 1;
         return _loc1_;
      }
      
      public function name_49(param1:Function) : class_795
      {
         var _loc4_:Object = null;
         var _loc2_:class_795 = new class_795();
         var _loc3_:Array = var_17;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_[0];
            _loc3_ = _loc3_[1];
            _loc2_.method_124(param1(_loc4_));
         }
         return _loc2_;
      }
      
      public function name_36() : Object
      {
         return var_236 == null?null:var_236[0];
      }
      
      public function method_651(param1:String) : String
      {
         var _loc2_:StringBuf = new StringBuf();
         var _loc3_:Boolean = true;
         var _loc4_:Array = var_17;
         while(_loc4_ != null)
         {
            if(_loc3_)
            {
               _loc3_ = false;
            }
            else
            {
               _loc2_.b = _loc2_.b + param1;
            }
            _loc2_.b = _loc2_.b + _loc4_[0];
            _loc4_ = _loc4_[1];
         }
         return _loc2_.b;
      }
      
      public function method_73() : Object
      {
         return {
            "\x0b\x01":var_17,
            "\'*+x\x03":function():*
            {
               return this.var_17 != null;
            },
            "?\x05\x10;\x01":function():*
            {
               if(this.var_17 == null)
               {
                  return null;
               }
               var _loc1_:* = this.var_17[0];
               this.var_17 = this.var_17[1];
               return _loc1_;
            }
         };
      }
      
      public function method_164() : Boolean
      {
         return var_17 == null;
      }
      
      public function name_13() : Object
      {
         return var_17 == null?null:var_17[0];
      }
      
      public function method_267(param1:Function) : class_795
      {
         var _loc4_:Object = null;
         var _loc2_:class_795 = new class_795();
         var _loc3_:Array = var_17;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_[0];
            _loc3_ = _loc3_[1];
            if(param1(_loc4_))
            {
               _loc2_.method_124(_loc4_);
            }
         }
         return _loc2_;
      }
      
      public function name_72() : void
      {
         var_17 = null;
         var_236 = null;
         name_37 = 0;
      }
      
      public function method_124(param1:Object) : void
      {
         var _loc2_:Array = [param1];
         if(var_17 == null)
         {
            var_17 = _loc2_;
         }
         else
         {
            var_236[1] = _loc2_;
         }
         var_236 = _loc2_;
         name_37 = name_37 + 1;
      }
   }
}
