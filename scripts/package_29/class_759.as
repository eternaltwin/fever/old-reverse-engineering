package package_29
{
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   
   public class class_759 extends Sprite
   {
      
      public static var var_810:int = 10;
       
      
      public var var_811:int;
      
      public var var_807:int;
      
      public var NCRx:Object;
      
      public var GLWA:TextField;
      
      public var var_369:int;
      
      public var var_201:Sprite;
      
      public var var_713:class_738;
      
      public var var_303:Function;
      
      public function class_759(param1:String = undefined, param2:int = 0, param3:int = 16777215)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_811 = param3;
         var_369 = param2;
         class_757.var_214.var_804.push(this);
         class_757.var_214.var_800.method_124(this,1);
         var_807 = 0;
         var_201 = new Sprite();
         addChild(var_201);
         GLWA = class_742.method_308(param3,8,-1,"nokia");
         GLWA.y = -2;
         GLWA.text = param1;
         GLWA.width = Number(GLWA.textWidth + 3);
         GLWA.height = class_759.var_810 + 4;
         addChild(GLWA);
         method_347(GLWA.width,class_759.var_810);
      }
      
      public function method_347(param1:Number, param2:Number) : void
      {
         var_201.graphics.clear();
         var_201.graphics.beginFill(var_369);
         var_201.graphics.drawRect(0,0,param1,param2);
      }
      
      public function method_553(param1:Function) : void
      {
         var_303 = param1;
         addEventListener(MouseEvent.ROLL_OVER,method_171);
         addEventListener(MouseEvent.ROLL_OUT,method_554);
         addEventListener(MouseEvent.CLICK,name_5);
      }
      
      public function method_171(param1:*) : void
      {
         if(NCRx != null)
         {
            GLWA.textColor = NCRx;
         }
         switch(var_807)
         {
            case 0:
               alpha = 0.5;
               break;
            case 1:
               if(var_713 == null)
               {
                  var_713 = new class_738();
                  var_713.method_128(class_702.package_15.method_98(null,"arrow"));
                  var_713.x = -4;
                  var_713.y = 6;
                  addChild(var_713);
               }
               var_713.visible = true;
         }
      }
      
      public function method_554(param1:*) : void
      {
         if(NCRx != null)
         {
            GLWA.textColor = var_811;
         }
         switch(var_807)
         {
            case 0:
               alpha = 1;
               break;
            case 1:
               var_713.visible = false;
         }
      }
      
      public function method_89() : void
      {
         if(var_303 != null)
         {
            removeEventListener(MouseEvent.ROLL_OVER,method_171);
            removeEventListener(MouseEvent.ROLL_OUT,method_554);
            removeEventListener(MouseEvent.CLICK,name_5);
         }
         class_757.var_214.var_804.method_116(this);
      }
      
      public function name_5(param1:*) : void
      {
         if(var_303 != null)
         {
            var_303();
         }
      }
   }
}
