package package_29
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.Sprite;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_840 extends Sprite
   {
      
      public static var var_574:Number = 0.01;
      
      public static var var_268:int = 10;
      
      public static var var_269:int = 10;
      
      public static var var_1166:int = 2000;
      
      public static var var_1167:Array;
      
      public static var var_1168:int = 0;
       
      
      public var var_496:Sprite;
      
      public var var_1170:int;
      
      public var var_737:Number;
      
      public var var_736:Number;
      
      public var var_254:Number;
      
      public var var_334:Number;
      
      public var var_1171:Number;
      
      public var var_1169:class_858;
      
      public function class_840(param1:int = 0, param2:int = 0, param3:class_858 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_1169 = param3;
         var_1170 = param2;
         param3.var_232.method_124(this,class_756.var_786);
         super();
         if(class_840.var_1167 == null)
         {
            class_840.method_830();
         }
         while(class_840.var_1168 < class_840.var_268 * class_840.var_269)
         {
            class_840.method_834();
         }
         method_836();
         var_254 = 0;
         var _loc4_:* = class_840.method_831(param1);
         var _loc5_:* = class_840.method_831(param2);
         var_1171 = int(_loc4_.var_244);
         var_334 = int(_loc4_.var_245);
         var_736 = Number(class_663.method_112(int(_loc5_.var_244) - int(_loc4_.var_244),5));
         var_737 = Number(class_663.method_112(int(_loc5_.var_245) - int(_loc4_.var_245),5));
      }
      
      public static function method_830() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Bitmap = null;
         var _loc4_:BitmapData = null;
         class_840.var_1167 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = new Bitmap();
            _loc4_ = new BitmapData(class_840.var_1166,class_840.var_1166,false,-256);
            class_840.var_1167.push(_loc4_);
         }
      }
      
      public static function method_832(param1:class_645) : void
      {
         var _loc2_:* = class_840.method_831(param1.var_215);
         var _loc3_:int = 0;
         if(int(_loc2_.var_244) >= 5)
         {
            _loc2_.var_244 = int(_loc2_.var_244) - 5;
            _loc3_++;
         }
         if(int(_loc2_.var_245) >= 5)
         {
            _loc2_.var_245 = int(_loc2_.var_245) - 5;
            _loc3_ = _loc3_ + 2;
         }
         var _loc5_:Matrix = new Matrix();
         var _loc6_:int = int(_loc2_.var_244) * class_645.var_211;
         var _loc7_:int = int(_loc2_.var_245) * class_645.var_212;
         _loc5_.translate(_loc6_,_loc7_);
         class_840.var_1167[_loc3_].draw(param1,_loc5_,null,null,new Rectangle(_loc6_,_loc7_,class_645.var_211,class_645.var_212));
      }
      
      public static function method_833(param1:int) : void
      {
         var _loc2_:class_645 = class_645.method_71(param1);
         _loc2_.method_95(0);
         class_840.method_832(_loc2_);
         _loc2_.method_89();
      }
      
      public static function method_831(param1:int) : Object
      {
         return {
            "L\x01":int(param1 % class_840.var_268),
            "\x03\x01":int(int(param1 / class_840.var_268) % class_840.var_269)
         };
      }
      
      public static function method_834() : Boolean
      {
         if(class_840.var_1168 == class_840.var_268 * class_840.var_269)
         {
            return false;
         }
         class_840.method_833(class_840.var_1168);
         class_840.var_1168 = class_840.var_1168 + 1;
         return true;
      }
      
      public function method_79() : void
      {
         var_254 = Number(Math.min(Number(var_254 + class_840.var_574),1));
         name_94();
         if(var_254 == 1)
         {
            var_1169.method_835();
            method_89();
         }
      }
      
      public function method_89() : void
      {
         parent.removeChild(this);
      }
      
      public function method_836() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:BitmapData = null;
         var _loc6_:Bitmap = null;
         var_496 = new Sprite();
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = 0;
            while(_loc3_ < 4)
            {
               _loc3_++;
               _loc4_ = _loc3_;
               _loc5_ = class_840.var_1167[_loc4_];
               _loc6_ = new Bitmap(_loc5_);
               var_496.addChild(_loc6_);
               _loc6_.x = int(_loc4_ % 2) * class_840.var_1166;
               _loc6_.y = int(_loc4_ / 2) * class_840.var_1166;
               _loc6_.x = Number(_loc6_.x + int(_loc2_ % 2) * class_840.var_1166 * 2);
               _loc6_.y = Number(_loc6_.y + int(_loc2_ / 2) * class_840.var_1166 * 2);
            }
         }
         addChild(var_496);
      }
      
      public function name_94() : void
      {
         var _loc1_:Number = Number(Math.sin(var_254 * Math.PI));
         var _loc2_:Number = 1 / (Number(1 + _loc1_ * 9));
         var _loc3_:Number = _loc2_;
         var_496.scaleY = _loc3_;
         var_496.scaleX = _loc3_;
         _loc3_ = Number(var_1171 + var_254 * var_736);
         var _loc4_:Number = Number(var_334 + var_254 * var_737);
         var _loc5_:Number = (class_645.var_211 - class_645.var_211 * _loc2_) * 0.5;
         var _loc6_:Number = (class_645.var_212 - class_645.var_212 * _loc2_) * 0.5;
         var_496.x = _loc5_ - _loc3_ * (class_645.var_211 * _loc2_);
         var_496.y = _loc6_ - _loc4_ * (class_645.var_212 * _loc2_);
         var _loc7_:Number = class_840.var_1166 * 2 * _loc2_;
         var_496.x = class_663.method_113(var_496.x,_loc7_) - _loc7_;
         var_496.y = class_663.method_113(var_496.y,_loc7_) - _loc7_;
         var_496.y = Number(var_496.y + 18);
      }
   }
}
