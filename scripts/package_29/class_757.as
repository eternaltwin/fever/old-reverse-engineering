package package_29
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObjectContainer;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   import package_10.class_769;
   import package_10.class_870;
   import package_11.class_755;
   import package_11.class_844;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_13.class_765;
   import package_13.class_868;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_660;
   
   public class class_757 extends class_756
   {
      
      public static var var_788:int = 160;
      
      public static var var_789:int = 100;
      
      public static var var_214:class_757;
       
      
      public var var_283:package_11.package_12.class_660;
      
      public var var_251:int;
      
      public var var_792:int;
      
      public var var_790:Boolean;
      
      public var name_3:int;
      
      public var var_796:int;
      
      public var var_799:class_755;
      
      public var var_109:Sprite;
      
      public var var_800:class_755;
      
      public var var_803:Sprite;
      
      public var w3Yn:class_738;
      
      public var var_798:class_348;
      
      public var var_793:Bitmap;
      
      public var var_791:Object;
      
      public var var_794:Sprite;
      
      public var var_254:Number;
      
      public var name_5:Function;
      
      public var var_795:int;
      
      public var var_801:class_755;
      
      public var var_797:Array;
      
      public var miTx:Array;
      
      public var var_804:Array;
      
      public var class_319:Sprite;
      
      public var var_41:class_758;
      
      public var class_318:class_738;
      
      public function class_757(param1:Array = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         class_757.var_214 = this;
         super();
         class_870.name_18(16711680);
         var_797 = param1;
         method_362();
         var_792 = 0;
         method_542();
      }
      
      public function method_529() : void
      {
         if(int(var_251 % 2) != 0)
         {
            return;
         }
         var_254 = Number(Math.min(Number(var_254 + 0.04),1));
         if(var_254 > 0.5 && var_790)
         {
            var_790 = false;
            method_525(int(var_791._id));
            var_536.visible = false;
            var_41.method_526();
            var_41.method_527(var_792);
         }
         var _loc1_:Number = Number(Math.sin(var_254 * 3.14));
         _loc1_ = Number(Math.pow(_loc1_,0.5));
         var _loc3_:Number = Number(0.01 + (1 - _loc1_) * (1 - 0.01));
         var _loc4_:BitmapData = var_793.bitmapData;
         if(_loc3_ < 0.1 && _loc4_.width > class_742.var_216 * 0.1)
         {
            var_793.bitmapData = new BitmapData(40,40,false,0);
         }
         if(_loc3_ >= 0.1 && _loc4_.width < class_742.var_216)
         {
            var_793.bitmapData = new BitmapData(400,400,false,0);
         }
         var _loc5_:Matrix = new Matrix();
         _loc5_.scale(_loc3_,_loc3_);
         var_793.bitmapData.draw(var_536,_loc5_);
         var _loc6_:Number = 1 / _loc3_;
         var_793.scaleY = _loc6_;
         var_793.scaleX = _loc6_;
         if(var_254 == 1)
         {
            var_536.visible = true;
            var_793.bitmapData.dispose();
            var_109.removeChild(var_793);
            name_3 = 4;
            var_41.method_528();
         }
      }
      
      override public function method_79(param1:* = undefined) : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:* = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Array = null;
         var _loc7_:* = null;
         var _loc8_:* = null;
         var _loc9_:int = 0;
         var _loc10_:* = null as package_8.package_9.class_660;
         super.method_79(param1);
         var_251 = var_251 + 1;
         switch(name_3)
         {
            case 0:
               var_254 = Number(Math.min(Number(var_254 + 0.025),1));
               class_657.gfof(var_109,Number(Math.pow(var_254,2)),16777215);
               if(var_254 == 1)
               {
                  name_3 = name_3 + 1;
                  var_254 = 0;
                  var_798 = new class_348();
                  var_799.method_124(var_798,0);
                  var_798.x = 200;
                  var_798.y = 200;
                  _loc2_ = 2;
                  var_798.scaleY = _loc2_;
                  var_798.scaleX = _loc2_;
                  class_657.name_18(var_109,0,0);
                  method_533(16777215);
                  break;
               }
               break;
            case 1:
               var_254 = Number(Math.min(Number(var_254 + 0.01),1));
               if(var_254 == 1)
               {
                  var_254 = 0;
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 2:
               var_254 = Number(Math.min(Number(var_254 + 0.09),1));
               class_657.name_18(var_109,0,-(int(var_254 * 255)));
               if(var_254 == 1)
               {
                  var_109.removeChild(var_798);
                  method_534();
                  break;
               }
               break;
            case 3:
               break;
            case 4:
               if(var_536.var_227[0] < class_757.var_788)
               {
                  var_41.method_535(var_536.var_227[0]);
               }
               class_318.visible = var_536.name_5;
               var_536.method_79();
               break;
            case 5:
               method_529();
               break;
            default:
            default:
            default:
            default:
               break;
            case 10:
               var_254 = Number(Math.min(Number(var_254 + 0.05),1));
               _loc2_ = 0.5 - Math.cos(var_254 * 3.14) * 0.5;
               _loc3_ = var_283.method_127(_loc2_);
               var_794.x = Number(_loc3_.var_244);
               var_794.y = Number(_loc3_.var_245);
               if(var_254 == 1)
               {
                  method_530();
                  break;
               }
               break;
            case 11:
               break;
            case 12:
               var_254 = Number(Math.min(Number(var_254 + 0.15),1));
               _loc2_ = 0.5 - Math.cos(var_254 * 3.14) * 0.5;
               _loc4_ = 0;
               if(var_254 == 1)
               {
                  var_795 = int(Number(class_663.method_113(var_795 - var_796,int(var_797.length))));
                  method_531();
                  _loc2_ = 0;
                  name_3 = 11;
               }
               _loc5_ = 0;
               _loc6_ = miTx;
               while(_loc5_ < int(_loc6_.length))
               {
                  _loc3_ = _loc6_[_loc5_];
                  _loc5_++;
                  _loc3_.class_319.x = Number(method_532(Number(_loc4_ + _loc2_ * var_796)));
                  _loc4_++;
               }
               break;
            case 13:
               var_254 = Number(Math.min(Number(var_254 + 0.1),1));
               _loc2_ = Number(Math.pow(var_254,2));
               _loc3_ = var_283.method_127(_loc2_);
               _loc7_ = miTx[2];
               _loc7_.class_319.x = Number(_loc3_.var_244);
               _loc7_.class_319.y = Number(_loc3_.var_245);
               _loc4_ = 0;
               while(_loc4_ < 2)
               {
                  _loc4_++;
                  _loc5_ = _loc4_;
                  _loc8_ = miTx[1 + _loc5_ * 2];
                  _loc9_ = _loc5_ * 2 - 1;
                  _loc8_.class_319.x = Number(class_742.var_216 * 0.5 + _loc9_ * (180 + _loc2_ * 180));
               }
               if(var_254 == 1)
               {
                  _loc4_ = 0;
                  _loc6_ = miTx;
                  while(_loc4_ < int(_loc6_.length))
                  {
                     _loc8_ = _loc6_[_loc4_];
                     _loc4_++;
                     if(_loc8_.class_319 != w3Yn)
                     {
                        _loc8_.class_319.parent.removeChild(_loc8_.class_319);
                     }
                  }
                  _loc10_ = new package_8.package_9.class_660(var_794,class_742.var_216 * 0.5,Number(class_742.var_218 * 0.5 + 20));
                  _loc10_.var_258 = method_526;
                  _loc10_.method_122();
                  name_3 = -1;
                  break;
               }
         }
      }
      
      public function method_536(param1:String) : TextField
      {
         var _loc2_:TextField = class_742.method_308(16777215,8,-1,"nokia");
         var_800.method_124(_loc2_,1);
         _loc2_.y = 6;
         _loc2_.text = param1;
         _loc2_.width = Number(_loc2_.textWidth + 3);
         _loc2_.x = (100 - _loc2_.width) * 0.5;
         return _loc2_;
      }
      
      public function method_533(param1:uint) : void
      {
         var_109.graphics.clear();
         var_109.graphics.beginFill(param1);
         var_109.graphics.drawRect(0,0,400,400);
      }
      
      public function method_538(param1:String) : void
      {
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:class_812 = null;
         var _loc2_:Sprite = new Sprite();
         _loc2_.y = 8;
         _loc2_.graphics.beginFill(0);
         _loc2_.graphics.drawRect(0,0,100,16);
         _loc2_.graphics.beginFill(16777215);
         _loc2_.graphics.drawRect(0,1,100,1);
         _loc2_.graphics.drawRect(0,14,100,1);
         var_800.method_124(_loc2_,1);
         var _loc3_:TextField = class_742.method_308(16777215,20,-1,"upheaval");
         var_800.method_124(_loc3_,1);
         _loc3_.y = 4;
         _loc3_.text = param1;
         _loc3_.width = Number(_loc3_.textWidth + 3);
         _loc3_.x = (100 - _loc3_.width) * 0.5;
         var _loc4_:class_844 = new class_844(int(var_791._id) * 1000);
         var _loc6_:Number = _loc4_.var_503 * 16807 % 2147483647;
         _loc4_.var_503 = _loc6_;
         var _loc5_:int = int(class_657.method_237(int((int(_loc6_) & 1073741823) % 10007) / 10007));
         var _loc7_:int = 0;
         while(_loc7_ < 2)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc9_ = _loc5_;
            if(_loc8_ == 0)
            {
               _loc9_ = int(class_657.method_151(_loc9_,-100));
            }
            _loc10_ = method_537(_loc9_,30 - _loc8_ * 15);
         }
      }
      
      public function method_540(param1:int) : void
      {
         var_792 = param1 * 5;
         method_539();
      }
      
      public function method_541(param1:*) : void
      {
         if(name_3 != 11)
         {
            return;
         }
         name_3 = 13;
         var_254 = 0;
         var_791 = var_797[var_795];
         var _loc2_:* = miTx[2];
         _loc2_.class_319.parent.removeChild(_loc2_.class_319);
         var_801.method_124(_loc2_.class_319,0);
         _loc2_.class_319.x = _loc2_.class_319.x - var_794.x;
         _loc2_.class_319.y = _loc2_.class_319.y - var_794.y;
         var_283 = new package_11.package_12.class_660(_loc2_.class_319.x,_loc2_.class_319.y,0,-116);
         w3Yn = _loc2_.class_319;
      }
      
      public function method_526() : void
      {
         name_3 = 0;
         var_254 = 0;
         method_533(3355443);
         name_5 = method_542;
      }
      
      public function method_543() : void
      {
         var _loc1_:package_8.package_9.class_660 = new package_8.package_9.class_660(w3Yn,method_532(2) - var_794.x,class_757.var_789 - var_794.y);
         _loc1_.method_122();
         var var_214:class_757 = this;
         _loc1_.var_258 = function():void
         {
            var_214.w3Yn.parent.removeChild(var_214.w3Yn);
            var_214.w3Yn = null;
            var_214.method_530();
         };
         name_3 = -1;
      }
      
      public function method_531() : void
      {
         var _loc4_:int = 0;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 2 * 2 + 1;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = miTx[_loc4_];
            _loc6_ = int(Number(class_663.method_113(var_795 + _loc4_ - 2,int(var_797.length))));
            _loc5_.var_802.gotoAndStop(int(var_797[_loc6_]._id) + 1);
         }
      }
      
      public function method_545() : void
      {
         name_3 = 3;
         var_251 = 0;
         if(var_803 != null)
         {
            method_544();
         }
         var_804 = [];
         var_803 = new Sprite();
         var_803.scaleY = Number(4);
         var_803.scaleX = 4;
         var_799.method_124(var_803,0);
         var_800 = new class_755(var_803);
      }
      
      override public function method_525(param1:int) : void
      {
         var_237 = var_792 * 0.01;
         super.method_525(param1);
         var_799.method_124(var_536,1);
         var_536.var_234 = true;
      }
      
      public function method_546(param1:int) : void
      {
         if(name_3 != 11)
         {
            return;
         }
         var_796 = param1;
         var_254 = 0;
         name_3 = 12;
      }
      
      public function method_547() : void
      {
         var _loc4_:* = null;
         var _loc5_:* = null as package_8.package_9.class_660;
         name_3 = -1;
         name_5 = null;
         var _loc1_:package_8.package_9.class_660 = new package_8.package_9.class_660(var_794,class_742.var_216 * 0.5,Number(class_742.var_323 + var_794.height * 0.5),0.05);
         _loc1_.method_122();
         _loc1_.var_258 = method_89;
         var _loc2_:int = 0;
         var _loc3_:Array = miTx;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = new package_8.package_9.class_660(_loc4_.class_319,_loc4_.class_319.x,-80);
            _loc5_.method_122();
         }
      }
      
      public function method_539() : void
      {
         if(class_652.var_214 != null)
         {
            class_652.var_214.method_186(_PlayerAction._FeverXPlay);
            class_868.var_214.method_195();
            class_652.var_214.var_109.method_79();
            method_197();
         }
         name_5 = null;
         method_544();
         var_41 = new class_758();
         var_799.method_124(var_41,3);
         method_525(int(var_791._id));
         name_3 = 4;
      }
      
      override public function method_89() : void
      {
         super.method_89();
         if(class_652.var_214 == null)
         {
            return;
         }
         class_652.var_214.method_192();
      }
      
      public function method_542() : void
      {
         name_5 = null;
         var_109.graphics.clear();
         class_657.name_18(var_109,0,0);
         var_799.method_207();
         name_3 = -1;
         var _loc1_:package_8.package_9.class_660 = new package_8.package_9.class_660(var_794,class_742.var_216 * 0.5,364);
         _loc1_.method_122();
         _loc1_.var_258 = method_530;
      }
      
      public function method_362() : void
      {
         class_319 = new Sprite();
         class_319.graphics.beginFill(0);
         class_319.graphics.drawRect(0,0,class_742.var_216,class_742.var_323);
         class_319.addEventListener(MouseEvent.CLICK,method_548);
         addChild(class_319);
         var_794 = new Sprite();
         var_794.x = class_742.var_216 * 0.5;
         var_794.y = class_742.var_323 * 0.5;
         addChild(var_794);
         var_801 = new class_755(var_794);
         var _loc1_:class_738 = new class_738();
         _loc1_.method_128(class_702.package_15.method_98(null,"fever_x_back"));
         var _loc2_:Number = 2;
         _loc1_.scaleY = _loc2_;
         _loc1_.scaleX = _loc2_;
         _loc1_.y = _loc1_.y - 168;
         var_801.method_124(_loc1_,0);
         var _loc3_:class_738 = new class_738();
         _loc3_.method_128(class_702.package_15.method_98(null,"fever_x"));
         _loc2_ = 2;
         _loc3_.scaleY = _loc2_;
         _loc3_.scaleX = _loc2_;
         var_801.method_124(_loc3_,2);
         var_794.y = Number(class_742.var_323 + var_794.height * 0.5);
         class_318 = new class_738();
         class_318.method_128(class_702.package_15.method_98(null,"fever_x_click"),0,0);
         _loc2_ = 2;
         class_318.scaleY = _loc2_;
         class_318.scaleX = _loc2_;
         var_801.method_124(class_318,2);
         class_318.x = -40;
         class_318.y = 72;
         class_318.visible = false;
         var_109 = new Sprite();
         var_109.x = -100;
         var_109.y = -148;
         _loc2_ = 0.5;
         var_109.scaleY = _loc2_;
         var_109.scaleX = _loc2_;
         var_799 = new class_755(var_109);
         var_801.method_124(var_109,3);
         var _loc4_:Sprite = new Sprite();
         _loc4_.x = var_109.x;
         _loc4_.y = var_109.y;
         _loc4_.graphics.beginFill(16711680);
         _loc4_.graphics.drawRect(0,0,200,200);
         var_801.method_124(_loc4_,3);
         var_109.mask = _loc4_;
      }
      
      public function method_549() : void
      {
         name_3 = 5;
         var_254 = 0;
         var_536.visible = false;
         var_793 = new Bitmap();
         var_793.bitmapData = new BitmapData(class_742.var_216,class_742.var_218,false,0);
         var_799.method_124(var_793,2);
         var_790 = true;
         var_251 = 0;
         method_529();
      }
      
      public function method_530() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_738 = null;
         var _loc5_:Number = NaN;
         var _loc6_:class_643 = null;
         if(w3Yn != null)
         {
            method_543();
            return;
         }
         name_5 = method_547;
         name_3 = 11;
         var_795 = 0;
         var _loc1_:int = 0;
         var _loc2_:int = int(var_797.length);
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            if(var_797[_loc3_] != var_791)
            {
               continue;
            }
            var_795 = _loc3_;
            break;
         }
         miTx = [];
         _loc1_ = 2;
         _loc2_ = 0;
         _loc3_ = 2 * _loc1_ + 1;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            var var_805:Array = [_loc2_];
            _loc4_ = new class_738();
            _loc5_ = 2;
            _loc4_.scaleY = _loc5_;
            _loc4_.scaleX = _loc5_;
            _loc4_.method_128(class_702.package_15.method_98(null,"cart"));
            _loc4_.x = Number(method_532(int(var_805[0])));
            _loc4_.y = class_757.var_789;
            _loc6_ = new class_643();
            _loc6_.alpha = 0.8;
            _loc6_.x = -11;
            _loc6_.y = -31;
            _loc6_.width = 38;
            _loc6_.scaleY = _loc6_.scaleX;
            addChild(_loc4_);
            _loc4_.addChild(_loc6_);
            miTx.push({
               "U\\\x01":_loc4_,
               "\x16x\x15\x01":_loc6_
            });
            var var_214:Array = [this];
            switch(int(var_805[0]))
            {
               default:
                  continue;
               case 1:
                  _loc4_.addEventListener(MouseEvent.CLICK,function(param1:Array, param2:Array):Function
                  {
                     var var_214:Array = param1;
                     var var_805:Array = param2;
                     return function(param1:*):void
                     {
                        var_214[0].method_546(2 - int(var_805[0]));
                     };
                  }(var_214,var_805));
                  continue;
               case 2:
               case 3:
                  _loc4_.addEventListener(MouseEvent.CLICK,method_541);

            }
         }
         method_531();
      }
      
      public function method_551() : Boolean
      {
         return Boolean(class_765.var_214.method_550());
      }
      
      public function method_537(param1:int, param2:int) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:class_738 = null;
         var _loc10_:int = 0;
         var _loc3_:Sprite = new Sprite();
         var_800.method_124(_loc3_,0);
         var _loc4_:int = 0;
         while(_loc4_ < 25)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = 0;
            while(_loc6_ < param2)
            {
               _loc6_++;
               _loc7_ = _loc6_;
               _loc8_ = _loc7_ / param2;
               _loc9_ = new class_738();
               _loc10_ = int(_loc8_ * 7);
               _loc10_ = _loc10_ + (int(class_691.method_114(3)) - 1);
               _loc10_ = int(class_663.method_256(0,_loc10_,6));
               if(_loc10_ < 6)
               {
                  _loc9_.method_128(class_702.package_15.method_98(_loc10_,"gradient_blocks"),0,0);
               }
               _loc3_.addChild(_loc9_);
               _loc9_.x = _loc5_ * 4;
               _loc9_.y = _loc7_ * 4;
            }
         }
         class_657.name_18(_loc3_,param1);
      }
      
      public function method_532(param1:Number) : Number
      {
         return Number(class_742.var_216 * 0.5 + (param1 - 2) * 180);
      }
      
      public function name_27() : void
      {
         var_536.method_89();
         var_536 = null;
         var_41.parent.removeChild(var_41);
         var_41 = null;
         class_769.name_47(method_534,2000);
         method_545();
         var _loc1_:TextField = method_536("game over");
         _loc1_.y = 42;
         var _loc2_:Boolean = false;
         if(var_792 > int(var_791._lvl))
         {
            var_791._lvl = var_792;
            _loc2_ = true;
         }
         if(class_652.var_214 == null || !_loc2_)
         {
            return;
         }
         class_652.var_214.method_186(_PlayerAction._MajCartridge(var_791));
      }
      
      override public function method_524() : void
      {
         if(!var_536.var_220)
         {
            name_27();
            return;
         }
         var_792 = var_792 + 1;
         method_549();
      }
      
      public function method_197() : void
      {
         var _loc1_:BitmapData = class_652.var_214.var_109.bitmapData;
         var _loc2_:Bitmap = new Bitmap(_loc1_);
         var _loc3_:Number = 2;
         _loc2_.scaleY = _loc3_;
         _loc2_.scaleX = _loc3_;
         class_319.addChild(_loc2_);
         _loc3_ = 0.2;
         var _loc5_:Rectangle = new Rectangle(0,13,class_742.var_216 * 0.5,class_742.var_323 * 0.5 - 13);
         _loc1_.colorTransform(_loc5_,new ColorTransform(_loc3_,_loc3_,_loc3_,1,0,0,0,0));
      }
      
      public function method_534() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_759 = null;
         if(!method_551())
         {
            method_547();
            return;
         }
         name_5 = method_542;
         class_657.name_18(var_109,0,0);
         method_533(0);
         method_545();
         method_538(class_885.var_308._games[int(var_791._id)]._name);
         var _loc1_:Array = class_808.var_806;
         var _loc2_:int = 0;
         while(_loc2_ < 2)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = new class_759(_loc1_[_loc3_]);
            _loc4_.x = 36;
            _loc4_.y = 64 + _loc3_ * 12;
            _loc4_.var_807 = 1;
            _loc4_.method_553(_loc3_ == 0?method_539:method_552);
         }
      }
      
      public function method_552() : void
      {
         var _loc4_:int = 0;
         var _loc5_:Boolean = false;
         var _loc6_:String = null;
         var _loc7_:class_759 = null;
         method_545();
         method_536(class_808.var_808);
         var _loc3_:int = 0;
         while(_loc3_ < 20)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = _loc4_ * 5 <= int(var_791._lvl);
            _loc6_ = class_691.method_97(_loc4_ * 5);
            while(_loc6_.length < 2)
            {
               _loc6_ = "0" + _loc6_;
            }
            _loc7_ = new class_759(_loc6_,0,!!_loc5_?11184810:3355443);
            _loc7_.x = 10 + int(_loc4_ % 5) * 16;
            _loc7_.y = 36 + int(_loc4_ / 5) * 14;
            if(_loc5_)
            {
               _loc7_.method_553(function(param1:Function, param2:int):Function
               {
                  var var_27:Function = param1;
                  var var_18:int = param2;
                  return function():void
                  {
                     return var_27(var_18);
                  };
               }(method_540,_loc4_));
               _loc7_.NCRx = 16777215;
               _loc7_.var_807 = -1;
            }
         }
      }
      
      public function method_548(param1:*) : void
      {
         if(name_5 == null)
         {
            return;
         }
         name_5();
      }
      
      public function method_544() : void
      {
         if(var_803.parent == null)
         {
            return;
         }
         var_803.parent.removeChild(var_803);
         var_803 = null;
         var_800 = null;
         while(int(var_804.length) > 0)
         {
            var_804.pop().method_89();
         }
      }
   }
}
