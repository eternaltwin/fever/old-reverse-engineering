package package_29
{
   import flash.display.Sprite;
   import flash.text.TextField;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   
   public class class_758 extends Sprite
   {
       
      
      public var var_251:Sprite;
      
      public var GLWA:TextField;
      
      public var var_809:Array;
      
      public function class_758()
      {
         var _loc3_:int = 0;
         var _loc4_:class_738 = null;
         if(class_899.var_239)
         {
            return;
         }
         super();
         scaleY = Number(4);
         scaleX = 4;
         GLWA = new TextField();
         GLWA = class_742.method_308(16777215,8,1,"nokia");
         GLWA.background = true;
         GLWA.backgroundColor = 0;
         GLWA.y = -1;
         GLWA.visible = false;
         var_809 = [];
         var_251 = new Sprite();
         var _loc2_:int = 0;
         while(_loc2_ < 4)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = new class_738();
            var_251.addChild(_loc4_);
            var_809.push(_loc4_);
            _loc4_.x = _loc3_ * 7;
         }
         addChild(var_251);
         addChild(GLWA);
      }
      
      public function method_527(param1:int) : void
      {
         GLWA.text = class_691.method_97(param1);
         GLWA.visible = true;
         GLWA.width = Number(GLWA.textWidth + 3);
         GLWA.x = 100 - GLWA.width;
         GLWA.height = Number(GLWA.textHeight + 2);
      }
      
      public function method_526() : void
      {
         var_251.visible = false;
      }
      
      public function method_528() : void
      {
         GLWA.visible = false;
      }
      
      public function method_535(param1:Number) : void
      {
         var _loc5_:int = 0;
         var _loc6_:class_738 = null;
         var _loc7_:int = 0;
         var_251.visible = true;
         var _loc3_:Number = param1;
         var _loc4_:int = 0;
         while(_loc4_ < 4)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = var_809[_loc5_];
            _loc7_ = 2;
            if(_loc3_ > 20)
            {
               _loc7_ = 1;
            }
            if(_loc3_ > 40)
            {
               _loc7_ = 0;
            }
            _loc3_ = _loc3_ - int([40,20,0][_loc7_]);
            _loc6_.method_128(class_702.package_15.method_98(_loc7_,"timer_brick"),0,0);
         }
      }
   }
}
