package
{
   import flash.display.MovieClip;
   import flash.geom.Rectangle;
   import package_32.class_899;
   import package_8.package_9.class_873;
   
   public class class_721 extends class_645
   {
       
      
      public var var_251:int;
      
      public var var_685:int;
      
      public var var_683:int;
      
      public var var_681:class_615;
      
      public var var_684:int;
      
      public var var_686:class_614;
      
      public var var_682:class_613;
      
      public function class_721()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         super.method_79();
         if(var_681.var_200 == null)
         {
            return;
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(!!name_5 && var_220 == null)
               {
                  var_681.gotoAndStop("punch");
                  var_251 = 0;
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 2:
            default:
            default:
               if(name_5)
               {
                  var_681.var_200.nextFrame();
               }
               else
               {
                  var_681.var_200.prevFrame();
               }
               if(var_681.var_200.currentFrame == 1)
               {
                  method_423();
               }
               if(var_681.var_200.currentFrame == 11)
               {
                  var_682.play();
                  var_683 = var_683 - var_684;
                  name_3 = 5;
                  var_251 = 0;
                  var_682.var_199.gotoAndStop(int(var_683 * var_682.var_199.totalFrames / var_685));
                  var_681.var_200.play();
                  if(var_682.var_199.currentFrame == 1 && var_220 == null)
                  {
                     method_81(true,24);
                     new class_873(this,0,8,0.75);
                     method_424();
                     break;
                  }
                  method_425();
                  break;
               }
               break;
            case 5:
               _loc1_ = var_251;
               var_251 = var_251 + 1;
               if(_loc1_ >= 8 && !name_5)
               {
                  method_423();
                  break;
               }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [300];
         super.method_95(param1);
         var_686 = new class_614();
         var_232.method_124(var_686,0);
         var_682 = new class_613();
         var_232.method_124(var_682,0);
         var_681 = new class_615();
         var_232.method_124(var_681,0);
         var_685 = int(Number(50 + param1 * 120));
         var_683 = var_685;
         var_684 = 9;
         var_682.var_199.gotoAndStop(int(var_683 * var_682.var_199.totalFrames / var_685));
      }
      
      public function method_426() : class_892
      {
         var _loc1_:Array = [new Rectangle(183,217,95,39)];
         var _loc2_:Rectangle = _loc1_[0];
         var _loc3_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("8\x11I\\\x02"),class_645.var_209));
         _loc3_.var_233 = 0.99;
         var var_38:class_892 = _loc3_;
         var_38.var_244 = Number(_loc2_.x + Math.random() * _loc2_.width);
         var_38.var_245 = Number(_loc2_.y + Math.random() * _loc2_.height);
         var_38.var_243.gotoAndStop(int(class_691.method_114(4)) + 1);
         var_38.var_250 = Number(0.25 + Math.random() * 0.25);
         var_38.var_580 = (Math.random() * 2 - 1) * 8;
         var_38.var_687 = 0.97;
         var_38.method_119(Number(1 + Number(Math.random())));
         var_38.var_688 = Number(270 + Math.random() * 20);
         var_38.var_251 = 10 + int(class_691.method_114(50));
         var_38.var_689 = function():void
         {
            var_38.var_278 = var_38.var_278 * 0.75;
         };
         var _loc4_:Number = _loc2_.x + _loc2_.width * 0.5 - var_38.var_244;
         var _loc5_:Number = _loc2_.y + _loc2_.height * 0.5 - var_38.var_245;
         var _loc6_:Number = Number(3.14 + Number(Math.atan2(_loc5_,_loc4_)));
         var_38.var_278 = Number(Math.cos(_loc6_));
         var_38.var_279 = Number(Math.sin(_loc6_));
         return var_38;
      }
      
      public function method_425() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc1_:int = 0;
         while(_loc1_ < 9)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = method_426();
            _loc3_.var_278 = Number(1 + Math.random() * 4);
            _loc3_.var_279 = -Math.random() * 5;
         }
      }
      
      public function method_424() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc4_:Number = NaN;
         var _loc1_:int = 0;
         while(_loc1_ < 64)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = method_426();
            _loc4_ = Math.random() * 8;
            _loc3_.var_278 = _loc3_.var_278 * _loc4_;
            _loc3_.var_279 = _loc3_.var_279 * _loc4_;
         }
      }
      
      public function method_423() : void
      {
         name_3 = 1;
         var_681.gotoAndStop("stand");
      }
   }
}
