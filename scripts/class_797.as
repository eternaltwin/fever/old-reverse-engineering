package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_797 extends class_645
   {
      
      public static var var_986:Array = [[15467010,16540078,9964042],[15649860,16055716,16750848],[11202405,3047704,3973921],[4031725,8768762,6035143]];
       
      
      public var var_987:Number;
      
      public var var_988:Number;
      
      public var var_989:Number;
      
      public var package_24:class_892;
      
      public var var_991:Number;
      
      public var var_990:Number;
      
      public var var_996:int;
      
      public var var_20:Object;
      
      public var var_992:class_23;
      
      public var var_995:int;
      
      public var var_994:int;
      
      public var var_997:class_795;
      
      public var name_56:class_795;
      
      public var var_993:int;
      
      public var z2Eo:int;
      
      public function class_797()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = method_100();
               _loc2_ = _loc1_.var_244 - package_24.var_244;
               _loc3_ = _loc1_.var_245 - package_24.var_245;
               _loc4_ = Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)));
               if(_loc4_ < 30)
               {
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 2:
               method_728();
               method_729();
            case 3:
               method_728();
               method_729();
         }
         _loc2_ = var_990;
         var_990 = var_990 - 1;
         if(_loc2_ <= 0)
         {
            var_990 = 30;
            _loc1_ = name_56.method_73();
            while(_loc1_.method_74())
            {
               _loc5_ = _loc1_.name_1();
               new class_755(_loc5_).method_84(class_899.__unprotect__("c;e"),0);
            }
         }
         method_730();
         super.method_79();
      }
      
      public function method_728() : void
      {
         if(package_24 == null)
         {
            return;
         }
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - package_24.var_244;
         var _loc3_:Number = _loc1_.var_245 - package_24.var_245;
         var _loc4_:Number = Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)));
         var _loc5_:Number = Number(Math.atan2(_loc3_,_loc2_));
         var _loc6_:Number = Number(class_663.method_112(_loc5_ - package_24.var_65,3.14));
         package_24.var_65 = Number(Number(package_24.var_65) + Number(class_663.method_99(-0.6,_loc6_ * 0.3,0.6)));
         var _loc8_:Number = Math.min(_loc4_ / 80,1) * 6;
         package_24.var_278 = Number(package_24.var_278 + Math.cos(Number(package_24.var_65)) * _loc8_);
         package_24.var_279 = Number(package_24.var_279 + Math.sin(Number(package_24.var_65)) * _loc8_);
         var_991 = Number(var_991 + Number(Math.sqrt(Number(package_24.var_278 * package_24.var_278 + package_24.var_279 * package_24.var_279))));
         while(var_991 > 3)
         {
            var_991 = var_991 - 3;
            method_731();
         }
         var _loc10_:Number = class_663.method_99(0,Number(0.5 + package_24.var_245 * 0.15),1) - var_987;
         var_987 = Number(var_987 + _loc10_ * 0.2);
         var _loc11_:Number = var_988;
         var _loc12_:Number = var_987;
         var _loc13_:Number = _loc5_;
         _loc12_ = (1 + Number(Math.sin(_loc13_))) * 0.5;
         _loc11_ = Math.cos(_loc13_) * 2;
         var_992.var_21.gotoAndStop(1 + int(Math.floor(_loc12_ * 40)));
         var_992.var_22.var_4.var_4.scaleY = 1 - 0.8 * _loc12_;
         var_992.var_23.var_4.var_4.scaleY = 1 - 0.8 * _loc12_;
         var_989 = (var_989 + (80 - package_24.var_279 * 16)) % 628;
         var _loc14_:Number = Number(Math.cos(var_989 / 100) * 45 * (1 - _loc12_) + 75 * _loc12_);
         var _loc15_:Number = (50 + _loc14_) * 0.01;
         var_992.var_23.var_4.scaleX = _loc15_;
         var_992.var_22.var_4.var_4.scaleX = _loc15_;
         var_992.var_22.var_4.rotation = Math.cos(var_989 / 100) * 70 * _loc12_;
         var_992.var_23.var_4.rotation = Math.cos(var_989 / 100) * 70 * _loc12_;
         _loc15_ = package_24.var_278 - _loc11_;
         _loc11_ = Number(_loc11_ + _loc15_ * 0.2);
         var _loc16_:Number = _loc11_ * 12;
         var _loc17_:Number = -_loc11_ * 50;
         if(package_24.var_278 > 0)
         {
            var_992.var_22.scaleX = Number(1 + _loc16_ / 100);
            var_992.var_23.scaleX = Number(1 + _loc17_ / 100);
         }
         else
         {
            var_992.var_22.scaleX = 1 - _loc17_ / 100;
            var_992.var_23.scaleX = 1 - _loc16_ / 100;
         }
         package_24.var_243.rotation = _loc11_ * 12;
         var_992.var_21.rotation = _loc11_ * 5;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [520 - 100 * param1];
         super.method_95(param1);
         var_993 = 0;
         z2Eo = 8;
         var_994 = 1;
         if(param1 > 0.1)
         {
            var_994 = var_994 + 1;
         }
         if(param1 > 0.35)
         {
            var_994 = var_994 + 1;
         }
         if(param1 > 0.7)
         {
            var_994 = var_994 + 1;
         }
         if(param1 > 0.9)
         {
            z2Eo = z2Eo + 1;
         }
         if(param1 > 1)
         {
            z2Eo = z2Eo + 1;
         }
         z2Eo = z2Eo - var_994;
         var_995 = 0;
         var_990 = 0;
         var_996 = 0;
         method_117();
         method_732(var_995);
      }
      
      public function method_731() : class_892
      {
         var _loc2_:Number = Number(0.1 + Math.random() * 0.4);
         var _loc4_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("T3\f8"),class_645.var_209));
         _loc4_.var_233 = 0.99;
         var _loc3_:class_892 = _loc4_;
         _loc3_.var_244 = Number(package_24.var_244 + (Math.random() * 2 - 1) * 6);
         _loc3_.var_245 = Number(package_24.var_245 + (Math.random() * 2 - 1) * 6);
         _loc3_.var_243.gotoAndPlay(int(class_691.method_114(10)) + 1);
         _loc3_.var_278 = package_24.var_278 * _loc2_;
         _loc3_.var_279 = package_24.var_279 * _loc2_;
         _loc3_.var_250 = Number(0.05 + Math.random() * 0.1);
         _loc3_.var_251 = 16 + int(class_691.method_114(10));
         _loc3_.method_119(Number(0.3 + Math.random() * 0.5));
         return _loc3_;
      }
      
      public function method_734(param1:MovieClip) : void
      {
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:class_892 = null;
         var _loc3_:int = 0;
         while(_loc3_ < 8)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = _loc4_ / 8;
            _loc6_ = _loc5_ * 20;
            _loc7_ = new class_892(var_232.method_84(class_899.__unprotect__("t\x1c\x16;\x03"),class_645.var_210));
            _loc7_.method_119(1 - _loc5_ * 0.5);
            _loc7_.var_244 = Number(param1.x + (Math.random() * 2 - 1) * _loc6_);
            _loc7_.var_245 = Number(param1.y + (Math.random() * 2 - 1) * _loc6_);
            _loc7_.name_20 = int(_loc5_ * 6);
            _loc7_.var_243.stop();
            _loc7_.var_243.visible = false;
            _loc7_.var_243.rotation = Math.random() * 360;
            _loc7_.var_580 = (Math.random() * 2 - 1) * 16;
            _loc7_.var_687 = 0.92;
            _loc7_.var_250 = -Math.random() * 0.35 * _loc5_;
            _loc7_.var_251 = 17;
         }
         method_733(48,6);
      }
      
      public function method_735() : void
      {
         var_20 = 100;
         method_733(24,4);
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("\r~oD"),class_645.var_210);
         _loc1_.x = package_24.var_244;
         _loc1_.y = package_24.var_245;
         var_990 = 0;
      }
      
      public function method_733(param1:int, param2:int, param3:Object = undefined) : void
      {
         var _loc6_:int = 0;
         var _loc7_:class_892 = null;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc5_:int = 0;
         while(_loc5_ < param1)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = method_731();
            _loc8_ = _loc6_ / param1 * 6.28;
            _loc9_ = Number(2 + Math.random() * param2);
            _loc7_.var_278 = Math.cos(_loc8_) * _loc9_;
            _loc7_.var_279 = Math.sin(_loc8_) * _loc9_;
            _loc7_.var_233 = 0.92;
            _loc7_.var_244 = Number(_loc7_.var_244 + _loc7_.var_278 * 6);
            _loc7_.var_245 = Number(_loc7_.var_245 + _loc7_.var_279 * 6);
            _loc7_.var_251 = _loc7_.var_251 + 20;
            if(param3 != null)
            {
               _loc7_.var_278 = Number(_loc7_.var_278 + package_24.var_278 * param3 * (0.5 + Math.random() * 0.5));
               _loc7_.var_279 = Number(_loc7_.var_279 + package_24.var_279 * param3 * (0.5 + Math.random() * 0.5));
            }
         }
      }
      
      public function method_730() : void
      {
         var _loc2_:* = null;
         if(package_24 == null)
         {
            return;
         }
         var _loc1_:Number = 1;
         if(var_20 != null)
         {
            _loc1_ = Number(_loc1_ + var_20 / 100 * 2);
            _loc2_ = var_20;
            var_20 = var_20 * 0.9;
            if(var_20 < 1)
            {
               var_20 = null;
               _loc2_ = 0;
            }
            class_657.gfof(package_24.var_243,_loc2_ * 0.01,16777215);
         }
         package_24.var_243.filters = [];
         class_658.var_146(package_24.var_243,2,4 * _loc1_,16777215);
         class_658.var_146(package_24.var_243,10,_loc1_,16777215);
      }
      
      public function method_732(param1:int) : void
      {
         var _loc4_:MovieClip = null;
         var _loc2_:Array = class_797.var_986[param1];
         class_657.name_18(var_992.var_22.var_4,int(_loc2_[0]));
         class_657.name_18(var_992.var_23.var_4,int(_loc2_[0]));
         class_657.name_18(var_992.var_21.var_5.var_3,int(_loc2_[1]));
         class_657.name_18(var_992.var_21.var_6.var_2,int(_loc2_[2]));
         class_657.name_18(var_992.var_21.var_7.var_2,int(_loc2_[2]));
         name_56 = new class_795();
         var _loc3_:* = var_997.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(int(_loc4_.var_795) == var_995)
            {
               name_56.method_103(_loc4_);
            }
         }
      }
      
      public function method_729() : void
      {
         var _loc2_:MovieClip = null;
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:class_892 = null;
         var _loc11_:class_892 = null;
         var_996 = int((var_996 + 23) % 628);
         var _loc1_:* = var_997.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            _loc3_ = int((var_996 + int(_loc2_.name_21)) % 628);
            _loc2_.y = Number(Number(_loc2_.var_334) + Math.cos(_loc3_ * 0.01) * 3);
            _loc4_ = _loc2_.x - package_24.var_244;
            _loc5_ = _loc2_.y - package_24.var_245;
            _loc6_ = int(_loc2_.var_795) == var_995;
            if(Number(Math.sqrt(Number(_loc4_ * _loc4_ + _loc5_ * _loc5_))) < (!!_loc6_?26:20))
            {
               if(_loc6_)
               {
                  _loc7_ = 10;
                  _loc8_ = 0;
                  while(_loc8_ < 16)
                  {
                     _loc8_++;
                     _loc9_ = _loc8_;
                     _loc11_ = new class_892(var_232.method_84(class_899.__unprotect__("\x039cu\x02"),class_645.var_209));
                     _loc11_.var_233 = 0.99;
                     _loc10_ = _loc11_;
                     _loc10_.var_244 = Number(_loc2_.x + (Math.random() * 2 - 1) * _loc7_);
                     _loc10_.var_245 = Number(_loc2_.y + (Math.random() * 2 - 1) * _loc7_);
                     _loc10_.name_20 = int(class_691.method_114(20));
                     _loc10_.var_243.visible = false;
                     _loc10_.var_243.stop();
                     _loc10_.var_250 = -(0.1 + Math.random() * 0.1);
                     _loc10_.var_243.blendMode = BlendMode.ADD;
                     _loc10_.var_243.alpha = 0.5;
                     _loc10_.var_251 = 18;
                     class_657.gfof(_loc10_.var_243,Number(0.5 + Math.random() * 0.5),int(class_797.var_986[int(_loc2_.var_795)][0]));
                  }
                  _loc2_.parent.removeChild(_loc2_);
                  var_997.method_116(_loc2_);
                  var_993 = var_993 + 1;
                  if(var_993 == z2Eo)
                  {
                     var_995 = var_995 + 1;
                     method_735();
                     if(var_995 == var_994)
                     {
                        method_81(true,10);
                        name_3 = 3;
                     }
                     else
                     {
                        var_993 = 0;
                        method_732(var_995);
                     }
                  }
                  continue;
               }
               method_81(false,30);
               method_734(_loc2_);
               package_24.method_89();
               _loc2_.parent.removeChild(_loc2_);
               package_24 = null;
               name_3 = 3;
               return;
            }
         }
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Boolean = false;
         var _loc14_:* = null;
         var _loc15_:MovieClip = null;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("Bej\t\x01"),0);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("|jG!\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         package_24 = _loc1_;
         package_24.var_244 = class_742.var_216 * 0.5;
         package_24.var_245 = class_742.var_218 * 0.5;
         package_24.var_65 = 0;
         package_24.var_233 = 0.7;
         package_24.method_118();
         package_24.method_119(1.2);
         var_989 = 0;
         var_987 = 0;
         var_988 = 0;
         var_991 = 0;
         var_992 = package_24.var_243;
         var_992.var_21.stop();
         var_997 = new class_795();
         var _loc3_:int = 0;
         var _loc4_:int = var_994;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = 0;
            _loc7_ = z2Eo;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc9_ = 0;
               _loc10_ = 0;
               do
               {
                  _loc9_ = Number(20 + Math.random() * (class_742.var_216 - 2 * 20));
                  _loc10_ = Number(20 + Math.random() * (class_742.var_218 - 2 * 20));
                  _loc11_ = class_742.var_216 * 0.5 - _loc9_;
                  _loc12_ = class_742.var_218 * 0.5 - _loc10_;
                  _loc13_ = Number(Math.sqrt(Number(_loc11_ * _loc11_ + _loc12_ * _loc12_))) > 80;
                  if(_loc13_)
                  {
                     _loc14_ = var_997.method_73();
                     while(_loc14_.method_74())
                     {
                        _loc15_ = _loc14_.name_1();
                        _loc16_ = _loc15_.x - _loc9_;
                        _loc17_ = _loc15_.y - _loc10_;
                        if(Number(Math.sqrt(Number(_loc16_ * _loc16_ + _loc17_ * _loc17_))) < 30)
                        {
                           _loc13_ = false;
                           break;
                        }
                     }
                  }
               }
               while(!_loc13_);
               
               _loc15_ = var_232.method_84(class_899.__unprotect__("\n\x14;}\x03"),class_645.var_209);
               _loc15_.x = _loc9_;
               _loc15_.y = _loc10_;
               _loc15_.var_334 = _loc10_;
               _loc15_.var_795 = _loc5_;
               _loc15_.name_21 = int(class_691.method_114(628));
               _loc18_ = _loc15_.var_1;
               class_657.name_18(_loc18_,int(class_797.var_986[_loc5_][0]));
               var_997.method_103(_loc15_);
            }
         }
      }
   }
}
