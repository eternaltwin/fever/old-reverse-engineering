package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_882 extends class_645
   {
      
      public static var var_1336:Array = [[1,0],[0,1]];
      
      public static var var_1337:Number = 0.25;
      
      public static var var_1338:Number = 0.25;
      
      public static var var_332:int = 8;
      
      public static var var_456:int = 40;
       
      
      public var var_445:Array;
      
      public var var_1342:int;
      
      public var var_1340:Array;
      
      public var var_496:Object;
      
      public var var_1339:int;
      
      public var var_254:Number;
      
      public var var_1341:int;
      
      public function class_882()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_687() : void
      {
         var _loc4_:MovieClip = null;
         var_254 = Number(Math.min(Number(var_254 + class_882.var_1338),1));
         var _loc1_:Array = [[0,1],[-1,0]][var_1339];
         var _loc2_:int = 0;
         var _loc3_:Array = var_445;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.x = Number(method_394(Number(int(_loc4_.var_319) + int(_loc1_[0]) * var_254)));
            _loc4_.y = Number(method_395(Number(int(_loc4_.var_318) + int(_loc1_[1]) * var_254)));
            if(var_254 == 1)
            {
               var_1340[int(_loc4_.var_319)][int(_loc4_.var_318)] = null;
               _loc4_.var_319 = int(_loc4_.var_319) + int(_loc1_[0]);
               _loc4_.var_318 = int(_loc4_.var_318) + int(_loc1_[1]);
               var_1340[int(_loc4_.var_319)][int(_loc4_.var_318)] = _loc4_;
            }
         }
         if(var_254 == 1)
         {
            method_691();
         }
      }
      
      public function method_1007() : void
      {
         var _loc3_:MovieClip = null;
         var_254 = Number(Math.min(Number(var_254 + class_882.var_1337),1));
         var _loc1_:int = 0;
         var _loc2_:Array = var_445;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            class_657.gfof(_loc3_,Number(Math.pow(var_254,0.5)),16777215);
            if(var_254 == 1)
            {
               _loc3_.parent.removeChild(_loc3_);
            }
         }
         if(var_254 == 1)
         {
            var_1339 = 0;
            method_691();
            method_1006(var_496);
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = method_1008();
               var_1341 = 0;
               if(method_1009(_loc1_) != null)
               {
                  name_94(_loc1_);
                  method_574();
                  break;
               }
               break;
            case 2:
               break;
            case 3:
               method_687();
               break;
            case 4:
               method_1007();
         }
         super.method_79();
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < class_882.var_332 && param2 >= 0 && param2 < class_882.var_332;
      }
      
      public function method_574() : void
      {
         var _loc4_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:MovieClip = null;
         var _loc11_:Sprite = null;
         var _loc12_:* = null;
         name_3 = 2;
         var _loc1_:Array = method_1010(var_496);
         if(int(_loc1_.length) == 0)
         {
            method_81(true);
         }
         var _loc2_:Boolean = true;
         var _loc3_:Array = [];
         _loc4_ = 0;
         var _loc5_:int = class_882.var_332;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = class_882.var_332;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               var var_38:Array = [var_496.name_133[_loc6_][_loc9_]];
               if(var_38[0] != null)
               {
                  if(int(_loc1_[var_38[0].name_134].length) > 1)
                  {
                     _loc2_ = false;
                     _loc10_ = var_1340[_loc6_][_loc9_];
                     _loc11_ = new Sprite();
                     _loc11_.graphics.beginFill(16711680,0);
                     _loc11_.graphics.drawCircle(0,0,20);
                     var var_214:Array = [this];
                     _loc11_.addEventListener(MouseEvent.CLICK,function(param1:Array, param2:Array):Function
                     {
                        var var_214:Array = param1;
                        var var_38:Array = param2;
                        return function(param1:*):void
                        {
                           var_214[0].method_1011(var_38[0].name_134);
                        };
                     }(var_214,var_38));
                     _loc10_.var_951 = _loc11_;
                     _loc10_.addChild(_loc11_);
                  }
                  _loc3_.push({
                     "L\x01":_loc6_,
                     "\x03\x01":_loc9_
                  });
               }
            }
         }
         if(_loc2_)
         {
            _loc4_ = 0;
            while(_loc4_ < int(_loc3_.length))
            {
               _loc12_ = _loc3_[_loc4_];
               _loc4_++;
               _loc10_ = var_232.method_84(class_899.__unprotect__("$\fG\x0e"),3);
               _loc10_.x = Number(method_394(int(_loc12_.var_244)));
               _loc10_.y = Number(method_395(int(_loc12_.var_245)));
            }
            method_81(false,20);
         }
      }
      
      public function method_691() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Boolean = false;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         name_3 = 3;
         var_254 = 0;
         var_445 = [];
         loop4:
         switch(var_1339)
         {
            case 0:
               _loc1_ = 0;
               _loc2_ = class_882.var_332;
               while(_loc1_ < _loc2_)
               {
                  _loc1_++;
                  _loc3_ = _loc1_;
                  _loc4_ = false;
                  _loc5_ = 0;
                  _loc6_ = class_882.var_332;
                  while(_loc5_ < _loc6_)
                  {
                     _loc5_++;
                     _loc7_ = _loc5_;
                     _loc8_ = class_882.var_332 - (1 + _loc7_);
                     if(var_1340[_loc3_][_loc8_] == null)
                     {
                        _loc4_ = true;
                     }
                     else if(_loc4_)
                     {
                        var_445.push(var_1340[_loc3_][_loc8_]);
                     }
                  }
               }
               break;
            case 1:
               _loc1_ = 0;
               _loc2_ = class_882.var_332;
               while(true)
               {
                  if(_loc1_ >= _loc2_)
                  {
                     break loop4;
                  }
                  _loc1_++;
                  _loc3_ = _loc1_;
                  _loc4_ = false;
                  _loc5_ = 0;
                  _loc6_ = class_882.var_332;
                  while(_loc5_ < _loc6_)
                  {
                     _loc5_++;
                     _loc7_ = _loc5_;
                     if(var_1340[_loc7_][_loc3_] == null)
                     {
                        _loc4_ = true;
                     }
                     else if(_loc4_)
                     {
                        var_445.push(var_1340[_loc7_][_loc3_]);
                     }
                  }
               }
         }
         if(int(var_445.length) == 0)
         {
            if(var_1339 == 0)
            {
               var_1339 = var_1339 + 1;
               method_691();
            }
            else
            {
               method_574();
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [550 - 200 * param1];
         super.method_95(param1);
         method_117();
         var_1342 = 10 + int(param1 * 25);
         do
         {
            var_496 = method_1008();
            var_1341 = 0;
         }
         while(method_1009(var_496) == null);
         
         name_94(var_496);
         method_574();
      }
      
      public function method_1006(param1:Object) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc2_:int = 0;
         var _loc3_:int = class_882.var_332;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = 0;
            _loc7_ = class_882.var_332;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc9_ = class_882.var_332 - (1 + _loc8_);
               _loc10_ = param1.name_133[_loc4_][_loc9_];
               if(_loc10_ == null)
               {
                  _loc5_++;
               }
               else if(_loc5_ > 0)
               {
                  param1.name_133[_loc4_][_loc9_ + _loc5_] = _loc10_;
                  param1.name_133[_loc4_][_loc9_] = null;
               }
            }
         }
         _loc2_ = 0;
         _loc3_ = class_882.var_332;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = 0;
            _loc7_ = class_882.var_332;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc10_ = param1.name_133[_loc8_][_loc4_];
               if(_loc10_ == null)
               {
                  _loc5_++;
               }
               else if(_loc5_ > 0)
               {
                  param1.name_133[_loc8_ - _loc5_][_loc4_] = _loc10_;
                  param1.name_133[_loc8_][_loc4_] = null;
               }
            }
         }
      }
      
      public function method_395(param1:Number) : Number
      {
         return Number(60 + param1 * 40);
      }
      
      public function method_394(param1:Number) : Number
      {
         return Number(60 + param1 * 40);
      }
      
      public function method_1009(param1:Object) : Object
      {
         var _loc6_:Array = null;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:* = null;
         if(var_1341 > 100)
         {
            return null;
         }
         var _loc2_:Array = method_1010(param1);
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         while(_loc5_ < int(_loc2_.length))
         {
            _loc6_ = _loc2_[_loc5_];
            _loc5_++;
            if(int(_loc6_.length) > 1)
            {
               _loc3_.push(_loc4_);
            }
            _loc4_++;
         }
         _loc5_ = 0;
         while(_loc5_ < int(_loc3_.length))
         {
            _loc7_ = int(_loc3_[_loc5_]);
            _loc5_++;
            _loc8_ = method_417(param1);
            method_207(_loc8_,_loc7_);
            if(int(_loc8_.name_28) <= 0)
            {
               return int(_loc8_.var_338);
            }
            if(int(_loc8_.var_338) < 10)
            {
               method_1006(_loc8_);
               _loc9_ = method_1009(_loc8_);
               if(_loc9_ != null)
               {
                  return _loc9_;
               }
            }
         }
         return null;
      }
      
      public function method_1010(param1:Object) : Array
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc11_:int = 0;
         var _loc12_:Array = null;
         var _loc13_:Array = null;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:* = null;
         var _loc17_:* = null;
         var _loc18_:* = null;
         var _loc2_:int = 0;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         var _loc5_:int = class_882.var_332;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = class_882.var_332;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               _loc10_ = param1.name_133[_loc6_][_loc9_];
               if(_loc10_ != null)
               {
                  _loc10_.name_134 = null;
               }
            }
         }
         _loc4_ = 0;
         _loc5_ = class_882.var_332;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = class_882.var_332;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               _loc10_ = param1.name_133[_loc6_][_loc9_];
               if(_loc10_ != null)
               {
                  if(_loc10_.name_134 == null)
                  {
                     _loc10_.name_134 = _loc2_;
                     _loc3_[_loc2_] = [_loc10_];
                     _loc2_++;
                  }
                  _loc11_ = 0;
                  _loc12_ = class_882.var_1336;
                  while(_loc11_ < int(_loc12_.length))
                  {
                     _loc13_ = _loc12_[_loc11_];
                     _loc11_++;
                     _loc14_ = _loc6_ + int(_loc13_[0]);
                     _loc15_ = _loc9_ + int(_loc13_[1]);
                     if(method_222(_loc14_,_loc15_))
                     {
                        _loc16_ = param1.name_133[_loc14_][_loc15_];
                        if(_loc16_ != null)
                        {
                           if(int(_loc16_.var_215) == int(_loc10_.var_215))
                           {
                              if(_loc16_.name_134 == null)
                              {
                                 _loc16_.name_134 = _loc10_.name_134;
                                 _loc3_[_loc10_.name_134].push(_loc16_);
                              }
                              else if(_loc16_.name_134 != _loc10_.name_134)
                              {
                                 _loc17_ = _loc16_.name_134;
                                 while(int(_loc3_[_loc17_].length) > 0)
                                 {
                                    _loc18_ = _loc3_[_loc17_].pop();
                                    _loc18_.name_134 = _loc10_.name_134;
                                    _loc3_[_loc10_.name_134].push(_loc18_);
                                 }
                              }
                              continue;
                           }

                        }
                     }
                  }

               }
            }
         }
         return _loc3_;
      }
      
      public function method_1012(param1:int) : Object
      {
         var _loc5_:int = 0;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = class_882.var_332;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_.push([]);
         }
         return {
            "l\x06\x01":_loc2_,
            "\f\x1c3\x01":param1,
            "ZQ;\\\x01":0
         };
      }
      
      public function method_1008() : Object
      {
         var_496 = method_1012(0);
         method_1013(var_496,var_1342);
         method_1006(var_496);
         return var_496;
      }
      
      public function method_1013(param1:Object, param2:int) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         _loc5_ = class_882.var_332;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = class_882.var_332;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               _loc3_.push({
                  "L\x01":_loc6_,
                  "\x03\x01":_loc9_
               });
            }
         }
         _loc4_ = 0;
         while(_loc4_ < param2)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = int(class_691.method_114(int(_loc3_.length)));
            _loc10_ = _loc3_[_loc6_];
            _loc3_.splice(_loc6_,1);
            param1.name_133[int(_loc10_.var_244)][int(_loc10_.var_245)] = {
               "\x1d\x0b\x01":int(class_691.method_114(3)),
               "\x04Ap\x01":null
            };
            param1.name_28 = int(param1.name_28) + 1;
         }
      }
      
      public function name_94(param1:Object) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:MovieClip = null;
         var_1340 = [];
         var _loc2_:int = 0;
         var _loc3_:int = class_882.var_332;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            var_1340.push([]);
            _loc5_ = 0;
            _loc6_ = class_882.var_332;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = param1.name_133[_loc4_][_loc7_];
               if(_loc8_ != null)
               {
                  _loc9_ = var_232.method_84(class_899.__unprotect__("`Lk6"),2);
                  _loc9_.x = Number(method_394(_loc4_));
                  _loc9_.y = Number(method_395(_loc7_));
                  _loc9_.var_319 = _loc4_;
                  _loc9_.var_318 = _loc7_;
                  _loc9_.gotoAndStop(int(_loc8_.var_215) + 1);
                  _loc9_.var_215 = int(var_496.name_133[_loc4_][_loc7_].var_215);
                  var_1340[_loc4_][_loc7_] = _loc9_;
               }
            }
         }
      }
      
      public function method_1011(param1:Object) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc9_:* = null;
         var_445 = [];
         var _loc2_:int = 0;
         var _loc3_:int = class_882.var_332;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = class_882.var_332;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = var_1340[_loc4_][_loc7_];
               if(_loc8_ != null)
               {
                  if(_loc8_.var_951 != null)
                  {
                     _loc8_.removeChild(_loc8_.var_951);
                     _loc8_.var_951 = null;
                  }
                  _loc8_.useHandCursor = false;
                  _loc9_ = var_496.name_133[_loc4_][_loc7_];
                  if(_loc9_.name_134 == param1)
                  {
                     var_445.push(_loc8_);
                     var_496.name_133[_loc4_][_loc7_] = null;
                     var_1340[_loc4_][_loc7_] = null;
                  }
               }
            }
         }
         method_207(var_496,param1);
         name_3 = 4;
         var_254 = 0;
      }
      
      public function method_207(param1:Object, param2:int) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null;
         var _loc3_:int = 0;
         var _loc4_:int = class_882.var_332;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = 0;
            _loc7_ = class_882.var_332;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc9_ = param1.name_133[_loc5_][_loc8_];
               if(_loc9_ != null && _loc9_.name_134 == param2)
               {
                  param1.name_133[_loc5_][_loc8_] = null;
                  param1.name_28 = int(param1.name_28) - 1;
               }
            }
         }
      }
      
      public function method_417(param1:Object) : Object
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null;
         var_1341 = var_1341 + 1;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = class_882.var_332;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_.push([]);
            _loc6_ = 0;
            _loc7_ = class_882.var_332;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc9_ = param1.name_133[_loc5_][_loc8_];
               if(_loc9_ != null)
               {
                  _loc2_[_loc5_][_loc8_] = {
                     "\x1d\x0b\x01":int(_loc9_.var_215),
                     "\x04Ap\x01":_loc9_.name_134
                  };
               }
            }
         }
         return {
            "l\x06\x01":_loc2_,
            "\f\x1c3\x01":int(param1.var_338) + 1,
            "ZQ;\\\x01":int(param1.name_28)
         };
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("`p%Z\x02"),0);
      }
   }
}
