package package_14.package_15
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import package_13.class_672;
   import package_13.class_765;
   import package_13.class_868;
   import package_24.class_738;
   import package_24.class_739;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_673 extends class_672
   {
      
      public static var var_433:int = 68;
      
      public static var var_434:int = 8;
       
      
      public var var_251:int;
      
      public var name_3:int;
      
      public var var_437:Bitmap;
      
      public var var_435:class_739;
      
      public var var_270:Object;
      
      public var var_439:int;
      
      public var var_438:class_738;
      
      public var var_436:int;
      
      public function class_673()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_431 = 128;
         var_432 = 96;
         super();
         var_15();
         var_439 = int(class_691.method_114(6));
         class_652.var_214.method_186(_PlayerAction._Dice(var_439 == 0));
         class_868.var_214.method_195();
         var_437 = new Bitmap();
         var_437.bitmapData = new BitmapData(var_431,class_673.var_433 + class_673.var_434,true,0);
         var_232.method_124(var_437,1);
         var_438 = new class_738();
         var_438.method_128(class_702.package_9.method_98(null,"rainbow"));
         var_435 = new class_739();
         var_435.method_128(class_702.package_9.method_98(0));
         var_435.method_129(-30,40);
         var_435.var_278 = 5;
         var_435.var_279 = -6;
         var_435.var_250 = 1;
         var_435.var_233 = 0.99;
         var_232.method_124(var_435,2);
         var _loc1_:class_738 = new class_738();
         _loc1_.method_128(class_702.package_15.method_98(null,"table"),0,0);
         var_232.method_124(_loc1_,0);
         _loc1_.x = 16;
         _loc1_.y = 64;
         name_3 = 0;
         var_436 = 0;
      }
      
      override public function method_79(param1:*) : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Matrix = null;
         var _loc5_:int = 0;
         switch(name_3)
         {
            case 0:
               if(var_435.y > class_673.var_433)
               {
                  var_436 = var_436 + 1;
                  var_435.y = class_673.var_433;
                  var_435.var_279 = var_435.var_279 * -0.9;
                  var_435.var_278 = var_435.var_278 * 0.8;
               }
               if(var_270 != null)
               {
                  _loc2_ = var_435.x - var_270.var_244;
                  _loc3_ = var_435.y + class_673.var_434 - var_270.var_245;
                  _loc4_ = new Matrix();
                  _loc4_.scale((Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_))) + 4) / 16,1);
                  _loc4_.rotate(Number(Math.atan2(_loc3_,_loc2_)));
                  _loc4_.translate(Number(var_270.var_244),Number(var_270.var_245));
                  var_437.bitmapData.draw(var_438,_loc4_);
               }
               var_270 = {
                  "L\x01":var_435.var_262,
                  "\x03\x01":Number(var_435.var_263 + class_673.var_434)
               };
               _loc5_ = var_439;
               if(var_436 == 0)
               {
                  _loc5_ = int(class_691.method_114(6));
               }
               var_435.method_128(class_702.package_15.method_98(_loc5_,"dice"));
               var_435.rotation = Number(var_435.rotation + 12);
               if(var_436 == 2)
               {
                  name_41();
                  break;
               }
               break;
            case 1:
               _loc5_ = var_251;
               var_251 = var_251 + 1;
               if(_loc5_ > 24)
               {
                  var_435.method_89();
                  method_89();
                  break;
               }
         }
         var _loc6_:BitmapData = var_437.bitmapData;
         _loc6_.colorTransform(_loc6_.rect,new ColorTransform(1,1,1,1,3,0,-5,-5));
      }
      
      public function name_41() : void
      {
         var _loc1_:class_738 = null;
         var _loc2_:class_931 = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:class_739 = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var_435.method_128(class_702.package_15.method_98(var_439,"dice"));
         var_435.var_250 = 0;
         var_435.var_278 = 0;
         var_435.var_279 = 0;
         var_435.rotation = 0;
         name_3 = name_3 + 1;
         var_251 = 0;
         if(var_439 == 0)
         {
            class_765.var_214.package_31._plays = int(class_765.var_214.package_31._plays) + 1;
            class_868.var_214.method_195();
            _loc1_ = class_868.var_214.method_283();
            new class_931(_loc1_);
            _loc2_ = new class_931(var_435);
            _loc2_.var_146(2,8);
            _loc3_ = 0;
            while(_loc3_ < 4)
            {
               _loc3_++;
               _loc4_ = _loc3_;
               _loc5_ = method_125();
               class_652.var_214.var_232.method_124(_loc5_,class_652.var_297);
               _loc5_.var_262 = _loc1_.x + int(class_691.method_114(9)) - 4;
               _loc5_.var_263 = _loc1_.y + int(class_691.method_114(9)) - 4;
               _loc5_.var_279 = _loc5_.var_279 - Math.random();
               _loc5_.method_118();
            }
            _loc3_ = 18;
            _loc4_ = 3;
            _loc6_ = 0;
            while(_loc6_ < _loc3_)
            {
               _loc6_++;
               _loc7_ = _loc6_;
               _loc8_ = _loc7_ / _loc3_ * 6.28;
               _loc9_ = Math.random() * 5;
               _loc5_ = method_125();
               _loc5_.var_278 = Math.cos(_loc8_) * _loc9_;
               _loc5_.var_279 = Math.sin(_loc8_) * _loc9_;
               _loc5_.var_262 = Number(var_435.x + _loc5_.var_278 * _loc4_);
               _loc5_.var_263 = Number(var_435.y + _loc5_.var_278 * _loc4_);
               var_232.method_124(_loc5_,3);
               _loc5_.method_118();
            }
         }
      }
   }
}
