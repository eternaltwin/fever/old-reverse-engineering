package package_14.package_15
{
   import flash.display.Sprite;
   import package_11.package_12.class_658;
   import package_13.class_672;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   
   public class class_920 extends class_672
   {
      
      public static var var_332:int = 16;
      
      public static var var_771:int = 1;
      
      public static var var_1507:int = 10;
      
      public static var var_213:int = 100;
       
      
      public var var_1508:Array;
      
      public var var_1510:class_738;
      
      public var var_1509:Object;
      
      public var var_205:Sprite;
      
      public function class_920()
      {
         var _loc3_:int = 0;
         var _loc4_:class_643 = null;
         var _loc5_:* = null;
         if(class_899.var_239)
         {
            return;
         }
         var_431 = class_920.var_771 * 4 + class_920.var_332 * class_920.var_1507 + class_920.var_771 * (class_920.var_1507 - 1);
         var_432 = var_431;
         super();
         method_281(0);
         var_1508 = [];
         var _loc1_:int = 0;
         var _loc2_:int = class_920.var_213;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = new class_643();
            _loc4_.width = class_920.var_332;
            _loc4_.scaleY = _loc4_.scaleX;
            _loc5_ = method_127(_loc3_);
            _loc4_.x = int(_loc5_.var_244);
            _loc4_.y = int(_loc5_.var_245);
            _loc4_.gotoAndStop(_loc3_ + 1);
            var_232.method_124(_loc4_,1);
            var_1508.push({
               "gB\x01":_loc4_,
               "\x1d\x0b\x01":_loc3_
            });
         }
         var_205 = new Sprite();
         var_232.method_124(var_205,1);
         var_205.visible = false;
         var_205.graphics.lineStyle(1,16777215);
         var_205.graphics.drawRect(0,0,class_920.var_332,class_920.var_332);
         class_658.var_146(var_205,2,200,0);
         var_1510 = new class_738();
         var_1510.method_128(class_702.var_304.method_98(null,"no_entry"));
         var_232.method_124(var_1510,2);
         method_1117(int(class_652.var_214.var_296.name_123));
      }
      
      override public function method_79(param1:*) : void
      {
         super.method_79(param1);
         var _loc2_:* = method_1116();
         if(_loc2_ == null && var_1509 != null)
         {
            method_554();
         }
         if(_loc2_ != null && _loc2_ != var_1509)
         {
            if(var_1509 != null)
            {
               method_554();
            }
            method_171(_loc2_);
         }
      }
      
      public function method_171(param1:Object) : void
      {
         var_1509 = param1;
         var_205.visible = true;
         var_205.x = var_1509.var_631.x;
         var_205.y = var_1509.var_631.y;
      }
      
      public function method_554() : void
      {
         var_1509 = null;
         var_205.visible = false;
      }
      
      public function method_1117(param1:int) : void
      {
         var _loc2_:* = method_127(param1);
         var_1510.x = int(_loc2_.var_244) + 8;
         var_1510.y = int(_loc2_.var_245) + 8;
      }
      
      public function method_127(param1:int) : Object
      {
         return {
            "L\x01":class_920.var_771 * 2 + (class_920.var_332 + class_920.var_771) * (int(param1 % class_920.var_1507)),
            "\x03\x01":class_920.var_771 * 2 + (class_920.var_332 + class_920.var_771) * (int(param1 / class_920.var_1507))
         };
      }
      
      public function method_1116() : Object
      {
         var _loc4_:* = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc1_:Number = class_920.var_332 * 0.5;
         var _loc2_:int = 0;
         var _loc3_:Array = var_1508;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = _loc4_.var_631.x + _loc1_ - mouseX;
            _loc6_ = _loc4_.var_631.y + _loc1_ - mouseY;
            if(Number(Math.abs(_loc6_)) < Number(_loc1_ + 1) && Number(Math.abs(_loc5_)) < Number(_loc1_ + 1))
            {
               return _loc4_;
            }
         }
         return null;
      }
      
      override public function name_5(param1:*) : void
      {
         super.name_5(param1);
         if(var_1509 == null)
         {
            method_89();
            return;
         }
         class_652.var_214.var_296.name_123 = int(var_1509.var_215);
         class_652.var_214.method_189();
         method_1117(int(var_1509.var_215));
      }
   }
}
