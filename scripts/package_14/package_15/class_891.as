package package_14.package_15
{
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_13.class_672;
   import package_13.class_777;
   import package_13.class_868;
   import package_18.package_19.class_872;
   import package_29.class_858;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_891 extends class_672
   {
       
      
      public var var_251:int;
      
      public var var_447:class_872;
      
      public var name_40:Array;
      
      public function class_891()
      {
         var _loc6_:int = 0;
         var _loc7_:class_643 = null;
         if(class_899.var_239)
         {
            return;
         }
         var_431 = 88;
         var_432 = 37;
         super();
         method_281(0);
         var_15();
         var _loc1_:class_777 = class_652.var_214.var_288;
         var_447 = _loc1_.method_297(2);
         if(var_447 == null)
         {
            method_89();
            class_868.var_214.method_467(class_808.var_1038);
            return;
         }
         x = Number(x + (var_447.var_289.var_244 - _loc1_.var_289.var_244) * 16);
         y = Number(y + (var_447.var_289.var_245 - _loc1_.var_289.var_245) * 16);
         y = Number(y + (8 + int(var_432 * 0.5)));
         var _loc4_:Array = class_858.method_923(var_447.package_31,int(var_447.var_289.var_322[0]));
         name_40 = [];
         var _loc5_:int = 0;
         while(_loc5_ < 10)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = new class_643();
            _loc7_.width = 16;
            _loc7_.scaleY = _loc7_.scaleX;
            _loc7_.y = 2 + (16 + 1) * (int(_loc6_ / 5));
            _loc7_.x = 2 + (16 + 1) * (int(_loc6_ % 5));
            class_657.gfof(_loc7_,1,3355443);
            _loc7_.gotoAndStop(int(_loc4_[_loc6_]) + 1);
            var_232.method_124(_loc7_,1);
            name_40.push(_loc7_);
         }
         class_658.var_146(var_447,2,12,16777215,false);
         var_251 = 0;
      }
      
      override public function method_79(param1:*) : void
      {
         var _loc3_:class_931 = null;
         var _loc2_:int = var_251;
         var_251 = var_251 + 1;
         if(_loc2_ > 3 && int(name_40.length) > 0)
         {
            var_251 = 0;
            _loc3_ = new class_931(name_40.shift());
            _loc3_.var_146(2,4);
         }
      }
      
      override public function name_5(param1:*) : void
      {
         super.name_5(param1);
         var_447.filters = [];
         method_89();
      }
   }
}
