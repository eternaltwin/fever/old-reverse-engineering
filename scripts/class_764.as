package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.class_755;
   import package_11.package_12.class_658;
   import package_11.package_12.class_660;
   import package_32.class_899;
   
   public class class_764 extends class_645
   {
      
      public static var var_332:int = 8;
      
      public static var var_456:int = 40;
      
      public static var var_826:Array = [[1,2],[1,-2],[2,1],[2,-1],[-1,2],[-1,-2],[-2,1],[-2,-1]];
      
      public static var var_827:Array = [[1,1],[-1,1]];
       
      
      public var var_828:Array;
      
      public var var_283:class_660;
      
      public var var_831:Array;
      
      public var var_832:Array;
      
      public var var_799:class_755;
      
      public var y693:Array;
      
      public var var_446:Array;
      
      public var var_641:Number;
      
      public var var_642:Number;
      
      public var var_833:Array;
      
      public var var_829:Object;
      
      public var var_254:Number;
      
      public var var_830:Object;
      
      public function class_764()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_109(param1:MovieClip, param2:MovieClip) : int
      {
         if(param1.y < param2.y)
         {
            return -1;
         }
         return 1;
      }
      
      public function method_575() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:* = null;
         var_254 = Number(Math.min(Number(var_254 + 0.1),1));
         var _loc1_:* = var_283.method_127(var_254);
         var_829.var_631.x = Number(_loc1_.var_244);
         var_829.var_631.y = _loc1_.var_245 - Math.sin(var_254 * 3.14) * 40;
         if(var_254 == 1)
         {
            _loc2_ = 0;
            _loc3_ = y693;
            while(_loc2_ < int(_loc3_.length))
            {
               _loc4_ = _loc3_[_loc2_];
               _loc2_++;
               if(int(_loc4_.var_319) == int(var_829.var_319) && int(_loc4_.var_318) == int(var_829.var_318) && _loc4_ != var_829)
               {
                  _loc4_.var_631.parent.removeChild(_loc4_.var_631);
                  y693.method_116(_loc4_);
                  method_424(int(var_829.var_319),int(var_829.var_318),1);
                  method_102(8);
                  break;
               }
            }
            _loc3_ = method_573();
            var_830 = _loc3_[int(var_829.var_319)][int(var_829.var_318)];
            if(var_830 == null)
            {
               if(int(y693.length) > 1)
               {
                  method_574();
               }
               else
               {
                  name_3 = 4;
                  method_81(true,30);
               }
            }
            else
            {
               name_3 = 3;
               var_254 = 0;
               var_283 = new class_660(Number(method_394(int(var_830.var_319))),Number(method_395(int(var_830.var_318))),Number(method_394(int(var_829.var_319))),Number(method_395(int(var_829.var_318))));
            }
         }
      }
      
      public function method_576() : void
      {
         var _loc1_:Number = var_283.var_346 - var_283.var_347;
         var _loc2_:Number = var_283.var_345 - var_283.var_348;
         var _loc3_:Number = 10 / Math.sqrt(Number(_loc1_ * _loc1_ + _loc2_ * _loc2_));
         var_254 = Number(Math.min(Number(var_254 + _loc3_),1));
         var _loc4_:* = var_283.method_127(var_254);
         var_830.var_631.x = Number(_loc4_.var_244);
         var_830.var_631.y = Number(_loc4_.var_245);
         if(var_254 == 1)
         {
            method_424(int(var_829.var_319),int(var_829.var_318),0);
            var_829.var_631.parent.removeChild(var_829.var_631);
            name_3 = 4;
            method_81(false,30);
         }
      }
      
      override public function method_79() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               method_575();
               break;
            case 3:
               method_576();
         }
         var_828.sort(method_109);
         super.method_79();
         var _loc1_:Array = var_446.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            if(_loc3_.var_243.visible != true)
            {
               var_446.method_116(_loc3_);
               _loc3_.var_531.parent.removeChild(_loc3_.var_531);
            }
            else
            {
               if(_loc3_.var_233 != null)
               {
                  _loc3_.name_65 = _loc3_.name_65 * _loc3_.var_233;
               }
               _loc3_.name_66 = Number(Number(_loc3_.name_66) + Number(_loc3_.name_65));
               _loc3_.name_65 = Number(Number(_loc3_.name_65) + Number(_loc3_.name_77));
               if(Number(_loc3_.name_66) > 0)
               {
                  _loc3_.name_66 = 0;
                  _loc3_.name_65 = _loc3_.name_65 * -0.75;
               }
               _loc3_.var_243.y = Number(_loc3_.var_243.y + Number(_loc3_.name_66));
               _loc3_.var_531.x = _loc3_.var_244;
               _loc3_.var_531.y = _loc3_.var_245;
               _loc4_ = _loc3_.var_243.scaleX;
               _loc3_.var_531.scaleY = _loc4_;
               _loc3_.var_531.scaleX = _loc4_;
            }
         }
      }
      
      public function method_400(param1:int, param2:int) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:MovieClip = null;
         var _loc3_:int = 0;
         var _loc4_:int = class_764.var_332;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = 0;
            _loc7_ = class_764.var_332;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               method_577(var_831[_loc5_][_loc8_]);
            }
         }
         while(int(var_832.length) > 0)
         {
            _loc9_ = var_832.pop();
            _loc9_.parent.removeChild(_loc9_);
         }
         var_283 = new class_660(Number(method_394(int(var_829.var_319))),Number(method_395(int(var_829.var_318))),Number(method_394(param1)),Number(method_395(param2)));
         name_3 = 2;
         var_254 = 0;
         var_829.var_319 = param1;
         var_829.var_318 = param2;
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < class_764.var_332 && param2 >= 0 && param2 < class_764.var_332;
      }
      
      public function method_574() : void
      {
         var _loc3_:* = null;
         var _loc4_:MovieClip = null;
         name_3 = 1;
         var _loc1_:Array = method_578(0,int(var_829.var_319),int(var_829.var_318));
         var_832 = [];
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc4_ = var_232.method_84(class_899.__unprotect__("\x1e\x05\x13\x0b"),0);
            _loc4_.x = method_394(int(_loc3_.var_244)) - class_764.var_456 * 0.5;
            _loc4_.y = method_395(int(_loc3_.var_245)) - class_764.var_456;
            var_832.push(_loc4_);
            method_579(var_831[int(_loc3_.var_244)][int(_loc3_.var_245)],int(_loc3_.var_244),int(_loc3_.var_245));
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [560 - 100 * param1];
         super.method_95(param1);
         var_828 = [];
         var_446 = [];
         method_117();
         method_574();
      }
      
      public function method_578(param1:int, param2:int, param3:int, param4:Array = undefined) : Array
      {
         var _loc7_:int = 0;
         var _loc8_:Array = null;
         var _loc9_:Array = null;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc5_:Array = [];
         var _loc6_:Array = null;
         switch(param1)
         {
            case 0:
               _loc7_ = 0;
               _loc8_ = class_764.var_826;
               while(_loc7_ < int(_loc8_.length))
               {
                  _loc9_ = _loc8_[_loc7_];
                  _loc7_++;
                  _loc10_ = param2 + int(_loc9_[0]);
                  _loc11_ = param3 + int(_loc9_[1]);
                  if(method_222(_loc10_,_loc11_))
                  {
                     _loc5_.push({
                        "L\x01":_loc10_,
                        "\x03\x01":_loc11_
                     });
                  }
               }
               break;
            case 1:
               _loc6_ = class_742.var_264;
               break;
            case 2:
               _loc6_ = class_742.var_748;
               break;
            case 3:
               _loc7_ = 0;
               _loc8_ = class_764.var_827;
               while(_loc7_ < int(_loc8_.length))
               {
                  _loc9_ = _loc8_[_loc7_];
                  _loc7_++;
                  _loc10_ = param2 + int(_loc9_[0]);
                  _loc11_ = param3 + int(_loc9_[1]);
                  if(method_222(_loc10_,_loc11_))
                  {
                     _loc5_.push({
                        "L\x01":_loc10_,
                        "\x03\x01":_loc11_
                     });
                  }
               }
               break;
            case 4:
               _loc6_ = class_742.var_747;
         }
         if(_loc6_ != null)
         {
            _loc7_ = 0;
            while(_loc7_ < int(_loc6_.length))
            {
               _loc8_ = _loc6_[_loc7_];
               _loc7_++;
               _loc10_ = 1;
               while(_loc10_ < class_764.var_332)
               {
                  _loc11_ = param2 + int(_loc8_[0]) * _loc10_;
                  _loc12_ = param3 + int(_loc8_[1]) * _loc10_;
                  if(method_222(_loc11_,_loc12_) && param4[_loc11_][_loc12_] != 0)
                  {
                     _loc5_.push({
                        "L\x01":_loc11_,
                        "\x03\x01":_loc12_
                     });
                     _loc10_++;
                     continue;
                  }
                  break;
               }
            }
         }
         return _loc5_;
      }
      
      public function method_395(param1:int) : Number
      {
         return Number(var_641 + (param1 + 1) * class_764.var_456);
      }
      
      public function method_394(param1:int) : Number
      {
         return Number(var_642 + (param1 + 0.5) * class_764.var_456);
      }
      
      public function method_580() : Array
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc6_:* = null;
         var _loc7_:Array = null;
         var _loc8_:* = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         _loc3_ = class_764.var_332;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc1_[_loc4_] = [];
         }
         var _loc5_:Array = y693.method_78();
         _loc5_.method_116(var_829);
         _loc2_ = 0;
         while(_loc2_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc2_];
            _loc2_++;
            _loc1_[int(_loc6_.var_319)][int(_loc6_.var_318)] = 0;
         }
         _loc2_ = 0;
         while(_loc2_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc2_];
            _loc2_++;
            _loc7_ = method_578(int(_loc6_.var_252),int(_loc6_.var_319),int(_loc6_.var_318),_loc1_);
            _loc3_ = 0;
            while(_loc3_ < int(_loc7_.length))
            {
               _loc8_ = _loc7_[_loc3_];
               _loc3_++;
               _loc1_[int(_loc8_.var_244)][int(_loc8_.var_245)] = 1;
            }
         }
         return _loc1_;
      }
      
      public function method_573() : Array
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc7_:* = null;
         var _loc8_:Array = null;
         var _loc9_:* = null;
         var _loc1_:Array = method_580();
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         _loc4_ = class_764.var_332;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_[_loc5_] = [];
         }
         _loc3_ = 0;
         var _loc6_:Array = y693;
         while(_loc3_ < int(_loc6_.length))
         {
            _loc7_ = _loc6_[_loc3_];
            _loc3_++;
            if(_loc7_ != var_829)
            {
               _loc8_ = method_578(int(_loc7_.var_252),int(_loc7_.var_319),int(_loc7_.var_318),_loc1_);
               _loc4_ = 0;
               while(_loc4_ < int(_loc8_.length))
               {
                  _loc9_ = _loc8_[_loc4_];
                  _loc4_++;
                  _loc2_[int(_loc9_.var_244)][int(_loc9_.var_245)] = _loc7_;
               }

            }
         }
         return _loc2_;
      }
      
      public function method_582() : void
      {
         var _loc5_:* = null;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc8_:Array = null;
         var _loc9_:Array = null;
         var _loc10_:Array = null;
         var _loc11_:int = 0;
         var _loc12_:* = null;
         y693 = [];
         var _loc1_:int = int(class_691.method_114(class_764.var_332));
         var _loc2_:int = int(class_691.method_114(class_764.var_332));
         var _loc3_:Array = [3,null,null,3,null,3,3,null,2,null,1,null,2,null,1,4,null];
         var _loc4_:Number = int(_loc3_.length) * Math.min(Number(Math.max(0.1,var_237[0])),1);
         while(int(_loc3_.length) > _loc4_)
         {
            _loc3_.pop();
         }
         while(int(_loc3_.length) > 0)
         {
            _loc5_ = null;
            _loc6_ = int(class_691.method_114(int(_loc3_.length)));
            _loc7_ = _loc3_[_loc6_];
            _loc3_.splice(_loc6_,1);
            if(_loc7_ != null)
            {
               _loc5_ = method_581(_loc7_,1,_loc1_,_loc2_);
            }
            _loc8_ = method_580();
            _loc9_ = method_578(0,_loc1_,_loc2_);
            _loc10_ = [];
            _loc11_ = 0;
            while(_loc11_ < int(_loc9_.length))
            {
               _loc12_ = _loc9_[_loc11_];
               _loc11_++;
               if(_loc8_[int(_loc12_.var_244)][int(_loc12_.var_245)] == null)
               {
                  _loc10_.push(_loc12_);
               }
            }
            if(int(_loc10_.length) == 0)
            {
               if(_loc5_ != null)
               {
                  _loc12_ = y693.pop();
                  _loc12_.var_631.parent.removeChild(_loc12_.var_631);
                  break;
               }
               break;
            }
            _loc12_ = _loc10_[int(class_691.method_114(int(_loc10_.length)))];
            _loc1_ = int(_loc12_.var_244);
            _loc2_ = int(_loc12_.var_245);
         }
         var_829 = method_581(0,0,_loc1_,_loc2_);
         if(int(_loc3_.length) > 1)
         {
            while(int(y693.length) > 0)
            {
               _loc5_ = y693.pop();
               _loc5_.var_631.parent.removeChild(_loc5_.var_631);
            }
            method_582();
         }
      }
      
      public function method_581(param1:int, param2:int, param3:int, param4:int) : Object
      {
         var _loc5_:* = {
            "gB\x01":var_232.method_84(class_899.__unprotect__("n0\x05%\x03"),1),
            "cHX@\x01":param1,
            "k\x15\tx\x02":param2,
            "{e\x01":param3,
            "\x0fX\x01":param4
         };
         var _loc6_:class_1 = _loc5_.var_631;
         _loc6_.var_1.gotoAndStop(param1 + 1);
         _loc5_.var_631.x = Number(method_394(param3));
         _loc5_.var_631.y = Number(method_395(param4));
         if(param2 == 1)
         {
            class_658.var_146(_loc5_.var_631,2,4,16777215);
         }
         class_658.var_146(_loc5_.var_631,2,4,0);
         if(param2 == 0)
         {
            class_658.var_146(_loc5_.var_631,2,4,16777215);
         }
         y693.push(_loc5_);
         var_828.push(_loc5_.var_631);
         _loc5_.var_631.mouseEnabled = false;
         _loc5_.var_631.mouseChildren = false;
         return _loc5_;
      }
      
      public function method_424(param1:int, param2:int, param3:int) : void
      {
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:MovieClip = null;
         var _loc6_:int = 0;
         while(_loc6_ < 20)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc8_ = new class_892(var_232.method_84(class_899.__unprotect__("))\x178\x02"),1));
            _loc9_ = _loc7_ / 20 * 6.28;
            _loc10_ = Math.random() * 3;
            _loc8_.var_278 = Math.cos(_loc9_) * _loc10_;
            _loc8_.var_279 = Math.sin(_loc9_) * _loc10_;
            _loc8_.name_65 = _loc10_ - 5;
            _loc8_.var_244 = Number(Number(method_394(param1)) + _loc8_.var_278 * 3);
            _loc8_.var_245 = Number(Number(method_395(param2)) + _loc8_.var_279 * 3);
            _loc8_.name_66 = _loc8_.name_65 * 3;
            _loc8_.name_77 = Number(0.15 + Math.random() * 0.4);
            _loc8_.var_251 = 20 + int(class_691.method_114(40));
            _loc8_.var_272 = 0;
            _loc8_.var_243.gotoAndStop(param3 + 1);
            class_658.var_146(_loc8_.var_243,2,4,int([0,16777215][param3]));
            var_828.push(_loc8_.var_243);
            var_446.push(_loc8_);
            _loc8_.var_531 = var_799.method_84(class_899.__unprotect__("))\x178\x02"),0);
            _loc8_.var_531.x = _loc8_.var_244;
            _loc8_.var_531.y = _loc8_.var_245;
            _loc11_ = _loc8_.var_531;
            _loc11_.gotoAndStop(2);
         }
      }
      
      public function method_577(param1:MovieClip) : void
      {
         param1.mouseEnabled = false;
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("Rui\x18\x02"),0);
         var_642 = (class_742.var_216 - class_764.var_332 * class_764.var_456) * 0.5;
         var_641 = (class_742.var_218 - class_764.var_332 * class_764.var_456) * 0.5;
         var_831 = [];
         var _loc1_:int = 0;
         var _loc2_:int = class_764.var_332;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_831[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = class_764.var_332;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               _loc7_ = var_232.method_84(class_899.__unprotect__("h\r\x19k\x01"),0);
               _loc7_.x = Number(var_642 + _loc3_ * class_764.var_456);
               _loc7_.y = Number(var_641 + _loc6_ * class_764.var_456);
               _loc7_.gotoAndStop(1 + int((_loc3_ + _loc6_) % 2));
               var_831[_loc3_][_loc6_] = _loc7_;
            }
         }
         _loc7_ = var_232.method_92(0);
         var_799 = new class_755(_loc7_);
         _loc7_.blendMode = BlendMode.LAYER;
         _loc7_.alpha = 0.2;
         method_582();
      }
      
      public function method_579(param1:MovieClip, param2:int, param3:int) : void
      {
         var var_244:int = param2;
         var var_245:int = param3;
         var var_214:class_764 = this;
         if(param1.tabIndex != 99)
         {
            param1.addEventListener(MouseEvent.CLICK,function(param1:*):void
            {
               var_214.method_400(var_244,var_245);
            });
            param1.tabIndex = 99;
         }
         param1.mouseEnabled = true;
      }
   }
}
