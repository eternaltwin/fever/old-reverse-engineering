package
{
   import package_32.class_899;
   
   public class class_722
   {
       
      
      public var result:Array;
      
      public var r:RegExp;
      
      public function class_722(param1:String = undefined, param2:String = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         r = new RegExp(param1,param2);
      }
      
      public function method_427(param1:String) : Array
      {
         return param1.replace(r,"#__delim__#").method_427("#__delim__#");
      }
      
      public function name_54(param1:String, param2:String) : String
      {
         return param1.replace(r,param2);
      }
      
      public function method_428() : String
      {
         if(result == null)
         {
            class_899.var_317 = new Error();
            throw "No string matched";
         }
         var _loc1_:int = int(result.index) + result[0].length;
         var _loc2_:String = result.input;
         return _loc2_.substr(_loc1_,_loc2_.length - _loc1_);
      }
      
      public function method_429() : Object
      {
         if(result == null)
         {
            class_899.var_317 = new Error();
            throw "No string matched";
         }
         return {
            "gz&\x01":int(result.index),
            "OHc\x01":result[0].length
         };
      }
      
      public function method_430() : String
      {
         if(result == null)
         {
            class_899.var_317 = new Error();
            throw "No string matched";
         }
         var _loc1_:String = result.input;
         return _loc1_.substr(0,int(result.index));
      }
      
      public function method_431(param1:int) : String
      {
         if(result != null && param1 >= 0 && param1 < result.length)
         {
            return result[param1];
         }
         class_899.var_317 = new Error();
         throw "EReg::matched";
      }
      
      public function method_432(param1:String) : Boolean
      {
         result = r.exec(param1);
         return result != null;
      }
      
      public function method_433(param1:String, param2:Function) : String
      {
         var _loc3_:StringBuf = new StringBuf();
         while(method_432(param1))
         {
            _loc3_.b = _loc3_.b + method_430();
            _loc3_.b = _loc3_.b + param2(this);
            param1 = method_428();
         }
         _loc3_.b = _loc3_.b + param1;
         return _loc3_.b;
      }
   }
}
