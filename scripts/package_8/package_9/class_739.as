package package_8.package_9
{
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import package_32.class_899;
   
   public class class_739 extends class_647
   {
       
      
      public var var_245:Number;
      
      public var var_244:Number;
      
      public var var_250:Number;
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_580:Number;
      
      public var var_251:int;
      
      public var var_1343:Number;
      
      public var var_344:Number;
      
      public var var_243:Sprite;
      
      public var var_947:Number;
      
      public var var_1344:Function;
      
      public var var_532:Object;
      
      public var var_233:Number;
      
      public var var_476:Boolean;
      
      public var var_272:int;
      
      public var var_273:int;
      
      public var var_1345:Object;
      
      public var var_1012:Number;
      
      public function class_739(param1:Sprite = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_243 = param1;
         var_476 = false;
         var_244 = var_243.x;
         var_245 = var_243.y;
         var_278 = 0;
         var_279 = 0;
         var_250 = 0;
         var_580 = 0;
         var_1343 = 1;
         var_947 = 1;
         var_233 = 1;
         var_947 = 1;
         var_344 = 1;
         var_1012 = 1;
         var_251 = -1;
         var_273 = 10;
         var_272 = 0;
      }
      
      public function method_118() : void
      {
         var_243.x = var_244;
         var_243.y = var_245;
         if(var_476)
         {
            var_243.x = int(var_243.x);
            var_243.y = int(var_243.y);
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var_279 = Number(var_279 + var_250);
         var_278 = var_278 * var_233;
         var_279 = var_279 * var_233;
         var_244 = Number(var_244 + var_278);
         var_245 = Number(var_245 + var_279);
         var_580 = var_580 * var_947;
         var_243.rotation = Number(var_243.rotation + var_580);
         var_344 = var_344 * var_1343;
         method_119(var_344);
         if(var_532 != null)
         {
            if(var_245 > Number(var_532.var_245))
            {
               var_245 = Number(var_532.var_245);
               var_278 = var_278 * var_532.name_73;
               var_279 = var_279 * -var_532.name_74;
               if(var_1344 != null)
               {
                  var_1344();
               }
            }
         }
         if(var_1345 != null)
         {
            var_1345.var_251 = int(var_1345.var_251) + 1;
            _loc1_ = int(var_1345.var_251) / int(var_1345.var_142);
            switch(var_272)
            {
               case 0:
                  break;
               case 1:
                  var_243.alpha = _loc1_ * var_1012;
                  break;
               case 2:
                  _loc2_ = _loc1_ * var_344;
                  var_243.scaleY = _loc2_;
                  var_243.scaleX = _loc2_;
                  break;
               case 3:
                  var_243.scaleX = _loc1_ * var_344;
                  break;
               case 4:
                  var_243.scaleY = _loc1_ * var_344;
            }
            if(_loc1_ == 1)
            {
               var_1345 = null;
            }
         }
         var_251 = var_251 - 1;
         if(var_251 < var_273 && var_251 >= 0)
         {
            _loc1_ = var_251 / var_273;
            switch(var_272)
            {
               case 0:
                  break;
               case 1:
                  var_243.alpha = _loc1_ * var_1012;
                  break;
               case 2:
                  _loc2_ = _loc1_ * var_344;
                  var_243.scaleY = _loc2_;
                  var_243.scaleX = _loc2_;
                  break;
               case 3:
                  var_243.scaleX = _loc1_ * var_344;
                  break;
               case 4:
                  var_243.scaleY = _loc1_ * var_344;
            }
         }
         if(var_251 == 0)
         {
            method_89();
         }
         method_118();
      }
      
      public function method_1014(param1:int, param2:Object = undefined) : void
      {
         if(param2 != null)
         {
            var_947 = param2;
         }
         var_243.rotation = Math.random() * 360;
         var_580 = (Math.random() * 2 - 1) * param1;
      }
      
      public function name_20(param1:int, param2:Boolean = false) : void
      {
         var _loc3_:class_921 = new class_921(this,null,param1);
         var_243.visible = false;
         var var_214:class_739 = this;
         var _loc4_:Function = function():void
         {
            var_214.var_243.visible = true;
         };
         if(param2)
         {
            var var_631:MovieClip = var_243;
            var_631.stop();
            _loc4_ = function():void
            {
               var_214.var_243.visible = true;
               var_631.play();
            };
         }
         _loc3_.var_258 = _loc4_;
      }
      
      public function method_119(param1:Number) : void
      {
         var_344 = param1;
         var_243.scaleX = param1;
         var_243.scaleY = param1;
      }
      
      public function method_129(param1:Number, param2:Number) : void
      {
         var_244 = param1;
         var_245 = param2;
         method_118();
      }
      
      public function method_488(param1:Number, param2:Number, param3:Number, param4:Object = undefined) : void
      {
         var var_251:Object = param4;
         var_532 = {
            "\x03\x01":param1,
            "n,A\x01":param2,
            "vA;\x01":param3
         };
         if(var_251 != null)
         {
            var var_214:class_739 = this;
            var_1344 = function():void
            {
               var_214.var_251 = var_251;
            };
         }
      }
      
      public function method_685(param1:Number) : void
      {
         var_1012 = param1;
         var_243.alpha = param1;
      }
      
      public function name_50(param1:int) : void
      {
         var_244 = Number(var_244 + var_278 * param1);
         var_245 = Number(var_245 + var_279 * param1);
         method_118();
      }
      
      override public function method_89() : void
      {
         if(var_243.parent != null)
         {
            var_243.parent.removeChild(var_243);
         }
         super.method_89();
      }
      
      public function method_1015() : void
      {
         var _loc1_:* = var_243;
         _loc1_.method_89 = method_89;
      }
      
      public function name_91(param1:int) : void
      {
         var_1345 = {
            "oCq\r\x03":0,
            "_(\f":param1
         };
         var_243.scaleY = Number(0);
         var_243.scaleX = 0;
      }
   }
}
