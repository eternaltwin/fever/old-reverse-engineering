package package_8.package_9
{
   import flash.display.DisplayObject;
   import package_32.class_899;
   
   public class class_660 extends class_647
   {
       
      
      public var var_283:package_11.package_12.class_660;
      
      public var var_351:Number;
      
      public var name_9:Object;
      
      public var var_243:DisplayObject;
      
      public var var_476:Boolean;
      
      public var var_27:Function;
      
      public function class_660(param1:DisplayObject = undefined, param2:Object = undefined, param3:Object = undefined, param4:Number = 0.1, param5:Object = undefined, param6:Object = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_243 = param1;
         var_351 = param4;
         if(param5 == null)
         {
            param5 = var_243.x;
         }
         if(param6 == null)
         {
            param6 = var_243.y;
         }
         var_283 = new package_11.package_12.class_660(param5,param6,param2,param3);
         var_27 = var_283.method_127;
      }
      
      override public function method_79() : void
      {
         var _loc3_:Number = NaN;
         var_254 = Number(Math.min(Number(var_254 + var_351),1));
         var _loc1_:Number = Number(var_255(var_254));
         var _loc2_:* = var_27(_loc1_);
         if(name_9 != null)
         {
            _loc3_ = Number(Math.sin(_loc1_ * 3.14));
            _loc2_.var_244 = Number(Number(_loc2_.var_244) + name_9.var_336 * _loc3_);
            _loc2_.var_245 = Number(Number(_loc2_.var_245) + name_9.var_337 * _loc3_);
         }
         var_243.x = Number(_loc2_.var_244);
         var_243.y = Number(_loc2_.var_245);
         if(var_476)
         {
            var_243.x = int(var_243.x);
            var_243.y = int(var_243.y);
         }
         if(var_254 == 1)
         {
            method_89();
         }
      }
      
      public function method_303(param1:int, param2:Number = -1.57) : void
      {
         name_9 = {
            "4j\x01":Math.cos(param2) * param1,
            "_s\x01":Math.sin(param2) * param1
         };
      }
      
      public function method_304(param1:Number) : void
      {
         var _loc2_:Number = var_283.var_347 - var_283.var_346;
         var _loc3_:Number = var_283.var_348 - var_283.var_345;
         var_351 = param1 / Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_));
      }
      
      public function method_305(param1:Object, param2:Object) : void
      {
         var var_244:Object = param1;
         var var_245:Object = param2;
         var var_214:package_8.package_9.class_660 = this;
         var_27 = function(param1:Number):Object
         {
            return var_214.var_283.method_251(param1,var_244,var_245);
         };
      }
      
      public function method_306() : void
      {
         var var_214:package_8.package_9.class_660 = this;
         var_258 = function():void
         {
            var_214.var_243.x = int(var_214.var_243.x);
            var_214.var_243.y = int(var_214.var_243.y);
         };
      }
   }
}
