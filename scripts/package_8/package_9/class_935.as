package package_8.package_9
{
   import flash.display.DisplayObject;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_935 extends class_647
   {
       
      
      public var var_251:int;
      
      public var var_351:Number;
      
      public var var_243:DisplayObject;
      
      public var var_369:Object;
      
      public function class_935(param1:DisplayObject = undefined, param2:Number = 0.1, param3:Object = undefined, param4:int = -1)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_369 = param3;
         super();
         var_243 = param1;
         var_351 = param2;
         var_251 = param4;
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         if(!var_243.visible || _loc1_ == 0)
         {
            method_89();
            return;
         }
         method_1016();
      }
      
      public function method_1016() : void
      {
         var_254 = (var_254 + var_351) % 1;
         var _loc1_:Number = Number(0.5 + Math.cos(var_255(var_254) * 6.28) * 0.5);
         if(var_369 == null)
         {
            class_657.name_18(var_243,0,int(255 * _loc1_));
         }
         else
         {
            class_657.gfof(var_243,_loc1_,var_369);
         }
      }
      
      override public function method_89() : void
      {
         class_657.name_18(var_243,0,0);
         super.method_89();
      }
   }
}
