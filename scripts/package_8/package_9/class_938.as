package package_8.package_9
{
   import package_32.class_899;
   
   public class class_938
   {
       
      
      public var var_1561:Array;
      
      public function class_938()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_1561 = [];
         if(class_647.var_253 == null)
         {
            class_647.var_253 = this;
         }
      }
      
      public function method_79() : void
      {
         var _loc3_:class_647 = null;
         var _loc1_:Array = var_1561.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.method_79();
         }
      }
      
      public function method_116(param1:class_647) : void
      {
         var_1561.method_116(param1);
      }
      
      public function method_266() : void
      {
         while(int(var_1561.length) > 0)
         {
            var_1561[0].method_89();
         }
      }
      
      public function method_124(param1:class_647) : void
      {
         var_1561.push(param1);
      }
   }
}
