package package_8.package_9
{
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_671 extends class_647
   {
       
      
      public var var_428:DisplayObjectContainer;
      
      public var name_40:Array;
      
      public var var_430:int;
      
      public var var_429:Boolean;
      
      public function class_671(param1:DisplayObjectContainer = undefined, param2:String = undefined, param3:int = 0, param4:Boolean = false)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_429 = param4;
         var_430 = param3;
         var_428 = param1;
         name_40 = [param2];
         method_79();
      }
      
      override public function method_79() : void
      {
         var _loc4_:String = null;
         var _loc1_:MovieClip = var_428;
         var _loc2_:int = 0;
         var _loc3_:Array = name_40;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc1_ = Reflect.field(_loc1_,_loc4_);
            if(_loc1_ == null)
            {
               break;
            }
         }
         if(_loc1_ != null)
         {
            if(var_429)
            {
               _loc1_.gotoAndPlay(var_430);
            }
            else
            {
               _loc1_.gotoAndStop(var_430);
            }
            method_89();
         }
      }
   }
}
