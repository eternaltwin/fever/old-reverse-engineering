package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_828 extends class_645
   {
      
      public static var var_1092:int = 48;
      
      public static var var_350:int = 220;
      
      public static var var_493:int = 10;
      
      public static var var_892:int = 70;
      
      public static var var_1093:int = 17;
      
      public static var var_1094:int = 3;
      
      public static var var_1095:int = 8;
       
      
      public var var_251:Number;
      
      public var var_1097:Number;
      
      public var var_1096:int;
      
      public var name_36:Object;
      
      public var var_700:Number;
      
      public var var_24:MovieClip;
      
      public var var_713:MovieClip;
      
      public var var_495:Array;
      
      public function class_828()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         var_251 = var_251 - 1;
         if(var_251 < 0)
         {
            var_251 = class_828.var_1092;
            method_801();
         }
         method_802();
         if(name_36 == var_1096 - 1)
         {
            method_81(true,20);
         }
      }
      
      public function method_802() : void
      {
         var _loc6_:class_892 = null;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Array = null;
         var _loc11_:class_892 = null;
         var _loc12_:Number = NaN;
         var _loc1_:Boolean = name_3 == 1;
         var _loc2_:Array = [];
         var _loc3_:Array = var_495.method_78();
         var _loc4_:int = 0;
         var _loc5_:Array = var_495;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            loop3:
            switch(int(_loc6_.name_3))
            {
               case 0:
                  _loc6_.var_244 = Number(_loc6_.var_244 + int(_loc6_.var_796) * class_828.var_1094);
                  _loc7_ = 30;
                  if(int(_loc6_.var_796) == -1 && int(_loc6_.var_1098) == 1 && _loc6_.var_244 < class_828.var_892 + class_828.var_1093 + class_828.var_493)
                  {
                     _loc6_.var_244 = class_828.var_892 + class_828.var_1093 + class_828.var_493;
                     _loc6_.var_796 = 1;
                     _loc6_.var_243.scaleX = -1;
                  }
                  if(!!_loc1_ && name_5 && int(_loc6_.var_796) == -1 && _loc6_.var_244 > class_828.var_892 + _loc7_ && _loc6_.var_244 < class_742.var_217 - _loc7_)
                  {
                     name_3 = 2;
                     _loc6_.name_3 = 1;
                     var_700 = 314;
                     var_713 = var_232.method_84(class_899.__unprotect__("\x16h\b;"),class_645.var_209);
                     var_713.x = _loc6_.var_244;
                     var_713.y = _loc6_.var_245;
                     var_713.rotation = -180;
                     _loc1_ = false;
                     _loc6_.var_243.gotoAndPlay("prepare");
                     _loc6_.var_278 = -class_828.var_1094;
                  }
                  if(Number(_loc6_.var_244 + class_828.var_493) < 0 || _loc6_.var_244 > class_742.var_217 + class_828.var_493 + 10)
                  {
                     _loc6_.method_89();
                     var_495.method_116(_loc6_);
                     break;
                  }
                  break;
               case 1:
                  _loc6_.var_278 = _loc6_.var_278 * 0.8;
                  var_700 = (var_700 + 12) % 628;
                  _loc8_ = Number(3.14 + (Number(0.77 + Math.cos(var_700 / 100) * 0.77)));
                  var_713.x = _loc6_.var_244;
                  var_713.y = _loc6_.var_245;
                  var_713.rotation = _loc8_ / 0.0174;
                  if(!name_5)
                  {
                     _loc6_.var_243.gotoAndPlay("fly");
                     name_3 = 1;
                     _loc6_.name_3 = 2;
                     _loc6_.var_250 = 0.5;
                     _loc9_ = class_828.var_1095 * 1;
                     if(name_36 != null)
                     {
                        _loc9_ = Number(_loc9_ + name_36 * 1.5);
                     }
                     _loc6_.var_278 = Number(_loc6_.var_278 + Math.cos(_loc8_) * _loc9_);
                     _loc6_.var_279 = Number(_loc6_.var_279 + Math.sin(_loc8_) * _loc9_);
                     var_713.parent.removeChild(var_713);
                     var_713 = null;
                     break;
                  }
                  break;
               case 2:
                  _loc6_.var_243.rotation = Number(Math.atan2(_loc6_.var_279,_loc6_.var_278) / 0.0174 + 180 * (int(_loc6_.var_796) - 1) * 0.5);
                  if(_loc6_.var_245 > class_828.var_350 - class_828.var_493)
                  {
                     _loc6_.var_245 = class_828.var_350 - class_828.var_493;
                     _loc6_.var_278 = 0;
                     _loc6_.var_279 = 0;
                     _loc6_.var_250 = null;
                     _loc6_.name_108 = false;
                     _loc6_.name_3 = 0;
                     _loc6_.var_1098 = _loc6_.var_244 < class_828.var_892?-1:1;
                     _loc6_.var_243.gotoAndPlay("1");
                     _loc6_.var_243.rotation = 0;
                  }
                  if(name_36 == null)
                  {
                     if(_loc6_.var_279 > 0 && Number(Math.abs(class_828.var_892 - _loc6_.var_244)) < class_828.var_1093 && _loc6_.var_245 > class_828.var_350 - (2 * class_828.var_493 + 4))
                     {
                        method_717(_loc6_);
                        break;
                     }
                     break;
                  }
                  _loc7_ = 0;
                  _loc10_ = var_495;
                  while(true)
                  {
                     if(_loc7_ >= int(_loc10_.length))
                     {
                        break loop3;
                     }
                     _loc11_ = _loc10_[_loc7_];
                     _loc7_++;
                     if(_loc11_.var_460 != null)
                     {
                        _loc8_ = Number(_loc6_.method_115(_loc11_));
                        if(_loc8_ < 2 * class_828.var_493 * 1.2)
                        {
                           if(name_36 == _loc11_.var_460 && _loc6_.var_245 < _loc11_.var_245)
                           {
                              method_717(_loc6_);
                              break loop3;
                           }
                           if(!_loc6_.name_108)
                           {
                              _loc6_.var_796 = int(_loc6_.var_796) * -1;
                              _loc6_.var_278 = _loc6_.var_278 * -1;
                              _loc9_ = Number(_loc11_.method_231(_loc6_));
                              _loc12_ = 2 * class_828.var_493 - _loc8_;
                              _loc6_.var_244 = Number(_loc6_.var_244 + Math.cos(_loc9_) * _loc12_);
                              _loc6_.var_245 = Number(_loc6_.var_245 + Math.sin(_loc9_) * _loc12_);
                              _loc6_.name_108 = true;
                              _loc6_.var_243.scaleX = -int(_loc6_.var_796);
                              break loop3;
                           }
                           break loop3;
                        }
                     }
                  }
                  break;
               case 3:
                  _loc2_[_loc6_.var_460] = _loc6_;
                  if(!_loc6_.name_108 && Number(Math.random()) < 0.02)
                  {
                     if(_loc6_.var_460 == 0)
                     {
                        break;
                     }
                     _loc6_.var_243.gotoAndPlay("$anim" + int(class_691.method_114(6)));
                     _loc6_.name_108 = true;
                     break;
                  }
            }
            _loc6_.var_243.x = _loc6_.var_244;
            _loc6_.var_243.y = _loc6_.var_245;
         }
         var_1097 = (var_1097 + 8) % 628;
         var _loc13_:* = {
            "L\x01":class_828.var_892 * 1,
            "\x03\x01":class_828.var_350 - 23
         };
         _loc8_ = Math.cos(var_1097 / 100) * 0.05;
         _loc9_ = -1.57;
         _loc4_ = 0;
         while(_loc4_ < int(_loc2_.length))
         {
            _loc6_ = _loc2_[_loc4_];
            _loc4_++;
            _loc6_.var_244 = Number(_loc13_.var_244);
            _loc6_.var_245 = Number(_loc13_.var_245);
            _loc6_.var_243.rotation = Number(_loc9_ / 0.0174 + 90);
            _loc13_.var_244 = Number(Number(_loc13_.var_244) + Math.cos(_loc9_) * 2 * class_828.var_493);
            _loc13_.var_245 = Number(Number(_loc13_.var_245) + Math.sin(_loc9_) * 2 * class_828.var_493);
            _loc9_ = Number(_loc9_ + _loc8_);
            _loc8_ = _loc8_ * 1.23;
         }
      }
      
      public function method_717(param1:class_892) : void
      {
         var _loc2_:int = 2;
         if(name_36 == null)
         {
            name_36 = 0;
            _loc2_ = 1;
         }
         else
         {
            name_36 = name_36 + 1;
         }
         param1.var_460 = name_36;
         param1.name_3 = 3;
         param1.var_278 = 0;
         param1.var_279 = 0;
         param1.var_250 = null;
         param1.var_243.gotoAndPlay("base");
         param1.var_243.tabIndex = _loc2_;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [500];
         super.method_95(param1);
         var_495 = [];
         var_251 = 0;
         var_1097 = 0;
         var_1096 = 1 + int(Math.floor(param1 * 8.5));
         method_117();
         method_72();
      }
      
      public function method_801() : void
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("u\b\x04v\x02"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_244 = class_742.var_217 + class_828.var_493;
         _loc1_.var_245 = class_828.var_350 - class_828.var_493;
         _loc1_.name_3 = 0;
         _loc1_.var_796 = -1;
         _loc1_.var_1098 = 1;
         _loc1_.method_118();
         var_495.push(_loc1_);
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("z@9s\x01"),0);
         var_24 = class_319.var_24;
         var_24.x = class_828.var_892;
         var _loc1_:int = -18;
         var _loc2_:class_755 = new class_755(var_24);
         var _loc3_:int = 0;
         var _loc4_:int = var_1096;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = _loc2_.method_84(class_899.__unprotect__("*\x1b9p\x01"),class_645.var_209);
            _loc6_.y = _loc1_;
            _loc1_ = _loc1_ - 2 * class_828.var_493;
         }
      }
   }
}
