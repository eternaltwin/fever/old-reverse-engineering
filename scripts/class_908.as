package
{
   import flash.display.Stage;
   import flash.events.Event;
   import flash.events.KeyboardEvent;
   import package_10.class_741;
   import package_10.class_870;
   import package_32.App;
   
   public class class_908 implements class_741
   {
      
      public static var var_1451:Boolean = false;
      
      public static var var_1452:int = 38;
      
      public static var var_1453:int = 40;
      
      public static var var_1454:int = 37;
      
      public static var var_1455:int = 39;
      
      public static var var_1456:int = 32;
      
      public static var var_1457:int = 27;
      
      public static var var_1458:int = 9;
      
      public static var var_1459:int = 222;
      
      public static var var_300:Boolean = false;
      
      public static var var_1460:Boolean = false;
      
      public static var var_1461:Function;
      
      public static var var_1462:Function;
      
      public static var var_1463:Function;
      
      public static var var_1464:Function;
      
      public static var var_1465:Function;
      
      public static var var_1466:Function;
      
      public static var var_1467:Function;
      
      public static var var_1468:Function;
      
      public static var var_1469:Function;
      
      public static var var_1470:Function;
      
      public static var var_1471:Array;
      
      public static var var_1263:Array;
      
      public static var var_65:Array = [];
       
      
      public function class_908()
      {
      }
      
      public static function method_95() : void
      {
         class_908.var_1471 = [];
         class_908.var_1263 = [];
         var _loc1_:Stage = App.rootMc.stage;
         _loc1_.addEventListener(KeyboardEvent.KEY_DOWN,class_908.method_1080);
         _loc1_.addEventListener(KeyboardEvent.KEY_UP,class_908.method_1081);
         _loc1_.addEventListener(Event.DEACTIVATE,class_908.method_1082);
         class_908.method_266();
      }
      
      public static function method_1083(param1:int, param2:Function) : void
      {
         class_908.var_1471[param1] = param2;
      }
      
      public static function method_1080(param1:KeyboardEvent) : void
      {
         var _loc3_:String = null;
         var _loc5_:Function = null;
         class_908.var_65[param1.keyCode] = true;
         class_908.var_300 = param1.ctrlKey;
         class_908.var_1460 = param1.altKey;
         var _loc2_:String = String.fromCharCode(param1.charCode);
         if(class_908.var_1451)
         {
            _loc3_ = String.fromCharCode(param1.charCode);
            class_870.name_17(" keyCode : " + param1.keyCode,{
               "\x06\fi2\x03":"Keyb.hx",
               "d\x0f^S\x03":67,
               "v2z\x01":"Keyb",
               "d\x19o\f\x03":"onKeyDown"
            });
         }
         if(param1.keyCode >= 65 && param1.keyCode <= 90)
         {
            class_908.var_1469(param1.keyCode);
         }
         class_908.var_1470(param1.keyCode);
         var _loc4_:uint = param1.keyCode;
         if(_loc4_ == class_908.var_1452)
         {
            class_908.var_1461();
         }
         else if(_loc4_ == class_908.var_1453)
         {
            class_908.var_1462();
         }
         else if(_loc4_ == class_908.var_1454)
         {
            class_908.var_1463();
         }
         else if(_loc4_ == class_908.var_1455)
         {
            class_908.var_1464();
         }
         else if(_loc4_ == class_908.var_1456)
         {
            class_908.var_1465();
         }
         else if(_loc4_ == class_908.var_1457)
         {
            class_908.var_1466();
         }
         else if(_loc4_ == class_908.var_1458)
         {
            class_908.var_1467();
         }
         else if(_loc4_ == class_908.var_1459)
         {
            class_908.var_1468();
         }
         else
         {
            _loc5_ = class_908.var_1263[param1.keyCode];
            if(_loc5_ != null)
            {
               _loc5_();
            }
         }
      }
      
      public static function method_1081(param1:KeyboardEvent) : void
      {
         class_908.var_65[param1.keyCode] = false;
         class_908.var_300 = param1.ctrlKey;
         class_908.var_1460 = param1.altKey;
      }
      
      public static function method_266() : void
      {
         var _loc4_:Function = null;
         class_908.var_1263 = [];
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = class_908.var_1471;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            class_908.var_1263[_loc1_] = _loc4_;
            _loc1_++;
         }
         class_908.var_1461 = function():void
         {
         };
         class_908.var_1462 = function():void
         {
         };
         class_908.var_1463 = function():void
         {
         };
         class_908.var_1464 = function():void
         {
         };
         class_908.var_1465 = function():void
         {
         };
         class_908.var_1466 = function():void
         {
         };
         class_908.var_1467 = function():void
         {
         };
         class_908.var_1468 = function():void
         {
         };
         class_908.var_1469 = function(param1:int):void
         {
         };
         class_908.var_1470 = function(param1:int):void
         {
         };
      }
      
      public static function method_1084(param1:int) : String
      {
         if(param1 >= 65 && param1 <= 90)
         {
            return String.fromCharCode(97 + (param1 - 65));
         }
         switch(param1)
         {
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
               return "?";
            case 13:
            default:
            default:
               return "enter";
            case 16:
               return "shift";
            case 17:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
               return "control";
            case 32:
               return "space";
         }
      }
      
      public static function method_1085() : Boolean
      {
         return Boolean(class_908.var_65[16]);
      }
      
      public static function method_1086() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1452]);
      }
      
      public static function method_1087() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1453]);
      }
      
      public static function method_1088() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1454]);
      }
      
      public static function method_1089() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1455]);
      }
      
      public static function method_1090() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1456]);
      }
      
      public static function method_1091() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1457]);
      }
      
      public static function method_1092() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1458]);
      }
      
      public static function method_1093() : Boolean
      {
         return Boolean(class_908.var_65[class_908.var_1459]);
      }
      
      public static function method_1082(param1:*) : void
      {
         var _loc4_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = int(class_908.var_65.length);
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            class_908.var_65[_loc4_] = false;
         }
      }
   }
}
