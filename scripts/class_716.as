package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_716 extends class_645
   {
      
      public static var var_653:Array = [120,360,480];
       
      
      public var var_251:Number;
      
      public var var_351:Number;
      
      public var var_577:Array;
      
      public var var_288:class_892;
      
      public var var_470:Array;
      
      public var var_654:int;
      
      public function class_716()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_407() : void
      {
         var _loc1_:MovieClip = null;
         if(var_654 > 0)
         {
            _loc1_ = Reflect.field(var_288.var_243,class_899.__unprotect__("U\\\x01"));
            _loc1_.gotoAndStop(var_654);
         }
         else
         {
            var_288.var_243.gotoAndPlay("fall");
            method_81(false,40);
         }
         var_288.var_250 = 0.3 - var_654 * 0.08;
      }
      
      override public function method_79() : void
      {
         method_408();
         method_409();
         if(var_654 > 0)
         {
            if(var_288.var_245 > class_742.var_219)
            {
               method_219();
            }
            if(var_288.var_245 < 0)
            {
               method_219();
            }
         }
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - var_288.var_244;
         var_288.var_244 = Number(var_288.var_244 + Number(class_663.method_99(-1.5,_loc2_ * 0.05,1.5)));
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      override public function method_83() : void
      {
         if(var_654 > 0)
         {
            var_288.var_279 = var_288.var_279 - 3;
            var_288.var_243.gotoAndPlay("throw");
         }
      }
      
      public function method_409() : void
      {
         var _loc3_:class_656 = null;
         var _loc4_:Boolean = false;
         var _loc5_:* = null;
         var _loc6_:Number = NaN;
         var_251 = var_251 - 1;
         if(var_251 < 0)
         {
            var_251 = 40 / var_351;
            method_410();
         }
         var _loc1_:Array = var_577.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.var_244 = Number(_loc3_.var_244 + var_351);
            _loc4_ = _loc3_.var_244 > class_742.var_217 + 16;
            if(var_654 > 0 && var_220 == null)
            {
               _loc5_ = {
                  "L\x01":var_288.var_244,
                  "\x03\x01":var_288.var_245 - 22
               };
               _loc6_ = Number(_loc3_.method_115(_loc5_));
               if(_loc6_ < 20)
               {
                  _loc4_ = true;
                  var_654 = var_654 - 1;
                  method_407();
               }
               _loc5_ = {
                  "L\x01":var_288.var_244,
                  "\x03\x01":Number(var_288.var_245 + 10)
               };
               _loc6_ = Number(_loc3_.method_115(_loc5_));
               if(_loc6_ < 16)
               {
                  var_288.var_279 = -6;
                  var_288.var_580 = 20;
                  method_219();
               }
            }
            if(_loc4_)
            {
               var_577.method_116(_loc3_);
               _loc3_.method_89();
            }
         }
      }
      
      public function method_408() : void
      {
         var _loc4_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = var_470;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.x = (_loc4_.x + (_loc1_ + 0.5) * var_351 * 0.3) % int(class_716.var_653[_loc1_]);
            _loc1_++;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc4_:int = 0;
         var_227 = [360];
         super.method_95(param1);
         var_351 = Number(1 + param1 * 2.2);
         var_577 = [];
         var_251 = 0;
         var_654 = 3 - int(Math.round(param1));
         method_117();
         var _loc2_:int = int(50 / var_351);
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            method_409();
         }
         method_72();
      }
      
      public function method_219() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         var _loc1_:int = 0;
         var _loc2_:int = var_654;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("0bI\x17"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc4_ = _loc5_;
            _loc4_.var_244 = Number(var_288.var_244 + (_loc3_ - 1) * 8);
            _loc4_.var_245 = var_288.var_245 - 22;
            _loc4_.var_278 = Number(var_288.var_278 + (Math.random() * 2 - 1 + (_loc3_ - 1)) * 0.8);
            _loc4_.var_279 = Number(var_288.var_279 + (Math.random() * 2 - 1) * 0.3);
            _loc4_.method_118();
            _loc4_.var_250 = -0.1;
         }
         var_654 = 0;
         method_407();
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\rj?\x01\x03"),0);
         var_470 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = var_232.method_84(class_899.__unprotect__("Q\bl\x03"),class_645.var_209);
            _loc3_.x = 0;
            _loc3_.y = class_742.var_219;
            _loc3_.gotoAndStop(3 - _loc2_);
            var_470.push(_loc3_);
         }
         var _loc4_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("[Od2\x02"),class_645.var_209));
         _loc4_.var_233 = 0.99;
         var_288 = _loc4_;
         var_288.var_250 = 0.1;
         var_288.var_244 = class_742.var_217 * 0.8;
         var_288.var_245 = class_742.var_219 * 0.5;
         var_288.var_279 = -3;
         var_288.method_118();
         method_407();
      }
      
      public function method_410() : void
      {
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__(",\x15\x19O\x01"),class_645.var_209));
         var _loc1_:class_656 = _loc2_;
         _loc1_.var_244 = -20;
         _loc1_.var_245 = Number(20 + Math.random() * (class_742.var_219 - 2 * 20));
         _loc1_.method_118();
         var_577.push(_loc1_);
      }
   }
}
