package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_915 extends class_645
   {
      
      public static var var_1494:int = 16;
      
      public static var var_1495:int = 2;
      
      public static var var_1496:Number = 1.5;
       
      
      public var var_577:Array;
      
      public var var_740:Number;
      
      public var var_1500:Number;
      
      public var var_103:MovieClip;
      
      public var var_1499:Number;
      
      public var var_488:Array;
      
      public var var_708:Array;
      
      public var var_1497:Number;
      
      public var var_1498:int;
      
      public var var_495:Array;
      
      public function class_915()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_918() : void
      {
         var _loc3_:class_892 = null;
         var _loc1_:Array = var_577.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.var_8 = _loc3_.var_8 - class_915.var_1495;
            if(Number(_loc3_.var_8) < 0)
            {
               method_920(_loc3_.var_244,_loc3_.var_245,var_1497);
               _loc3_.method_89();
               var_577.method_116(_loc3_);
            }
         }
      }
      
      public function method_1107() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:MovieClip = null;
         var _loc1_:Array = var_488.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            if(_loc3_.var_245 > class_742.var_219 - 2)
            {
               _loc4_ = var_232.method_84(class_899.__unprotect__("{!!\x03\x03"),class_645.var_209);
               _loc4_.x = _loc3_.var_244;
               _loc4_.y = class_742.var_219;
               _loc3_.method_89();
               var_488.method_116(_loc3_);
               method_81(false,20);
            }
         }
      }
      
      public function method_1108() : void
      {
         var _loc4_:class_656 = null;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:class_892 = null;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc2_:Array = var_708.method_78();
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            _loc4_.name_144 = _loc4_.name_144 * 0.5;
            _loc4_.var_243.scaleX = Number(_loc4_.var_243.scaleX + Number(_loc4_.name_144));
            _loc4_.var_243.scaleY = _loc4_.var_243.scaleX;
            _loc5_ = var_488.method_78();
            _loc6_ = 0;
            while(_loc6_ < int(_loc5_.length))
            {
               _loc7_ = _loc5_[_loc6_];
               _loc6_++;
               _loc8_ = _loc4_.var_243.scaleX * 50;
               _loc9_ = Number(_loc4_.method_115(_loc7_));
               if(_loc9_ < _loc8_)
               {
                  method_920(_loc7_.var_244,_loc7_.var_245,var_1497 * 0.75);
                  _loc7_.method_89();
                  var_488.method_116(_loc7_);
               }
            }
            if(Number(_loc4_.name_144) < 0.001)
            {
               _loc4_.method_89();
               var_708.method_116(_loc4_);
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:class_892 = null;
         var _loc6_:class_892 = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = method_100();
               _loc2_ = _loc1_.var_244 - var_103.x;
               _loc3_ = _loc1_.var_245 - var_103.y;
               var_740 = Number(class_663.method_99(-3.14,Number(Math.atan2(_loc3_,_loc2_)),0));
               _loc4_ = var_740 / 0.0174;
               var_103.rotation = _loc4_ == 0?Number(_loc2_ < 0?180:0):_loc4_;
               if(!!name_5 && var_1498 > 0)
               {
                  name_3 = 2;
                  _loc6_ = new class_892(var_232.method_84(class_899.__unprotect__("D\f\x04\x11"),class_645.var_209));
                  _loc6_.var_233 = 0.99;
                  _loc5_ = _loc6_;
                  _loc7_ = var_740 == 0?_loc2_ < 0?180 * 0.0174:Number(0):var_740;
                  _loc8_ = Number(Math.cos(_loc7_));
                  _loc9_ = Number(Math.sin(_loc7_));
                  _loc5_.var_244 = Number(var_103.x + _loc8_ * class_915.var_1494);
                  _loc5_.var_245 = Number(var_103.y + _loc9_ * class_915.var_1494);
                  _loc5_.var_278 = _loc8_ * class_915.var_1495;
                  _loc5_.var_279 = _loc9_ * class_915.var_1495;
                  _loc5_.var_8 = Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)) - class_915.var_1494;
                  _loc5_.method_118();
                  _loc5_.var_243.rotation = _loc7_ / 0.0174;
                  var_577.push(_loc5_);
                  _loc5_.var_233 = 1;
                  var_1498 = var_1498 - 1;
                  method_1109();
                  break;
               }
               break;
            case 2:
               if(!name_5)
               {
                  name_3 = 1;
                  break;
               }
         }
         var_1499 = var_1499 - 1;
         while(var_1499 < 0)
         {
            var_1499 = Number(var_1499 + var_1500);
            var_1500 = var_1500 * 1.2;
            method_1110();
         }
         method_918();
         method_1108();
         method_1107();
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [420];
         super.method_95(param1);
         var_740 = 0;
         var_1499 = 30;
         var_1500 = 30 - param1 * 25;
         if(var_1500 < 1)
         {
            var_1500 = 1;
         }
         var_1497 = 0.7;
         var_577 = [];
         var_488 = [];
         var_708 = [];
         var_1498 = 8;
         method_1109();
         method_117();
         method_72();
      }
      
      public function method_920(param1:Number, param2:Number, param3:Number) : void
      {
         var _loc5_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("0\x03BF\x01"),class_645.var_209));
         var _loc4_:class_656 = _loc5_;
         _loc4_.var_244 = param1;
         _loc4_.var_245 = param2;
         _loc4_.name_144 = param3;
         _loc4_.method_118();
         _loc4_.var_243.scaleX = 0.06;
         _loc4_.var_243.scaleY = 0.06;
         var_708.push(_loc4_);
      }
      
      public function method_1109() : void
      {
         var _loc1_:MovieClip = null;
         var _loc4_:int = 0;
         if(var_495 != null)
         {
            while(int(var_495.length) > 0)
            {
               _loc1_ = var_495.pop();
               _loc1_.parent.removeChild(_loc1_);
            }
         }
         var_495 = [];
         var _loc2_:int = 0;
         var _loc3_:int = var_1498;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc1_ = var_232.method_84(class_899.__unprotect__(";;\x1d8\x01"),class_645.var_208);
            _loc1_.x = Number((class_742.var_217 - (var_1498 * 2 + (var_1498 - 1))) * 0.5 + _loc4_ * 3);
            _loc1_.y = class_742.var_219 - 4;
            var_495.push(_loc1_);
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\np\x1a$\x03"),0);
         var_103 = class_319.var_103;
      }
      
      public function method_1110() : void
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("W;D|\x03"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_244 = Math.random() * class_742.var_217;
         _loc1_.var_245 = -10;
         var _loc3_:* = {
            "L\x01":Math.random() * class_742.var_217,
            "\x03\x01":class_742.var_219 * 1
         };
         var _loc4_:Number = Number(_loc1_.method_231(_loc3_));
         _loc1_.var_278 = Math.cos(_loc4_) * class_915.var_1496;
         _loc1_.var_279 = Math.sin(_loc4_) * class_915.var_1496;
         _loc1_.var_233 = 1;
         _loc1_.method_118();
         _loc1_.var_243.rotation = _loc4_ / 0.0174;
         var_488.push(_loc1_);
      }
   }
}
