package
{
   import flash.display.MovieClip;
   import package_17.class_709;
   import package_17.class_718;
   import package_17.class_725;
   import package_17.class_800;
   import package_17.class_863;
   import package_17.class_875;
   import package_32.class_899;
   
   public dynamic class class_689 extends class_656
   {
       
      
      public var var_535:Boolean;
      
      public var var_538:class_709;
      
      public var var_536:class_645;
      
      public var var_21:class_725;
      
      public function class_689(param1:MovieClip = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(param1);
         var_21 = new class_725(0,0);
         var_21.var_539 = method_89;
         var_535 = true;
      }
      
      override public function method_79() : void
      {
         var_244 = var_21.var_244;
         var_245 = var_21.var_245;
         if(var_535)
         {
            var_243.rotation = var_21.var_65 / 0.0174;
         }
         super.method_79();
      }
      
      public function method_339(param1:Number, param2:Number) : void
      {
         var_21.method_337(param1,param2);
         var_536.package_13.method_338(var_21);
      }
      
      public function method_342(param1:Boolean) : void
      {
         var_536.package_13.method_340(var_21);
         var_21.var_537 = param1;
         var_536.package_13.method_341(var_21);
      }
      
      public function method_129(param1:Number, param2:Number) : void
      {
         var_21.method_129(param1,param2);
         var_536.package_13.method_338(var_21);
      }
      
      public function method_344(param1:Array, param2:Number = 0.0, param3:Number = 0.0) : void
      {
         var _loc6_:Array = null;
         var _loc7_:class_718 = null;
         var _loc4_:Array = [];
         var _loc5_:int = 0;
         while(_loc5_ < int(param1.length))
         {
            _loc6_ = param1[_loc5_];
            _loc5_++;
            _loc7_ = new class_718(Number(_loc6_[0]),Number(_loc6_[1]));
            _loc4_.push(_loc7_);
         }
         var _loc8_:class_863 = new class_863(_loc4_,new class_718(param2,param3));
         method_343(_loc8_);
      }
      
      public function method_345(param1:Number) : void
      {
         var _loc2_:class_875 = new class_875(param1,new class_718(0,0));
         method_343(_loc2_);
      }
      
      public function method_347(param1:Object, param2:Object = undefined) : void
      {
         if(param2 == null)
         {
            param2 = param1;
         }
         var _loc3_:class_863 = class_800.method_346(param1,param2,-param1 * 0.5,-param2 * 0.5);
         method_343(_loc3_);
      }
      
      public function method_348() : void
      {
      }
      
      public function method_349(param1:Number) : void
      {
         var_21.method_349(param1);
         var_536.package_13.method_338(var_21);
      }
      
      override public function method_89() : void
      {
         var_536.package_13.method_340(var_21);
         super.method_89();
      }
      
      public function method_343(param1:class_800) : void
      {
         var_536.var_231.method_216(class_691.method_97(param1.var_215),this);
         if(var_538 != null)
         {
            param1.var_538 = var_538;
         }
         var_536.package_13.method_340(var_21);
         var_21.method_343(param1);
         var_21.method_350();
         var_536.package_13.method_341(var_21);
      }
   }
}
