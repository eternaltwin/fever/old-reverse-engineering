package package_17
{
   import package_32.class_899;
   
   public class class_875 extends class_800
   {
       
      
      public var var_1315:class_718;
      
      public var var_93:Number;
      
      public var var_68:class_718;
      
      public function class_875(param1:Number = 0.0, param2:class_718 = undefined, param3:class_709 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(0,param3);
         var_1006 = this;
         var_1004 = new class_718(param2.var_244,param2.var_245);
         var_68 = new class_718(param2.var_244,param2.var_245);
         var_93 = param1;
         var_692 = Math.PI * (var_93 * var_93);
         var _loc4_:class_718 = var_68;
         var_1315 = new class_718(_loc4_.var_244,_loc4_.var_245);
      }
      
      override public function method_79() : void
      {
         var _loc1_:class_718 = var_68;
         var _loc2_:class_725 = var_21;
         var_1315.var_244 = Number(var_21.var_244 + (_loc1_.var_244 * _loc2_.var_673 - _loc1_.var_245 * _loc2_.var_674));
         _loc1_ = var_68;
         _loc2_ = var_21;
         var_1315.var_245 = Number(var_21.var_245 + (Number(_loc1_.var_244 * _loc2_.var_674 + _loc1_.var_245 * _loc2_.var_673)));
         var_1007.var_291 = var_1315.var_244 - var_93;
         var_1007.var_93 = Number(var_1315.var_244 + var_93);
         var_1007.var_37 = var_1315.var_245 - var_93;
         var_1007.var_12 = Number(var_1315.var_245 + var_93);
      }
      
      override public function method_435() : Number
      {
         var _loc1_:class_718 = var_1004;
         var _loc2_:class_718 = var_1004;
         return Number(0.5 * (var_93 * var_93) + (Number(_loc1_.var_244 * _loc2_.var_244 + _loc1_.var_245 * _loc2_.var_245)));
      }
   }
}
