package package_17
{
   import package_32.class_899;
   
   public class class_860
   {
       
      
      public var name_1:class_860;
      
      public var var_1264:class_718;
      
      public var var_8:Number;
      
      public function class_860(param1:class_718 = undefined, param2:Number = 0.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_1264 = param1;
         var_8 = param2;
      }
      
      public function toString() : String
      {
         return "[Axis= " + var_1264.var_244 + "," + var_1264.var_245 + " d=" + var_8 + "]";
      }
      
      public function method_417() : class_860
      {
         var _loc1_:class_718 = var_1264;
         return new class_860(new class_718(_loc1_.var_244,_loc1_.var_245),var_8);
      }
   }
}
