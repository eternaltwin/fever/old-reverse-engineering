package package_17
{
   import package_10.class_678;
   import package_10.class_803;
   import package_10.class_821;
   
   public class class_827
   {
       
      
      public var var_1090:class_719;
      
      public var var_1091:class_939;
      
      public function class_827()
      {
      }
      
      public function method_794(param1:class_719) : void
      {
         param1.var_662.var_84 = null;
         param1.var_222.var_84 = null;
         param1.var_668.var_84 = null;
         param1.var_679 = false;
         param1.var_680 = var_1090;
         var_1090 = param1;
      }
      
      public function method_795(param1:class_939) : void
      {
         param1.name_1 = var_1091;
         var_1091 = param1;
      }
      
      public function method_796(param1:class_833) : void
      {
      }
      
      public function method_797(param1:class_939) : void
      {
         var _loc2_:class_939 = null;
         while(param1 != null)
         {
            _loc2_ = param1.name_1;
            param1.name_1 = var_1091;
            var_1091 = param1;
            param1 = _loc2_;
         }
      }
      
      public function method_798(param1:class_652) : class_719
      {
         var _loc2_:class_719 = var_1090;
         if(_loc2_ == null)
         {
            return new class_719(param1);
         }
         var_1090 = _loc2_.var_680;
         return _loc2_;
      }
      
      public function method_799() : class_939
      {
         var _loc1_:class_939 = var_1091;
         if(_loc1_ == null)
         {
            return new class_939();
         }
         var_1091 = _loc1_.name_1;
         return _loc1_;
      }
      
      public function method_800() : class_833
      {
         return new class_833(this);
      }
   }
}
