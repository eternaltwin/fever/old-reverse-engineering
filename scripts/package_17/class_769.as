package package_17
{
   import package_32.class_899;
   
   public class class_769
   {
       
      
      public var var_863:Number;
      
      public var var_864:Array;
      
      public var var_866:class_720;
      
      public var var_865:Array;
      
      public function class_769()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_863 = 0;
         var_866 = new class_720();
         var_864 = [];
         var_865 = [];
      }
      
      public function method_134() : void
      {
         var _loc1_:Number = (package_10.class_769.name_81() - var_864.pop()) * 1000;
         var _loc2_:String = var_865.pop();
         var _loc3_:* = var_866.method_98(_loc2_);
         if(_loc3_ == null)
         {
            _loc3_ = {
               "1-\x1d\x1a\x02":0,
               "2ej\x01":0
            };
            var_866.method_216(_loc2_,_loc3_);
         }
         _loc3_.var_863 = Number(Number(_loc3_.var_863) + _loc1_);
         _loc3_.name_82 = Number(_loc3_.name_82 * 0.99 + 0.01 * _loc1_);
         if(int(var_865.length) == 0)
         {
            var_863 = Number(var_863 + _loc1_);
         }
      }
      
      public function name_51(param1:String) : void
      {
         var_864.push(Number(package_10.class_769.name_81()));
         var_865.push(param1);
      }
      
      public function method_625(param1:String) : Number
      {
         var _loc2_:* = var_866.method_98(param1);
         if(_loc2_ == null)
         {
            return 0;
         }
         return Number(_loc2_.var_863);
      }
      
      public function package_43(param1:String) : String
      {
         var _loc2_:* = var_866.method_98(param1);
         if(_loc2_ == null)
         {
            return param1 + " ????";
         }
         return param1 + " : " + int(_loc2_.name_82 * 1000) + " (" + int(_loc2_.var_863 * 1000 / var_863) / 10 + "%)";
      }
   }
}
