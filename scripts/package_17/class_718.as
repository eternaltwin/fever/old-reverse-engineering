package package_17
{
   import package_32.class_899;
   
   public class class_718
   {
       
      
      public var var_245:Number;
      
      public var var_244:Number;
      
      public var name_1:class_718;
      
      public function class_718(param1:Number = 0.0, param2:Number = 0.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_244 = param1;
         var_245 = param2;
      }
      
      public static function method_412(param1:Number, param2:Number) : class_718
      {
         var _loc3_:Number = Number(Math.sqrt(Number(param1 * param1 + param2 * param2)));
         var _loc4_:Number = _loc3_ < 1.0e-99?Number(0):1 / _loc3_;
         return new class_718(-param2 * _loc4_,param1 * _loc4_);
      }
      
      public function toString() : String
      {
         return "(" + int(Math.round(var_244 * 100)) / 100 + "," + int(Math.round(var_245 * 100)) / 100 + ")";
      }
      
      public function method_216(param1:Number, param2:Number) : void
      {
         var_244 = param1;
         var_245 = param2;
      }
      
      public function method_413(param1:class_718) : class_718
      {
         return new class_718(Number(var_244 + param1.var_244),Number(var_245 + param1.var_245));
      }
      
      public function method_414(param1:Number) : class_718
      {
         return new class_718(var_244 * param1,var_245 * param1);
      }
      
      public function method_415(param1:class_718) : class_718
      {
         return new class_718(var_244 - param1.var_244,var_245 - param1.var_245);
      }
      
      public function name_37() : Number
      {
         return Number(Math.sqrt(Number(var_244 * var_244 + var_245 * var_245)));
      }
      
      public function method_416(param1:class_718) : Number
      {
         return Number(var_244 * param1.var_244 + var_245 * param1.var_245);
      }
      
      public function name_62(param1:class_718) : Number
      {
         return var_244 * param1.var_245 - var_245 * param1.var_244;
      }
      
      public function method_417() : class_718
      {
         return new class_718(var_244,var_245);
      }
   }
}
