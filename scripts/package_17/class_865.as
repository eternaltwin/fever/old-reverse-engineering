package package_17
{
   import package_32.class_899;
   
   public class class_865 extends class_800
   {
       
      
      public var var_1281:class_718;
      
      public var var_1280:class_718;
      
      public var var_1279:class_718;
      
      public var var_1278:class_718;
      
      public var var_93:Number;
      
      public var var_1264:class_718;
      
      public var var_12:class_718;
      
      public var var_65:class_718;
      
      public function class_865(param1:class_718 = undefined, param2:class_718 = undefined, param3:Number = 0.0, param4:class_709 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(1,param4);
         var_1002 = this;
         var_1004 = new class_718(0,0);
         var_65 = new class_718(param1.var_244,param1.var_245);
         var_12 = new class_718(param2.var_244,param2.var_245);
         var_93 = param3;
         var _loc5_:class_718 = new class_718(param2.var_244 - param1.var_244,param2.var_245 - param1.var_245);
         var _loc6_:Number = _loc5_.var_244;
         var _loc7_:Number = _loc5_.var_245;
         var _loc8_:Number = Number(Math.sqrt(Number(_loc6_ * _loc6_ + _loc7_ * _loc7_)));
         var _loc9_:Number = _loc8_ < 1.0e-99?Number(0):1 / _loc8_;
         var_1264 = new class_718(-_loc7_ * _loc9_,_loc6_ * _loc9_);
         var_692 = param3 * Math.sqrt(Number(_loc5_.var_244 * _loc5_.var_244 + _loc5_.var_245 * _loc5_.var_245));
         var_1278 = new class_718(0,0);
         var_1279 = new class_718(0,0);
         var_1280 = new class_718(0,0);
         var_1281 = new class_718(0,0);
      }
      
      override public function method_79() : void
      {
         var _loc1_:class_718 = var_65;
         var _loc2_:class_725 = var_21;
         var_1278.var_244 = Number(var_21.var_244 + (_loc1_.var_244 * _loc2_.var_673 - _loc1_.var_245 * _loc2_.var_674));
         _loc1_ = var_65;
         _loc2_ = var_21;
         var_1278.var_245 = Number(var_21.var_245 + (Number(_loc1_.var_244 * _loc2_.var_674 + _loc1_.var_245 * _loc2_.var_673)));
         _loc1_ = var_12;
         _loc2_ = var_21;
         var_1279.var_244 = Number(var_21.var_244 + (_loc1_.var_244 * _loc2_.var_673 - _loc1_.var_245 * _loc2_.var_674));
         _loc1_ = var_12;
         _loc2_ = var_21;
         var_1279.var_245 = Number(var_21.var_245 + (Number(_loc1_.var_244 * _loc2_.var_674 + _loc1_.var_245 * _loc2_.var_673)));
         _loc1_ = var_1264;
         _loc2_ = var_21;
         var_1280.var_244 = _loc1_.var_244 * _loc2_.var_673 - _loc1_.var_245 * _loc2_.var_674;
         _loc1_ = var_1264;
         _loc2_ = var_21;
         var_1280.var_245 = Number(_loc1_.var_244 * _loc2_.var_674 + _loc1_.var_245 * _loc2_.var_673);
         var_1281.var_244 = -var_1280.var_244;
         var_1281.var_245 = -var_1280.var_245;
         if(var_1278.var_244 < var_1279.var_244)
         {
            var_1007.var_291 = var_1278.var_244 - var_93;
            var_1007.var_93 = Number(var_1279.var_244 + var_93);
         }
         else
         {
            var_1007.var_291 = var_1279.var_244 - var_93;
            var_1007.var_93 = Number(var_1278.var_244 + var_93);
         }
         if(var_1278.var_245 < var_1279.var_245)
         {
            var_1007.var_37 = var_1278.var_245 - var_93;
            var_1007.var_12 = Number(var_1279.var_245 + var_93);
         }
         else
         {
            var_1007.var_37 = var_1279.var_245 - var_93;
            var_1007.var_12 = Number(var_1278.var_245 + var_93);
         }
      }
      
      override public function method_435() : Number
      {
         return 1;
      }
   }
}
