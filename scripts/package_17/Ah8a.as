package package_17
{
   public class Ah8a
   {
      
      public static var var_370:Boolean;
      
      public static var var_583:Number = 1.0e99;
      
      public static var var_1321:Number = 1.0e-99;
      
      public static var var_1322:int = 120;
      
      public static var var_1008:class_709;
      
      public static var var_695:class_809;
      
      public static var var_1323:Number = 0.95;
      
      public static var Q_mM:Number = 0.002;
      
      public static var var_1324:int = 2;
      
      public static var var_1325:Number = 30;
       
      
      public function Ah8a()
      {
      }
      
      public static function method_998(param1:class_718, param2:class_725) : Number
      {
         return param1.var_244 * param2.var_673 - param1.var_245 * param2.var_674;
      }
      
      public static function method_999(param1:class_718, param2:class_725) : Number
      {
         return Number(param1.var_244 * param2.var_674 + param1.var_245 * param2.var_673);
      }
   }
}
