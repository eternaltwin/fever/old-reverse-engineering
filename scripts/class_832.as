package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_832 extends class_645
   {
      
      public static var var_965:int = 6;
      
      public static var var_967:int = 15;
      
      public static var var_1106:int = 25;
       
      
      public var var_251:Number;
      
      public var var_351:Number;
      
      public var name_50:Number;
      
      public var var_688:Number;
      
      public var var_1107:class_892;
      
      public var var_1108:MovieClip;
      
      public var var_709:MovieClip;
      
      public var var_494:Boolean;
      
      public var class_318:class_892;
      
      public function class_832()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_811(param1:Boolean) : void
      {
         var_494 = param1;
         name_3 = 3;
         var_251 = 12;
      }
      
      public function method_812() : void
      {
         var_709.x = var_1107.var_243.x;
         var_1108.x = var_1107.var_243.x;
         var_709.y = var_1107.var_243.y - var_688;
         var_1108.y = var_1107.var_243.y - var_688;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         method_720();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_442();
               if(name_5)
               {
                  name_3 = 2;
                  break;
               }
               break;
            case 2:
               var_688 = var_688 - class_832.var_1106;
               if(var_688 < 0)
               {
                  var_688 = 0;
                  _loc1_ = Number(var_1107.method_115(class_318));
                  if(_loc1_ < class_832.var_967)
                  {
                     var_229 = [true];
                     method_811(true);
                  }
                  else
                  {
                     method_811(false);
                     if(class_318.var_245 < var_1107.var_245)
                     {
                        var_232.method_521(class_318.var_243);
                     }
                     if(class_318.var_245 > var_1107.var_245)
                     {
                        var_232.method_110(class_318.var_243);
                     }
                  }
                  method_813();
                  break;
               }
               break;
            case 3:
               var_251 = var_251 - 1;
               if(var_251 == 0)
               {
                  method_81(var_494);
                  break;
               }
         }
         super.method_79();
      }
      
      override public function Dm6L() : void
      {
         var_1107.var_243.y = Number(class_742.var_219 * 0.5 + var_1107.var_245 * 0.5);
         class_318.var_243.y = Number(class_742.var_219 * 0.5 + class_318.var_245 * 0.5);
         method_812();
      }
      
      public function method_813() : void
      {
         var _loc5_:Number = NaN;
         var _loc1_:Number = Number(var_1107.method_115(class_318));
         var _loc2_:Number = Number(var_1107.method_231(class_318));
         var _loc3_:Number = Number(Math.cos(_loc2_));
         var _loc4_:Number = Number(Math.sin(_loc2_));
         if(!!var_494 && _loc1_ > class_832.var_967 - class_832.var_965)
         {
            _loc5_ = _loc1_ - (class_832.var_967 - class_832.var_965);
            class_318.var_244 = class_318.var_244 - _loc3_ * _loc5_;
            class_318.var_245 = class_318.var_245 - _loc4_ * _loc5_;
            class_318.var_65 = Number(Number(_loc2_ + 3.14) + (Math.random() * 2 - 1) * 0.3);
         }
         if(!var_494 && _loc1_ < class_832.var_967 + class_832.var_965)
         {
            _loc5_ = class_832.var_967 + class_832.var_965 - _loc1_;
            class_318.var_244 = Number(class_318.var_244 + _loc3_ * _loc5_);
            class_318.var_245 = Number(class_318.var_245 + _loc4_ * _loc5_);
            class_318.var_65 = Number(_loc2_ + (Math.random() * 2 - 1) * 0.3);
         }
      }
      
      public function method_442() : void
      {
         var _loc1_:* = method_100();
         var_1107.method_229(_loc1_,0.2,null);
      }
      
      public function method_720() : void
      {
         class_318.name_88 = Number(Number(class_318.name_88) + (Math.random() * 2 - 1) * 0.05);
         class_318.name_88 = class_318.name_88 * 0.92;
         class_318.var_65 = Number(Number(class_318.var_65) + Number(class_318.name_88));
         while(Number(class_318.var_65) > 3.14)
         {
            class_318.var_65 = class_318.var_65 - 6.28;
         }
         while(Number(class_318.var_65) < -3.14)
         {
            class_318.var_65 = Number(Number(class_318.var_65) + 6.28);
         }
         var _loc1_:Number = Math.cos(Number(class_318.var_65)) * var_351;
         var _loc2_:Number = Math.sin(Number(class_318.var_65)) * var_351;
         class_318.var_244 = Number(class_318.var_244 + _loc1_);
         class_318.var_245 = Number(class_318.var_245 + _loc2_);
         if(class_318.var_244 < class_832.var_965 || class_318.var_244 > class_742.var_217 - class_832.var_965)
         {
            _loc1_ = _loc1_ * -1;
            class_318.var_65 = Number(Math.atan2(_loc2_,_loc1_));
            class_318.name_88 = class_318.name_88 * 0.5;
         }
         if(class_318.var_245 < class_832.var_965 || class_318.var_245 > class_742.var_219 - class_832.var_965)
         {
            _loc2_ = _loc2_ * -1;
            class_318.var_65 = Number(Math.atan2(_loc2_,_loc1_));
            class_318.name_88 = class_318.name_88 * 0.5;
         }
         var _loc3_:int = int((class_318.var_65 / 6.28 + 0.5) * 40);
         class_318.var_243.gotoAndStop(_loc3_ + 1);
         name_50 = (name_50 + var_351) % 20;
         var _loc5_:MovieClip = Reflect.field(class_318.var_243,class_899.__unprotect__("\'O\x01"));
         var _loc4_:MovieClip = _loc5_;
         if(_loc4_ != null)
         {
            _loc4_.gotoAndStop(int(name_50));
         }
         var _loc6_:MovieClip = Reflect.field(class_318.var_243,class_899.__unprotect__(";(\x01"));
         _loc5_ = _loc6_;
         if(_loc5_ != null)
         {
            _loc5_.gotoAndStop(int((name_50 + 10) % 20));
         }
         if(name_3 == 3)
         {
            method_813();
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [200];
         super.method_95(param1);
         var_351 = Number(1 + param1 * 4);
         var_688 = 60;
         name_50 = 0;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("geZa\x02"),0);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("H}\b2\x03"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_1107 = _loc1_;
         var_1107.var_244 = class_742.var_217 * 0.5;
         var_1107.var_245 = class_742.var_219 * 0.5;
         var_1107.method_118();
         var_1107.var_243.y = Number(class_742.var_219 * 0.5 + var_1107.var_245 * 0.5);
         var_1108 = var_232.method_84(class_899.__unprotect__("\x1f\x04 +\x03"),class_645.var_209);
         var_1108.gotoAndStop("2");
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("\x01_\x10v\x03"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         class_318 = _loc1_;
         class_318.var_245 = class_742.var_219 * 0.5;
         class_318.var_244 = class_742.var_217 * (0.25 + Math.random() * 0.5);
         class_318.var_65 = 3.14;
         class_318.name_88 = 0;
         class_318.method_118();
         class_318.var_243.y = Number(class_742.var_219 * 0.5 + class_318.var_245 * 0.5);
         var_709 = var_232.method_84(class_899.__unprotect__("\x1f\x04 +\x03"),class_645.var_209);
         var_709.x = class_742.var_217 * 0.5;
         var_709.y = class_742.var_219 * 0.25;
         var_709.stop();
         method_812();
      }
   }
}
