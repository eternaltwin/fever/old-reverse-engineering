package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_682 extends class_645
   {
      
      public static var var_493:int = 9;
      
      public static var var_264:Array = [{
         "L\x01":1,
         "\x03\x01":0
      },{
         "L\x01":0,
         "\x03\x01":1
      },{
         "L\x01":-1,
         "\x03\x01":0
      },{
         "L\x01":0,
         "\x03\x01":-1
      }];
       
      
      public var var_351:Number;
      
      public var name_36:Array;
      
      public var var_496:Array;
      
      public var var_494:Boolean;
      
      public var var_495:Array;
      
      public function class_682()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:* = null;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Array = null;
         var _loc9_:class_892 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:* = null;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:Boolean = false;
         var _loc17_:Boolean = false;
         var_494 = true;
         var _loc1_:int = 0;
         var _loc2_:Array = var_495;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.var_244 < class_682.var_493 || _loc3_.var_244 > class_742.var_217 - class_682.var_493)
            {
               _loc3_.var_244 = Number(class_663.method_99(class_682.var_493,_loc3_.var_244,class_742.var_217 - class_682.var_493));
               _loc3_.var_278 = _loc3_.var_278 * -1;
            }
            if(_loc3_.var_245 < class_682.var_493 || _loc3_.var_245 > class_742.var_219 - class_682.var_493)
            {
               _loc3_.var_245 = Number(class_663.method_99(class_682.var_493,_loc3_.var_245,class_742.var_219 - class_682.var_493));
               _loc3_.var_279 = _loc3_.var_279 * -1;
            }
            if(_loc3_.var_37 != null)
            {
               _loc3_.var_37 = _loc3_.var_37 - 1;
               if(_loc3_.var_37 < 0)
               {
                  _loc3_.var_37 = null;
               }
            }
            if(_loc3_.var_460 != null)
            {
               _loc4_ = method_100();
               _loc5_ = {
                  "L\x01":Number(Number(_loc4_.var_244) + _loc3_.var_460.var_244 * class_682.var_493 * 2),
                  "\x03\x01":Number(Number(_loc4_.var_245) + _loc3_.var_460.var_245 * class_682.var_493 * 2)
               };
               _loc3_.method_229(_loc5_,0.15,null);
               _loc6_ = 0;
               _loc7_ = 0;
               _loc8_ = var_495;
               while(_loc7_ < int(_loc8_.length))
               {
                  _loc9_ = _loc8_[_loc7_];
                  _loc7_++;
                  if(_loc9_.var_460 == null && _loc9_.var_37 == null)
                  {
                     _loc10_ = Number(_loc3_.method_115(_loc9_));
                     if(_loc10_ < class_682.var_493 * 2)
                     {
                        _loc11_ = _loc9_.var_244 - _loc3_.var_244;
                        _loc12_ = _loc9_.var_245 - _loc3_.var_245;
                        _loc13_ = null;
                        if(Number(Math.abs(_loc11_)) < Number(Math.abs(_loc12_)))
                        {
                           _loc13_ = {
                              "L\x01":0,
                              "\x03\x01":int(_loc12_ / Math.abs(_loc12_))
                           };
                        }
                        else
                        {
                           _loc13_ = {
                              "L\x01":int(_loc11_ / Math.abs(_loc11_)),
                              "\x03\x01":0
                           };
                        }
                        _loc14_ = _loc3_.var_460.var_244 + int(_loc13_.var_244);
                        _loc15_ = _loc3_.var_460.var_245 + int(_loc13_.var_245);
                        _loc16_ = true;
                        _loc17_ = Boolean(method_222(_loc14_ + 1,_loc15_ + 1));
                        if(!!_loc17_ && Boolean(var_496[_loc14_ + 1][_loc15_ + 1]))
                        {
                           _loc16_ = false;
                        }
                        if(_loc16_)
                        {
                           if(_loc17_)
                           {
                              var_496[_loc14_ + 1][_loc15_ + 1] = true;
                           }
                           _loc9_.var_460 = {
                              "L\x01":_loc14_,
                              "\x03\x01":_loc15_
                           };
                           _loc9_.var_278 = 0;
                           _loc9_.var_279 = 0;
                           name_36.push(_loc6_);
                           if(Number(Math.abs(_loc14_)) < 2 && Number(Math.abs(_loc15_)) < 2)
                           {
                              _loc9_.var_243.gotoAndStop("2");
                           }
                           else
                           {
                              _loc9_.var_243.gotoAndStop("4");
                           }
                        }
                     }
                  }
                  _loc6_++;
               }
               if(_loc3_.var_243.currentFrame == 4)
               {
                  var_494 = false;
               }
            }
            else
            {
               var_494 = false;
            }
         }
         if(var_494)
         {
            method_81(true,20);
         }
         super.method_79();
      }
      
      override public function method_83() : void
      {
         var _loc5_:int = 0;
         var _loc6_:class_892 = null;
         if(int(name_36.length) == 0)
         {
            return;
         }
         var _loc1_:class_892 = null;
         var _loc2_:* = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = name_36;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = int(_loc4_[_loc3_]);
            _loc3_++;
            _loc6_ = var_495[_loc5_];
            if(_loc1_ == null || _loc1_.var_243.currentFrame == 2 || _loc1_.var_243.currentFrame == 4 && _loc6_.var_243.currentFrame == 4)
            {
               _loc1_ = _loc6_;
               _loc2_ = _loc5_;
            }
         }
         if(_loc2_ != null)
         {
            name_36.method_116(_loc2_);
         }
         if(method_222(_loc1_.var_460.var_244 + 1,_loc1_.var_460.var_245 + 1))
         {
            var_496[_loc1_.var_460.var_244 + 1][_loc1_.var_460.var_245 + 1] = false;
         }
         var _loc7_:Number = Number(Math.atan2(_loc1_.var_460.var_245,_loc1_.var_460.var_244));
         _loc1_.var_278 = Math.cos(_loc7_) * var_351;
         _loc1_.var_279 = Math.sin(_loc7_) * var_351;
         _loc1_.var_460 = null;
         _loc1_.var_37 = 10;
         _loc1_.var_243.gotoAndStop("3");
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < 3 && param2 >= 0 && param2 < 3;
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var_227 = [480 - param1 * 80];
         super.method_95(param1);
         var_351 = Number(1.2 + param1 * 3);
         name_36 = [];
         var_496 = [];
         var _loc2_:int = 0;
         while(_loc2_ < 3)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            var_496[_loc3_] = [];
            _loc4_ = 0;
            while(_loc4_ < 3)
            {
               _loc4_++;
               _loc5_ = _loc4_;
               var_496[_loc3_][_loc5_] = _loc3_ == 1 && _loc5_ == 1;
            }
         }
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         class_319 = var_232.method_84(class_899.__unprotect__("+(_/\x03"),0);
         var_495 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 9)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc4_ = new class_892(var_232.method_84(class_899.__unprotect__("Jw$D\x02"),class_645.var_209));
            _loc4_.var_233 = 0.99;
            _loc3_ = _loc4_;
            _loc3_.var_244 = Number(class_682.var_493 + Math.random() * (class_742.var_217 - 2 * class_682.var_493));
            _loc3_.var_245 = Number(class_682.var_493 + Math.random() * (class_742.var_219 - 2 * class_682.var_493));
            _loc3_.var_233 = 1;
            if(_loc2_ == 0)
            {
               _loc3_.var_243.gotoAndStop("1");
               _loc3_.var_460 = {
                  "L\x01":0,
                  "\x03\x01":0
               };
            }
            else
            {
               _loc3_.var_243.gotoAndStop("3");
               _loc5_ = Math.random() * 6.28;
               _loc3_.var_278 = Math.cos(_loc5_) * var_351;
               _loc3_.var_279 = Math.sin(_loc5_) * var_351;
            }
            _loc3_.method_118();
            var_495.push(_loc3_);
         }
      }
   }
}
