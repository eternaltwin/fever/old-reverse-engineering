package package_39.package_40
{
   import package_27.package_28.Bytes;
   import package_32.class_899;
   
   public final class class_829
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["\x10)c\x1f\x03","wH6\n\x02","F$\'{"];
      
      public static var var_1099:class_829;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function class_829(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_803(param1:int, param2:Bytes) : class_829
      {
         return new class_829("\x10)c\x1f\x03",0,[param1,param2]);
      }
      
      public static function method_804(param1:String, param2:int) : class_829
      {
         return new class_829("wH6\n\x02",1,[param1,param2]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
