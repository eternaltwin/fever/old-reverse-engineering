package
{
   import flash.display.BitmapData;
   import package_11.package_12.class_657;
   import package_24.class_874;
   
   public class class_702
   {
      
      public static var var_567:Number = 4278190080;
      
      public static var var_568:Number = 4294967295;
      
      public static var var_569:Number = 4283267837;
      
      public static var var_570:Number = 4284492967;
      
      public static var var_242:class_874;
      
      public static var package_13:class_874;
      
      public static var var_304:class_874;
      
      public static var var_288:class_874;
      
      public static var package_9:class_874;
      
      public static var package_15:class_874;
      
      public static var var_571:class_874;
      
      public static var var_572:class_874;
       
      
      public function class_702()
      {
      }
      
      public static function method_95() : void
      {
         var _loc5_:int = 0;
         var _loc6_:Array = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         class_702.method_91();
         class_702.method_360();
         class_702.method_361();
         class_702.method_362();
         class_702.method_363();
         class_702.method_364();
         var _loc1_:class_701 = new class_701(10,10);
         class_702.var_572 = new class_874(_loc1_);
         class_702.var_572.method_365("medusa");
         class_702.var_572.method_366(0,0,50,55,4);
         class_702.var_572.method_367("medusa",[0,1,2,3],[4,4,3,2]);
         class_702.var_572.method_365("voodoo_mask");
         class_702.var_572.method_366(200,0,50,50);
         var _loc2_:class_700 = new class_700(0,0);
         class_702.method_368(_loc2_,class_702.var_568);
         class_702.var_571 = new class_874(_loc2_);
         class_702.var_571.method_365("horde_toys");
         class_702.var_571.method_366(0,0,16,16,15,3);
         class_702.var_571.method_365("horde_bg");
         class_702.var_571.method_366(0,48,200,100);
         class_702.var_571.method_365("horde_card");
         class_702.var_571.method_366(200,48,32,32);
         class_702.var_571.method_365("super_knight");
         class_702.var_571.method_366(200,104,16,16,1,4);
         class_702.var_571.method_369(5,0);
         class_702.var_571.method_366(216,152,24,16);
         class_702.var_571.method_369(0,5);
         class_702.var_571.method_366(216,104,16,24);
         class_702.var_571.method_369(-5,0);
         class_702.var_571.method_366(216,168,24,16);
         class_702.var_571.method_369(0,-5);
         class_702.var_571.method_366(216,128,16,24);
         class_702.var_571.method_369();
         class_702.var_571.method_365("orc");
         class_702.var_571.method_366(0,152,16,16,6,4);
         var _loc3_:Array = ["orc_front","orc_back","orc_side","orc_carry"];
         var _loc4_:int = 0;
         while(_loc4_ < 4)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = [];
            _loc7_ = 0;
            while(_loc7_ < 6)
            {
               _loc7_++;
               _loc8_ = _loc7_;
               _loc6_.push(_loc8_ + _loc5_ * 6);
            }
            class_702.var_571.method_367(_loc3_[_loc5_],_loc6_,[2]);
         }
         class_702.var_571.method_365("orc_die");
         class_702.var_571.method_366(96,152,16,16);
         class_702.var_571.method_365("knight_naked");
         class_702.var_571.method_366(96,168,16,16);
         class_702.var_571.method_365("knight_tile");
         class_702.var_571.method_366(96,200,16,16);
         class_702.var_571.method_365("knight_holes");
         class_702.var_571.method_366(112,200,8,8,2,2);
         class_702.var_571.method_366(120,208,4,4,2,2);
         class_702.var_571.method_365("knight_armor");
         class_702.var_571.method_366(96,184,8,8,3,2);
         class_702.var_571.method_366(120,184,8,16);
         class_702.var_571.method_365("laby_slide_tiles");
         class_702.var_571.method_366(0,216,16,16,4,4);
         class_702.var_571.method_365("laby_slide_ball");
         class_702.var_571.method_366(0,148,4,4);
         class_702.var_571.method_365("laby_ball");
         class_702.var_571.method_366(217,81,8,8);
         class_702.var_571.method_365("laby_ball_wall");
         class_702.var_571.method_366(200,80,16,16);
      }
      
      public static function method_364() : void
      {
         var _loc1_:class_697 = new class_697(0,0);
         class_702.method_368(_loc1_,class_702.var_568);
         var _loc2_:int = int(class_657.method_151(class_905.var_573,-120));
         var _loc3_:* = class_657.method_232(_loc2_);
         _loc2_ = int(class_657.method_235({
            "z\x01":int(_loc3_.var_93),
            "6\x01":int(_loc3_.var_39),
            "r\x01":int(_loc3_.var_12),
            "@\x01":255
         }));
         class_702.method_370(_loc1_,class_702.var_567,_loc2_);
         class_702.var_288 = new class_874(_loc1_);
         class_702.var_288.method_365("hero_front");
         class_702.var_288.method_366(0,0,16,16,12);
         class_702.var_288.method_367("hero_front",[0,1,2,3,4,5,6,7,8,9,10,11],[2]);
         class_702.var_288.method_367("hero_stand",[0],[2]);
         class_702.var_288.method_365("hero_right");
         class_702.var_288.method_366(0,16,16,16,12);
         class_702.var_288.method_367("hero_right",[0,1,2,3,4,5,6,7,8,9,10,11],[2]);
         class_702.var_288.method_365("hero_left");
         class_702.var_288.method_366(0,16,16,16,12,1,true);
         class_702.var_288.method_367("hero_left",[0,1,2,3,4,5,6,7,8,9,10,11],[2]);
         class_702.var_288.method_365("hero_back");
         class_702.var_288.method_366(0,32,16,16,12);
         class_702.var_288.method_367("hero_back",[0,1,2,3,4,5,6,7,8,9,10,11],[2]);
         class_702.var_288.method_365("hero_hurt");
         class_702.var_288.method_366(160,64,16,16,2);
         class_702.var_288.method_367("hero_hurt",[0],[24]);
         class_702.var_288.method_365("hero_sorcery");
         class_702.var_288.method_366(0,80,16,16,5);
         class_702.var_288.method_367("hero_sorcery",[0,1,2,3,4],[2]);
         class_702.var_288.method_367("hero_sorcery_loop",[3,2,3,4],[2,4,2,4]);
         class_702.var_288.method_365("hero_slash");
         class_702.var_288.method_366(96,80,16,16,5);
         class_702.var_288.method_367("hero_slash",[0,1,2,3,4],[2]);
         class_702.var_288.method_369(0,-8);
         class_702.var_288.method_365("hero_explode");
         class_702.var_288.method_366(0,176,32,32,6,4);
         class_702.var_288.method_367("hero_explode",[0,1,2,3,4,5,6,8,9,10,11,12,13,14,15,16,17,18,19],[4,4,4,5,6,7,8,2]);
         class_702.var_288.method_369(0,-1);
         class_702.var_288.method_365("hero_cheese");
         class_702.var_288.method_366(0,96,16,16,12);
         class_702.var_288.method_366(0,112,16,16,4);
         class_702.var_288.method_367("hero_cheese",[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],[2,6,5,4,3,2]);
         class_702.var_288.method_369(0,-8);
         class_702.var_288.method_365("hero_happy_jump");
         class_702.var_288.method_366(0,48,16,32,10);
         class_702.var_288.method_367("hero_happy_jump",[0,1,2,3,4,5,6,7,8,9],[2]);
         class_702.var_288.method_369(0,-4);
         class_702.var_288.method_365("hero_fork");
         class_702.var_288.method_366(72,152,16,24,5);
         class_702.var_288.method_367("hero_fork",[0,1,2,3,4],[2,6,2,2,16]);
         class_702.var_288.method_369(-1,-6);
         class_702.var_288.method_365("hero_knife");
         class_702.var_288.method_366(0,128,24,24,8);
         class_702.var_288.method_366(0,152,24,24,3);
         class_702.var_288.method_367("hero_knife",[0,1,2,3,4,5,6,7,8,9,10],[2,2,2,3,4,5,2,2,3]);
      }
      
      public static function method_363() : void
      {
         var _loc1_:class_699 = new class_699(0,0);
         class_702.method_368(_loc1_,class_702.var_568);
         class_702.package_15 = new class_874(_loc1_);
         class_702.package_15.method_365("radar");
         class_702.package_15.method_366(0,0,128,128);
         class_702.package_15.method_365("dice");
         class_702.package_15.method_366(128,0,32,32,2,3);
         class_702.package_15.method_365("table");
         class_702.package_15.method_366(0,128,96,36);
         class_702.package_15.method_365("fever_x");
         class_702.package_15.method_366(0,168,120,168);
         class_702.package_15.method_365("fever_x_back");
         class_702.package_15.method_366(0,164,120,4);
         class_702.package_15.method_365("arrow");
         class_702.package_15.method_366(128,96,8,8);
         class_702.package_15.method_365("gradient_blocks");
         class_702.package_15.method_366(128,176,4,4,3,2);
         class_702.package_15.method_365("timer_brick");
         class_702.package_15.method_366(136,96,7,5,3);
         class_702.package_15.method_365("cart");
         class_702.package_15.method_366(128,104,64,72);
         class_702.package_15.method_365("fever_x_click");
         class_702.package_15.method_366(128,184,40,40);
      }
      
      public static function method_362() : void
      {
         var _loc1_:class_696 = new class_696(0,0);
         class_702.method_368(_loc1_,class_702.var_568);
         class_702.var_304 = new class_874(_loc1_);
         class_702.var_304.method_365("bar");
         class_702.var_304.method_366(0,0,200,9,1,2);
         class_702.var_304.method_365("heart");
         class_702.var_304.method_366(0,18,8,7,2);
         class_702.var_304.method_366(23,18,8,7);
         class_702.var_304.method_365("timebar");
         class_702.var_304.method_366(16,18,1,7,2);
         class_702.var_304.method_365("icon_temp");
         class_702.var_304.method_366(18,18,5,5);
         class_702.var_304.method_365("heart_explode");
         class_702.var_304.method_366(0,32,16,16,8);
         class_702.var_304.method_367("heart_explode",[0,1,2,3,4,5,6,7],[2]);
         class_702.var_304.method_365("loading_bar");
         class_702.var_304.method_366(140,18,60,5,1,5);
         class_702.var_304.method_367("loading_bar",[0,1,2,3,4],[2]);
         class_702.var_304.method_365("icecube");
         class_702.var_304.method_366(0,48,10,12,2);
         class_702.var_304.method_365("rainbow");
         class_702.var_304.method_366(20,48,15,10);
         class_702.var_304.method_365("bonus_game");
         class_702.var_304.method_366(0,64,16,16,3);
         class_702.var_304.method_365("bonus_island");
         class_702.var_304.method_366(48,64,16,16,3);
         class_702.var_304.method_365("bonus_ground");
         class_702.var_304.method_366(0,80,16,16,12);
         class_702.var_304.method_365("items");
         class_702.var_304.method_366(0,96,16,16,12,3);
         class_702.var_304.method_365("bag");
         class_702.var_304.method_366(48,48,16,16,4);
         class_702.var_304.method_367("bag",[0,1,2,3],[2]);
         class_702.var_304.method_365("inv");
         class_702.var_304.method_366(0,144,200,88);
         class_702.var_304.method_365("inv_slash");
         class_702.var_304.method_366(112,48,16,16);
         class_702.var_304.method_365("big_heart");
         class_702.var_304.method_366(0,240,32,32,4);
         class_702.var_304.method_365("bg_timebar");
         class_702.var_304.method_366(31,18,1,18);
         class_702.var_304.method_365("adv_icons");
         class_702.var_304.method_366(32,18,10,9,4);
         class_702.var_304.method_365("rainbow_arrow");
         class_702.var_304.method_366(96,64,16,16,5);
         class_702.var_304.method_367("rainbow_arrow",[0,1,2,3,4],[4]);
         class_702.var_304.method_365("classic_arrow");
         class_702.var_304.method_366(96,80,16,16);
         class_702.var_304.method_365("runes");
         class_702.var_304.method_366(128,224,16,16,3,2);
         class_702.var_304.method_365("no_entry");
         class_702.var_304.method_366(112,48,16,16);
         class_702.var_304.method_365("bonus_daily");
         class_702.var_304.method_366(112,80,16,16,2);
      }
      
      public static function method_360() : void
      {
         var _loc4_:int = 0;
         var _loc5_:String = null;
         var _loc1_:class_695 = new class_695(0,0);
         class_702.method_368(_loc1_,class_702.var_568);
         class_702.var_242 = new class_874(_loc1_);
         var _loc2_:Array = ["blob_yellow","blob_green","blob_rose"];
         var _loc3_:int = 0;
         while(_loc3_ < 3)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = _loc2_[_loc4_];
            class_702.var_242.method_365(_loc5_);
            class_702.var_242.method_366(0,_loc4_ * 16,16,16,4,1);
            class_702.var_242.method_366(0,_loc4_ * 16,16,16,4,1,true);
            class_702.var_242.method_367(_loc5_,[0,1,2,3,1,4,5,6,7,5],[2,3,4,5,3,2,3,4,5,3]);
            class_702.var_242.method_365(_loc5_ + "_explode");
            class_702.var_242.method_366(64,_loc4_ * 16,16,16,8);
            class_702.var_242.method_367(_loc5_ + "_explode",[0,1,2,3,4,5,6,7],[2,4,8,2]);
            class_702.var_242.method_367(_loc5_ + "_hurt",[0,1,2,1,0],[1,2,4,3,2]);
         }
         class_702.var_242.method_365("piaf");
         class_702.var_242.method_366(0,48,16,24,16);
         class_702.var_242.method_367("piaf",[0,1,2,3,4,5],[8]);
         class_702.var_242.method_367("piaf_hurt",[5],[16]);
         class_702.var_242.method_367("piaf_explode",[8,9,10,9,10,9,10,9,10,11,12,12,13,13,14,14,15,15],[2]);
         class_702.var_242.method_365("scarab");
         class_702.var_242.method_366(0,72,16,24,10,2);
         class_702.var_242.method_367("scarab",[0,1,2,0,1,2,0,1,2,3,4,5,6,7,8],[4]);
         class_702.var_242.method_367("scarab_hurt",[9],[16]);
         class_702.var_242.method_367("scarab_explode",[5,6,10,11,12,13,12,13,12,13,14,15,16,17,18,19],[2,2,2,3,4,4,2,2,2,2,2,2,2,3,4,5]);
         class_702.var_242.method_365("poulpe");
         class_702.var_242.method_366(0,120,16,16,16);
         class_702.var_242.method_367("poulpe",[0,1,2,3,4,5,6,7,0,1,2,3,0,1,2,3],[4]);
         class_702.var_242.method_367("poulpe_hurt",[15],[16]);
         class_702.var_242.method_367("poulpe_explode",[0,8,9,8,9,8,9,8,9,10,11,12,12,13,13,14,14],[2]);
         class_702.var_242.method_365("red");
         class_702.var_242.method_366(0,136,16,24,16);
         class_702.var_242.method_367("red",[0,1,2,3,4,5],[4]);
         class_702.var_242.method_367("red_hurt",[6,7,6],[4,8,4]);
         class_702.var_242.method_367("red_explode",[6,7,8,9,10,11,12,13,14,15],[2,2,2,2,2,2,2,3,4]);
         class_702.var_242.method_365("stinky");
         class_702.var_242.method_366(0,160,16,24,16);
         class_702.var_242.method_367("stinky",[0,1,2,3,4,5,6,7],[2,4,6,2,2,4,6,2]);
         class_702.var_242.method_367("stinky_hurt",[8,9,8],[4,8,4]);
         class_702.var_242.method_367("stinky_explode",[0,8,9,10,11,12,13,14,15],[2,4,8,2,2,3,4]);
         class_702.var_242.method_365("ghost");
         class_702.var_242.method_366(0,184,16,24,16);
         class_702.var_242.method_367("ghost",[0,1,2,3,4,5],[6,4,4,6,4,4]);
         class_702.var_242.method_367("ghost_hurt",[6,7,6,7,6,7],[4]);
         class_702.var_242.method_367("ghost_explode",[8,9,10,11,12,13,14],[4]);
         class_702.var_242.method_365("naram");
         class_702.var_242.method_366(0,208,16,24,16);
         class_702.var_242.method_367("naram",[0,1,2,3,4,5,6,7,8,9],[4]);
         class_702.var_242.method_367("naram_hurt",[15],[16]);
         class_702.var_242.method_367("naram_explode",[15,7,15,7,15,7,15,11,12,13,14,14,14],[4,3,4,2,4,1,4]);
         class_702.var_242.method_365("sargon");
         class_702.var_242.method_366(0,232,16,24,16);
         class_702.var_242.method_367("sargon",[0,1,2,3,4,5,6],[4]);
         class_702.var_242.method_367("sargon_hurt",[10],[16]);
         class_702.var_242.method_367("sargon_explode",[7,8,9,10,11,12,13,14,15,14,15,14,15,14,15],[2]);
         class_702.var_242.method_365("sargon_repel");
         class_702.var_242.method_366(176,256,16,24,5);
         class_702.var_242.method_367("sargon_repel",[0,1,2,3,4],[4]);
         class_702.var_242.method_365("braino");
         class_702.var_242.method_366(0,256,16,24,5,4);
         class_702.var_242.method_367("braino",[0,1,2,2,1,0,3,4,4,3],[2]);
         class_702.var_242.method_367("braino_hurt",[6],[16]);
         class_702.var_242.method_367("braino_explode",[5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],[2,2,3,4,2]);
         class_702.var_242.method_365("medusa");
         class_702.var_242.method_366(80,256,16,24,6,5);
         class_702.var_242.method_367("medusa",[0,1,2,3,4,5],[4]);
         class_702.var_242.method_367("medusa_explode",[6,7,8,9,10,11,12,13,14,15,16],[3]);
         class_702.var_242.method_367("medusa_stone",[0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,19,20,21,22,23,24,25,26,27,28,29],[3]);
         class_702.var_242.method_367("medusa_hurt",[6],[16]);
         class_702.var_242.method_365("kourou");
         class_702.var_242.method_366(176,280,16,16,5,2);
         class_702.var_242.method_367("kourou",[0,1,2,3,4,5,6,7,8,9],[4]);
         class_702.var_242.method_365("kourou_explode");
         class_702.var_242.method_369(0,-4);
         class_702.var_242.method_366(0,376,24,24,10,2);
         class_702.var_242.method_367("kourou_explode",[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],[2,4,6,8,10,2]);
         class_702.var_242.method_367("kourou_hurt",[1],[16]);
      }
      
      public static function method_91() : void
      {
         var _loc1_:class_694 = new class_694(0,0);
         class_702.method_368(_loc1_,class_702.var_568);
         class_702.package_13 = new class_874(_loc1_);
         class_702.package_13.method_366(0,0,16,16,10);
         class_702.package_13.method_365("dirt");
         class_702.package_13.method_366(0,16,16,16,16);
         class_702.package_13.method_365("grass");
         class_702.package_13.method_366(0,96,16,16,16);
         class_702.package_13.method_365("cliff");
         class_702.package_13.method_366(0,32,16,16,16);
         class_702.package_13.method_365("waves_3");
         class_702.package_13.method_366(96,64,16,24,2);
         class_702.package_13.method_366(96,64,16,8);
         class_702.package_13.method_365("clouds");
         class_702.package_13.method_366(0,168,24,24,4);
         class_702.package_13.method_365("elements_dirt_small");
         class_702.package_13.method_366(0,48,8,8,4,2);
         class_702.package_13.method_365("elements_dirt_medium");
         class_702.package_13.method_366(32,48,16,16,5);
         class_702.package_13.method_369(0,-1);
         class_702.package_13.method_365("elements_grass_medium");
         class_702.package_13.method_366(0,128,16,15,8);
         class_702.package_13.method_369(0,0);
         class_702.package_13.method_365("ladder");
         class_702.package_13.method_366(64,0,16,16);
         class_702.package_13.method_365("jump_stone");
         class_702.package_13.method_366(80,0,16,16,2);
         class_702.package_13.method_365("selector");
         class_702.package_13.method_366(144,0,16,16,3);
         class_702.package_13.method_367("selector",[0,1,2],[4]);
         class_702.package_13.method_365("portal");
         class_702.package_13.method_366(0,144,22,18);
         class_702.package_13.method_365("portal_light");
         class_702.package_13.method_366(22,144,12,12,4,2);
         class_702.package_13.method_367("portal_light",[0,1,2,3,4,5,6,7],[2]);
         class_702.package_13.method_365("portal_stone");
         class_702.package_13.method_366(0,162,2,2,4);
         class_702.package_13.method_367("portal_stone",[0,2,1,2,3],[16,2,4,2]);
         class_702.package_13.method_365("chest");
         class_702.package_13.method_366(80,144,16,16,6);
         class_702.package_13.method_365("statue");
         class_702.package_13.method_366(112,32,16,32);
         class_702.package_13.method_365("fever_head");
         class_702.package_13.method_366(128,48,32,48,4);
         class_702.package_13.method_369(0,16);
         class_702.package_13.method_365("fever_head");
         class_702.package_13.method_366(128,48,32,48,4);
      }
      
      public static function method_361() : void
      {
         var _loc1_:class_698 = new class_698(0,0);
         class_702.method_368(_loc1_,class_702.var_568);
         class_702.package_9 = new class_874(_loc1_);
         class_702.package_9.method_365("bonus_vanish");
         class_702.package_9.method_366(0,0,16,16,9);
         class_702.package_9.method_367("bonus_vanish",[0,1,2,3,4,5,6,7,8],[2]);
         class_702.package_9.method_365("spark_twinkle");
         class_702.package_9.method_366(0,16,8,8,4);
         class_702.package_9.method_367("spark_twinkle",[0,1,2,3],[40,2]);
         class_702.package_9.method_365("spark_grow");
         class_702.package_9.method_366(32,16,3,3,3);
         class_702.package_9.method_366(32,19,5,5,2);
         class_702.package_9.method_367("spark_grow",[0,1,2,3],[4]);
         class_702.package_9.method_367("spark_grow_loop",[4,3],[2]);
         class_702.package_9.method_367("spark",[0,1,2],[2]);
         class_702.package_9.method_365("twinkle_gray");
         class_702.package_9.method_366(0,24,8,8,4);
         class_702.package_9.method_367("twinkle_gray",[0,1,2,3],[16,2]);
         class_702.package_9.method_365("rainbow");
         class_702.package_9.method_366(144,0,16,16);
         class_702.package_9.method_365("scan_hero");
         class_702.package_9.method_366(144,16,8,8);
         class_702.package_9.method_365("knife");
         class_702.package_9.method_366(144,24,16,4,1,2);
         class_702.package_9.method_365("dirt_stones");
         class_702.package_9.method_366(128,32,8,8,3);
         class_702.package_9.method_366(152,32,4,4,2,2);
         class_702.package_9.method_365("grey_stones");
         class_702.package_9.method_366(128,40,8,8,3);
         class_702.package_9.method_366(152,40,4,4,2,2);
         class_702.package_9.method_365("butterfly");
         class_702.package_9.method_366(0,88,8,8,6);
         class_702.package_9.method_367("butterfly",[0,1,2,3,4,5],[2]);
         class_702.package_9.method_369(0,-6);
         class_702.package_9.method_365("tornado");
         class_702.package_9.method_366(48,16,16,16,6);
         class_702.package_9.method_367("tornado",[0,1,2,3,4,5],[4]);
         class_702.package_9.method_369(0,-4);
         class_702.package_9.method_365("square_explosion");
         class_702.package_9.method_366(0,64,16,24,10);
         class_702.package_9.method_367("square_explosion",[0,1,2,3,4,5,6,7,7,8,8,9,9],[2]);
         class_702.package_9.method_369(-8,-8);
         class_702.package_9.method_365("fireball");
         class_702.package_9.method_366(0,32,32,33,4);
         class_702.package_9.method_367("fireball",[0,1,2,3],[4]);
      }
      
      public static function method_368(param1:BitmapData, param2:Number) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = param1.width;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = 0;
            _loc7_ = param1.height;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               if(uint(param1.getPixel32(_loc5_,_loc8_)) == param2)
               {
                  param1.setPixel32(_loc5_,_loc8_,0);
               }
            }
         }
      }
      
      public static function method_370(param1:BitmapData, param2:Number, param3:Number) : BitmapData
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = param1.width;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = param1.height;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               if(uint(param1.getPixel32(_loc6_,_loc9_)) == param2)
               {
                  param1.setPixel32(_loc6_,_loc9_,uint(param3));
               }
            }
         }
         return param1;
      }
   }
}
