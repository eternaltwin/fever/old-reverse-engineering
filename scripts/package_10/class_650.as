package package_10
{
   import package_17.class_725;
   
   public class class_650
   {
       
      
      public var var_84:class_803;
      
      public function class_650()
      {
      }
      
      public function toString() : String
      {
         var _loc1_:Array = [];
         var _loc2_:class_803 = var_84;
         while(_loc2_ != null)
         {
            _loc1_.push(_loc2_.var_223);
            _loc2_ = _loc2_.name_1;
         }
         return "{" + _loc1_.join(",") + "}";
      }
      
      public function method_116(param1:class_725) : Boolean
      {
         var _loc2_:* = null;
         var _loc3_:class_803 = var_84;
         while(_loc3_ != null)
         {
            if(_loc3_.var_223 == param1)
            {
               if(_loc2_ == null)
               {
                  var_84 = _loc3_.name_1;
                  break;
               }
               _loc2_.name_1 = _loc3_.name_1;
               break;
            }
            _loc2_ = _loc3_;
            _loc3_ = _loc3_.name_1;
         }
         return _loc3_ != null;
      }
      
      public function name_12() : class_725
      {
         var _loc1_:class_803 = var_84;
         if(_loc1_ == null)
         {
            return null;
         }
         var_84 = _loc1_.name_1;
         return _loc1_.var_223;
      }
      
      public function method_73() : Object
      {
         var var_291:class_803 = var_84;
         return {
            "\'*+x\x03":function():Boolean
            {
               return var_291 != null;
            },
            "?\x05\x10;\x01":function():class_725
            {
               var _loc1_:class_803 = var_291;
               var_291 = _loc1_.name_1;
               return _loc1_.var_223;
            }
         };
      }
      
      public function method_164() : Boolean
      {
         return var_84 == null;
      }
      
      public function name_13() : class_725
      {
         return var_84 == null?null:var_84.var_223;
      }
      
      public function method_124(param1:class_725) : void
      {
         var_84 = new class_803(param1,var_84);
      }
   }
}
