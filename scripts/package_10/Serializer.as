package package_10 {
import flash.utils.describeType;

import package_27.package_28.Bytes;

import package_32.class_899;

/**
 * JPEXS name: `class_876`
 */
public class Serializer {
  /**
   * JPEXS name: `var_1316`
   */
  public static var USE_CACHE:Boolean = false;

  /**
   * JPEXS name: `var_1317`
   */
  public static var USE_ENUM_INDEX:Boolean = false;

  /**
   * JPEXS name: `var_1226`
   */
  public static var BASE64:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";

  /**
   * JPEXS name: `kgcx`
   */
  public var useEnumIndex:Boolean;

  /**
   * JPEXS name: `var_1318`
   */
  public var useCache:Boolean;

  /**
   * JPEXS name: `var_1319`
   */
  public var shash:class_720;

  /**
   * JPEXS name: `var_1320`
   */
  public var scount:int;

  /**
   * JPEXS name: `var_526`
   */
  public var cache:Array;

  /**
   * JPEXS name: `var_1228`
   */
  public var buf:StringBuf;

  public function Serializer() {
    if (class_899.var_239) {
      return;
    }
    buf = new StringBuf();
    cache = [];
    useCache = Serializer.USE_CACHE;
    useEnumIndex = Serializer.USE_ENUM_INDEX;
    shash = new class_720();
    scount = 0;
  }

  /**
   * JPEXS name: `name_50`
   */
  public static function run(v:*):String {
    var s:Serializer = new Serializer();
    s.serialize(v);
    return s.toString();
  }

  public function toString():String {
    return buf.b;
  }

  /**
   * JPEXS name: `method_752`
   */
  public function serializeString(s:String):void {
    var _loc2_:* = shash.method_98(s);
    if (_loc2_ != null) {
      buf.b = buf.b + "R";
      buf.b = buf.b + _loc2_;
      return;
    }
    var _loc3_:int = scount;
    scount = scount + 1;
    shash.method_216(s, _loc3_);
    buf.b = buf.b + "y";
    s = class_761.method_556(s);
    buf.b = buf.b + s.length;
    buf.b = buf.b + ":";
    buf.b = buf.b + s;
  }

  /**
   * JPEXS name: `method_996`
   */
  public function serializeRef(param1:*):Boolean {
    var _loc4_:int = 0;
    var _loc2_:int = 0;
    var _loc3_:int = int(cache.length);
    while (_loc2_ < _loc3_) {
      _loc2_++;
      _loc4_ = _loc2_;
      if (cache[_loc4_] == param1) {
        buf.b = buf.b + "r";
        buf.b = buf.b + _loc4_;
        return true;
      }
    }
    cache.push(param1);
    return false;
  }

  /**
   * JPEXS name: `pB6y`
   */
  public function serializeFields(param1:*):void {
    var _loc4_:String = null;
    var _loc2_:int = 0;
    var _loc3_:Array = Reflect.method_649(param1);
    while (_loc2_ < int(_loc3_.length)) {
      _loc4_ = _loc3_[_loc2_];
      _loc2_++;
      serializeString(_loc4_);
      serialize(Reflect.field(param1, _loc4_));
    }
    buf.b = buf.b + "g";
  }

  /**
   * JPEXS name: `method_753`
   */
  public function serializeException(e:*):void {
    var _loc2_:Error = null;
    var _loc3_:String = null;
    buf.b = buf.b + "x";
    if (e is Error) {
      _loc2_ = e;
      _loc3_ = _loc2_.getStackTrace();
      if (_loc3_ == null) {
        serialize(_loc2_.message);
      } else {
        serialize(_loc3_);
      }
      return;
    }
    serialize(e);
  }

  /**
   * JPEXS name: `method_997`
   */
  public function serializeClassFields(v:Object, c:Class):void {
    var _loc7_:int = 0;
    var _loc8_:String = null;
    var _loc3_:XML = describeType(c);
    var _loc4_:XMLList = _loc3_.factory[0].child("variable");
    var _loc5_:int = 0;
    var _loc6_:int = int(_loc4_.length());
    while (_loc5_ < _loc6_) {
      _loc5_++;
      _loc7_ = _loc5_;
      _loc8_ = _loc4_[_loc7_].attribute("name").toString();
      if (v.hasOwnProperty(_loc8_)) {
        serializeString(_loc8_);
        serialize(Reflect.field(v, _loc8_));
      }
    }
    buf.b = buf.b + "g";
  }

  /**
   * JPEXS name: `method_745`
   */
  public function serialize(param1:*):void {
    var _loc4_:Class = null;
    var _loc5_:Class = null;
    var _loc6_:int = 0;
    var _loc7_:Array = null;
    var _loc8_:int = 0;
    var _loc9_:int = 0;
    var _loc10_:int = 0;
    var _loc11_:class_795 = null;
    var _loc13_:* = null;
    var _loc14_:Date = null;
    var _loc15_:class_720 = null;
    var _loc16_:String = null;
    var _loc17_:class_654 = null;
    var _loc18_:Bytes = null;
    var _loc19_:String = null;
    var _loc20_:int = 0;
    var _loc3_:Array = Type.method_887(param1).var_296;
    switch (int(Type.method_887(param1).var_295)) {
      case 0:
        buf.b = buf.b + "n";
        break;
      case 1:
        if (param1 == 0) {
          buf.b = buf.b + "z";
          return;
        }
        buf.b = buf.b + "i";
        buf.b = buf.b + param1;
        break;
      case 2:
        if (Math.isNaN(param1)) {
          buf.b = buf.b + "k";
          break;
        }
        if (!Math.isFinite(param1)) {
          buf.b = buf.b + (param1 < 0 ? "m" : "p");
          break;
        }
        buf.b = buf.b + "d";
        buf.b = buf.b + param1;
        break;
      case 3:
        buf.b = buf.b + (!!param1 ? "t" : "f");
        break;
      case 4:
        if (!!useCache && Boolean(serializeRef(param1))) {
          return;
        }
        buf.b = buf.b + "o";
        serializeFields(param1);
        break;
      case 5:
        class_899.var_317 = new Error();
        throw "Cannot serialize function";
      case 6:
        _loc4_ = _loc3_[0];
        if (_loc4_ == String) {
          serializeString(param1);
          return;
        }
        if (!!useCache && Boolean(serializeRef(param1))) {
          return;
        }
        _loc5_ = _loc4_;
        if (_loc5_ == Array) {
          _loc6_ = 0;
          buf.b = buf.b + "a";
          _loc7_ = param1;
          _loc8_ = int(_loc7_.length);
          _loc9_ = 0;
          while (_loc9_ < _loc8_) {
            _loc9_++;
            _loc10_ = _loc9_;
            if (_loc7_[_loc10_] == null) {
              _loc6_++;
            } else {
              if (_loc6_ > 0) {
                if (_loc6_ == 1) {
                  buf.b = buf.b + "n";
                } else {
                  buf.b = buf.b + "u";
                  buf.b = buf.b + _loc6_;
                }
                _loc6_ = 0;
              }
              serialize(_loc7_[_loc10_]);
            }
          }
          if (_loc6_ > 0) {
            if (_loc6_ == 1) {
              buf.b = buf.b + "n";
            } else {
              buf.b = buf.b + "u";
              buf.b = buf.b + _loc6_;
            }
          }
          buf.b = buf.b + "h";
          break;
        }
        if (_loc5_ == class_795) {
          buf.b = buf.b + "l";
          _loc11_ = param1;
          var _loc12_:* = _loc11_.method_73();
          while (_loc12_.method_74()) {
            _loc13_ = _loc12_.name_1();
            serialize(_loc13_);
          }
          buf.b = buf.b + "h";
          break;
        }
        if (_loc5_ == Date) {
          _loc14_ = param1;
          buf.b = buf.b + "v";
          buf.b = buf.b + _loc14_.toString();
          break;
        }
        if (_loc5_ == class_720) {
          buf.b = buf.b + "b";
          _loc15_ = param1;
          _loc12_ = _loc15_.method_215();
          while (_loc12_.method_74()) {
            _loc16_ = _loc12_.name_1();
            serializeString(_loc16_);
            serialize(_loc15_.method_98(_loc16_));
          }
          buf.b = buf.b + "h";
          break;
        }
        if (_loc5_ == class_654) {
          buf.b = buf.b + "q";
          _loc17_ = param1;
          _loc12_ = _loc17_.method_215();
          while (_loc12_.method_74()) {
            _loc6_ = _loc12_.name_1();
            buf.b = buf.b + ":";
            buf.b = buf.b + _loc6_;
            serialize(_loc17_.method_98(_loc6_));
          }
          buf.b = buf.b + "h";
          break;
        }
        if (_loc5_ == Bytes) {
          _loc18_ = param1;
          _loc6_ = 0;
          _loc8_ = _loc18_.length - 2;
          _loc16_ = "";
          _loc19_ = Serializer.BASE64;
          while (_loc6_ < _loc8_) {
            _loc6_++;
            _loc9_ = int(_loc18_.b[_loc6_]);
            _loc6_++;
            _loc10_ = int(_loc18_.b[_loc6_]);
            _loc6_++;
            _loc20_ = int(_loc18_.b[_loc6_]);
            _loc16_ = _loc16_ + (_loc19_.charAt(_loc9_ >> 2) + _loc19_.charAt((_loc9_ << 4 | _loc10_ >> 4) & 63) + _loc19_.charAt((_loc10_ << 2 | _loc20_ >> 6) & 63) + _loc19_.charAt(_loc20_ & 63));
          }
          if (_loc6_ == _loc8_) {
            _loc6_++;
            _loc9_ = int(_loc18_.b[_loc6_]);
            _loc6_++;
            _loc10_ = int(_loc18_.b[_loc6_]);
            _loc16_ = _loc16_ + (_loc19_.charAt(_loc9_ >> 2) + _loc19_.charAt((_loc9_ << 4 | _loc10_ >> 4) & 63) + _loc19_.charAt(_loc10_ << 2 & 63));
          } else if (_loc6_ == _loc8_ + 1) {
            _loc6_++;
            _loc9_ = int(_loc18_.b[_loc6_]);
            _loc16_ = _loc16_ + (_loc19_.charAt(_loc9_ >> 2) + _loc19_.charAt(_loc9_ << 4 & 63));
          }
          buf.b = buf.b + "s";
          buf.b = buf.b + _loc16_.length;
          buf.b = buf.b + ":";
          buf.b = buf.b + _loc16_;
          break;
        }
        cache.pop();
        try {
        } catch (_loc12_:*) {
        }
        buf.b = buf.b + "c";
        serializeString(Type.method_880(_loc4_));
        cache.push(param1);
        serializeClassFields(param1, _loc4_);
        break;
      case 7:
        _loc4_ = _loc3_[0];
        if (!!useCache && Boolean(serializeRef(param1))) {
          return;
        }
        cache.pop();
        buf.b = buf.b + (!!useEnumIndex ? "j" : "w");
        serializeString(Type.method_881(_loc4_));
        if (useEnumIndex) {
          buf.b = buf.b + ":";
          buf.b = buf.b + param1.var_295;
        } else {
          serializeString(param1.var_294);
        }
        buf.b = buf.b + ":";
        _loc7_ = param1.var_296;
        if (_loc7_ == null) {
          buf.b = buf.b + 0;
        } else {
          buf.b = buf.b + int(_loc7_.length);
          _loc6_ = 0;
          while (_loc6_ < int(_loc7_.length)) {
            _loc6_++;
            serialize(_loc12_);
          }
        }
        cache.push(param1);
        break;
    }
  }
}
}
