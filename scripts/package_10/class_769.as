package package_10
{
   import flash.utils.clearInterval;
   import flash.utils.getTimer;
   import flash.utils.setInterval;
   import package_32.class_899;
   
   public class class_769
   {
       
      
      public var name_50:Function;
      
      public var var_215:Object;
      
      public function class_769(param1:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(!name_50)
         {
            name_50 = function():void
            {
            };
         }
         var var_214:class_769 = this;
         var_215 = setInterval(function():void
         {
            var_214.name_50();
         },param1);
      }
      
      public static function name_47(param1:Function, param2:int) : class_769
      {
         var var_27:Function = param1;
         var var_37:class_769 = new class_769(param2);
         var_37.name_50 = function():void
         {
            var_37.method_134();
            var_27();
         };
         return var_37;
      }
      
      public static function method_1019(param1:Function, param2:Object = undefined) : Object
      {
         var _loc3_:Number = Number(class_769.name_81());
         var _loc4_:Object = param1();
         class_870.name_17(class_769.name_81() - _loc3_ + "s",param2);
         return _loc4_;
      }
      
      public static function name_81() : Number
      {
         return getTimer() / 1000;
      }
      
      public function method_134() : void
      {
         if(var_215 == null)
         {
            return;
         }
         clearInterval(var_215);
         var_215 = null;
      }
   }
}
