package package_10
{
   import package_32.class_899;
   
   public class class_728
   {
       
      
      public function class_728()
      {
      }
      
      public static function method_445(param1:int, param2:int) : int
      {
         return param1 << 16 | param2;
      }
      
      public static function method_446(param1:int) : int
      {
         return param1;
      }
      
      public static function method_256(param1:int) : int
      {
         return param1;
      }
      
      public static function method_447(param1:int) : int
      {
         if((param1 >> 30 & 1) != param1 >>> 31)
         {
            class_899.var_317 = new Error();
            throw "Overflow " + param1;
         }
         return param1;
      }
      
      public static function method_448(param1:int) : int
      {
         return param1;
      }
      
      public static function method_124(param1:int, param2:int) : int
      {
         return param1 + param2;
      }
      
      public static function var_200(param1:int, param2:int) : int
      {
         return param1 - param2;
      }
      
      public static function method_449(param1:int, param2:int) : int
      {
         return param1 * param2;
      }
      
      public static function method_450(param1:int, param2:int) : int
      {
         return int(param1 / param2);
      }
      
      public static function package_15(param1:int, param2:int) : int
      {
         return int(param1 % param2);
      }
      
      public static function method_451(param1:int, param2:int) : int
      {
         return param1 << param2;
      }
      
      public static function method_452(param1:int, param2:int) : int
      {
         return param1 >> param2;
      }
      
      public static function method_453(param1:int, param2:int) : int
      {
         return param1 >>> param2;
      }
      
      public static function method_454(param1:int, param2:int) : int
      {
         return param1 & param2;
      }
      
      public static function method_455(param1:int, param2:int) : int
      {
         return param1 | param2;
      }
      
      public static function method_456(param1:int, param2:int) : int
      {
         return param1 ^ param2;
      }
      
      public static function method_457(param1:int) : int
      {
         return -param1;
      }
      
      public static function method_458(param1:int) : Boolean
      {
         return param1 < 0;
      }
      
      public static function method_459(param1:int) : Boolean
      {
         return param1 == 0;
      }
      
      public static function method_460(param1:int) : int
      {
         return ~param1;
      }
      
      public static function method_461(param1:int, param2:int) : int
      {
         return param1 - param2;
      }
      
      public static function method_462(param1:int, param2:int) : int
      {
         if(param1 < 0)
         {
            return param2 < 0?~param2 - ~param1:1;
         }
         return param2 < 0?-1:param1 - param2;
      }
   }
}
