package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_723 extends class_645
   {
      
      public static var var_690:int = 4;
       
      
      public var var_646:MovieClip;
      
      public var var_576:Array;
      
      public var var_691:Number;
      
      public var var_251:Number;
      
      public var var_235:int;
      
      public function class_723()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function name_14() : void
      {
         var _loc1_:MovieClip = null;
         var_229 = [true];
         name_3 = 2;
         var_251 = 25;
         var_691 = 0;
         while(int(var_576.length) > 1)
         {
            _loc1_ = var_576.pop();
            _loc1_.parent.removeChild(_loc1_);
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:MovieClip = null;
         var _loc2_:Number = NaN;
         var _loc3_:int = 0;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_434();
               break;
            case 2:
               _loc1_ = var_576[0];
               _loc2_ = 0 - _loc1_.rotation;
               _loc3_ = 5;
               var_691 = Number(var_691 + Number(class_663.method_99(-_loc3_,_loc2_ * 0.15,_loc3_)));
               var_691 = var_691 * 0.94;
               _loc1_.rotation = Number(_loc1_.rotation + var_691);
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  var_229 = [false];
                  method_81(true,10);
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_434() : void
      {
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc1_:MovieClip = var_576[var_235 - 1];
         var _loc2_:Number = _loc1_.rotation;
         var _loc3_:Number = method_100().var_244 / class_742.var_217 * 500;
         var _loc4_:Number = _loc3_ - _loc2_;
         while(_loc4_ > 180)
         {
            _loc4_ = _loc4_ - 360;
         }
         while(_loc4_ < -180)
         {
            _loc4_ = Number(_loc4_ + 360);
         }
         if(Number(Math.abs(_loc4_)) < 1.3)
         {
            new class_931(_loc1_);
            var_235 = var_235 - 1;
            if(var_235 == 0)
            {
               name_14();
            }
         }
         var _loc5_:int = var_235;
         var _loc6_:int = int(var_576.length);
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc7_ = _loc5_;
            _loc8_ = var_576[_loc7_];
            _loc8_.rotation = _loc3_;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [300];
         super.method_95(param1);
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         class_319 = var_232.method_84(class_899.__unprotect__("\x1a\x10/d\x01"),0);
         var_576 = [];
         var _loc1_:int = int(class_691.method_114(4));
         var _loc2_:int = 3 + int(Math.floor(var_237[0] * 4));
         var _loc3_:Number = 0;
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = var_232.method_84(class_899.__unprotect__("\x0b~dI\x02"),class_645.var_209);
            _loc6_.x = class_742.var_217 * 0.5;
            _loc6_.y = class_742.var_219 * 0.5;
            _loc7_ = 1 - _loc5_ / _loc2_;
            _loc6_.var_69.scaleX = _loc7_;
            _loc6_.var_69.scaleY = _loc7_;
            _loc6_.gotoAndStop(_loc1_ + 1);
            _loc8_ = 0;
            do
            {
               _loc8_ = Math.random() * 360;
               _loc9_ = Number(class_663.method_112(_loc8_ - _loc3_,180));
            }
            while(Number(Math.abs(_loc9_)) <= 30);
            
            _loc6_.rotation = _loc8_;
            _loc3_ = _loc8_;
            var_576.push(_loc6_);
            class_658.var_146(_loc6_,2,1,0,true);
         }
         var_235 = _loc2_ - 1;
      }
   }
}
