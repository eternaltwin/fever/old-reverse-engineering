package package_22.package_23
{
   import package_27.package_28.Bytes;
   import package_27.package_28.class_780;
   import package_27.package_28.class_855;
   import package_27.package_28.class_916;
   import package_32.class_899;
   import package_43.package_44._InflateImpl.class_762;
   import package_43.package_44._InflateImpl.class_906;
   
   public class class_907
   {
      
      public static var var_1433:Array = [0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,0,-1,-1];
      
      public static var var_1434:Array = [3,4,5,6,7,8,9,10,11,13,15,17,19,23,27,31,35,43,51,59,67,83,99,115,131,163,195,227,258];
      
      public static var var_1435:Array = [0,0,0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,-1,-1];
      
      public static var var_1436:Array = [1,2,3,4,5,7,9,13,17,25,33,49,65,97,129,193,257,385,513,769,1025,1537,2049,3073,4097,6145,8193,12289,16385,24577];
      
      public static var var_1437:Array = [16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15];
      
      public static var var_1438:class_732 = null;
       
      
      public var var_813:class_762;
      
      public var var_508:class_906;
      
      public var var_1443:Bytes;
      
      public var var_1442:int;
      
      public var var_1441:int;
      
      public var var_1440:int;
      
      public var var_1444:Array;
      
      public var var_1445:int;
      
      public var var_1446:class_855;
      
      public var var_1448:class_732;
      
      public var var_1449:class_732;
      
      public var var_1450:class_733;
      
      public var var_1447:Boolean;
      
      public var var_522:int;
      
      public var var_1439:int;
      
      public function class_907(param1:class_855 = undefined, param2:Boolean = true, param3:Boolean = true)
      {
         var _loc5_:int = 0;
         if(class_899.var_239)
         {
            return;
         }
         var_1447 = false;
         var_1450 = new class_733();
         var_1448 = method_1074();
         var_1449 = null;
         var_1445 = 0;
         var_522 = 0;
         var_508 = !!param2?class_906.var_1426:class_906.var_1432;
         var_1446 = param1;
         var_1439 = 0;
         var_1440 = 0;
         var_1441 = 0;
         var_1443 = null;
         var_1442 = 0;
         var_1444 = [];
         var _loc4_:int = 0;
         while(_loc4_ < 19)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            var_1444.push(-1);
         }
         var_813 = new class_762(param3);
      }
      
      public static function name_50(param1:class_855, param2:int = 65536) : Bytes
      {
         var _loc6_:int = 0;
         var _loc3_:Bytes = Bytes.alloc(param2);
         var _loc4_:class_916 = new class_916();
         var _loc5_:class_907 = new class_907(param1);
         while(true)
         {
            _loc6_ = int(_loc5_.method_615(_loc3_,0,param2));
            if(_loc6_ < 0 || _loc6_ > _loc3_.length)
            {
               break;
            }
            _loc4_.var_12.writeBytes(_loc3_.b,0,_loc6_);
            if(_loc6_ < param2)
            {
               return _loc4_.method_914();
            }
         }
         class_899.var_317 = new Error();
         throw class_780.var_768;
      }
      
      public function method_1070() : void
      {
         var_1439 = 0;
         var_1440 = 0;
      }
      
      public function method_615(param1:Bytes, param2:int, param3:int) : int
      {
         var_1441 = param3;
         var_1442 = param2;
         var_1443 = param1;
         if(param3 > 0)
         {
            while(method_1071())
            {
            }
         }
         return param3 - var_1441;
      }
      
      public function method_1071() : Boolean
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Boolean = false;
         var _loc7_:class_822 = null;
         var _loc8_:class_822 = null;
         var _loc9_:int = 0;
         var _loc10_:Array = null;
         var _loc11_:int = 0;
         var _loc12_:Bytes = null;
         switch(int(var_508.var_295))
         {
            case 0:
               _loc2_ = int(var_1446.method_774());
               _loc3_ = _loc2_ & 15;
               _loc4_ = _loc2_ >> 4;
               if(_loc3_ != 8 || _loc4_ != 7)
               {
                  class_899.var_317 = new Error();
                  throw "Invalid data";
               }
               _loc5_ = int(var_1446.method_774());
               _loc6_ = (_loc5_ & 32) != 0;
               if(int(((_loc2_ << 8) + _loc5_) % 31) != 0)
               {
                  class_899.var_317 = new Error();
                  throw "Invalid data";
               }
               if(_loc6_)
               {
                  class_899.var_317 = new Error();
                  throw "Unsupported dictionary";
               }
               var_508 = class_906.var_1432;
               return true;
            case 1:
               var_1447 = Boolean(method_1072());
               switch(int(method_1073(2)))
               {
                  case 0:
                     var_1445 = int(var_1446.method_904());
                     _loc2_ = int(var_1446.method_904());
                     if(_loc2_ != 65535 - var_1445)
                     {
                        class_899.var_317 = new Error();
                        throw "Invalid data";
                     }
                     var_508 = class_906.var_1427;
                     _loc6_ = Boolean(method_1071());
                     method_1070();
                     return _loc6_;
                  case 1:
                     var_1448 = method_1074();
                     var_1449 = null;
                     var_508 = class_906.var_1063;
                     return true;
                  case 2:
                     _loc2_ = int(method_1073(5)) + 257;
                     _loc3_ = int(method_1073(5)) + 1;
                     _loc4_ = int(method_1073(4)) + 4;
                     _loc5_ = 0;
                     while(_loc5_ < _loc4_)
                     {
                        _loc5_++;
                        _loc9_ = _loc5_;
                        var_1444[int(class_907.var_1437[_loc9_])] = int(method_1073(3));
                     }
                     _loc5_ = _loc4_;
                     while(_loc5_ < 19)
                     {
                        _loc5_++;
                        _loc9_ = _loc5_;
                        var_1444[int(class_907.var_1437[_loc9_])] = 0;
                     }
                     var_1448 = var_1450.method_445(var_1444,0,19,8);
                     _loc10_ = [];
                     _loc5_ = 0;
                     _loc9_ = _loc2_ + _loc3_;
                     while(_loc5_ < _loc9_)
                     {
                        _loc5_++;
                        _loc11_ = _loc5_;
                        _loc10_.push(0);
                     }
                     method_1075(_loc10_,_loc2_ + _loc3_);
                     var_1449 = var_1450.method_445(_loc10_,_loc2_,_loc3_,16);
                     var_1448 = var_1450.method_445(_loc10_,0,_loc2_,16);
                     var_508 = class_906.var_1063;
                     return true;
               }
            case 2:
               _loc2_ = int(method_1078(var_1448));
               if(_loc2_ < 256)
               {
                  method_1069(_loc2_);
                  return var_1441 > 0;
               }
               if(_loc2_ == 256)
               {
                  var_508 = !!var_1447?class_906.var_1431:class_906.var_1432;
                  return true;
               }
               _loc2_ = _loc2_ - 257;
               _loc3_ = int(class_907.var_1433[_loc2_]);
               if(_loc3_ == -1)
               {
                  class_899.var_317 = new Error();
                  throw "Invalid data";
               }
               var_1445 = int(class_907.var_1434[_loc2_]) + int(method_1073(_loc3_));
               _loc4_ = var_1449 == null?int(method_1079(5)):int(method_1078(var_1449));
               _loc3_ = int(class_907.var_1435[_loc4_]);
               if(_loc3_ == -1)
               {
                  class_899.var_317 = new Error();
                  throw "Invalid data";
               }
               var_522 = int(class_907.var_1436[_loc4_]) + int(method_1073(_loc3_));
               if(var_522 > int(var_813.method_1067()))
               {
                  class_899.var_317 = new Error();
                  throw "Invalid data";
               }
               var_508 = var_522 == 1?class_906.var_1429:class_906.var_1430;
               return true;
            case 3:
               _loc2_ = var_1445 < var_1441?var_1445:var_1441;
               _loc12_ = var_1446.method_653(_loc2_);
               var_1445 = var_1445 - _loc2_;
               method_1068(_loc12_,0,_loc2_);
               if(var_1445 == 0)
               {
                  var_508 = !!var_1447?class_906.var_1431:class_906.var_1432;
               }
               return var_1441 > 0;
            case 4:
               _loc7_ = var_813.method_1066();
               if(_loc7_ == null)
               {
                  var_508 = class_906.var_1428;
                  return true;
               }
               _loc8_ = class_822.method_653(var_1446);
               if(!_loc7_.method_775(_loc8_))
               {
                  class_899.var_317 = new Error();
                  throw "Invalid CRC";
               }
               var_508 = class_906.var_1428;
               return true;
            case 5:
               while(var_1445 > 0 && var_1441 > 0)
               {
                  _loc2_ = var_1445 < var_522?var_1445:var_522;
                  _loc3_ = var_1441 < _loc2_?var_1441:_loc2_;
                  method_1077(var_522,_loc3_);
                  var_1445 = var_1445 - _loc3_;
               }
               if(var_1445 == 0)
               {
                  var_508 = class_906.var_1063;
               }
               return var_1441 > 0;
            case 6:
               _loc2_ = var_1445 < var_1441?var_1445:var_1441;
               method_1076(_loc2_);
               var_1445 = var_1445 - _loc2_;
               if(var_1445 == 0)
               {
                  var_508 = class_906.var_1063;
               }
               return var_1441 > 0;
            case 7:
               return false;
         }
      }
      
      public function method_1075(param1:Array, param2:int) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         while(_loc3_ < param2)
         {
            _loc5_ = int(method_1078(var_1448));
            switch(_loc5_)
            {
               case 0:
               case 1:
               case 2:
               case 3:
               case 4:
               case 5:
               case 6:
               case 7:
               case 8:
               case 9:
               case 10:
               case 11:
               case 12:
               case 13:
               case 14:
               case 15:
                  _loc4_ = _loc5_;
                  param1[_loc3_] = _loc5_;
                  _loc3_++;
                  continue;
               case 16:
                  _loc6_ = _loc3_ + 3 + int(method_1073(2));
                  if(_loc6_ > param2)
                  {
                     class_899.var_317 = new Error();
                     throw "Invalid data";
                  }
                  while(_loc3_ < _loc6_)
                  {
                     param1[_loc3_] = _loc4_;
                     _loc3_++;
                  }
                  continue;
               case 17:
                  _loc3_ = _loc3_ + (3 + int(method_1073(3)));
                  if(_loc3_ > param2)
                  {
                     class_899.var_317 = new Error();
                     throw "Invalid data";
                  }
                  continue;
               case 18:
                  _loc3_ = _loc3_ + (11 + int(method_1073(7)));
                  if(_loc3_ > param2)
                  {
                     class_899.var_317 = new Error();
                     throw "Invalid data";
                  }

            }
         }
      }
      
      public function method_1079(param1:int) : int
      {
         return param1 == 0?0:Boolean(method_1072())?1 << param1 - 1 | int(method_1079(param1 - 1)):int(method_1079(param1 - 1));
      }
      
      public function method_1073(param1:int) : int
      {
         while(var_1440 < param1)
         {
            var_1439 = var_1439 | int(var_1446.method_774()) << var_1440;
            var_1440 = var_1440 + 8;
         }
         var _loc2_:int = var_1439 & (1 << param1) - 1;
         var_1440 = var_1440 - param1;
         var_1439 = var_1439 >> param1;
         return _loc2_;
      }
      
      public function method_1072() : Boolean
      {
         if(var_1440 == 0)
         {
            var_1440 = 8;
            var_1439 = int(var_1446.method_774());
         }
         var _loc1_:Boolean = (var_1439 & 1) == 1;
         var_1440 = var_1440 - 1;
         var_1439 = var_1439 >> 1;
         return _loc1_;
      }
      
      public function method_1074() : class_732
      {
         var _loc3_:int = 0;
         if(class_907.var_1438 != null)
         {
            return class_907.var_1438;
         }
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         while(_loc2_ < 288)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc1_.push(_loc3_ <= 143?8:_loc3_ <= 255?9:_loc3_ <= 279?7:8);
         }
         class_907.var_1438 = var_1450.method_445(_loc1_,0,288,10);
         return class_907.var_1438;
      }
      
      public function method_1078(param1:class_732) : int
      {
         var _loc3_:int = 0;
         var _loc4_:class_732 = null;
         var _loc5_:class_732 = null;
         var _loc6_:Array = null;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc3_ = _loc2_[0];
               §§push(_loc3_);
               break;
            case 1:
               _loc4_ = _loc2_[0];
               _loc5_ = _loc2_[1];
               §§push(int(method_1078(Boolean(method_1072())?_loc5_:_loc4_)));
               break;
            case 2:
               _loc3_ = _loc2_[0];
               _loc6_ = _loc2_[1];
               §§push(int(method_1078(_loc6_[int(method_1073(_loc3_))])));
         }
         return; §§pop();
      }
      
      public function method_1076(param1:int) : void
      {
         var _loc4_:int = 0;
         var _loc2_:int = int(var_813.method_1065());
         var _loc3_:int = 0;
         while(_loc3_ < param1)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            method_1069(_loc2_);
         }
      }
      
      public function method_1077(param1:int, param2:int) : void
      {
         method_1068(var_813.var_1425,var_813.var_460 - param1,param2);
      }
      
      public function method_1068(param1:Bytes, param2:int, param3:int) : void
      {
         var_813.method_1068(param1,param2,param3);
         var_1443.method_504(var_1442,param1,param2,param3);
         var_1441 = var_1441 - param3;
         var_1442 = var_1442 + param3;
      }
      
      public function method_1069(param1:int) : void
      {
         var_813.method_1069(param1);
         var_1443.b[var_1442] = param1;
         var_1441 = var_1441 - 1;
         var_1442 = var_1442 + 1;
      }
   }
}
