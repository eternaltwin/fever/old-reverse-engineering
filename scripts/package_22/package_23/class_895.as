package package_22.package_23
{
   import flash.utils.ByteArray;
   import package_27.package_28.Bytes;
   
   public class class_895
   {
       
      
      public function class_895()
      {
      }
      
      public static function name_50(param1:Bytes) : Bytes
      {
         var _loc2_:ByteArray = new ByteArray();
         _loc2_.writeBytes(param1.b,0,param1.length);
         _loc2_.uncompress();
         return Bytes.ofData(_loc2_);
      }
   }
}
