package package_22.package_23
{
   import package_27.package_28.Bytes;
   import package_27.package_28.class_855;
   import package_32.class_899;
   
   public class class_822
   {
       
      
      public var var_940:int;
      
      public var var_18:int;
      
      public function class_822()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_18 = 1;
         var_940 = 0;
      }
      
      public static function method_653(param1:class_855) : class_822
      {
         var _loc2_:class_822 = new class_822();
         var _loc3_:int = int(param1.method_774());
         var _loc4_:int = int(param1.method_774());
         var _loc5_:int = int(param1.method_774());
         var _loc6_:int = int(param1.method_774());
         _loc2_.var_18 = _loc5_ << 8 | _loc6_;
         _loc2_.var_940 = _loc3_ << 8 | _loc4_;
         return _loc2_;
      }
      
      public function method_79(param1:Bytes, param2:int, param3:int) : void
      {
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc4_:int = var_18;
         var _loc5_:int = var_940;
         var _loc6_:int = param2;
         var _loc7_:int = param2 + param3;
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc9_ = int(param1.b[_loc8_]);
            _loc4_ = int((_loc4_ + _loc9_) % 65521);
            _loc5_ = int((_loc5_ + _loc4_) % 65521);
         }
         var_18 = _loc4_;
         var_940 = _loc5_;
      }
      
      public function method_775(param1:class_822) : Boolean
      {
         return param1.var_18 == var_18 && param1.var_940 == var_940;
      }
   }
}
