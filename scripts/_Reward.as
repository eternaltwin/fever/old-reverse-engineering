package
{
   import package_32.class_899;
   
   public final class _Reward
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = [
         "i\b3\x14\x01",
         "\x12?E|\x03",
         "4\x10J\x11",
         "ZF\'-",
         "|5M\'",
         ",EJv\x02",
         "e\x03M\x01",
         "9JG\x01",
         "RLY\'\x02",
      ];
      
      public static var class_794:_Reward;
      
      public static var var_394:_Reward;
      
      public static var var_395:_Reward;
      
      public static var var_396:_Reward;
      
      public static var var_397:_Reward;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function _Reward(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_273(param1:_Item) : _Reward
      {
         return new _Reward("i\b3\x14\x01",0,[param1]);
      }
      
      public static function method_274(param1:_IslandBonus) : _Reward
      {
         return new _Reward("\x12?E|\x03",1,[param1]);
      }
      
      public static function method_275(param1:_GameBonus) : _Reward
      {
         return new _Reward("RLY\'\x02",8,[param1]);
      }
      
      public static function method_276(param1:int) : _Reward
      {
         return new _Reward("|5M\'",4,[param1]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
