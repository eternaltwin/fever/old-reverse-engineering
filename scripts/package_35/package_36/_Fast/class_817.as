package package_35.package_36._Fast
{
   import package_32.class_899;
   
   public dynamic class class_817
   {
       
      
      public var __x:Xml;
      
      public function class_817(param1:Xml = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         __x = param1;
      }
      
      public function method_657(param1:String) : Boolean
      {
         if(__x.var_880 == Xml.class_771)
         {
            class_899.var_317 = new Error();
            throw "Cannot access document attribute " + param1;
         }
         return Boolean(__x.method_217(param1));
      }
   }
}
