package package_35.package_36._Fast
{
   import package_32.class_899;
   import package_37.package_38.class_820;
   
   public dynamic class class_819
   {
       
      
      public var __x:Xml;
      
      public function class_819(param1:Xml = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         __x = param1;
      }
      
      public function method_657(param1:String) : class_795
      {
         var _loc4_:Xml = null;
         var _loc2_:class_795 = new class_795();
         var _loc3_:* = __x.method_768(param1);
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            _loc2_.method_124(new class_820(_loc4_));
         }
         return _loc2_;
      }
   }
}
