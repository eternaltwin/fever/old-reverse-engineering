package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   import package_8.package_9.class_671;
   
   public class class_779 extends class_645
   {
      
      public static var var_810:int = 87;
      
      public static var var_911:int = 55;
      
      public static var var_912:int = 80;
       
      
      public var var_351:Number;
      
      public var var_531:MovieClip;
      
      public var var_288:MovieClip;
      
      public var var_915:Number;
      
      public var var_914:Number;
      
      public var var_522:Number;
      
      public var var_581:int;
      
      public var var_913:class_795;
      
      public function class_779()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_686(param1:class_892) : void
      {
         param1.var_244 = Number(param1.var_244 + var_351 * int(param1.var_796));
         var _loc3_:MovieClip = param1.var_243.var_1;
         var _loc2_:MovieClip = _loc3_;
         switch(int(param1.var_252))
         {
            case 0:
               param1.name_90 = (Number(param1.name_90) + var_351) % 16;
               if(_loc2_ != null)
               {
                  _loc2_.gotoAndStop(int(Number(param1.name_90)) + 1);
                  break;
               }
               break;
            case 1:
               param1.name_90 = (Number(param1.name_90) + var_351) % 20;
               if(_loc2_ != null)
               {
                  _loc2_.gotoAndStop(int(Number(param1.name_90)) + 1);
                  break;
               }
               break;
            case 2:
               param1.name_90 = (Number(param1.name_90) + var_351) % 18;
               if(_loc2_ != null)
               {
                  _loc2_.gotoAndStop(int(Number(param1.name_90)) + 1);
                  break;
               }
         }
         if(param1.var_244 > 72 && var_220 == null)
         {
            var_288.gotoAndPlay("gameover_" + int(param1.var_252));
            method_81(false,40);
            name_3 = 2;
            if(int(param1.var_252) != 0)
            {
               param1.method_89();
               var_913.method_116(param1);
            }
         }
         if(param1.var_244 > 75)
         {
            param1.var_245 = class_779.var_810 + int(param1.var_17) - (param1.var_244 - 75);
            param1.var_531.visible = false;
         }
      }
      
      public function method_299() : void
      {
         var _loc2_:* = null;
         if(var_581 > 0)
         {
            var_581 = var_581 - 1;
            return;
         }
         var _loc1_:Number = var_201.mouseY;
         if(_loc1_ > class_779.var_912)
         {
            var_288.gotoAndStop("crouch");
         }
         else if(_loc1_ < class_779.var_911)
         {
            var_288.gotoAndStop("up");
         }
         else
         {
            var_288.gotoAndStop("stand");
         }
         if(name_5)
         {
            _loc2_ = null;
            if(_loc1_ > class_779.var_912)
            {
               var_288.gotoAndPlay("kick");
               var_581 = 10;
               _loc2_ = 0;
            }
            else if(_loc1_ < class_779.var_911)
            {
               var_288.gotoAndPlay("jump");
               var_581 = 16;
               _loc2_ = 1;
            }
            else
            {
               var_288.gotoAndPlay("punch");
               var_581 = 8;
               _loc2_ = 2;
            }
            if(_loc2_ != null)
            {
               name_80(_loc2_);
            }
         }
      }
      
      public function method_687(param1:class_892) : void
      {
         var _loc2_:MovieClip = null;
         if(param1.var_279 > 0)
         {
            _loc2_ = param1.var_243.var_1;
            _loc2_.gotoAndStop("fall");
         }
         if(param1.var_245 > class_779.var_810)
         {
            param1.var_245 = class_779.var_810;
            param1.var_250 = 0;
            param1.var_279 = 0;
            param1.var_233 = 0.75;
            _loc2_ = param1.var_243.var_1;
            _loc2_.gotoAndPlay("rebond");
         }
      }
      
      public function YAfS() : void
      {
         var _loc2_:class_892 = null;
         var _loc1_:* = var_913.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            switch(int(_loc2_.var_508))
            {
               case 0:
                  method_686(_loc2_);
                  break;
               case 1:
                  method_687(_loc2_);
            }
            _loc2_.var_531.x = _loc2_.var_244;
            _loc2_.var_243.x = int(_loc2_.var_244);
            _loc2_.var_243.y = int(_loc2_.var_245);
         }
      }
      
      override public function method_79() : void
      {
         var_522 = var_522 - var_351;
         if(var_522 <= 0)
         {
            var_522 = Number(var_914 + Math.random() * var_915);
            method_688();
         }
         if(name_3 == 1)
         {
            method_299();
         }
         YAfS();
         super.method_79();
      }
      
      public function name_80(param1:int) : void
      {
         var _loc4_:class_892 = null;
         var _loc8_:MovieClip = null;
         var _loc2_:class_892 = null;
         var _loc3_:* = var_913.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(int(_loc4_.var_508) == 0)
            {
               _loc2_ = _loc4_;
            }
         }
         if(_loc2_ == null)
         {
            return;
         }
         if(!_loc2_.var_155[param1])
         {
            return;
         }
         var _loc5_:Array = [17,11,15];
         var _loc6_:Array = [8,8,9];
         var _loc7_:Number = _loc2_.var_244 - (var_288.x - int(_loc5_[param1]));
         if(Number(Math.abs(_loc7_)) < int(_loc6_[param1]))
         {
            _loc8_ = _loc2_.var_243.var_1;
            _loc8_.gotoAndPlay("strike");
            _loc2_.var_508 = 1;
            _loc2_.var_278 = -Math.random() * 5;
            _loc2_.var_279 = -(2 + Math.random() * 3);
            _loc2_.var_250 = 1;
            _loc2_.var_233 = 0.9;
            _loc8_ = var_232.method_84(class_899.__unprotect__("\nyr\x12\x01"),3);
            _loc8_.x = var_288.x - int(_loc5_[param1]);
            _loc8_.y = var_288.y - int([2,20,15][param1]);
            _loc8_.rotation = int(class_691.method_114(4)) * 90;
            switch(param1)
            {
               case 0:
                  _loc2_.var_279 = -2;
                  _loc2_.var_245 = _loc2_.var_245 - 5;
                  _loc2_.var_278 = _loc2_.var_278 * 0.25;
                  break;
               case 1:
                  _loc2_.var_279 = _loc2_.var_279 - 5;
                  _loc2_.var_278 = _loc2_.var_278 * 0.5;
            }
         }
      }
      
      override public function method_80() : void
      {
         var _loc2_:class_892 = null;
         method_81(true,12);
         var _loc1_:* = var_913.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            if(int(_loc2_.var_508) == 0)
            {
               _loc2_.var_796 = -1;
               _loc2_.var_243.scaleX = -1;
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_351 = Number(0.8 + param1 * 1.6);
         var_522 = 60;
         var_914 = 32;
         var_915 = 20;
         var_913 = new class_795();
         method_117();
         var_201.scaleY = Number(4);
         var_201.scaleX = 4;
         var_522 = 0;
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\'\x0e\tQ\x03"),0);
         var_288 = var_232.method_84(class_899.__unprotect__("5\\&d"),2);
         var_288.x = 69;
         var_288.y = class_779.var_810;
         var_531 = var_232.method_84(class_899.__unprotect__("c\x12;\x1a\x02"),0);
         var_531.x = 69;
         var_531.y = class_779.var_810;
      }
      
      public function method_688() : void
      {
         if(var_220)
         {
            return;
         }
         var _loc1_:int = 1;
         if(var_237[0] > 0.15)
         {
            _loc1_++;
         }
         if(var_237[0] > 0.35)
         {
            _loc1_++;
         }
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("_tb\x04\x02"),1));
         _loc2_.var_252 = int(class_691.method_114(_loc1_));
         _loc2_.name_90 = 0;
         _loc2_.var_243.gotoAndStop(int(_loc2_.var_252) + 1);
         new class_671(_loc2_.var_243,class_899.__unprotect__("D\x07\x01\x02"),1);
         _loc2_.var_244 = -10;
         _loc2_.var_508 = 0;
         _loc2_.var_155 = [true,true,true];
         _loc2_.var_531 = var_232.method_84(class_899.__unprotect__("c\x12;\x1a\x02"),0);
         _loc2_.var_531.gotoAndStop(int(_loc2_.var_252) + 1);
         _loc2_.var_17 = 0;
         _loc2_.var_796 = 1;
         _loc2_.var_531.x = -100;
         _loc2_.var_531.y = class_779.var_810;
         switch(int(_loc2_.var_252))
         {
            case 0:
               _loc2_.var_17 = 0;
               break;
            case 1:
               _loc2_.var_17 = -10;
               if(var_237[0] > 0.45 && int(class_691.method_114(2)) == 0)
               {
                  _loc2_.var_17 = int(_loc2_.var_17) - 8;
                  _loc2_.var_155[2] = false;
               }
               _loc2_.var_155[0] = false;
               break;
            case 2:
               _loc2_.var_17 = 0;
               _loc2_.var_155[1] = false;
               _loc2_.var_155[2] = false;
         }
         _loc2_.var_245 = class_779.var_810 + int(_loc2_.var_17);
         var_913.method_103(_loc2_);
      }
   }
}
