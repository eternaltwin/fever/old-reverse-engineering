package package_20.package_21
{
   import package_17.class_718;
   import package_17.class_800;
   import package_32.class_899;
   
   public class class_893
   {
       
      
      public var var_37:Number;
      
      public var var_773:class_800;
      
      public var var_93:Number;
      
      public var name_25:class_893;
      
      public var name_1:class_893;
      
      public var var_291:Number;
      
      public var var_510:class_724;
      
      public var var_12:Number;
      
      public function class_893(param1:Number = 0.0, param2:Number = 0.0, param3:Number = 0.0, param4:Number = 0.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_291 = param1;
         var_37 = param2;
         var_93 = param3;
         var_12 = param4;
      }
      
      public function toString() : String
      {
         return "[l=" + var_291 + " b=" + var_12 + " r=" + var_93 + " t=" + var_37 + "]";
      }
      
      public function method_1025(param1:class_893) : Boolean
      {
         return var_291 <= param1.var_93 && param1.var_291 <= var_93 && var_37 <= param1.var_12 && param1.var_37 <= var_12;
      }
      
      public function method_1026(param1:class_893) : Boolean
      {
         return !(param1.var_291 > var_93 || param1.var_93 < var_291 || param1.var_37 > var_12 || param1.var_12 < var_37);
      }
      
      public function method_1027(param1:class_718) : Boolean
      {
         return !(param1.var_245 < var_37 || param1.var_245 > var_12 || param1.var_244 < var_291 || param1.var_244 > var_93);
      }
   }
}
