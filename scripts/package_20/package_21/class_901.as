package package_20.package_21
{
   import package_10.class_782;
   import package_10.class_806;
   import package_17.class_725;
   import package_17.class_800;
   
   public class class_901 implements class_735
   {
       
      
      public var var_676:class_806;
      
      public var var_1403:class_734;
      
      public function class_901()
      {
      }
      
      public function method_477() : Boolean
      {
         return true;
      }
      
      public function method_421(param1:class_800) : void
      {
      }
      
      public function method_436(param1:class_800) : void
      {
         var_676.method_116(param1);
      }
      
      public function method_478(param1:class_893) : class_806
      {
         var _loc4_:class_800 = null;
         var _loc5_:class_893 = null;
         var _loc2_:class_806 = new class_806();
         var _loc3_:class_782 = var_676.var_84;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_.var_223;
            _loc3_ = _loc3_.name_1;
            _loc5_ = _loc4_.var_1007;
            if(!(param1.var_291 > _loc5_.var_93 || param1.var_93 < _loc5_.var_291 || param1.var_37 > _loc5_.var_12 || param1.var_12 < _loc5_.var_37))
            {
               _loc2_.var_84 = new class_782(_loc4_,_loc2_.var_84);
            }
         }
         return _loc2_;
      }
      
      public function method_95(param1:class_893, param2:class_734, param3:class_725) : void
      {
         var_1403 = param2;
         var_676 = new class_806();
      }
      
      public function method_479() : void
      {
      }
      
      public function method_480() : void
      {
         var _loc2_:class_893 = null;
         var _loc3_:class_782 = null;
         var _loc4_:class_893 = null;
         var _loc1_:class_782 = var_676.var_84;
         while(_loc1_ != null)
         {
            _loc2_ = _loc1_.var_223.var_1007;
            _loc3_ = _loc1_.name_1;
            while(_loc3_ != null)
            {
               _loc4_ = _loc3_.var_223.var_1007;
               if(_loc2_.var_291 <= _loc4_.var_93 && _loc4_.var_291 <= _loc2_.var_93 && _loc2_.var_37 <= _loc4_.var_12 && _loc4_.var_37 <= _loc2_.var_12)
               {
                  var_1403.method_476(_loc1_.var_223,_loc3_.var_223);
               }
               _loc3_ = _loc3_.name_1;
            }
            _loc1_ = _loc1_.name_1;
         }
      }
      
      public function method_343(param1:class_800) : void
      {
         var _loc2_:class_806 = var_676;
         _loc2_.var_84 = new class_782(param1,_loc2_.var_84);
      }
   }
}
