package
{
   import package_10.class_741;
   
   public class class_830 implements class_741
   {
       
      
      public function class_830()
      {
      }
      
      public static function method_176(param1:_Reward) : Boolean
      {
         var _loc3_:_Item = null;
         var _loc4_:_IslandBonus = null;
         var _loc5_:int = 0;
         if(param1 == null)
         {
            return false;
         }
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc3_ = _loc2_[0];
               return true;
            case 1:
               _loc4_ = _loc2_[0];
               return true;
            case 2:
               return true;
            case 3:
               return true;
            case 4:
               _loc5_ = _loc2_[0];
               return true;
         }
      }
      
      public static function method_805(param1:Object) : void
      {
         var _loc5_:_Item = null;
         var _loc2_:int = 0;
         _loc2_ = _loc2_ + int(param1._wid) * 20000;
         _loc2_ = _loc2_ + int(param1._frags);
         _loc2_ = _loc2_ + int(param1._isl_visited) * 2;
         _loc2_ = _loc2_ + int(param1._isl_done) * 5;
         _loc2_ = _loc2_ + int(param1._carts) * 10;
         _loc2_ = _loc2_ + int(param1._statues) * 100;
         var _loc3_:int = 0;
         var _loc4_:Array = param1._items;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            switch(int(_loc5_.var_295))
            {
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
               default:
                  _loc2_ = _loc2_ + 200;
                  continue;
               case 21:
               case 22:
               case 23:
               case 24:
               case 25:
               case 26:
               case 27:
                  _loc2_ = _loc2_ + 500;

            }
         }
         param1._score = _loc2_;
      }
      
      public static function method_807(param1:int) : Object
      {
         var _loc2_:class_911 = new class_911(param1);
         _loc2_.method_95();
         while(!_loc2_.var_302)
         {
            _loc2_.method_79();
         }
         var _loc3_:* = _loc2_.var_443[int(int(_loc2_.var_443.length) * class_911.var_1100)];
         return {
            "C!F[\x01":_loc2_.var_307,
            "f4;E\x03":_loc2_.method_806(),
            "Sx_\x18\x02":{
               "L\x01":int(_loc3_.var_244),
               "\x03\x01":int(_loc3_.var_245)
            }
         };
      }
   }
}
