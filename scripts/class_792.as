package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_792 extends class_645
   {
       
      
      public var var_66:Number;
      
      public var var_319:Number;
      
      public var var_752:Array;
      
      public var var_698:MovieClip;
      
      public var var_295:int;
      
      public var var_714:Number;
      
      public var var_964:Boolean;
      
      public var var_963:Boolean;
      
      public var var_491:Boolean;
      
      public var var_962:class_892;
      
      public var var_700:Number;
      
      public var var_248:Number;
      
      public function class_792()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:class_656 = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:class_13 = null;
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Boolean = false;
         var _loc10_:MovieClip = null;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:class_656 = null;
         super.method_79();
         loop3:
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = var_752[var_295];
               if(_loc1_ != null)
               {
                  _loc2_ = (method_100().var_244 / class_742.var_217 * 2 - 1) * 0.6;
                  _loc3_ = _loc2_ - var_248;
                  var_248 = Number(var_248 + _loc3_ * 0.15);
                  _loc1_.var_243.rotation = var_248 / 0.0174;
               }
               var_700 = Number(var_700 + var_962.var_278 * 12);
               while(var_700 > 314)
               {
                  var_700 = var_700 - 628;
               }
               while(var_700 < -314)
               {
                  var_700 = Number(var_700 + 628);
               }
               var_962.var_243.rotation = var_700 / 100 / 0.0174;
               _loc4_ = var_962.var_243;
               _loc4_.var_10.rotation = -_loc4_.rotation;
               _loc2_ = var_66;
               _loc5_ = 0;
               if(var_700 > 157 || var_700 < -157)
               {
                  _loc3_ = Math.abs(var_700 / 157) - 1;
                  _loc2_ = Number(_loc2_ + _loc2_ * _loc3_ * 0.65);
                  if(var_491)
                  {
                     var_962.var_278 = var_962.var_278 - var_700 / 157 * 0.1;
                  }
               }
               if(!var_491)
               {
                  _loc3_ = Math.sin(var_248) / Math.cos(var_248);
                  _loc6_ = 0;
                  if(_loc1_ != null)
                  {
                     _loc6_ = _loc1_.var_245 - _loc3_ * _loc1_.var_244;
                  }
                  _loc7_ = var_962.var_244;
                  _loc8_ = Number(var_962.var_245 + var_66);
                  if(_loc8_ > Number(_loc3_ * _loc7_ + _loc6_))
                  {
                     _loc9_ = false;
                     if(_loc1_ != null)
                     {
                        _loc9_ = Number(_loc1_.method_115({
                           "L\x01":_loc7_,
                           "\x03\x01":_loc8_
                        })) < _loc1_.var_371 * 0.5;
                     }
                     if(_loc9_)
                     {
                        if(var_963)
                        {
                           method_716();
                           var_962.var_279 = var_962.var_279 * -0.2;
                        }
                        else
                        {
                           method_717();
                        }
                     }
                     else
                     {
                        method_718();
                        var_963 = true;
                     }
                  }
                  if(_loc8_ > var_714)
                  {
                     var_962.var_250 = 0;
                     var_962.var_245 = var_714 - _loc2_ * 0.5;
                     var_962.var_278 = 0;
                     var_962.var_279 = 0;
                     if(Number(Math.abs(_loc7_ - var_698.x)) < 10)
                     {
                        _loc10_ = var_232.method_84(class_899.__unprotect__("\x1f2\b\x17"),class_645.var_209);
                        _loc10_.x = var_698.x;
                        _loc10_.y = var_698.y;
                        var_962.var_243.mask = _loc10_;
                        method_81(true,20);
                        name_3 = 2;
                     }
                     else
                     {
                        method_716();
                     }
                  }
               }
               if(var_491)
               {
                  var_962.var_278 = Number(var_962.var_278 + var_248);
                  var_962.var_278 = var_962.var_278 * 0.92;
                  _loc9_ = false;
                  if(_loc1_ != null)
                  {
                     _loc9_ = Number(var_962.method_115(_loc1_)) < _loc1_.var_371 * 0.5;
                  }
                  if(_loc9_)
                  {
                     _loc3_ = var_962.var_244;
                     if(_loc1_ != null)
                     {
                        _loc3_ = var_962.var_244 - _loc1_.var_244;
                     }
                     _loc6_ = _loc3_ / Math.cos(var_248);
                     _loc7_ = Math.sin(var_248) * _loc6_;
                     var_962.var_245 = _loc7_ - _loc2_;
                     if(_loc1_ != null)
                     {
                        var_962.var_245 = Number(var_962.var_245 + _loc1_.var_245);
                     }
                  }
                  else
                  {
                     method_691();
                     method_718();
                  }
               }
               _loc11_ = 0;
               _loc12_ = var_295;
               while(true)
               {
                  if(_loc11_ >= _loc12_)
                  {
                     break loop3;
                  }
                  _loc11_++;
                  _loc13_ = _loc11_;
                  _loc14_ = var_752[_loc13_];
                  if(_loc14_ != null)
                  {
                     _loc14_.var_580 = _loc14_.var_580 + -_loc14_.var_243.rotation * 0.05;
                     _loc14_.var_580 = _loc14_.var_580 * 0.95;
                     _loc14_.var_243.rotation = Number(_loc14_.var_243.rotation + _loc14_.var_580);
                  }
               }
         }
         var_962.var_243.x = var_962.var_244;
         var_962.var_243.y = var_962.var_245;
      }
      
      public function method_718() : void
      {
         var_295 = var_295 + 1;
         var_248 = 0;
      }
      
      public function method_717() : void
      {
         var_491 = true;
         var_962.var_250 = 0;
         var_962.var_279 = 0;
         method_79();
         var_962.method_118();
      }
      
      public function method_691() : void
      {
         var_491 = false;
         var_962.var_250 = 1;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400 - param1 * 200];
         super.method_95(param1);
         var_66 = 7;
         var_491 = false;
         var_963 = false;
         var_248 = 0;
         var_700 = 0;
         var_295 = 0;
         var_714 = class_742.var_219 - 8;
         method_117();
         method_72();
      }
      
      public function method_716() : void
      {
         name_3 = 3;
         var_962.var_243.gotoAndPlay("break");
         method_81(false,20);
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:class_656 = null;
         var _loc6_:class_656 = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:class_16 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x03N@d"),0);
         var_698 = var_232.method_84(class_899.__unprotect__("*H 5\x02"),class_645.var_209);
         var_698.y = var_714;
         var _loc1_:* = {
            "L\x01":class_742.var_217 * 0.5,
            "1\x16\\#":50
         };
         var_752 = [];
         var _loc3_:int = 0;
         while(_loc3_ < 3)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc6_ = new class_656(var_232.method_84(class_899.__unprotect__("VT\x1e\x17\x02"),class_645.var_209));
            _loc5_ = _loc6_;
            _loc7_ = 0;
            _loc8_ = 0;
            _loc9_ = 0;
            do
            {
               _loc7_ = 80 - var_237[0] * 40;
               _loc7_ = Number(_loc7_ + Math.random() * _loc7_ * 0.5);
               _loc8_ = Number(_loc7_ * 0.5 + Math.random() * (class_742.var_217 - _loc7_));
               _loc9_ = Number(50 + (class_742.var_219 - 50) * (_loc4_ / 3));
               _loc10_ = Number(Math.abs(_loc8_ - _loc1_.var_244));
            }
            while(!(_loc10_ > int(_loc1_.var_371) - _loc7_ + 50 - var_237[0] * 0.3 && _loc10_ < (int(_loc1_.var_371) + _loc7_) * 0.5));
            
            _loc5_.var_371 = _loc7_;
            _loc5_.var_580 = 0;
            _loc5_.var_244 = _loc8_;
            _loc5_.var_245 = _loc9_;
            _loc11_ = _loc5_.var_243;
            _loc11_.var_12.var_69.scaleX = (_loc5_.var_371 - 4) * 0.01;
            _loc11_.var_12.x = -(_loc5_.var_371 - 4) * 0.5;
            _loc11_.var_13.x = _loc11_.var_12.x;
            _loc11_.var_14.x = -_loc11_.var_12.x;
            _loc5_.method_118();
            _loc1_ = _loc5_;
            var_752.push(_loc5_);
         }
         _loc3_ = 20;
         if(Number(_loc1_.var_244) > class_742.var_217 * 0.5)
         {
            var_698.x = Number(_loc1_.var_244) - (Number(_loc3_ + int(_loc1_.var_371) * 0.5));
         }
         else
         {
            var_698.x = Number(Number(_loc1_.var_244) + (Number(_loc3_ + int(_loc1_.var_371) * 0.5)));
         }
         var _loc12_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("!2\x17i\x01"),class_645.var_209));
         _loc12_.var_233 = 0.99;
         var_962 = _loc12_;
         var_962.var_244 = var_752[var_295].var_244;
         var_962.var_245 = var_752[var_295].var_245 - 10;
         var_962.var_250 = 1;
         var_962.var_243.stop();
         var_962.method_118();
      }
   }
}
