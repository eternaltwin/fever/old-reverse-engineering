package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_646 extends class_645
   {
       
      
      public var var_240:Array;
      
      public var var_241:Number;
      
      public var var_247:Array;
      
      public var var_242:Array;
      
      public var var_249:int;
      
      public var var_246:class_892;
      
      public function class_646()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_106() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:class_892 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var_241 = Number(var_241 + 0.05);
         var _loc1_:int = 0;
         var _loc2_:int = int(var_242.length);
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = var_242[_loc3_];
            if(_loc4_.name_6 != null)
            {
               _loc4_.name_6 = _loc4_.name_6 - 1;
               if(_loc4_.name_6 == 0)
               {
                  _loc4_.name_6 = null;
                  _loc4_.var_243.play();
               }
            }
            else
            {
               method_105(_loc4_);
            }
            _loc5_ = 14;
            _loc6_ = _loc3_ + 1;
            _loc7_ = int(var_242.length);
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc9_ = var_242[_loc8_];
               _loc10_ = _loc9_.var_244 - _loc4_.var_244;
               _loc11_ = _loc9_.var_245 - _loc4_.var_245;
               _loc12_ = Number(Math.sqrt(Number(_loc10_ * _loc10_ + _loc11_ * _loc11_)));
               if(_loc12_ < 2 * _loc5_)
               {
                  _loc13_ = (2 * _loc5_ - _loc12_) * 0.5;
                  _loc14_ = Number(Math.atan2(_loc11_,_loc10_));
                  _loc15_ = Math.cos(_loc14_) * _loc13_;
                  _loc16_ = Math.sin(_loc14_) * _loc13_;
                  _loc9_.var_244 = Number(_loc9_.var_244 + _loc15_);
                  _loc9_.var_245 = Number(_loc9_.var_245 + _loc16_);
                  _loc4_.var_244 = _loc4_.var_244 - _loc15_;
                  _loc4_.var_245 = _loc4_.var_245 - _loc16_;
               }
            }
         }
      }
      
      public function method_108() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         if(var_246.name_7 == null)
         {
            _loc1_ = method_100();
            _loc2_ = _loc1_.var_244 - var_246.var_244;
            _loc3_ = _loc1_.var_245 - var_246.var_245;
            _loc4_ = Number(Math.atan2(_loc3_,_loc2_));
            method_107(_loc4_);
         }
         else
         {
            _loc2_ = var_246.name_7.var_244 - var_246.var_244;
            _loc3_ = var_246.name_7.var_245 - var_246.var_245;
            _loc4_ = Number(Math.atan2(_loc3_,_loc2_));
            method_107(_loc4_);
            _loc5_ = Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)));
            _loc6_ = 10;
            _loc7_ = Number(class_663.method_99(-_loc6_,_loc5_ * 0.5,_loc6_));
            var_246.var_244 = Number(var_246.var_244 + Math.cos(_loc4_) * _loc7_);
            var_246.var_245 = Number(var_246.var_245 + Math.sin(_loc4_) * _loc7_);
            if(_loc5_ < 1)
            {
               var_246.name_7 = null;
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc3_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_108();
               if(var_227[0] == 0)
               {
                  method_81(true,20);
                  break;
               }
            case 2:
               method_108();
               if(var_227[0] == 0)
               {
                  method_81(true,20);
                  break;
               }
         }
         method_106();
         var_247.sort(class_742.method_109);
         var _loc1_:int = 0;
         var _loc2_:Array = var_247;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            var_232.method_110(_loc3_);
         }
         super.method_79();
      }
      
      override public function method_83() : void
      {
         var _loc4_:MovieClip = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         super.method_83();
         var _loc1_:* = method_100();
         var _loc2_:int = 0;
         var _loc3_:Array = var_240;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = _loc4_.x - _loc1_.var_244;
            _loc6_ = _loc4_.y - _loc1_.var_245;
            if(Number(Math.sqrt(Number(_loc5_ * _loc5_ + _loc6_ * _loc6_))) < 30)
            {
               method_111(_loc4_);
               return;
            }
         }
      }
      
      public function method_105(param1:class_892) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:Array = null;
         var _loc9_:class_892 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc15_:MovieClip = null;
         var _loc16_:MovieClip = null;
         if(param1.name_7 == null)
         {
            _loc2_ = 0;
            do
            {
               _loc3_ = 20;
               _loc4_ = Number(_loc3_ + Math.random() * (class_742.var_216 - 2 * _loc3_));
               _loc5_ = Number(_loc3_ + Math.random() * (class_742.var_218 - 2 * _loc3_));
               _loc6_ = true;
               _loc7_ = 0;
               _loc8_ = var_242;
               while(_loc7_ < int(_loc8_.length))
               {
                  _loc9_ = _loc8_[_loc7_];
                  _loc7_++;
                  if(_loc9_.name_7 != null)
                  {
                     _loc10_ = _loc4_ - _loc9_.name_7.var_244;
                     _loc11_ = _loc5_ - _loc9_.name_7.var_245;
                     if(Number(Math.sqrt(Number(_loc10_ * _loc10_ + _loc11_ * _loc11_))) < 100)
                     {
                        _loc6_ = false;
                        break;
                     }
                  }
               }
            }
            while(!(_loc6_ || _loc2_ > 50));
            
            param1.name_7 = {
               "L\x01":_loc4_,
               "\x03\x01":_loc5_
            };
            if(var_246 != null && Math.random() * 3 < var_237[0])
            {
               param1.name_7 = {
                  "L\x01":var_246.var_244,
                  "\x03\x01":var_246.var_245
               };
            }
         }
         _loc4_ = param1.name_7.var_244 - param1.var_244;
         _loc5_ = param1.name_7.var_245 - param1.var_245;
         _loc10_ = Number(class_663.method_112(Math.atan2(_loc5_,_loc4_) - param1.var_248,3.14));
         _loc11_ = 0.1;
         param1.var_248 = Number(Number(param1.var_248) + Number(class_663.method_99(-_loc11_,_loc10_ * 0.2,_loc11_)));
         var _loc12_:Number = 2;
         if(var_241 < 1)
         {
            _loc12_ = _loc12_ * var_241;
         }
         param1.var_244 = Number(param1.var_244 + Math.cos(Number(param1.var_248)) * _loc12_);
         param1.var_245 = Number(param1.var_245 + Math.sin(Number(param1.var_248)) * _loc12_);
         var _loc13_:Number = class_663.method_113(Number(param1.var_248),6.28) / 6.28;
         _loc15_ = param1.var_243.var_1;
         var _loc14_:MovieClip = _loc15_;
         if(_loc14_ != null)
         {
            _loc14_.gotoAndStop(1 + int(24 * _loc13_));
         }
         if(Number(Math.sqrt(Number(_loc4_ * _loc4_ + _loc5_ * _loc5_))) < 25 || int(class_691.method_114(100)) == 0)
         {
            param1.name_7 = null;
         }
         if(var_246 != null && Number(param1.method_115(var_246)) < 17 && var_220 == null)
         {
            name_3 = 2;
            param1.name_6 = 13;
            var_246.method_89();
            var_247.method_116(var_246.var_243);
            method_81(false,30);
            param1.var_243.gotoAndStop(1);
            _loc16_ = param1.var_243.var_1;
            _loc15_ = _loc16_;
            if(_loc15_ != null)
            {
               _loc15_.gotoAndPlay("eat");
            }
            var_246 = null;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         var_229 = [true];
         var_247 = [];
         var_241 = 0;
         var_249 = 2 + int(param1 * 11);
         super.method_95(param1);
         method_117();
      }
      
      public function method_111(param1:MovieClip) : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         if(var_246 == null)
         {
            return;
         }
         var_246.name_7 = {
            "L\x01":param1.x,
            "\x03\x01":param1.y
         };
         var _loc2_:int = 0;
         while(_loc2_ < 8)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("e/\x17$\x03"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc4_ = _loc5_;
            _loc4_.var_244 = Number(param1.x + (Math.random() * 2 - 1) * 20);
            _loc4_.var_245 = Number(param1.y + (Math.random() * 2 - 1) * 20);
            _loc4_.var_250 = -(0.05 + Math.random() * 0.3);
            _loc4_.var_251 = 15 + int(class_691.method_114(15));
            _loc4_.var_243.gotoAndPlay(int(class_691.method_114(9)) + 1);
         }
         param1.parent.removeChild(param1);
         var_240.method_116(param1);
      }
      
      public function method_107(param1:Number) : void
      {
         var _loc2_:Number = class_663.method_113(param1,6.28) / 6.28;
         var_246.var_243.gotoAndStop(1 + int(20 * _loc2_));
      }
      
      public function method_117() : void
      {
         var _loc1_:class_892 = null;
         var _loc3_:int = 0;
         var _loc4_:MovieClip = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Boolean = false;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:MovieClip = null;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:class_892 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("HICz\x01"),0);
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("K\x166u\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_246 = _loc1_;
         var_246.var_244 = class_742.var_216 * 0.5;
         var_246.var_245 = class_742.var_218 * 0.5;
         var_246.method_118();
         var_246.var_243.stop();
         var_246.method_119(0.8);
         var_247.push(var_246.var_243);
         class_658.var_146(var_246.var_243,2,1,4924929);
         var_240 = [];
         var _loc2_:int = 0;
         while(_loc2_ < 10)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = var_232.method_84(class_899.__unprotect__("a}4^\x02"),0);
            _loc5_ = 0;
            do
            {
               _loc6_ = 30;
               _loc7_ = Number(_loc6_ + Math.random() * (class_742.var_216 - 2 * _loc6_));
               _loc8_ = Number(_loc6_ + Math.random() * (class_742.var_218 - 2 * _loc6_));
               _loc9_ = true;
               _loc10_ = 0;
               _loc11_ = var_240;
               while(_loc10_ < int(_loc11_.length))
               {
                  _loc12_ = _loc11_[_loc10_];
                  _loc10_++;
                  _loc13_ = _loc7_ - _loc12_.x;
                  _loc14_ = _loc8_ - _loc12_.y;
                  _loc15_ = Number(Math.sqrt(Number(_loc13_ * _loc13_ + _loc14_ * _loc14_)));
                  if(_loc15_ < 50)
                  {
                     _loc9_ = false;
                     break;
                  }
               }
            }
            while(!(_loc9_ || _loc5_ > 100));
            
            _loc4_.x = _loc7_;
            _loc4_.y = _loc8_;
            var_240.push(_loc4_);
         }
         var_242 = [];
         _loc2_ = 0;
         _loc3_ = var_249;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc5_ = _loc2_;
            _loc16_ = new class_892(var_232.method_84(class_899.__unprotect__("(c)r\x01"),class_645.var_209));
            _loc16_.var_233 = 0.99;
            _loc1_ = _loc16_;
            _loc7_ = Math.random() * 6.28;
            _loc8_ = Number(200 + Math.random() * 200);
            _loc13_ = Number(class_742.var_216 * 0.5 + Math.cos(_loc7_) * _loc8_);
            _loc14_ = Number(class_742.var_218 * 0.5 + Math.sin(_loc7_) * _loc8_);
            _loc6_ = 30;
            _loc1_.var_244 = Number(class_663.method_99(_loc6_,_loc13_,class_742.var_216 - _loc6_));
            _loc1_.var_245 = Number(class_663.method_99(_loc6_,_loc14_,class_742.var_216 - _loc6_));
            _loc1_.var_252 = 0;
            _loc1_.var_248 = Number(_loc7_ + 3.14);
            _loc1_.method_119(1.2);
            _loc1_.method_118();
            _loc1_.var_243.gotoAndPlay(int(class_691.method_114(10)));
            var_247.push(_loc1_.var_243);
            var_242.push(_loc1_);
         }
      }
   }
}
