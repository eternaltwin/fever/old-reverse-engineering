package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_804 extends class_645
   {
      
      public static var var_1010:int = 12;
      
      public static var var_941:int = 20;
       
      
      public var var_796:int;
      
      public var var_66:Number;
      
      public var var_288:class_656;
      
      public var var_699:Array;
      
      public function class_804()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:MovieClip = null;
         super.method_79();
         method_720();
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = class_663.method_99(0,var_201.mouseY,class_742.var_219) - var_288.var_245;
               var_288.var_245 = Number(var_288.var_245 + _loc1_ * 0.1);
               _loc2_ = (-var_796 + 1) * 0.5 * 180;
               _loc3_ = _loc2_ - var_288.var_243.rotation;
               while(_loc3_ > 180)
               {
                  _loc3_ = _loc3_ - 360;
               }
               while(_loc3_ < -180)
               {
                  _loc3_ = Number(_loc3_ + 360);
               }
               var_288.var_243.rotation = Number(var_288.var_243.rotation + _loc3_ * 0.3);
               if(!!name_5 && Number(Math.abs(_loc3_)) < 2)
               {
                  name_3 = 2;
                  var_288.var_243.rotation = _loc2_;
                  break;
               }
               break;
            case 2:
               _loc1_ = Number(class_742.var_217 * 0.5 + (class_742.var_217 * 0.5 - class_804.var_1010) * var_796);
               _loc2_ = _loc1_ - var_288.var_244;
               _loc3_ = _loc2_ * 0.3;
               _loc4_ = 20;
               while(_loc3_ != 0)
               {
                  _loc5_ = Number(class_663.method_99(-_loc4_,_loc3_,_loc4_));
                  var_288.var_244 = Number(var_288.var_244 + _loc5_);
                  _loc3_ = _loc3_ - _loc5_;
                  method_390();
                  _loc6_ = var_232.method_84(class_899.__unprotect__("P\x02N"),class_645.var_208);
                  _loc6_.x = var_288.var_244;
                  _loc6_.y = var_288.var_245;
                  _loc6_.rotation = var_288.var_243.rotation;
               }
               if(Number(Math.abs(_loc2_)) < 3)
               {
                  var_288.var_244 = _loc1_;
                  name_3 = 1;
                  var_796 = var_796 * -1;
                  break;
               }
            case 3:
               _loc1_ = Number(class_742.var_217 * 0.5 + (class_742.var_217 * 0.5 - class_804.var_1010) * var_796);
               _loc2_ = _loc1_ - var_288.var_244;
               _loc3_ = _loc2_ * 0.3;
               _loc4_ = 20;
               while(_loc3_ != 0)
               {
                  _loc5_ = Number(class_663.method_99(-_loc4_,_loc3_,_loc4_));
                  var_288.var_244 = Number(var_288.var_244 + _loc5_);
                  _loc3_ = _loc3_ - _loc5_;
                  method_390();
                  _loc6_ = var_232.method_84(class_899.__unprotect__("P\x02N"),class_645.var_208);
                  _loc6_.x = var_288.var_244;
                  _loc6_.y = var_288.var_245;
                  _loc6_.rotation = var_288.var_243.rotation;
               }
               if(Number(Math.abs(_loc2_)) < 3)
               {
                  var_288.var_244 = _loc1_;
                  name_3 = 1;
                  var_796 = var_796 * -1;
                  break;
               }
         }
      }
      
      public function method_736(param1:Object) : void
      {
         var _loc2_:Number = Number(Number(var_66 + class_804.var_941) + 20);
         var _loc3_:Number = var_66;
         param1.var_280 = {
            "L\x01":Number(_loc2_ + Math.random() * (class_742.var_217 - 2 * _loc2_)),
            "\x03\x01":Number(_loc3_ + Math.random() * (class_742.var_219 - 2 * _loc3_))
         };
      }
      
      public function method_720() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:class_892 = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc1_:int = 0;
         var _loc2_:Array = var_699;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.method_358(_loc3_.var_280,0.1,1);
            if(Number(_loc3_.method_115(_loc3_.var_280)) < 20)
            {
               method_736(_loc3_);
            }
            _loc4_ = 0;
            _loc5_ = var_699;
            while(_loc4_ < int(_loc5_.length))
            {
               _loc6_ = _loc5_[_loc4_];
               _loc4_++;
               if(_loc3_ != _loc6_)
               {
                  _loc7_ = Number(_loc3_.method_115(_loc6_));
                  if(_loc7_ < 2 * var_66)
                  {
                     _loc8_ = 2 * var_66 - _loc7_;
                     _loc9_ = Number(_loc3_.method_231(_loc6_));
                     _loc10_ = Number(Math.cos(_loc9_));
                     _loc11_ = Number(Math.sin(_loc9_));
                     _loc3_.var_244 = _loc3_.var_244 - _loc10_ * _loc8_ * 0.5;
                     _loc3_.var_245 = _loc3_.var_245 - _loc11_ * _loc8_ * 0.5;
                     _loc6_.var_244 = Number(_loc6_.var_244 + _loc10_ * _loc8_ * 0.5);
                     _loc6_.var_245 = Number(_loc6_.var_245 + _loc11_ * _loc8_ * 0.5);
                  }
               }
            }
            _loc3_.var_243.x = _loc3_.var_244;
            _loc3_.var_243.y = _loc3_.var_245;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320 - param1 * 120];
         super.method_95(param1);
         var_66 = 32 - param1 * 20;
         if(var_66 < 12)
         {
            var_66 = 12;
         }
         var_796 = 1;
         method_117();
         method_72();
      }
      
      public function method_390() : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:class_892 = null;
         var _loc13_:class_892 = null;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc1_:* = {
            "L\x01":Number(var_288.var_244 + 8 * var_796),
            "\x03\x01":var_288.var_245
         };
         var _loc2_:Array = var_699.method_78();
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            if(Number(_loc4_.method_115(_loc1_)) < Number(var_66 + 7))
            {
               _loc5_ = int(_loc4_.var_243.scaleX * 100 / 7);
               _loc6_ = 0;
               while(_loc6_ < 2)
               {
                  _loc6_++;
                  _loc7_ = _loc6_;
                  _loc8_ = _loc7_ * (6.28 / _loc5_) * 0.5;
                  _loc9_ = 4;
                  _loc10_ = 0;
                  while(_loc10_ < _loc5_)
                  {
                     _loc10_++;
                     _loc11_ = _loc10_;
                     _loc13_ = new class_892(var_232.method_84(class_899.__unprotect__("\x10ym\'"),class_645.var_209));
                     _loc13_.var_233 = 0.99;
                     _loc12_ = _loc13_;
                     _loc14_ = Number(_loc8_ + _loc11_ / _loc5_ * 6.28);
                     _loc15_ = Number(Math.cos(_loc14_));
                     _loc16_ = Number(Math.sin(_loc14_));
                     _loc17_ = 0.8 - _loc7_ * 0.2;
                     _loc12_.var_244 = Number(_loc4_.var_244 + _loc15_ * var_66 * _loc17_);
                     _loc12_.var_245 = Number(_loc4_.var_245 + _loc16_ * var_66 * _loc17_);
                     _loc12_.var_278 = _loc15_ * _loc9_;
                     _loc12_.var_279 = _loc16_ * _loc9_;
                     _loc12_.var_233 = 0.95 - _loc7_ * 0.05;
                     _loc12_.var_251 = 16 + _loc7_ * 2 + int(class_691.method_114(8));
                     _loc12_.var_272 = 0;
                     _loc12_.var_580 = Number(24 + (_loc7_ + Number(Math.random())) * 8);
                     _loc12_.var_687 = 0.95;
                     _loc12_.method_118();
                     _loc12_.var_243.rotation = _loc14_ / 0.0174;
                  }
               }
               _loc4_.method_89();
               var_699.method_116(_loc4_);
            }
         }
         if(int(var_699.length) == 0)
         {
            method_81(true,10);
         }
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         var _loc6_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("`A(\x06"),0);
         var_699 = [];
         var _loc1_:int = 2 + int(var_237[0] * 5);
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("RJ*\x1e\x03"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc4_ = _loc5_;
            _loc6_ = 12;
            _loc4_.var_244 = Number(_loc6_ + Math.random() * (class_742.var_217 - 2 * _loc6_));
            _loc4_.var_245 = Number(_loc6_ + Math.random() * (class_742.var_219 - 2 * _loc6_));
            _loc4_.var_243.scaleX = var_66 * 0.02;
            _loc4_.var_243.scaleY = var_66 * 0.02;
            _loc4_.var_233 = 0.92;
            _loc4_.method_118();
            method_736(_loc4_);
            var_699.push(_loc4_);
         }
         var _loc7_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("!IR\x01"),class_645.var_209));
         var_288 = _loc7_;
         var_288.var_244 = class_804.var_1010;
         var_288.var_245 = class_742.var_219 * 0.5;
         var_288.method_118();
      }
   }
}
