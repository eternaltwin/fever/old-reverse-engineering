package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class DvTv extends class_645
   {
      
      public static var var_511:Number = 70;
      
      public static var var_512:Number = 200;
      
      public static var var_513:Number = 330;
      
      public static var var_514:int = 150;
      
      public static var var_515:int = 30;
      
      public static var var_516:int = 420;
      
      public static var var_517:int = 0;
      
      public static var var_518:Number = 4;
      
      public static var var_519:Number = 3;
       
      
      public var var_520:Boolean;
      
      public var var_521:Array;
      
      public var var_251:Number;
      
      public var var_351:Number;
      
      public var name_54:MovieClip;
      
      public var var_523:MovieClip;
      
      public var var_524:MovieClip;
      
      public var var_525:MovieClip;
      
      public var var_70:Number;
      
      public function DvTv()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:MovieClip = null;
         var _loc4_:Number = NaN;
         var _loc5_:* = null;
         var _loc6_:MovieClip = null;
         var _loc7_:class_892 = null;
         var_70 = Number(var_70 + 3);
         loop3:
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_330();
               _loc1_ = 0;
               _loc2_ = var_521;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  var_155(_loc3_);
                  _loc3_.scaleX = Number(1 + Math.sin((var_70 + Number(_loc3_.var_70)) * Math.PI / 180) * 0.05);
                  if(_loc3_.name_52)
                  {
                     _loc3_.y = _loc3_.y - var_351;
                     if(Number(_loc3_.y + _loc3_.height) < DvTv.var_517)
                     {
                        var_521.method_116(_loc3_);
                        _loc3_.parent.removeChild(_loc3_);
                     }
                  }
                  else
                  {
                     _loc3_.y = Number(_loc3_.y + var_351);
                     if(_loc3_.y - _loc3_.height > DvTv.var_516)
                     {
                        var_521.method_116(_loc3_);
                        _loc3_.parent.removeChild(_loc3_);
                     }
                  }
               }
               _loc4_ = var_251;
               var_251 = var_251 - 1;
               if(_loc4_ < 0 && !var_520)
               {
                  method_331();
                  _loc5_ = method_127();
                  if(_loc5_ != null)
                  {
                     _loc3_ = var_232.method_84(class_899.__unprotect__("\'\b\x17/"),3);
                     switch(int(_loc5_.var_460))
                     {
                        case 0:
                           _loc3_.x = DvTv.var_511;
                           break;
                        case 1:
                           _loc3_.x = DvTv.var_512;
                           break;
                        case 2:
                           _loc3_.x = DvTv.var_513;
                           break;
                        case 3:
                           _loc3_.x = DvTv.var_511;
                           break;
                        case 4:
                           _loc3_.x = DvTv.var_512;
                           break;
                        case 5:
                           _loc3_.x = DvTv.var_513;
                     }
                     _loc3_.name_53 = int(_loc5_.var_460);
                     if(_loc5_.name_52)
                     {
                        _loc3_.y = DvTv.var_516;
                        _loc3_.y = _loc3_.y;
                        _loc3_.name_52 = true;
                     }
                     else
                     {
                        _loc3_.rotation = 180;
                        _loc3_.y = DvTv.var_517;
                        _loc3_.y = _loc3_.y;
                        _loc3_.name_52 = false;
                     }
                     _loc3_.var_70 = int(class_691.method_114(270));
                     var_521.push(_loc3_);
                     break;
                  }
                  break;
               }
               break;
            case 2:
               _loc1_ = 0;
               _loc2_ = var_521;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc4_ = _loc3_.var_522 - var_351;
                  _loc3_.var_522 = _loc4_;
                  if(_loc4_ > -60)
                  {
                     if(_loc3_.name_52)
                     {
                        _loc3_.y = _loc3_.y - var_351 * 0.25;
                     }
                     else
                     {
                        _loc3_.y = Number(_loc3_.y + var_351 * 0.25);
                     }
                  }
                  else
                  {
                     _loc3_.scaleX = _loc3_.scaleX - var_351 * 0.005;
                     _loc3_.scaleY = _loc3_.scaleX;
                     if(_loc3_.scaleX < 0.5)
                     {
                        _loc6_ = var_232.method_84(class_899.__unprotect__("-1;(\x03"),10);
                        _loc6_.x = _loc3_.x;
                        _loc6_.y = _loc3_.y;
                        _loc7_ = new class_892(_loc6_);
                        _loc3_.parent.removeChild(_loc3_);
                        method_81(false,10);
                        name_3 = 3;
                     }
                  }
               }
               break;
            case 3:
               _loc1_ = 0;
               _loc2_ = var_521;
               while(true)
               {
                  if(_loc1_ >= int(_loc2_.length))
                  {
                     break loop3;
                  }
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc4_ = 1;
                  _loc3_.scaleY = _loc4_;
                  _loc3_.scaleX = _loc4_;
               }
         }
         super.method_79();
      }
      
      override public function method_80() : void
      {
         name_54.parent.removeChild(name_54);
         var_520 = true;
         method_81(true);
      }
      
      public function method_330() : void
      {
         if(var_520)
         {
            return;
         }
         var _loc1_:Number = Number(method_100().var_244);
         if(_loc1_ <= 130)
         {
            name_54.x = DvTv.var_511;
            return;
         }
         if(_loc1_ > 130 && _loc1_ <= class_742.var_216 - 130)
         {
            name_54.x = DvTv.var_512;
            return;
         }
         name_54.x = DvTv.var_513;
      }
      
      public function method_331() : void
      {
         var_251 = (DvTv.var_515 + int(class_691.method_114(DvTv.var_515))) * (2.5 - var_237[0] * 1.5);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_521 = [];
         method_117();
         var_251 = 10;
         name_3 = 1;
         var_351 = Number(DvTv.var_519 + param1 * 10);
         var_70 = 0;
      }
      
      public function var_155(param1:MovieClip) : void
      {
         var _loc2_:Number = NaN;
         if(param1.name_52)
         {
            if(param1.y > DvTv.var_514 && param1.y < Number(DvTv.var_514 + name_54.height))
            {
               _loc2_ = param1.y - (Number(DvTv.var_514 + name_54.height));
               switch(int(param1.name_53))
               {
                  case 0:
                     if(name_54.x != DvTv.var_511)
                     {
                        name_3 = 2;
                        param1.var_522 = _loc2_;
                        break;
                     }
                     break;
                  case 1:
                     if(name_54.x != DvTv.var_512)
                     {
                        name_3 = 2;
                        param1.var_522 = _loc2_;
                        break;
                     }
                     break;
                  case 2:
                     if(name_54.x != DvTv.var_513)
                     {
                        name_3 = 2;
                        param1.var_522 = _loc2_;
                        break;
                     }
               }
            }
            return;
         }
         if(param1.y > DvTv.var_514 && param1.y < Number(DvTv.var_514 + name_54.height))
         {
            _loc2_ = param1.y - (Number(DvTv.var_514 + name_54.height));
            switch(int(param1.name_53))
            {
               default:
               default:
               default:
                  break;
               case 3:
                  if(name_54.x != DvTv.var_511)
                  {
                     name_3 = 2;
                     param1.var_522 = _loc2_;
                     break;
                  }
                  break;
               case 4:
                  if(name_54.x != DvTv.var_512)
                  {
                     name_3 = 2;
                     param1.var_522 = _loc2_;
                     break;
                  }
                  break;
               case 5:
                  if(name_54.x != DvTv.var_513)
                  {
                     name_3 = 2;
                     param1.var_522 = _loc2_;
                     break;
                  }
            }
         }
      }
      
      public function method_127() : Object
      {
         var _loc4_:MovieClip = null;
         var _loc6_:int = 0;
         var _loc7_:Boolean = false;
         var _loc1_:Array = [true,true,true,true,true,true];
         var _loc2_:int = 0;
         var _loc3_:Array = var_521;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc1_[int(_loc4_.name_53)] = false;
            if(_loc4_.name_52)
            {
               _loc1_[int(_loc4_.name_53) + 3] = false;
            }
            else
            {
               _loc1_[int(_loc4_.name_53) - 3] = false;
            }
         }
         _loc3_ = [];
         _loc2_ = 0;
         var _loc5_:int = int(_loc1_.length);
         while(_loc2_ < _loc5_)
         {
            _loc2_++;
            _loc6_ = _loc2_;
            _loc7_ = Boolean(_loc1_[_loc6_]);
            if(_loc7_)
            {
               _loc3_.push(_loc6_);
            }
         }
         _loc2_ = int(_loc3_[int(class_691.method_114(int(_loc3_.length)))]);
         if(int(_loc3_.length) <= 0)
         {
            return null;
         }
         _loc7_ = _loc2_ < 3?true:false;
         return {
            "gz&\x01":_loc2_,
            "R%\x01":_loc7_
         };
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("K[3d\x01"),0);
         var_525 = var_232.method_84(class_899.__unprotect__("B\b3c\x03"),1);
         var_525.x = 70;
         var_524 = var_232.method_84(class_899.__unprotect__("B\b3c\x03"),1);
         var_524.x = 200;
         var_523 = var_232.method_84(class_899.__unprotect__("B\b3c\x03"),1);
         var_523.x = 330;
         name_54 = var_232.method_84(class_899.__unprotect__("\x06v\\^"),2);
         var _loc1_:Number = DvTv.var_512;
         name_54.x = _loc1_;
         name_54.x = _loc1_;
         name_54.y = DvTv.var_514;
      }
   }
}
