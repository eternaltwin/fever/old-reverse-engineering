package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import flash.display.StageQuality;
   import flash.filters.BlurFilter;
   import flash.geom.ColorTransform;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_11.package_12.class_783;
   import package_32.App;
   import package_32.class_899;
   import package_8.package_9.class_671;
   import package_8.package_9.class_931;
   
   public class class_856 extends class_645
   {
      
      public static var var_574:int = 6;
      
      public static var var_1239:Array = [[{
         "L\x01":-3,
         "\x03\x01":-6,
         "@\x01":-0.75,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":-4,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":-2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":4,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-3,
         "\x03\x01":6,
         "@\x01":0.75,
         "DaO\x01":8
      }],[{
         "L\x01":-3,
         "\x03\x01":-4,
         "@\x01":-0.75,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":-2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-3,
         "\x03\x01":4,
         "@\x01":0.75,
         "DaO\x01":8
      }],[{
         "L\x01":-3,
         "\x03\x01":-4,
         "@\x01":-0.75,
         "DaO\x01":16
      },{
         "L\x01":-5,
         "\x03\x01":-2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-3,
         "\x03\x01":4,
         "@\x01":0.75,
         "DaO\x01":16
      }],[{
         "L\x01":-5,
         "\x03\x01":-2,
         "@\x01":0,
         "DaO\x01":8
      },{
         "L\x01":-5,
         "\x03\x01":2,
         "@\x01":0,
         "DaO\x01":8
      }],[{
         "L\x01":-5,
         "\x03\x01":-2,
         "@\x01":0,
         "DaO\x01":8
      }]];
       
      
      public var var_1240:class_795;
      
      public var var_780:class_795;
      
      public var var_1158:class_783;
      
      public var var_446:class_795;
      
      public var var_1241:class_795;
      
      public var var_288:class_892;
      
      public var var_783:int;
      
      public var var_1243:int;
      
      public var var_254:Number;
      
      public var var_945:int;
      
      public var var_1244:int;
      
      public var var_1245:class_795;
      
      public var var_1242:int;
      
      public var var_913:class_795;
      
      public function class_856()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_916() : void
      {
         var _loc3_:class_656 = null;
         var _loc2_:* = var_780.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            _loc3_.var_244 = _loc3_.var_244 - class_856.var_574 * _loc3_.var_68;
            if(_loc3_.var_244 < -2)
            {
               _loc3_.var_244 = Number(_loc3_.var_244 + (100 + 2 * 2));
            }
            _loc3_.var_243.x = int(_loc3_.var_244);
            _loc3_.var_243.y = int(_loc3_.var_245);
         }
      }
      
      public function method_918() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:* = null;
         var _loc5_:class_656 = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc2_:* = var_1241.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            _loc4_ = var_913.method_73();
            while(_loc4_.method_74())
            {
               _loc5_ = _loc4_.name_1();
               _loc6_ = _loc3_.var_244 - _loc5_.var_244;
               _loc7_ = _loc3_.var_245 - _loc5_.var_245;
               if(Number(Math.abs(_loc6_)) < Number(_loc5_.var_243.width * 0.5 + 2) && Number(Math.abs(_loc7_)) < Number(_loc5_.var_243.height * 0.5 + 2))
               {
                  var_1241.method_116(_loc3_);
                  _loc3_.method_89();
                  method_917(_loc5_,1);
               }
            }
            if(_loc3_.var_244 > 110)
            {
               var_1241.method_116(_loc3_);
            }
         }
      }
      
      public function method_299() : void
      {
         var _loc1_:* = method_100();
         if(var_945 > 0)
         {
            var_945 = var_945 - 1;
         }
         if(var_1242 > 0)
         {
            var_1242 = var_1242 - 1;
         }
         if(var_945 == 0 && name_5)
         {
            method_919();
         }
         var _loc2_:Number = _loc1_.var_244 - var_288.var_244;
         var _loc3_:Number = _loc1_.var_245 - var_288.var_245;
         var _loc4_:Number = Number(class_663.method_99(1,Number(2 + _loc3_ * 0.1),3));
         var_288.var_278 = Number(var_288.var_278 + _loc2_ * 0.05);
         var _loc5_:int = int(Math.round(_loc4_));
         if(_loc5_ != var_288.var_243.currentFrame)
         {
            var_288.var_243.gotoAndStop(_loc5_);
            new class_671(var_288.var_243,class_899.__unprotect__("\'O\x01"),1,true);
            new class_671(var_288.var_243,class_899.__unprotect__(";(\x01"),1,true);
         }
         var_288.var_279 = Number(var_288.var_279 + _loc3_ * 0.05);
         var_288.var_243.x = int(var_288.var_244);
         var_288.var_243.y = int(var_288.var_245);
      }
      
      public function YAfS() : void
      {
         var _loc4_:class_656 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc3_:* = var_913.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(_loc4_.var_243.currentFrame == 1)
            {
               _loc5_ = var_237[0] * 0.4;
               _loc6_ = 0;
               if(var_288 != null)
               {
                  _loc6_ = _loc4_.var_334 - var_288.var_245;
               }
               if(_loc6_ > 0)
               {
                  _loc4_.var_334 = _loc4_.var_334 - _loc5_;
               }
               if(_loc6_ < 0)
               {
                  _loc4_.var_334 = Number(Number(_loc4_.var_334) + _loc5_);
               }
            }
            _loc4_.name_21 = (Number(_loc4_.name_21) + Number(_loc4_.name_117)) % 1;
            _loc4_.var_245 = Number(Number(_loc4_.var_334) + (0.5 + Math.cos(_loc4_.name_21 * 6.28) * 0.5) * _loc4_.var_1145);
            _loc4_.var_244 = _loc4_.var_244 - _loc4_.var_351;
            if(var_288 != null)
            {
               _loc5_ = _loc4_.var_244 - var_288.var_244;
               _loc6_ = _loc4_.var_245 - (var_288.var_245 - 1);
               if(Number(Math.abs(_loc5_)) < 6 + 8 && Number(Math.abs(_loc6_)) < 2 + 4)
               {
                  method_920(_loc4_);
                  name_118();
               }
            }
            if(_loc4_.var_244 < -20)
            {
               var_913.method_116(_loc4_);
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
            default:
               _loc1_ = var_1243;
               var_1243 = var_1243 - 1;
               if(_loc1_ < 0)
               {
                  var_1243 = var_783 + int(class_691.method_114(var_783));
                  method_688();
               }
               method_299();
               break;
            case 3:
               var_254 = Number(var_254 + 0.1);
               if(var_254 > 1)
               {
                  name_3 = 4;
                  var_1158.var_917 = new ColorTransform(1,1,1,1,5,30,5,-20);
                  break;
               }
               break;
            case 4:
               var_288.var_278 = Number(var_288.var_278 + 3.5);
               var_288.var_243.blendMode = BlendMode.ADD;
               if(!var_1158.var_259)
               {
                  var_1158.method_335(var_288.var_243);
               }
               var_288.var_243.blendMode = BlendMode.NORMAL;
         }
         YAfS();
         method_916();
         method_918();
         super.method_79();
         if(!var_1158.var_259)
         {
            var_1158.scroll(-class_856.var_574,0);
            var_1158.method_79();
         }
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__("\n\'0u\x02"),0);
         var _loc3_:* = var_446.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(_loc4_.var_243.visible != true)
            {
               var_446.method_116(_loc4_);
            }
            else
            {
               _loc2_.x = _loc4_.var_243.x;
               _loc2_.y = _loc4_.var_243.y;
               _loc5_ = Math.min(_loc4_.var_251 / _loc4_.var_273,1) * (45 - _loc4_.var_243.currentFrame * 5) * 0.01;
               _loc2_.scaleY = _loc5_;
               _loc2_.scaleX = _loc5_;
               _loc2_.blendMode = BlendMode.ADD;
               _loc2_.rotation = _loc4_.var_243.rotation;
               if(!var_1158.var_259)
               {
                  var_1158.method_335(_loc2_);
               }
            }
         }
         _loc2_.parent.removeChild(_loc2_);
      }
      
      public function method_919() : void
      {
         var _loc1_:* = null;
         var _loc2_:* = null;
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         _loc1_ = var_1240.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            if(int(_loc2_.var_945) > 0)
            {
               _loc2_.var_945 = int(_loc2_.var_945) - 1;
            }
         }
         if(name_5)
         {
            _loc3_ = 6;
            _loc1_ = var_1240.method_73();
            while(_loc1_.method_74())
            {
               _loc2_ = _loc1_.name_1();
               if(int(_loc2_.var_945) == 0)
               {
                  _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("7H\t\x13\x03"),class_645.var_209));
                  _loc5_.var_233 = 0.99;
                  _loc4_ = _loc5_;
                  _loc4_.var_244 = Number(var_288.var_244 + int(_loc2_.var_244));
                  _loc4_.var_245 = Number(var_288.var_245 + int(_loc2_.var_245));
                  _loc4_.var_278 = Math.cos(Number(_loc2_.var_65)) * _loc3_;
                  _loc4_.var_279 = Math.sin(Number(_loc2_.var_65)) * _loc3_;
                  var_1241.method_103(_loc4_);
                  if(Number(_loc2_.var_65) < -0.3)
                  {
                     _loc4_.var_243.gotoAndStop(2);
                  }
                  else if(Number(_loc2_.var_65) > 0.3)
                  {
                     _loc4_.var_243.gotoAndStop(3);
                  }
                  else
                  {
                     _loc4_.var_243.stop();
                  }
                  _loc2_.var_945 = int(_loc2_.name_119);
               }
            }
         }
      }
      
      override public function method_80() : void
      {
         var _loc2_:class_656 = null;
         method_81(true,35);
         var _loc1_:* = var_913.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            method_920(_loc2_);
         }
         var_254 = 0;
         name_3 = 3;
      }
      
      override public function method_89() : void
      {
         var_1158.method_89();
         super.method_89();
      }
      
      public function method_921() : void
      {
         var _loc3_:* = null;
         var_1240 = new class_795();
         var _loc1_:Array = class_856.var_1239[int(Math.min(var_237[0],0.99) * int(class_856.var_1239.length))];
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            var_1240.method_103({
               "L\x01":_loc3_.var_244,
               "\x03\x01":_loc3_.var_245,
               "DaO\x01":_loc3_.name_119,
               "@\x01":_loc3_.var_65,
               "\fF\x01":0
            });
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_1244 = 6;
         var_783 = 5;
         if(param1 > 0.8)
         {
            var_783 = var_783 - 1;
         }
         if(param1 > 1)
         {
            var_783 = var_783 - 1;
         }
         if(param1 > 1.3)
         {
            var_783 = var_783 - 1;
         }
         var_1243 = 40;
         var_913 = new class_795();
         var_1245 = new class_795();
         var_1241 = new class_795();
         var_446 = new class_795();
         var_1242 = 0;
         var_945 = 0;
         method_117();
         method_921();
      }
      
      public function method_922(param1:Number, param2:Number) : void
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         App.rootMc.stage.quality = StageQuality.LOW;
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__("\n\'0u\x02"),0);
         var _loc4_:int = 0;
         while(_loc4_ < 3)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc3_.x = Number(param1 + (Math.random() * 2 - 1) * 6);
            _loc3_.y = Number(param2 + (Math.random() * 2 - 1) * 6);
            _loc6_ = Number(0.5 + _loc5_ * 0.25);
            _loc3_.scaleY = _loc6_;
            _loc3_.scaleX = _loc6_;
            _loc3_.rotation = Math.random() * 360;
            _loc3_.blendMode = BlendMode.ADD;
            if(!var_1158.var_259)
            {
               var_1158.method_335(_loc3_);
            }
         }
         App.rootMc.stage.quality = StageQuality.HIGH;
         _loc3_.parent.removeChild(_loc3_);
      }
      
      public function method_920(param1:class_656) : void
      {
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:class_892 = null;
         var _loc8_:class_892 = null;
         var _loc3_:int = 0;
         while(_loc3_ < 6)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = Math.random() * 4;
            _loc6_ = (_loc4_ + Number(Math.random())) / 3 * 6.28;
            _loc8_ = new class_892(var_232.method_84(class_899.__unprotect__("u;B\f\x01"),class_645.var_209));
            _loc8_.var_233 = 0.99;
            _loc7_ = _loc8_;
            _loc7_.var_278 = Math.cos(_loc6_) * _loc5_ - class_856.var_574;
            _loc7_.var_279 = Math.sin(_loc6_) * _loc5_;
            _loc7_.var_244 = Number(param1.var_244 + _loc7_.var_278 * 3);
            _loc7_.var_245 = Number(param1.var_245 + _loc7_.var_279 * 3);
            _loc7_.var_250 = 0.1;
            _loc7_.var_251 = 20;
            _loc7_.var_243.gotoAndStop(int(class_691.method_114(4)) + 1);
            var_446.method_103(_loc7_);
            _loc7_.method_118();
            _loc7_.var_243.rotation = 90 * int(class_691.method_114(4));
            _loc7_.var_272 = 0;
         }
         method_922(param1.var_244,param1.var_245);
         param1.method_89();
         var_913.method_116(param1);
      }
      
      public function method_917(param1:class_656, param2:int = 1) : void
      {
         param1.name_120 = int(param1.name_120) - param2;
         new class_931(param1.var_243,0.1);
         if(int(param1.name_120) <= 0)
         {
            method_920(param1);
         }
      }
      
      public function name_118() : void
      {
         method_922(var_288.var_244,var_288.var_245);
         method_81(false,15);
         var_288.method_89();
         var_288 = null;
         name_3 = 2;
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:class_656 = null;
         var _loc6_:int = 0;
         var_201.scaleY = Number(4);
         var_201.scaleX = 4;
         class_319 = var_232.method_84(class_899.__unprotect__("\x04!\x03e\x03"),0);
         var_288 = new class_892(var_232.method_84(class_899.__unprotect__("+\x05P!\x02"),1));
         var_288.var_244 = 20;
         var_288.var_245 = 50;
         var_288.var_233 = 0.75;
         var_288.method_118();
         var_288.var_243.stop();
         var_780 = new class_795();
         var _loc3_:int = 0;
         while(_loc3_ < 30)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = new class_656(var_232.method_84(class_899.__unprotect__("VekP\x03"),0));
            _loc5_.var_244 = Math.random() * 100;
            _loc5_.var_245 = Math.pow(Number(Math.random()),0.3) * 100;
            _loc5_.var_68 = Number(0.1 + _loc4_ / 30 * 0.5);
            var_780.method_103(_loc5_);
            _loc6_ = 20;
            if(Number(_loc5_.var_68) < 0.4)
            {
               _loc6_ = 50;
            }
            if(Number(_loc5_.var_68) < 0.2)
            {
               _loc6_ = 100;
            }
            class_657.gfof(_loc5_.var_243,_loc6_ * 0.01,3694737);
         }
         var_1158 = new class_783(var_232.method_92(0),150,100);
         var_1158.var_917 = new ColorTransform(1,1,1,1,-20,-50,-60,-1);
         var _loc7_:BlurFilter = new BlurFilter();
         _loc7_.blurX = 2;
         _loc7_.blurY = 2;
      }
      
      public function method_688() : void
      {
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("}\x0eX,\x03"),1));
         _loc2_.var_244 = 110;
         _loc2_.var_334 = Number(4 + Math.random() * (100 - 2 * 4));
         _loc2_.var_245 = Number(_loc2_.var_334);
         _loc2_.var_351 = Number(0.75 + Math.random() * 0.75);
         _loc2_.name_21 = Number(Math.random());
         _loc2_.name_117 = Number(0.003 + Math.random() * 0.005);
         _loc2_.var_1145 = Number(8 + Math.random() * 8);
         _loc2_.name_120 = 2;
         _loc2_.var_243.gotoAndStop(1);
         if(int(class_691.method_114(3)) == 0 && var_237[0] > 0.3)
         {
            _loc2_.var_243.nextFrame();
            _loc2_.name_120 = int(_loc2_.name_120) + 1;
            _loc2_.var_1145 = 1;
            _loc2_.var_351 = Number(Number(_loc2_.var_351) + 1);
         }
         var_913.method_103(_loc2_);
      }
   }
}
