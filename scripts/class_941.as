package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_941 extends class_645
   {
       
      
      public var var_341:int;
      
      public var var_340:int;
      
      public var var_1565:Array;
      
      public var var_307:int;
      
      public var var_1563:Array;
      
      public var var_1564:Boolean;
      
      public var var_700:Number;
      
      public var var_1566:Array;
      
      public function class_941()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:MovieClip = null;
         var _loc5_:MovieClip = null;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1172();
               break;
            case 2:
               method_1172();
               var_700 = Number(var_700 + 40);
               if(var_700 > 314)
               {
                  name_3 = 1;
                  var_700 = 314;
               }
               _loc1_ = 0;
               _loc2_ = int(var_1563.length);
               while(_loc1_ < _loc2_)
               {
                  _loc1_++;
                  _loc3_ = _loc1_;
                  _loc4_ = var_1563[_loc3_];
                  _loc5_ = Reflect.field(_loc4_,class_899.__unprotect__("O\x13p)\x01"));
                  _loc5_.y = Math.sin(var_700 / 100) * var_307 * 2;
               }
               if(!!var_1564 && var_700 > 157)
               {
                  method_1173();
                  var_1564 = false;
                  break;
               }
         }
      }
      
      public function method_241() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Array = null;
         var _loc8_:* = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:* = null;
         var _loc1_:int = 1 + int(Math.round(var_237[0] * 8));
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = int(class_691.method_114(var_340));
            _loc5_ = int(class_691.method_114(var_341));
            _loc6_ = 0;
            _loc7_ = var_1566;
            while(_loc6_ < int(_loc7_.length))
            {
               _loc8_ = _loc7_[_loc6_];
               _loc6_++;
               _loc9_ = _loc4_ + int(_loc8_.var_244);
               _loc10_ = _loc5_ + int(_loc8_.var_245);
               if(method_222(_loc9_,_loc10_))
               {
                  _loc11_ = var_1565[_loc9_][_loc10_];
                  _loc11_.var_215 = int((int(_loc11_.var_215) + 2) % 3);
               }
            }
         }
      }
      
      public function name_18(param1:int, param2:int, param3:Object) : void
      {
         var _loc6_:* = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Array = var_1566;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc7_ = param1 + int(_loc6_.var_244);
            _loc8_ = param2 + int(_loc6_.var_245);
            if(method_222(_loc7_,_loc8_))
            {
               var_1565[_loc7_][_loc8_].name_152 = param3;
            }
         }
      }
      
      public function method_400(param1:int, param2:int) : void
      {
         var _loc5_:* = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         if(name_3 == 2)
         {
            return;
         }
         var_1563 = [];
         name_3 = 2;
         var_700 = 0;
         var_1564 = true;
         var _loc3_:int = 0;
         var _loc4_:Array = var_1566;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc6_ = param1 + int(_loc5_.var_244);
            _loc7_ = param2 + int(_loc5_.var_245);
            if(method_222(_loc6_,_loc7_))
            {
               _loc8_ = var_1565[_loc6_][_loc7_];
               var_1563.push(_loc8_.var_631.var_243);
               _loc8_.var_215 = int((int(_loc8_.var_215) + 1) % 3);
            }
         }
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < var_340 && param2 >= 0 && param2 < var_341;
      }
      
      public function method_1174(param1:class_656, param2:int, param3:int) : void
      {
         var var_244:int = param2;
         var var_245:int = param3;
         var_1565[var_244][var_245].var_631 = param1;
         var var_214:class_941 = this;
         param1.var_243.addEventListener(MouseEvent.CLICK,function(param1:*):void
         {
            var_214.method_400(var_244,var_245);
         });
         param1.var_243.addEventListener(MouseEvent.ROLL_OVER,function(param1:*):void
         {
            var_214.name_18(var_244,var_245,70);
         });
         param1.var_243.addEventListener(MouseEvent.ROLL_OUT,function(param1:*):void
         {
            var_214.name_18(var_244,var_245,100);
         });
         param1.var_243.addEventListener(MouseEvent.MOUSE_OUT,function(param1:*):void
         {
            var_214.name_18(var_244,var_245,100);
         });
      }
      
      public function method_1175() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var_1565 = [];
         var _loc1_:int = 0;
         var _loc2_:int = var_340;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_1565[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = var_341;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               var_1565[_loc3_].push({
                  "\x1d\x0b\x01":0,
                  "gB\x01":null,
                  "a\x01":100,
                  "/}\x01":null
               });
            }
         }
         method_241();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [360];
         super.method_95(param1);
         var_340 = 4;
         var_341 = 4;
         var_307 = 40;
         var_1566 = [{
            "L\x01":0,
            "\x03\x01":0
         },{
            "L\x01":1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":1
         },{
            "L\x01":-1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":-1
         }];
         method_1175();
         method_117();
         method_72();
      }
      
      public function method_1172() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc8_:Number = NaN;
         var _loc1_:int = 0;
         var _loc2_:int = int(var_1565.length);
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = 0;
            _loc5_ = int(var_1565[_loc3_].length);
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               _loc7_ = var_1565[_loc3_][_loc6_];
               if(_loc7_.name_152 != null)
               {
                  _loc8_ = _loc7_.name_152 - _loc7_.var_38;
                  _loc7_.var_38 = Number(Number(_loc7_.var_38) + _loc8_ * 0.2);
                  class_657.gfof(_loc7_.var_631.var_243,1 - _loc7_.var_38 * 0.01,16777215);
               }
            }
         }
      }
      
      public function method_1173() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:MovieClip = null;
         var _loc10_:Array = null;
         var _loc11_:Array = null;
         var _loc1_:Boolean = true;
         _loc2_ = 0;
         _loc3_ = int(var_1565.length);
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = int(var_1565[_loc4_].length);
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = var_1565[_loc4_][_loc7_];
               _loc9_ = Reflect.field(_loc8_.var_631.var_243,class_899.__unprotect__("O\x13p)\x01"));
               _loc9_.gotoAndStop(int(_loc8_.var_215) + 1);
               if(int(_loc8_.var_215) != 0)
               {
                  _loc1_ = false;
               }
            }
         }
         if(_loc1_)
         {
            method_81(true,50);
            _loc2_ = 0;
            _loc10_ = var_1565;
            while(_loc2_ < int(_loc10_.length))
            {
               _loc11_ = _loc10_[_loc2_];
               _loc2_++;
               _loc3_ = 0;
               while(_loc3_ < int(_loc11_.length))
               {
                  _loc8_ = _loc11_[_loc3_];
                  _loc3_++;
                  _loc8_.var_631.var_243.mouseEnabled = false;
                  _loc8_.var_631.var_243.mouseChildren = false;
               }
            }
         }
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:class_656 = null;
         var _loc10_:class_656 = null;
         var _loc11_:* = null;
         var _loc12_:MovieClip = null;
         var_232.method_84(class_899.__unprotect__("B%+J\x01"),0);
         var _loc1_:Number = class_742.var_217 * 0.5;
         var _loc2_:Number = class_742.var_219 * 0.5 - var_307 * (var_341 + var_340) * 0.125;
         var _loc3_:int = 0;
         var _loc4_:int = var_340;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = 0;
            _loc7_ = var_341;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc10_ = new class_656(var_232.method_84(class_899.__unprotect__("(rz\t\x02"),class_645.var_209));
               _loc9_ = _loc10_;
               _loc11_ = var_1565[_loc5_][_loc8_];
               _loc9_.var_244 = Number(Number(_loc1_ + _loc5_ * var_307 * 0.5) + _loc8_ * (-var_307 * 0.5));
               _loc9_.var_245 = Number(Number(_loc2_ + _loc5_ * var_307 * 0.4) + _loc8_ * var_307 * 0.4);
               _loc9_.var_243.scaleX = var_307 * 0.01;
               _loc9_.var_243.scaleY = var_307 * 0.01;
               _loc12_ = Reflect.field(_loc9_.var_243,class_899.__unprotect__("O\x13p)\x01"));
               _loc12_.gotoAndStop(int(_loc11_.var_215) + 1);
               _loc9_.method_118();
               method_1174(_loc9_,_loc5_,_loc8_);
            }
         }
      }
   }
}
