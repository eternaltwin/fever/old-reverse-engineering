package
{
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.net.SharedObject;
   import flash.net.URLRequest;
   import flash.system.System;
   import package_10.class_870;
   import package_11.class_755;
   import package_13.class_719;
   import package_13.class_765;
   import package_13.class_777;
   import package_13.class_826;
   import package_13.class_868;
   import package_24.class_760;
   import package_29.class_757;
   import package_29.class_858;
   import package_32.App;
   import package_32.class_899;
   import package_8.package_9.class_938;
   import package_9.class_648;
   import package_9.class_737;
   import package_9.class_752;
   
   public class class_652
   {
      
      public static var var_297:int = 2;
      
      public static var var_287:int = 0;
      
      public static var var_214:class_652;
       
      
      public var name_14:Boolean;
      
      public var var_251:int;
      
      public var name_3:class_651;
      
      public var var_306:Array;
      
      public var var_314:SharedObject;
      
      public var var_298:Object;
      
      public var var_109:class_760;
      
      public var var_243:Sprite;
      
      public var var_318:Array;
      
      public var var_319:Array;
      
      public var package_29:class_858;
      
      public var var_296:Object;
      
      public var var_321:class_826;
      
      public var var_301:class_765;
      
      public var var_267:class_719;
      
      public var var_304:class_868;
      
      public var var_288:class_777;
      
      public var var_305:class_938;
      
      public var var_232:class_755;
      
      public var var_300:Boolean;
      
      public var var_324:Number;
      
      public var var_303:Function;
      
      public function class_652()
      {
         if(class_899.var_239)
         {
            return;
         }
         class_870.name_18(16777215);
         class_652.var_214 = this;
         var_243 = new Sprite();
         var_232 = new class_755(var_243);
         var_305 = new class_938();
         var_251 = 0;
         class_905.var_243.addChild(var_243);
         var_243.visible = false;
         var_243.scaleY = Number(2);
         var_243.scaleX = 2;
         var_243.addEventListener(Event.ENTER_FRAME,method_188);
         var_314 = SharedObject.getLocal("fever");
         if(var_314.data.var_296 != null)
         {
            var_296 = var_314.data.var_296;
         }
         else
         {
            var_296 = {",\rI`\x02":0};
            var_314.data.var_296 = var_296;
            var_314.flush();
         }
         var_300 = false;
         var_301 = new class_765(class_905.var_329);
         var_301.method_209();
         name_3 = class_651.var_293;
      }
      
      public function method_166() : void
      {
         if(var_298 == null)
         {
            return;
         }
         var_298 = var_298 - 1;
      }
      
      public function method_170() : void
      {
         var _loc3_:* = null as package_24.class_656;
         var _loc1_:Array = package_24.class_656.var_299.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.method_79();
         }
         method_167();
         var_267.method_79();
         var_288.method_79();
         method_168();
         var_267.method_169();
      }
      
      public function method_167() : void
      {
         if(!var_300)
         {
            return;
         }
         if(var_267 == null)
         {
            return;
         }
         var _loc1_:int = int(var_267.mouseX / 16);
         var _loc2_:int = int(var_267.mouseY / 16);
         var_267.method_171(_loc1_,_loc2_);
      }
      
      public function method_79(param1:* = undefined) : void
      {
         var_251 = var_251 + 1;
         switch(int(name_3.var_295))
         {
            case 0:
               var_301.method_79();
               if(var_301.var_302)
               {
                  var_301.method_172();
                  method_173();
                  break;
               }
               break;
            case 1:
               if(var_303 != null)
               {
                  var_303();
               }
               var_304.method_79();
               method_166();
               if(var_303 != null)
               {
                  var_109.method_79();
                  break;
               }
         }
         var_305.method_79();
      }
      
      public function method_174() : void
      {
         package_24.class_656.var_299 = package_24.class_656.var_299.concat(var_306);
      }
      
      public function method_177() : void
      {
         var _loc10_:* = null;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:* = null;
         var _loc18_:_Reward = null;
         var _loc19_:Array = null;
         var _loc20_:_GameBonus = null;
         var _loc21_:_IslandBonus = null;
         var _loc22_:int = 0;
         var _loc23_:int = 0;
         var _loc25_:String = null;
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = class_911.var_214.var_307 * class_911.var_214.var_307;
         var _loc7_:Array = [];
         var _loc8_:int = 0;
         var _loc9_:Array = class_885.var_308._monsters;
         while(_loc8_ < int(_loc9_.length))
         {
            _loc10_ = _loc9_[_loc8_];
            _loc8_++;
            _loc7_.push(0);
         }
         _loc9_ = [0,0,0];
         var _loc11_:Array = [0,0,0];
         _loc10_ = {
            "L\x01":0,
            "\x03\x01":0
         };
         _loc8_ = 0;
         _loc12_ = class_911.var_214.var_307;
         while(_loc8_ < _loc12_)
         {
            _loc8_++;
            _loc13_ = _loc8_;
            _loc14_ = 0;
            _loc15_ = class_911.var_214.var_307;
            while(_loc14_ < _loc15_)
            {
               _loc14_++;
               _loc16_ = _loc14_;
               _loc17_ = class_911.var_214.method_175(_loc13_,_loc16_);
               _loc18_ = _loc17_.var_309;
               if(_loc18_ != null)
               {
                  _loc5_++;
                  if(class_830.method_176(_loc18_))
                  {
                     _loc3_++;
                  }
                  _loc19_ = _loc18_.var_296;
                  switch(int(_loc18_.var_295))
                  {
                     default:
                        break;
                     case 1:
                        _loc21_ = _loc19_[0];
                        _loc11_[int(_loc21_.var_295)] = _loc11_[int(_loc21_.var_295)] + 1;
                        break;
                     case 2:
                     default:
                     default:
                        _loc2_++;
                        break;
                     case 5:
                        _loc10_ = {
                           "L\x01":_loc13_,
                           "\x03\x01":_loc16_
                        };
                        break;
                     case 6:
                        _loc1_++;
                        break;
                     case 7:
                        _loc4_++;
                        break;
                     case 8:
                        _loc20_ = _loc19_[0];
                        _loc9_[int(_loc20_.var_295)] = _loc9_[int(_loc20_.var_295)] + 1;
                  }
                  _loc22_ = 0;
                  _loc19_ = _loc17_.name_15.var_242;
                  while(_loc22_ < int(_loc19_.length))
                  {
                     _loc23_ = int(_loc19_[_loc22_]);
                     _loc22_++;
                     _loc7_[_loc23_] = _loc7_[_loc23_] + 1;
                  }
               }
            }
         }
         var _loc24_:String = "rewardTotal : " + _loc5_ + "(" + int(_loc5_ / _loc6_ * 100) + "%)\n";
         _loc24_ = _loc24_ + ("chest : " + _loc3_ + "(" + int(_loc3_ / _loc6_ * 100) + "%) \tkeyCheck :" + _loc4_ + "\n");
         _loc24_ = _loc24_ + ("extra : " + (_loc1_ * class_885.var_310 + _loc2_ * class_885.var_311) + " cubes - repartition : " + (_loc1_ + _loc2_) * 100 / _loc6_ + "%\n");
         _loc24_ = _loc24_ + "\n";
         _loc24_ = _loc24_ + "bonus island :\n";
         _loc8_ = 0;
         while(_loc8_ < 3)
         {
            _loc8_++;
            _loc12_ = _loc8_;
            _loc24_ = _loc24_ + (class_808.var_312[_loc12_] + " : " + int(_loc11_[_loc12_]) + "\n");
         }
         _loc24_ = _loc24_ + "\n";
         _loc24_ = _loc24_ + "bonus game :\n";
         _loc8_ = 0;
         while(_loc8_ < 3)
         {
            _loc8_++;
            _loc12_ = _loc8_;
            _loc24_ = _loc24_ + (class_808.var_313[_loc12_] + " : " + int(_loc9_[_loc12_]) + "\n");
         }
         _loc24_ = _loc24_ + "\n";
         _loc8_ = 0;
         _loc12_ = 0;
         _loc13_ = 0;
         while(_loc13_ < int(_loc7_.length))
         {
            _loc14_ = int(_loc7_[_loc13_]);
            _loc13_++;
            _loc12_ = _loc12_ + _loc14_;
         }
         _loc24_ = _loc24_ + ("monsters : " + _loc12_ + "\n");
         _loc13_ = 0;
         while(_loc13_ < int(_loc7_.length))
         {
            _loc14_ = int(_loc7_[_loc13_]);
            _loc13_++;
            _loc25_ = class_885.var_308._monsters[_loc8_]._name;
            while(_loc25_.length < 18)
            {
               _loc25_ = _loc25_ + " ";
            }
            _loc24_ = _loc24_ + (_loc25_ + " : " + _loc14_ + " \t(" + int(_loc14_ * 1000 / _loc12_) / 10 + "%)\n");
            _loc8_++;
         }
         _loc24_ = _loc24_ + ("portal is : " + int(_loc10_.var_244) + "," + int(_loc10_.var_245) + "\n");
         System.setClipboard(_loc24_);
      }
      
      public function method_178() : void
      {
         var _loc6_:* = null as package_24.class_656;
         var _loc1_:Sprite = new Sprite();
         var_232.method_124(_loc1_,20);
         _loc1_.graphics.beginFill(0,0.8);
         _loc1_.graphics.drawRect(0,0,class_742.var_216,class_742.var_218);
         var _loc2_:int = 20;
         var _loc3_:int = 20;
         var _loc4_:int = 0;
         var _loc5_:Array = package_24.class_656.var_299;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc1_.addChild(_loc6_);
            _loc6_.x = _loc2_;
            _loc6_.y = _loc3_;
            _loc2_ = _loc2_ + 10;
            if(_loc2_ > 380)
            {
               _loc2_ = 0;
               _loc3_ = _loc3_ + 10;
            }
         }
         var_303 = null;
         var_109.method_79();
      }
      
      public function method_135(param1:Boolean) : void
      {
         if(var_300 == param1)
         {
            return;
         }
         var_300 = param1;
         if(var_300)
         {
            App.rootMc.stage.addEventListener(MouseEvent.CLICK,name_5);
         }
         else
         {
            App.rootMc.stage.removeEventListener(MouseEvent.CLICK,name_5);
         }
         if(var_300)
         {
            var_267.method_179(var_288.var_289);
            var_288.var_289.method_180();
         }
         else
         {
            var_267.var_315.visible = false;
            var_288.method_181();
         }
      }
      
      public function method_182() : Boolean
      {
         return var_298 == null;
      }
      
      public function method_186(param1:_PlayerAction) : void
      {
         var_298 = 10000;
         var_304.method_183();
         var _loc2_:* = {"_action":param1};
         MtCodec.apiAction(class_905.var_316 + "/game/action",_loc2_,SROE);
         class_765.var_214.method_185(param1,var_267);
      }
      
      public function method_188(param1:*) : void
      {
         var _loc3_:Error = null;
         try
         {
            method_79();
            return;
         }
         catch(:Error;)
         {
            _loc3_ = ;
            if(_loc3_ as Error)
            {
               class_899.var_317 = _loc3_;
            }
            class_905.method_187(class_691.method_97(_loc3_));

         }
      }
      
      public function method_189() : void
      {
         var_314.data.var_296 = var_296;
         var_314.flush();
      }
      
      public function method_168() : void
      {
         var _loc1_:Point = var_288.var_243.localToGlobal(new Point(0,0));
         _loc1_ = var_267.globalToLocal(_loc1_);
         var _loc2_:Number = class_742.var_216 * 0.5;
         var _loc3_:Number = Number(var_304.method_190());
         var_267.x = _loc2_ * 0.5 - _loc1_.x;
         var_267.y = class_868.var_320 + _loc3_ * 0.5 - _loc1_.y;
         var_267.x = int(var_267.x);
         var_267.y = int(var_267.y);
      }
      
      public function method_192() : void
      {
         method_174();
         var_109.visible = true;
         var_303 = method_170;
         var_304.method_191(false);
      }
      
      public function method_196(param1:class_826) : void
      {
         var_321 = param1;
         method_193();
         package_29 = new class_858(param1.method_194(),int(param1.var_322[0]));
         new class_752(package_29);
         var_303 = null;
         method_135(false);
         method_186(_PlayerAction._Play(var_321.var_215));
         var_304.method_195();
         var_109.method_79();
      }
      
      public function method_198() : void
      {
         method_193();
         var_109.visible = false;
         var_303 = null;
         var_304.method_191(true);
         var _loc1_:class_757 = new class_757(class_765.var_214.package_31._carts);
         _loc1_.method_197();
         App.rootMc.addChild(_loc1_);
      }
      
      public function method_173() : void
      {
         var _loc5_:int = 0;
         var _loc6_:class_826 = null;
         name_3 = class_651.var_292;
         var_109 = new class_760(var_243,class_742.var_216,class_742.var_323,2);
         class_905.var_243.addChild(var_109);
         var _loc1_:* = var_301.package_31;
         var _loc2_:* = _loc1_._road[0];
         var_319 = [int(_loc2_._x)];
         var_318 = [int(_loc2_._y)];
         var_267 = new class_719(var_319[0],var_318[0]);
         var_267.method_142();
         var_232.method_124(var_267,class_652.var_287);
         var _loc3_:* = {
            "L\x01":int(class_719.var_268 * 0.5),
            "\x03\x01":int(class_719.var_269 * 0.5)
         };
         var _loc4_:* = class_765.var_214.package_31._road[1];
         if(_loc4_ != null)
         {
            _loc5_ = int(class_742.method_199(var_267.var_319,var_267.var_318,int(_loc4_._x),int(_loc4_._y)));
            _loc6_ = var_267.method_144(_loc5_);
            if(_loc6_ != null)
            {
               _loc3_ = {
                  "L\x01":_loc6_.var_244,
                  "\x03\x01":_loc6_.var_245
               };
            }
         }
         var_304 = new class_868();
         var_304.method_132(var_267.method_131());
         var_288 = new class_777(var_267,var_267.method_98(int(_loc3_.var_244),int(_loc3_.var_245)));
         var_303 = method_170;
         method_135(true);
      }
      
      public function method_200(param1:String = undefined) : void
      {
         if(param1 == null)
         {
            param1 = "chrono step!";
         }
         var _loc2_:Number = Number(Date.name_16().getTime());
         class_870.name_17(param1 + " : " + (_loc2_ - var_324),{
            "\x06\fi2\x03":"World.hx",
            "d\x0f^S\x03":136,
            "v2z\x01":"World",
            "d\x19o\f\x03":"flag"
         });
         var_324 = _loc2_;
      }
      
      public function SROE(param1:Object) : void
      {
         var _loc2_:URLRequest = null;
         if(param1._done != "ok")
         {
            class_905.method_187(class_808.var_325);
            return;
         }
         if(param1._url != null)
         {
            _loc2_ = new URLRequest(param1._url);
            App.method_201(_loc2_,"_self");
            return;
         }
         var_298 = null;
         var_304.method_202();
      }
      
      public function method_204() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc1_:String = "[";
         var _loc2_:int = 0;
         var _loc3_:int = class_911.var_214.var_307;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = class_911.var_214.var_307;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = class_911.var_214.method_175(_loc4_,_loc7_);
               _loc1_ = _loc1_ + (class_911.var_214.method_203(_loc8_) + ",");
            }
         }
         System.setClipboard(_loc1_);
      }
      
      public function name_5(param1:*) : void
      {
         var _loc2_:* = null;
         if(var_288.var_326 != null && Boolean(method_182()))
         {
            if(int(var_288.method_205()) == var_288.var_326 && var_243.mouseY < class_742.var_218 * 0.5)
            {
               _loc2_ = var_267.method_141(var_288.var_326);
               class_652.var_214.method_186(_PlayerAction._MoveTo(int(_loc2_.var_244),int(_loc2_.var_245)));
               class_868.var_214.method_195();
               new class_648(var_288.var_326);
               var_288.method_181();
               return;
            }
         }
         var _loc3_:class_826 = var_267.var_327;
         if(!var_267.var_315.visible)
         {
            return;
         }
         var_288.method_133(_loc3_);
      }
      
      public function method_206() : void
      {
         var_324 = Number(Date.name_16().getTime());
      }
      
      public function method_193() : void
      {
         var_306 = package_24.class_656.var_299.method_78();
         package_24.class_656.var_299 = [];
      }
      
      public function method_208() : void
      {
         var_303 = method_170;
         package_29.method_89();
         method_174();
         switch(package_29.var_328)
         {
            case 0:
               if(name_14)
               {
                  method_135(true);
                  var_321.package_19.method_207();
                  var_267.method_179(var_288.var_289);
                  break;
               }
               method_135(true);
               var_267.method_179(var_288.var_289);
               break;
            case 1:
               new class_737();
               break;
            case 2:
               class_652.var_214.method_135(true);
               var_321.package_19.method_207("_stone");
               var_267.method_179(var_288.var_289);
         }
         package_29 = null;
      }
   }
}
