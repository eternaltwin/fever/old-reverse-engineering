package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import package_32.class_899;
   
   public class class_834 extends class_645
   {
       
      
      public var var_1134:int;
      
      public var var_351:int;
      
      public var var_1133:Boolean;
      
      public var var_1132:int;
      
      public var name_78:class_795;
      
      public var var_462:int;
      
      public var var_1128:MovieClip;
      
      public var var_762:Boolean;
      
      public var var_1131:int;
      
      public var var_1130:MovieClip;
      
      public var var_1129:MovieClip;
      
      public var var_1127:class_795;
      
      public var var_816:int;
      
      public function class_834()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_818() : void
      {
         var _loc3_:class_892 = null;
         var _loc5_:Sprite = null;
         var _loc1_:Array = [];
         var _loc2_:* = var_1127.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            _loc1_.push(_loc3_.var_243);
         }
         _loc1_.push(var_1128);
         _loc1_.sort(method_817);
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc1_.length))
         {
            _loc5_ = _loc1_[_loc4_];
            _loc4_++;
            var_232.method_110(_loc5_);
         }
      }
      
      public function method_817(param1:Sprite, param2:Sprite) : int
      {
         if(param1.x > param2.x)
         {
            return 1;
         }
         return -1;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:* = null;
         var _loc4_:class_892 = null;
         var _loc5_:MovieClip = null;
         var _loc6_:MovieClip = null;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc9_:MovieClip = null;
         var _loc10_:class_892 = null;
         var _loc11_:int = 0;
         if(!var_762)
         {
            _loc1_ = Number(method_100().var_244);
            _loc2_ = (var_1128.x - _loc1_) * 0.7;
            var_1129.x = var_1129.x - _loc2_;
            var_1130.x = Number(var_1130.x + _loc2_);
            var_1128.x = Number(method_100().var_244);
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc3_ = var_1127.method_73();
               while(_loc3_.method_74())
               {
                  _loc4_ = _loc3_.name_1();
                  if(Number(_loc4_.var_243.x + _loc4_.var_243.width) < -2)
                  {
                     _loc6_ = _loc4_.var_243.var_1;
                     _loc5_ = _loc6_;
                     if(_loc5_ != null)
                     {
                        _loc6_ = _loc5_.var_1;
                        _loc5_ = _loc6_;
                     }
                     if(_loc5_ != null && _loc5_.currentFrame == var_1131)
                     {
                        method_81(false,20);
                     }
                     _loc4_.method_89();
                     var_1127.method_116(_loc4_);
                  }
               }
               _loc7_ = var_462;
               var_462 = var_462 - 1;
               if(_loc7_ < 0)
               {
                  _loc5_ = var_232.method_84(class_899.__unprotect__("/D*K\x03"),5);
                  _loc5_.x = class_742.var_216;
                  _loc5_.y = 376 + int(class_691.method_114(5)) * (int(class_691.method_114(2)) == 0?1:-1);
                  _loc5_.gotoAndStop(1);
                  _loc9_ = _loc5_.var_1;
                  _loc8_ = _loc9_.var_1;
                  _loc6_ = _loc8_;
                  _loc7_ = int(class_691.method_114(2)) == 0?var_1131:int(class_691.method_114(_loc6_.totalFrames)) + 1;
                  _loc6_.gotoAndStop(_loc7_);
                  _loc4_ = new class_892(_loc5_);
                  _loc4_.var_278 = var_351;
                  var_1127.method_124(_loc4_);
                  var_462 = var_816;
                  if(int(class_691.method_114(5)) == 0)
                  {
                     _loc8_ = var_232.method_84(class_899.__unprotect__("ic`\x10"),3);
                     _loc8_.gotoAndStop(int(class_691.method_114(_loc8_.totalFrames)) + 1);
                     _loc8_.x = Number(Number(_loc5_.x + _loc5_.width) + 15);
                     _loc8_.y = 366;
                     _loc10_ = new class_892(_loc8_);
                     _loc10_.var_278 = var_351;
                     name_78.method_124(_loc10_);
                     var_1132 = int(class_691.method_114(5));
                  }
               }
               if(var_762)
               {
                  _loc7_ = var_1128.currentFrame;
                  if(!var_1133)
                  {
                     var_1128.gotoAndStop(_loc7_ + 4);
                  }
                  _loc1_ = var_1128.x - 45;
                  if(_loc7_ >= var_1128.totalFrames)
                  {
                     if(!var_1133)
                     {
                        var_1133 = true;
                        _loc3_ = var_1127.method_73();
                        while(true)
                        {
                           if(_loc3_.method_74())
                           {
                              _loc4_ = _loc3_.name_1();
                              if(Number(_loc4_.var_244 + _loc4_.var_243.width) >= _loc1_ && Number(_loc4_.var_244 + _loc4_.var_243.width) < Number(var_1128.x + 70))
                              {
                                 _loc6_ = _loc4_.var_243.var_1;
                                 _loc5_ = _loc6_;
                                 if(_loc5_ != null)
                                 {
                                    _loc6_ = _loc5_.var_1;
                                    _loc5_ = _loc6_;
                                 }
                                 if(_loc5_ != null && _loc5_.currentFrame != var_1131 && _loc4_.var_243.currentFrame == 1)
                                 {
                                    method_81(false);
                                 }
                                 _loc4_.var_243.gotoAndStop(2);
                              }
                              method_819(var_1128.x);

                           }
                        }
                     }
                  }
                  else
                  {
                     _loc3_ = var_1127.method_73();
                     while(_loc3_.method_74())
                     {
                        _loc4_ = _loc3_.name_1();
                        if(_loc4_.var_244 >= Number(var_1128.x + 17))
                        {
                        }
                     }
                  }
                  if(var_1133)
                  {
                     _loc11_ = var_1134;
                     var_1134 = var_1134 - 1;
                     if(_loc11_ <= 0)
                     {
                        var_1128.gotoAndStop(_loc7_ - 6);
                        if(_loc7_ <= 6)
                        {
                           var_1134 = 2;
                           var_1133 = false;
                           var_762 = false;
                           var_1128.gotoAndStop(1);
                           break;
                        }
                        break;
                     }
                     break;
                  }
                  break;
               }
         }
         method_818();
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      override public function method_83() : void
      {
         if(var_762)
         {
            return;
         }
         var_762 = true;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_762 = false;
         var_1133 = false;
         var_351 = -3 - int(Math.ceil(param1 * 6));
         var _loc2_:int = 35 - int(Math.ceil(param1 * 20));
         var_816 = _loc2_;
         _loc2_ = _loc2_;
         var_462 = _loc2_;
         var_1132 = _loc2_;
         name_78 = new class_795();
         var_1127 = new class_795();
         method_117();
         var_1134 = 2;
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("dG%m\x02"),1);
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("\x0e-I\x14\x03"),3);
         var _loc4_:MovieClip = _loc1_.var_1;
         var _loc3_:MovieClip = _loc4_.var_1;
         var _loc2_:MovieClip = _loc3_;
         var_1131 = int(class_691.method_114(_loc2_.totalFrames)) + 1;
         _loc2_.gotoAndStop(var_1131);
         var_1129 = var_232.method_84(class_899.__unprotect__("IZ\'!\x02"),5);
         var_1129.alpha = 0.8;
         var_1128 = var_232.method_84(class_899.__unprotect__("ZML\x19\x03"),5);
         var_1130 = var_232.method_84(class_899.__unprotect__("IZ\'!\x02"),5);
         var_1129.scaleY = 0.6;
         var_1129.x = 0;
         var_1130.x = 400;
         var_1129.y = 3;
         var_1130.y = 12;
         var_1128.gotoAndStop(1);
         _loc3_ = var_232.method_84(class_899.__unprotect__("/t\\9\x02"),3);
         _loc3_.y = 370;
      }
      
      public function method_819(param1:Number) : void
      {
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__(";`Y\x0f\x03"),3);
         _loc2_.x = param1;
         _loc2_.y = 365;
         var _loc3_:class_892 = new class_892(_loc2_);
         _loc3_.var_278 = var_351;
         name_78.method_124(_loc3_);
         method_102(5);
      }
   }
}
