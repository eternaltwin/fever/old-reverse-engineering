package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_748 extends class_645
   {
      
      public static var var_216:int = 240;
      
      public static var var_218:int = 240;
      
      public static var var_765:int = 11;
      
      public static var var_350:int = 236;
       
      
      public var var_766:Object;
      
      public var var_752:Array;
      
      public var var_767:Object;
      
      public var var_288:class_892;
      
      public function class_748()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:MovieClip = null;
         method_497();
         if(var_766 != null)
         {
            var_288.var_244 = Number(var_288.var_244 + Number(var_752[var_766].var_351));
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(name_5)
               {
                  var_288.var_243.gotoAndPlay("prepare");
                  name_3 = 2;
                  break;
               }
               break;
            case 2:
               if(!name_5)
               {
                  var_766 = null;
                  var_288.var_250 = 0.5;
                  var_288.var_279 = -class_748.var_765;
                  var_288.var_243.gotoAndPlay("jump");
                  name_3 = 3;
                  break;
               }
               break;
            case 3:
               if(var_288.var_279 > 0)
               {
                  var_767 = null;
                  _loc1_ = 0;
                  _loc2_ = 0;
                  _loc3_ = var_752;
                  while(_loc2_ < int(_loc3_.length))
                  {
                     _loc4_ = _loc3_[_loc2_];
                     _loc2_++;
                     if(var_288.var_245 < _loc4_.y)
                     {
                        var_767 = _loc1_;
                        break;
                     }
                     _loc1_++;
                  }
                  var_288.var_243.gotoAndPlay("jump_end");
                  name_3 = 4;
                  break;
               }
               break;
            case 4:
               if(var_767 != null)
               {
                  _loc4_ = var_752[var_767];
                  if(var_288.var_245 > _loc4_.y)
                  {
                     if(Number(Math.abs(var_288.var_244 - _loc4_.x)) < Number(_loc4_.var_66))
                     {
                        var_766 = var_767;
                        method_498(_loc4_.y);
                        if(var_767 == 0)
                        {
                           name_3 = 5;
                           var_288.var_243.gotoAndPlay("win");
                           _loc4_.gotoAndStop(1);
                           method_81(true,34);
                           break;
                        }
                        break;
                     }
                     var_767 = var_767 + 1;
                     if(var_767 == int(var_752.length))
                     {
                        var_767 = null;
                        break;
                     }
                     break;
                  }
                  break;
               }
               if(var_288.var_245 > class_748.var_350)
               {
                  method_498(class_748.var_350);
                  break;
               }
               break;
         }
         super.method_79();
      }
      
      public function method_497() : void
      {
         var _loc3_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_752;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.x = Number(_loc3_.x + Number(_loc3_.var_351));
            if(_loc3_.x < Number(_loc3_.var_66) || _loc3_.x > class_748.var_216 - _loc3_.var_66)
            {
               _loc3_.x = Number(class_663.method_99(Number(_loc3_.var_66),_loc3_.x,class_748.var_216 - _loc3_.var_66));
               _loc3_.var_351 = _loc3_.var_351 * -1;
            }
         }
      }
      
      public function method_498(param1:Number) : void
      {
         var_288.var_250 = null;
         var_288.var_245 = param1;
         var_288.var_279 = 0;
         var_288.var_243.gotoAndPlay("land");
         name_3 = 1;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [280];
         super.method_95(param1);
         method_117();
         method_72();
         class_319 = var_232.method_84(class_899.__unprotect__("y=\x17[\x01"),0);
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc2_:Number = class_748.var_218 / (3 + 1);
         var _loc3_:Array = class_742.method_379(3);
         var_752 = [];
         var _loc4_:int = 0;
         while(_loc4_ < 3)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = var_232.method_84(class_899.__unprotect__("\fafk"),class_645.var_209);
            _loc6_.var_66 = 60 - var_237[0] * 40;
            if(Number(_loc6_.var_66) < 10)
            {
               _loc6_.var_66 = 10;
            }
            _loc6_.var_351 = Number(1.5 + _loc3_[_loc5_] * 3);
            _loc6_.x = Number(Number(_loc6_.var_66) + Math.random() * (class_748.var_216 - 2 * _loc6_.var_66));
            _loc6_.y = (_loc5_ + 1) * _loc2_;
            _loc6_.var_2.scaleX = _loc6_.var_66 * 0.02;
            _loc6_.var_13.x = -_loc6_.var_66;
            _loc6_.var_14.x = Number(_loc6_.var_66);
            if(_loc5_ == 0)
            {
               _loc6_.gotoAndStop(2);
            }
            else
            {
               _loc6_.gotoAndStop(1);
            }
            var_752.push(_loc6_);
         }
         var _loc7_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("hG^A\x03"),class_645.var_209));
         _loc7_.var_233 = 0.99;
         var_288 = _loc7_;
         var_288.var_233 = 0.96;
         var_288.var_244 = class_748.var_216 * 0.5;
         var_288.var_245 = class_748.var_350;
         var_288.method_118();
      }
   }
}
