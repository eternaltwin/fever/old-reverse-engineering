package {
/**
 * JPEXS name: `class_894`
 */
public class Reflect {
  public function Reflect() {
  }

  public static function method_679(param1:*, param2:String):Boolean {
    return param1.hasOwnProperty(param2);
  }

  /**
   * JPEXS name: `GLWA`
   */
  public static function field(param1:*, param2:String):* {
    try {
      return param1[param2];
    } catch (_loc4_:*) {
      return null;
    }
    return; §§pop();
  }

  public static function method_1041(param1:*, param2:String, param3:*):void {
    param1[param2] = param3;
  }

  public static function method_1042(param1:*, param2:*, param3:Array):* {
    return param2.apply(param1, param3);
  }

  public static function method_649(param1:*):Array {
    if (param1 == null) {
      return [];
    }
    var _loc4_:int = 0;
    var _loc3_:Array = [];
    var _loc5_:* = param1;
    while (§§hasnext(param1, _loc4_);
  )
    {
      _loc3_.push(;§§nextname(_loc4_, _loc5_);
    )

    }
    var _loc2_:Array = _loc3_;
    _loc4_ = 0;
    while (_loc4_ < int(_loc2_.length)) {
      if (!param1.hasOwnProperty(_loc2_[_loc4_])) {
        _loc2_.splice(_loc4_, 1);
      } else {
        _loc4_++;
      }
    }
    return _loc2_;
  }

  public static function method_739(param1:*):Boolean {
    return typeof param1 == "function";
  }

  public static function method_461(param1:Object, param2:Object):int {
    var _loc3_:* = param1;
    var _loc4_:* = param2;
    return _loc3_ == _loc4_ ? 0 : _loc3_ > _loc4_ ? 1 : -1;
  }

  public static function method_1043(param1:*, param2:*):Boolean {
    return param1 == param2;
  }

  public static function method_1044(param1:*):Boolean {
    if (param1 == null) {
      return false;
    }
    var _loc3_:String = typeof param1;
    if (_loc3_ == "object") {
      try {
        if (param1.__enum__ == true) {
          return false;
        }
      } catch (_loc4_:*) {
      }
      return true;
    }
    return _loc3_ == "string";
  }

  public static function method_740(param1:*, param2:String):Boolean {
    if (param1.hasOwnProperty(param2) != true) {
      return false;
    }
    delete param1[param2];
    return true;
  }

  public static function method_78(param1:Object):Object {
    var _loc5_:String = null;
    var _loc2_:* = {};
    var _loc3_:int = 0;
    var _loc4_:Array = Reflect.method_649(param1);
    while (_loc3_ < int(_loc4_.length)) {
      _loc5_ = _loc4_[_loc3_];
      _loc3_++;
      _loc2_[_loc5_] = Reflect.field(param1, _loc5_);
    }
    return _loc2_;
  }

  public static function method_1045(param1:Function):* {
    var var_27:Function = param1;
    return function (...rest):* {
      return var_27(rest);
    };
  }
}
}
