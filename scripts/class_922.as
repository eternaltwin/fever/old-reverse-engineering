package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_922 extends class_645
   {
       
      
      public var var_1516:Array;
      
      public var var_1514:class_656;
      
      public var var_1515:Number;
      
      public var var_1513:class_656;
      
      public var var_800:class_755;
      
      public var var_824:int;
      
      public var var_579:Object;
      
      public var var_1512:Boolean;
      
      public var var_638:int;
      
      public function class_922()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:MovieClip = null;
         var _loc12_:Array = null;
         var _loc13_:* = null;
         var _loc14_:class_9 = null;
         var _loc15_:Number = NaN;
         super.method_79();
         if(!var_1512)
         {
            method_375();
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_1513.method_229({
                  "L\x01":class_742.var_217 * 0.5,
                  "\x03\x01":class_742.var_219 * 0.5
               },0.2,null);
               var_1514.method_229(method_100(),0.5,null);
               _loc1_ = var_1514.method_115(var_579) * 0.1;
               _loc2_ = int(Math.round(_loc1_));
               _loc3_ = var_1514.var_244 - var_1513.var_244;
               _loc4_ = var_1514.var_245 - var_1513.var_245;
               _loc5_ = 0;
               while(_loc5_ < _loc2_)
               {
                  _loc5_++;
                  _loc6_ = _loc5_;
                  _loc7_ = int(class_691.method_114(int(var_1515)));
                  _loc8_ = int(class_691.method_114(628)) / 100;
                  _loc9_ = Number(_loc3_ + Math.cos(_loc8_) * _loc7_);
                  _loc10_ = Number(_loc4_ + Math.sin(_loc8_) * _loc7_);
                  if(Number(Math.sqrt(Number(_loc9_ * _loc9_ + _loc10_ * _loc10_))) < 100)
                  {
                     var_638 = var_638 + 1;
                     _loc11_ = var_800.method_84(class_899.__unprotect__("\x16b\x13?"),2);
                     _loc11_.x = _loc9_;
                     _loc11_.y = _loc10_;
                     _loc11_.scaleX = Number(1 + (Math.random() * 2 - 1) * 0.5);
                     _loc11_.scaleY = _loc11_.scaleX;
                     _loc11_.rotation = int(class_691.method_114(360));
                     _loc11_.gotoAndPlay(int(class_691.method_114(3)) + 1);
                  }
               }
               _loc8_ = 1;
               _loc12_ = var_1516.method_78();
               _loc5_ = 0;
               while(_loc5_ < int(_loc12_.length))
               {
                  _loc13_ = _loc12_[_loc5_];
                  _loc5_++;
                  _loc9_ = Number(var_1514.method_115({
                     "L\x01":Number(_loc13_.var_631.x + var_1513.var_244),
                     "\x03\x01":Number(_loc13_.var_631.y + var_1513.var_245)
                  }));
                  _loc10_ = 1 - _loc9_ / (Number(var_1515 + Number(_loc13_.var_66)));
                  if(_loc10_ > 0)
                  {
                     _loc13_.var_683 = Number(Math.max(0,_loc13_.var_683 - _loc10_ * _loc1_ * _loc8_));
                     _loc13_.var_631.alpha = _loc13_.var_683 / _loc13_.var_66;
                     if(Number(_loc13_.var_683) == 0)
                     {
                        var_1516.method_116(_loc13_);
                     }
                     _loc8_ = _loc8_ * 0.5;
                  }
               }
               if(int(var_1516.length) == 0)
               {
                  _loc5_ = 0;
                  while(_loc5_ < 24)
                  {
                     _loc5_++;
                     _loc6_ = _loc5_;
                     _loc14_ = new class_9();
                     _loc9_ = Math.random() * 6.28;
                     _loc10_ = Math.pow(Number(Math.random()),0.5) * 110;
                     _loc14_.x = Number(var_1513.var_244 + Math.cos(_loc9_) * _loc10_);
                     _loc14_.y = Number(var_1513.var_245 + Math.sin(_loc9_) * _loc10_);
                     _loc14_.gotoAndPlay(int(class_691.method_114(15)) + 1);
                     _loc15_ = Number(0.5 + Math.random() * 0.5);
                     _loc14_.scaleY = _loc15_;
                     _loc14_.scaleX = _loc15_;
                     var_232.method_124(_loc14_,class_645.var_210);
                  }
                  method_81(true,25);
                  name_3 = 2;
               }
               var_579 = {
                  "L\x01":var_1514.var_244,
                  "\x03\x01":var_1514.var_245
               };
               break;
            case 2:
               var_1514.var_245 = Number(var_1514.var_245 + 20);
         }
      }
      
      public function method_375() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var_1512 = true;
         var _loc1_:int = 0;
         while(_loc1_ < 10)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = var_800.method_84(class_899.__unprotect__("J&mg\x02"),0);
            _loc4_ = int(class_691.method_114(100));
            _loc5_ = int(class_691.method_114(628)) / 100;
            _loc3_.x = Math.cos(_loc5_) * _loc4_;
            _loc3_.y = Math.sin(_loc5_) * _loc4_;
            _loc3_.scaleX = Number(0.5 + Math.random() * 0.5);
            _loc3_.scaleY = _loc3_.scaleX;
            _loc3_.gotoAndPlay(int(class_691.method_114(10)) + 1);
         }
      }
      
      public function method_222(param1:Number, param2:Number) : void
      {
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_824 = 50;
         var_1515 = 30 - param1 * 15;
         var_638 = 0;
         var_579 = {
            "L\x01":0,
            "\x03\x01":0
         };
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         class_319 = var_232.method_84(class_899.__unprotect__("%~_J\x03"),0);
         var _loc1_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("aE\tq\x02"),class_645.var_209));
         var_1513 = _loc1_;
         var_1513.var_244 = class_742.var_217 * 0.5;
         var_1513.var_245 = class_742.var_219 * 1.5;
         var_1513.var_243.scaleX = var_824 * 0.02;
         var_1513.var_243.scaleY = var_824 * 0.02;
         var_1513.method_118();
         var_1516 = [];
         var _loc2_:int = int(Math.round(Number(6 + var_237[0] * 9)));
         var_800 = new class_755(var_1513.var_243);
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = var_800.method_84(class_899.__unprotect__("i}RI"),0);
            _loc6_ = Number(Number(10 + var_237[0] * 10) + int(class_691.method_114(30)));
            _loc7_ = int(class_691.method_114(int(100 - _loc6_)));
            _loc8_ = int(class_691.method_114(628)) / 100;
            _loc5_.x = Math.cos(_loc8_) * _loc7_;
            _loc5_.y = Math.sin(_loc8_) * _loc7_;
            _loc5_.scaleX = _loc6_ * 0.02;
            _loc5_.scaleY = _loc6_ * 0.02;
            _loc5_.alpha = 1;
            _loc5_.rotation = int(class_691.method_114(360));
            _loc5_.gotoAndStop(int(class_691.method_114(_loc5_.totalFrames)) + 1);
            var_1516.push({
               "gB\x01":_loc5_,
               "\f9\f\x02":_loc6_,
               "A_U{\x01":_loc6_
            });
         }
         _loc1_ = new class_656(var_232.method_84(class_899.__unprotect__("\x0fAe\f\x03"),class_645.var_209));
         var_1514 = _loc1_;
         var_1514.var_244 = Number(class_742.var_217 * 0.5 + 70);
         var_1514.var_245 = class_742.var_219 * 0.5 - 20;
         var_1514.var_243.scaleX = var_1515 * 0.02;
         var_1514.var_243.scaleY = var_1515 * 0.02;
         var_1514.method_118();
         var_1512 = false;
      }
   }
}
