package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import package_32.class_899;
   
   public class class_790 extends class_645
   {
       
      
      public var var_341:int;
      
      public var var_340:int;
      
      public var var_949:Array;
      
      public var var_66:int;
      
      public var name_11:int;
      
      public var var_496:Array;
      
      public var var_950:MovieClip;
      
      public var var_339:Array;
      
      public var var_265:Array;
      
      public var var_948:Number;
      
      public var var_15:Object;
      
      public function class_790()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc8_:MovieClip = null;
         var _loc9_:* = null;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:Array = null;
         var _loc13_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               _loc1_ = int(method_394(int(var_15.var_244),int(var_15.var_245)));
               _loc2_ = Number(method_395(int(var_15.var_244),int(var_15.var_245)));
               _loc3_ = (method_100().var_244 - _loc1_) / class_742.var_217;
               _loc3_ = int(Math.round(_loc3_ * 10));
               var_948 = Number(var_948 * 0.5 + _loc3_ * 0.5);
               _loc4_ = 0;
               _loc5_ = int(var_949.length);
               while(_loc4_ < _loc5_)
               {
                  _loc4_++;
                  _loc6_ = _loc4_;
                  _loc7_ = (_loc6_ / int(var_949.length) + (var_948 + 0.5) / 6) * 6.28;
                  _loc8_ = var_949[_loc6_];
                  _loc8_.x = Number(_loc1_ + Math.cos(_loc7_) * var_66 * 2 * 1.1);
                  _loc8_.y = Number(_loc2_ + Math.sin(_loc7_) * var_66 * 2 * 1.1);
               }
               if(!name_5)
               {
                  _loc4_ = 0;
                  _loc5_ = int(var_949.length);
                  while(_loc4_ < _loc5_)
                  {
                     _loc4_++;
                     _loc6_ = _loc4_;
                     _loc8_ = var_949[_loc6_];
                     _loc7_ = Number(_loc6_ + _loc3_);
                     if(_loc7_ < 0)
                     {
                        _loc7_ = Number(_loc7_ + 6);
                     }
                     if(_loc7_ > 5)
                     {
                        _loc7_ = _loc7_ - 6;
                     }
                     _loc9_ = var_265[int(_loc7_)];
                     _loc10_ = int(var_15.var_244) + int(_loc9_.var_244);
                     _loc11_ = int(var_15.var_245) + int(_loc9_.var_245);
                     if(method_222(_loc10_,_loc11_))
                     {
                        var_496[_loc10_][_loc11_] = _loc8_;
                        _loc8_.x = int(method_394(_loc10_,_loc11_));
                        _loc8_.y = Number(method_395(_loc10_,_loc11_));
                        method_710(_loc8_,_loc10_,_loc11_);
                     }
                  }
                  if(method_228())
                  {
                     method_81(true,15);
                  }
                  if(var_220)
                  {
                     _loc4_ = 0;
                     _loc12_ = var_339;
                     while(_loc4_ < int(_loc12_.length))
                     {
                        _loc8_ = _loc12_[_loc4_];
                        _loc4_++;
                        _loc13_ = Reflect.field(_loc8_,class_899.__unprotect__("\x1d\fb\x01"));
                        _loc8_.removeChild(_loc13_);
                     }
                  }
                  var_950.parent.removeChild(var_950);
                  name_3 = 1;
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_400(param1:int, param2:int) : void
      {
         var _loc6_:* = null;
         var _loc7_:MovieClip = null;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         var _loc5_:Array = var_265;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            if(!method_222(param1 + int(_loc6_.var_244),param2 + int(_loc6_.var_245)))
            {
               return;
            }
            _loc7_ = var_496[param1 + int(_loc6_.var_244)][param2 + int(_loc6_.var_245)];
            if(_loc7_ == null)
            {
               return;
            }
            _loc3_.push(_loc7_);
         }
         var_949 = _loc3_;
         var_15 = {
            "L\x01":param1,
            "\x03\x01":param2
         };
         name_3 = 2;
         var_950 = var_232.method_84(class_899.__unprotect__("\x0e\x06+\r\x02"),class_645.var_209);
         var_950.x = int(method_394(param1,param2));
         var_950.y = Number(method_395(param1,param2));
         var_950.scaleX = var_66 * 4 * 0.011;
         var_950.scaleY = var_66 * 4 * 0.011;
         var_232.method_521(var_950);
         var_948 = 0;
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < var_340 && param2 >= 0 && param2 < var_341;
      }
      
      public function method_710(param1:Sprite, param2:int, param3:int) : void
      {
         var var_244:int = param2;
         var var_245:int = param3;
         var _loc5_:MovieClip = Reflect.field(param1,class_899.__unprotect__("\x1d\fb\x01"));
         var _loc4_:MovieClip = _loc5_;
         if(_loc4_ != null)
         {
            param1.removeChild(_loc4_);
         }
         _loc5_ = new MovieClip();
         param1.var_951 = _loc5_;
         _loc5_.graphics.beginFill(16711680,0);
         _loc5_.graphics.drawCircle(0,0,50);
         var var_214:class_790 = this;
         _loc5_.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:*):void
         {
            var_214.method_400(var_244,var_245);
         });
         param1.addChild(_loc5_);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [500 - param1 * 300];
         super.method_95(param1);
         var_265 = [{
            "L\x01":1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":1
         },{
            "L\x01":-1,
            "\x03\x01":1
         },{
            "L\x01":-1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":-1
         },{
            "L\x01":1,
            "\x03\x01":-1
         }];
         var_340 = 7;
         var_341 = 7;
         var_66 = 10;
         name_11 = 9;
         method_117();
         method_72();
      }
      
      public function method_395(param1:int, param2:int) : Number
      {
         return Number(20 + (param2 * var_66 * 2 + param1 * 0.5 * var_66 * 2) * 1.1);
      }
      
      public function method_394(param1:int, param2:int) : int
      {
         return 56 + param1 * var_66 * 2;
      }
      
      public function method_711(param1:int, param2:int, param3:Array) : int
      {
         var _loc7_:* = null;
         var _loc8_:MovieClip = null;
         var _loc9_:Boolean = false;
         var _loc10_:int = 0;
         var _loc11_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Array = var_265;
         while(_loc5_ < int(_loc6_.length))
         {
            _loc7_ = _loc6_[_loc5_];
            _loc5_++;
            if(method_222(param1 + int(_loc7_.var_244),param2 + int(_loc7_.var_245)))
            {
               _loc8_ = var_496[param1 + int(_loc7_.var_244)][param2 + int(_loc7_.var_245)];
               if(_loc8_ != null && _loc8_.currentFrame > 1)
               {
                  _loc9_ = true;
                  _loc10_ = 0;
                  while(_loc10_ < int(param3.length))
                  {
                     _loc11_ = param3[_loc10_];
                     _loc10_++;
                     if(_loc11_ == _loc8_)
                     {
                        _loc9_ = false;
                     }
                  }
                  if(_loc9_)
                  {
                     param3.push(_loc8_);
                     _loc4_ = _loc4_ + int(method_711(param1 + int(_loc7_.var_244),param2 + int(_loc7_.var_245),param3));
                  }
               }
            }
         }
         return _loc4_ + 1;
      }
      
      public function method_228() : Boolean
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc9_:int = 0;
         var _loc1_:Boolean = false;
         var _loc2_:int = 0;
         var _loc3_:int = var_340;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = var_341;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = var_496[_loc4_][_loc7_];
               if(_loc8_ != null && _loc8_.currentFrame > 1)
               {
                  _loc9_ = int(method_711(_loc4_,_loc7_,[_loc8_]));
                  if(_loc9_ == name_11)
                  {
                     _loc1_ = true;
                  }
                  if(_loc1_)
                  {
                     _loc8_.gotoAndStop("3");
                  }
               }
            }
         }
         return _loc1_;
      }
      
      public function method_117() : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:MovieClip = null;
         var _loc10_:Array = null;
         class_319 = var_232.method_84(class_899.__unprotect__("VB\x16(\x03"),0);
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("L\',b"),0);
         _loc1_.x = class_742.var_217 * 0.5 - 5;
         _loc1_.y = class_742.var_219 * 0.5;
         _loc2_ = 1.3;
         _loc1_.scaleY = _loc2_;
         _loc1_.scaleX = _loc2_;
         _loc1_.rotation = 30;
         var_496 = [];
         var_339 = [];
         _loc3_ = 0;
         _loc4_ = var_340;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            var_496[_loc5_] = [];
            _loc6_ = 0;
            _loc7_ = var_341;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc2_ = Number(Number(Math.abs(_loc5_)) + Number(Math.abs(_loc8_)));
               if(_loc2_ > 2 && _loc2_ < 10)
               {
                  _loc9_ = var_232.method_84(class_899.__unprotect__("\x02Z\x12\x13\x03"),class_645.var_209);
                  _loc9_.x = int(method_394(_loc5_,_loc8_));
                  _loc9_.y = Number(method_395(_loc5_,_loc8_));
                  _loc9_.scaleX = var_66 * 0.022;
                  _loc9_.scaleY = var_66 * 0.022;
                  _loc9_.gotoAndStop("1");
                  method_710(_loc9_,_loc5_,_loc8_);
                  var_496[_loc5_][_loc8_] = _loc9_;
                  var_339.push(_loc9_);
               }
            }
         }
         while(true)
         {
            _loc10_ = [];
            _loc3_ = 0;
            _loc4_ = int(var_339.length);
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc10_.push(_loc5_);
            }
            _loc3_ = 0;
            _loc4_ = name_11;
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc6_ = int(class_691.method_114(int(_loc10_.length)));
               _loc9_ = var_339[int(_loc10_[_loc6_])];
               _loc10_.splice(_loc6_,1);
               _loc9_.gotoAndStop("2");
            }
            if(!method_228())
            {
               break;
            }
            _loc3_ = 0;
            _loc4_ = var_340;
            while(_loc3_ < _loc4_)
            {
               _loc3_++;
               _loc5_ = _loc3_;
               _loc6_ = 0;
               _loc7_ = var_341;
               while(_loc6_ < _loc7_)
               {
                  _loc6_++;
                  _loc8_ = _loc6_;
                  _loc9_ = var_496[_loc5_][_loc8_];
                  if(_loc9_ != null)
                  {
                     _loc9_.gotoAndStop("1");
                  }
               }
            }
         }
      }
   }
}
