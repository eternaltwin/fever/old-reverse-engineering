package package_13
{
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import package_11.class_755;
   import package_24.class_739;
   import package_24.class_878;
   import package_32.App;
   import package_32.class_899;
   
   public class class_672 extends Sprite
   {
       
      
      public var var_431:int;
      
      public var var_432:int;
      
      public var var_232:class_755;
      
      public function class_672()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         class_868.var_214.method_191(true);
         var_232 = new class_755(this);
         class_652.var_214.var_232.method_124(this,class_652.var_297);
         method_282();
         App.rootMc.stage.addEventListener(MouseEvent.CLICK,name_5);
         App.rootMc.stage.addEventListener(Event.ENTER_FRAME,method_79);
      }
      
      public function method_79(param1:*) : void
      {
      }
      
      public function method_89() : void
      {
         App.rootMc.stage.removeEventListener(MouseEvent.CLICK,name_5);
         App.rootMc.stage.removeEventListener(Event.ENTER_FRAME,method_79);
         parent.removeChild(this);
         class_868.var_214.method_191(false);
      }
      
      public function method_125() : class_739
      {
         var _loc1_:class_739 = new class_739();
         _loc1_.method_137(class_702.package_9.method_136("spark_twinkle"));
         _loc1_.var_233 = 0.95;
         _loc1_.var_250 = Number(0.05 + Math.random() * 0.05);
         _loc1_.var_251 = 12 + int(class_691.method_114(12));
         _loc1_.var_198.method_140();
         return _loc1_;
      }
      
      public function method_281(param1:uint) : void
      {
         graphics.beginFill(param1);
         graphics.drawRect(0,0,var_431,var_432);
      }
      
      public function name_5(param1:*) : void
      {
      }
      
      public function method_282() : void
      {
         x = int((class_742.var_216 * 0.5 - var_431) * 0.5);
         y = int((class_742.var_323 * 0.5 - var_432) * 0.5);
      }
      
      public function var_15() : void
      {
         var _loc1_:Number = class_742.var_216 * 0.5;
         var _loc2_:Number = Number(class_868.var_214.method_190());
         x = (_loc1_ - var_431) * 0.5;
         y = int(Number(class_868.var_320 + (_loc2_ - var_432) * 0.5));
      }
   }
}
