package package_13
{
   import flash.display.BlendMode;
   import flash.display.Sprite;
   import package_11.class_844;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   
   public class class_744 extends Sprite
   {
       
      
      public var var_756:Number;
      
      public var var_755:Number;
      
      public var name_13:Boolean;
      
      public var name_21:Number;
      
      public var var_334:Number;
      
      public var var_94:class_738;
      
      public function class_744(param1:class_719 = undefined, param2:class_826 = undefined, param3:class_844 = undefined)
      {
         var _loc9_:int = 0;
         var _loc10_:class_826 = null;
         if(class_899.var_239)
         {
            return;
         }
         super();
         name_13 = true;
         blendMode = BlendMode.LAYER;
         x = (param2.var_244 + 1) * 16;
         y = param2.var_245 * 16 + 1;
         var_94 = new class_738();
         var _loc4_:Number = param3.var_503 * 16807 % 2147483647;
         param3.var_503 = _loc4_;
         var_94.method_128(class_702.package_13.method_98(int((int(_loc4_) & 1073741823) % 4),"fever_head"),0.5,1);
         var _loc7_:Sprite = new Sprite();
         _loc7_.graphics.beginFill(16711680,0.5);
         _loc7_.graphics.drawRect(-16,8,32,56);
         _loc7_.graphics.endFill();
         _loc7_.graphics.beginFill(16711680,0.5);
         _loc7_.graphics.drawEllipse(-16,56 + 8 - 8,32,16);
         _loc7_.x = 0;
         _loc7_.y = -56;
         var_94.mask = _loc7_;
         addChild(var_94);
         addChild(_loc7_);
         var _loc8_:int = 0;
         while(_loc8_ < 2)
         {
            _loc8_++;
            _loc9_ = _loc8_;
            _loc10_ = param1.var_496[param2.var_244 + _loc9_][param2.var_245];
            _loc10_.var_757 = this;
         }
         var_334 = var_94.y;
         _loc4_ = param3.var_503 * 16807 % 2147483647;
         param3.var_503 = _loc4_;
         name_21 = int((int(_loc4_) & 1073741823) % 10007) / 10007 * 6.28;
         var_755 = 0;
         var_756 = 0;
      }
      
      public function method_79() : void
      {
         var _loc2_:Number = int(class_652.var_214.var_251 % 128) / 128;
         var _loc3_:Number = Number(Math.cos(Number(name_21 + _loc2_ * 6.28)));
         var_94.y = int(Number(Number(var_334 + _loc3_ * 4) + var_755 * 42));
         var _loc4_:Number = var_756 - var_755;
         var_755 = Number(var_755 + _loc4_ * 0.03);
         if(var_755 > 0.95)
         {
            alpha = alpha - 0.01;
         }
         if(alpha <= 0)
         {
            visible = false;
         }
      }
      
      public function method_493(param1:Number) : void
      {
         var_756 = param1;
         if(name_13)
         {
            name_13 = false;
            var_755 = var_756;
            if(var_755 == 1)
            {
               visible = false;
            }
         }
      }
   }
}
