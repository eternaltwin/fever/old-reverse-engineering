package package_13
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObjectContainer;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import package_11.class_755;
   import package_11.class_844;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_659;
   import package_11.package_12.class_663;
   import package_18.package_19.class_711;
   import package_18.package_19.class_730;
   import package_18.package_19.class_794;
   import package_18.package_19.class_859;
   import package_18.package_19.class_872;
   import package_24.class_656;
   import package_24.class_738;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_33.package_34.class_801;
   import package_45.package_46.class_940;
   import package_9.class_835;
   import package_9.class_869;
   
   public class class_719 extends Sprite
   {
      
      public static var var_268:int = 32;
      
      public static var var_269:int = 32;
      
      public static var var_332:int = 16;
      
      public static var var_1543:int = 10;
      
      public static var var_285:int = 5;
      
      public static var var_451:int = 4;
      
      public static var var_276:int = 3;
      
      public static var var_620:int = 2;
      
      public static var var_275:int = 1;
      
      public static var var_207:int = 0;
       
      
      public var var_455:Array;
      
      public var var_445:Array;
      
      public var var_1544:Boolean;
      
      public var var_220:Array;
      
      public var var_540:class_826;
      
      public var var_831:Array;
      
      public var var_315:class_656;
      
      public var var_503:class_844;
      
      public var var_281:class_826;
      
      public var var_318:int;
      
      public var var_319:int;
      
      public var var_1545:Array;
      
      public var var_242:Array;
      
      public var var_532:Sprite;
      
      public var var_496:Array;
      
      public var var_1548:Bitmap;
      
      public var var_1549:Boolean;
      
      public var var_1546:Array;
      
      public var name_78:Array;
      
      public var var_232:class_755;
      
      public var var_1311:Sprite;
      
      public var var_1309:Bitmap;
      
      public var package_31:Object;
      
      public var var_1547:int;
      
      public var var_327:class_826;
      
      public function class_719(param1:int = 0, param2:int = 0)
      {
         var _loc4_:Array = null;
         var _loc5_:Array = null;
         var _loc6_:_Reward = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc11_:class_826 = null;
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_319 = param1;
         var_318 = param2;
         var_503 = new class_844(int(method_1154()));
         var_242 = [];
         var_1545 = [];
         var_220 = [];
         var_1544 = false;
         var _loc3_:_IslandStatus = method_589();
         _loc4_ = _loc3_.var_296;
         loop5:
         switch(int(_loc3_.var_295))
         {
            case 0:
               break;
            case 1:
               _loc5_ = _loc4_[0];
               _loc6_ = _loc4_[1];
               _loc7_ = 0;
               while(true)
               {
                  if(_loc7_ >= int(_loc5_.length))
                  {
                     break loop5;
                  }
                  _loc8_ = int(_loc5_[_loc7_]);
                  _loc7_++;
                  var_220.push(_loc8_);
               }
               break;
            case 2:
               var_1544 = true;
         }
         var_1549 = false;
         package_31 = class_911.var_214.method_175(var_319,var_318);
         var_232 = new class_755(this);
         method_836();
         method_1159();
         var _loc9_:class_844 = var_503;
         var _loc10_:Number = _loc9_.var_503 * 16807 % 2147483647;
         _loc9_.var_503 = _loc10_;
         var_1547 = 1 + int(Math.pow(int((int(_loc10_) & 1073741823) % 10007) / 10007,0.5) * 70);
         if(var_1544)
         {
            _loc7_ = 0;
            _loc4_ = var_455;
            while(_loc7_ < int(_loc4_.length))
            {
               _loc11_ = _loc4_[_loc7_];
               _loc7_++;
               _loc11_.var_220 = true;
            }
         }
         else
         {
            _loc7_ = 0;
            _loc4_ = var_220;
            while(_loc7_ < int(_loc4_.length))
            {
               _loc8_ = int(_loc4_[_loc7_]);
               _loc7_++;
               var_455[_loc8_].var_220 = true;
            }
         }
         _loc7_ = 0;
         _loc4_ = var_831;
         while(_loc7_ < int(_loc4_.length))
         {
            _loc11_ = _loc4_[_loc7_];
            _loc7_++;
            _loc11_.method_788();
         }
         method_440();
         method_1160();
         method_1156();
         _loc7_ = 0;
         _loc4_ = var_831;
         while(_loc7_ < int(_loc4_.length))
         {
            _loc11_ = _loc4_[_loc7_];
            _loc7_++;
            _loc11_.method_788();
         }
         var_315 = new class_656();
         var_315.method_137(class_702.package_13.method_136("selector"));
         var_232.method_124(var_315,class_719.var_1543);
         var_315.visible = false;
         class_657.gfof(var_315,1,16777215);
         var_315.visible = false;
         new class_869(this);
         method_169();
         if(method_384())
         {
            method_381();
         }
      }
      
      public static function method_1138(param1:int, param2:int) : int
      {
         return param1 * 2 + param2;
      }
      
      public function method_1139() : void
      {
         var _loc5_:class_826 = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         var _loc9_:Array = null;
         var _loc10_:class_738 = null;
         var _loc11_:Number = NaN;
         var _loc2_:Number = int(class_652.var_214.var_251 % 40) / 40;
         var _loc3_:int = 0;
         var _loc4_:Array = var_1545;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc5_.method_783();
            _loc6_ = 48 - _loc5_.var_1085 * 24;
            _loc7_ = Number(int(class_719.method_1138(_loc5_.var_244,_loc5_.var_245)) % _loc6_ / _loc6_ + _loc2_);
            _loc8_ = 0;
            _loc9_ = _loc5_.var_1087;
            while(_loc8_ < int(_loc9_.length))
            {
               _loc10_ = _loc9_[_loc8_];
               _loc8_++;
               _loc11_ = Number(1 + _loc5_.var_1085 * 7);
               _loc10_.y = _loc5_.var_245 * 16 + int(Math.round(Math.cos(_loc7_ * 6.28) * _loc11_));
            }
         }
      }
      
      public function method_79() : void
      {
         var _loc1_:class_872 = null;
         var _loc4_:class_744 = null;
         if(int(var_242.length) > 0 && int(class_691.method_114(10)) == 0)
         {
            _loc1_ = var_242[int(class_691.method_114(int(var_242.length)))];
            _loc1_.method_989();
         }
         var _loc2_:int = 0;
         var _loc3_:Array = var_1546;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.method_79();
         }
         method_1139();
      }
      
      public function method_1140() : void
      {
         if(var_315 != null)
         {
            var_315.visible = false;
         }
         var_327 = null;
      }
      
      public function method_169() : void
      {
         var_232.method_519(class_719.var_451);
      }
      
      public function method_1141() : void
      {
         var _loc4_:class_826 = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.method_787() && int(int(_loc4_.var_322[0]) % var_1547) == 0)
            {
               new class_940(this,_loc4_,int(int(_loc4_.var_322[1]) % 10) / 10);
            }
         }
      }
      
      public function method_1142(param1:class_826, param2:class_826) : int
      {
         if(param1.var_895 > param2.var_895)
         {
            return -1;
         }
         return 1;
      }
      
      public function method_1143(param1:class_826) : void
      {
         var_315.x = (param1.var_244 + 0.5) * 16;
         var_315.y = (param1.var_245 + 0.5) * 16;
         var_315.visible = Boolean(param1.method_790());
         var_327 = param1;
         class_657.gfof(var_315,1,16777215);
      }
      
      public function method_144(param1:int) : class_826
      {
         var _loc4_:class_826 = null;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.package_19 == null)
            {
               continue;
            }
            _loc5_ = _loc4_.package_19.var_252.var_296;
            switch(int(_loc4_.package_19.var_252.var_295))
            {
               default:
               default:
               default:
                  continue;
               case 3:
                  _loc6_ = _loc5_[0];
                  if(_loc6_ == param1)
                  {
                     return _loc4_;
                  }

            }
         }
         return null;
      }
      
      public function method_171(param1:int, param2:int) : void
      {
         if(!method_222(param1,param2))
         {
            return;
         }
         var _loc3_:class_826 = var_496[param1][param2];
         if(var_327 == _loc3_)
         {
            return;
         }
         method_1140();
         method_1143(_loc3_);
      }
      
      public function method_1144(param1:int) : void
      {
         var _loc5_:class_826 = null;
         var _loc6_:class_826 = null;
         var _loc7_:class_844 = null;
         var _loc8_:Number = NaN;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:Array = var_445;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc5_.var_252 = 2;
            _loc5_.var_1089 = param1 - 1;
         }
         _loc3_ = 0;
         _loc4_ = var_445;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc5_.var_245 == 0)
            {
               return;
            }
            _loc6_ = var_496[_loc5_.var_244][_loc5_.var_245 - 1];
            _loc2_.push(_loc6_);
            _loc6_.var_252 = 1;
            _loc6_.var_1089 = param1;
         }
         _loc3_ = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc5_ = _loc2_[_loc3_];
            _loc3_++;
            var_445.method_116(_loc5_);
         }
         _loc4_ = var_445.method_78();
         _loc3_ = 0;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc6_ = _loc5_.var_899[1];
            if(_loc6_ != null)
            {
               if(_loc6_.var_252 == 3)
               {
                  var_445 = [_loc5_];
                  break;
               }
               if(_loc6_.var_252 != 1)
               {
                  var_445.method_116(_loc5_);
               }
            }
         }
         if(int(var_445.length) > 0 && param1 > 1)
         {
            _loc7_ = var_503;
            _loc8_ = _loc7_.var_503 * 16807 % 2147483647;
            _loc7_.var_503 = _loc8_;
            _loc5_ = var_445[int((int(_loc8_) & 1073741823) % int(var_445.length))];
            _loc5_.var_252 = 3;
         }
         var_445 = _loc2_;
      }
      
      public function method_1145(param1:class_826) : void
      {
         var _loc4_:class_826 = null;
         param1.var_252 = 1;
         var _loc2_:int = 0;
         var _loc3_:Array = param1.var_507;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.var_65 == 0)
            {
               _loc4_.var_65 = 1;
               var_445.push(_loc4_);
            }
         }
      }
      
      public function method_1146(param1:Boolean = false) : void
      {
         var _loc6_:class_872 = null;
         var _loc7_:Matrix = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:Number = NaN;
         var _loc14_:class_826 = null;
         var _loc2_:class_359 = new class_359();
         var _loc3_:BitmapData = new BitmapData(class_719.var_268,class_719.var_269,false,0);
         var _loc4_:int = 0;
         var _loc5_:Array = var_242;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc7_ = new Matrix();
            _loc7_.scale(2,2);
            _loc7_.translate(_loc6_.var_289.var_244 + 2,_loc6_.var_289.var_245);
            _loc3_.draw(_loc2_,_loc7_);
         }
         _loc4_ = 0;
         var _loc8_:int = class_719.var_268;
         while(_loc4_ < _loc8_)
         {
            _loc4_++;
            _loc9_ = _loc4_;
            _loc10_ = 0;
            _loc11_ = class_719.var_269;
            while(_loc10_ < _loc11_)
            {
               _loc10_++;
               _loc12_ = _loc10_;
               _loc13_ = 1 - uint(_loc3_.getPixel(_loc9_,_loc12_)) / 16777215;
               _loc14_ = var_496[_loc9_][_loc12_];
               if(_loc14_.var_1087 != null)
               {
                  if(_loc14_.var_757 != null)
                  {
                     _loc14_.var_757.method_493(_loc13_);
                  }
                  _loc14_.method_784(1 - _loc13_,param1);
               }
            }
         }
         _loc3_.dispose();
      }
      
      public function method_990() : void
      {
         method_1146();
         if(method_384())
         {
            method_381();
            method_1147(true);
         }
      }
      
      public function method_381() : void
      {
         var _loc3_:class_826 = null;
         method_1141();
         var _loc1_:int = 0;
         var _loc2_:Array = var_455;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.package_19 != null)
            {
               _loc3_.package_19.method_381();
            }
         }
      }
      
      public function method_179(param1:class_826) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:class_826 = null;
         var _loc5_:class_826 = null;
         _loc2_ = 0;
         _loc3_ = var_831;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.var_895 = -1;
         }
         param1.var_895 = 0;
         var_445 = [param1];
         while(int(var_445.length) > 0)
         {
            _loc4_ = var_445.shift();
            _loc2_ = 0;
            _loc3_ = _loc4_.var_541;
            while(_loc2_ < int(_loc3_.length))
            {
               _loc5_ = _loc3_[_loc2_];
               _loc2_++;
               if(_loc5_.var_895 <= -1)
               {
                  _loc5_.var_895 = _loc4_.var_895 + 1;
                  if(_loc5_.method_791())
                  {
                     _loc5_.var_895 = Boolean(_loc5_.method_382())?99:-1;
                  }
                  else
                  {
                     var_445.push(_loc5_);
                  }
               }
            }
         }
      }
      
      public function method_1148(param1:Array) : Boolean
      {
         var _loc4_:class_826 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_826 = null;
         var _loc9_:class_826 = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.var_895 = 2;
         }
         _loc2_ = 0;
         while(_loc2_ < int(param1.length))
         {
            _loc4_ = param1[_loc2_];
            _loc2_++;
            _loc4_.var_895 = 1;
         }
         _loc2_ = 0;
         while(_loc2_ < int(param1.length))
         {
            _loc4_ = param1[_loc2_];
            _loc2_++;
            _loc5_ = 0;
            while(_loc5_ < 2)
            {
               _loc5_++;
               _loc6_ = _loc5_;
               _loc7_ = int([1,3][_loc6_]);
               _loc8_ = _loc4_.var_899[_loc7_];
               if(_loc8_ != null && _loc8_.var_1089 > 0 && _loc8_.var_252 == 2)
               {
                  _loc9_ = _loc8_.var_899[_loc7_];
                  if(_loc9_ != null && _loc9_.var_895 == 2)
                  {
                     _loc8_.var_252 = 3;
                     return true;
                  }
               }
            }
         }
         return false;
      }
      
      public function method_89() : void
      {
         if(var_315 != null)
         {
            var_315.method_89();
         }
         while(int(var_242.length) > 0)
         {
            var_242.pop().method_89();
         }
         var_1309.bitmapData.dispose();
         var_1548.bitmapData.dispose();
         if(parent != null)
         {
            parent.removeChild(this);
         }
         class_801.name_50();
      }
      
      public function method_384() : Boolean
      {
         return int(var_242.length) == 0;
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < class_719.var_268 && param2 >= 0 && param2 < class_719.var_269;
      }
      
      public function method_836() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc8_:class_826 = null;
         var _loc9_:Array = null;
         var _loc10_:Array = null;
         var _loc11_:class_826 = null;
         var_496 = [];
         var_831 = [];
         var _loc1_:int = 0;
         _loc2_ = class_719.var_268;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_496[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = class_719.var_269;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               new class_826(_loc3_,_loc6_,this);
            }
         }
         _loc1_ = 0;
         var _loc7_:Array = var_831;
         while(_loc1_ < int(_loc7_.length))
         {
            _loc8_ = _loc7_[_loc1_];
            _loc1_++;
            _loc2_ = 0;
            _loc9_ = class_742.var_264;
            while(_loc2_ < int(_loc9_.length))
            {
               _loc10_ = _loc9_[_loc2_];
               _loc2_++;
               _loc3_ = _loc8_.var_244 + int(_loc10_[0]);
               _loc4_ = _loc8_.var_245 + int(_loc10_[1]);
               if(method_222(_loc3_,_loc4_))
               {
                  _loc11_ = var_496[_loc3_][_loc4_];
                  _loc8_.var_507.push(_loc11_);
                  _loc8_.var_899.push(_loc11_);
               }
               else
               {
                  _loc8_.var_899.push(null);
               }
            }
         }
      }
      
      public function method_1149(param1:Array) : Object
      {
         var _loc4_:class_826 = null;
         var _loc2_:* = {
            "O6EU\x01":99,
            "O\x15;\x03\x02":99,
            "^\'\x14^\x01":-99,
            "~tj[\x01":-99
         };
         var _loc3_:int = 0;
         while(_loc3_ < int(param1.length))
         {
            _loc4_ = param1[_loc3_];
            _loc3_++;
            if(_loc4_.var_244 < int(_loc2_.name_22))
            {
               _loc2_.name_22 = _loc4_.var_244;
            }
            if(_loc4_.var_244 > int(_loc2_.var_340))
            {
               _loc2_.var_340 = _loc4_.var_244;
            }
            if(_loc4_.var_245 < int(_loc2_.name_23))
            {
               _loc2_.name_23 = _loc4_.var_245;
            }
            if(_loc4_.var_245 > int(_loc2_.var_341))
            {
               _loc2_.var_341 = _loc4_.var_245;
            }
         }
         return _loc2_;
      }
      
      public function method_1150() : class_826
      {
         var _loc4_:class_826 = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         var _loc3_:Array = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.method_789())
            {
               _loc1_.push(_loc4_);
            }
         }
         if(int(_loc1_.length) == 0)
         {
            return null;
         }
         var _loc5_:class_844 = var_503;
         var _loc6_:Number = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         return _loc1_[int((int(_loc6_) & 1073741823) % int(_loc1_.length))];
      }
      
      public function method_1151(param1:Object) : class_738
      {
         var _loc2_:class_738 = new class_738();
         _loc2_.method_128(class_702.package_13.method_98(null,"waves_3"),0,0);
         _loc2_.x = int(param1.var_244) * 16;
         _loc2_.y = int(param1.var_245) * 16;
         param1.method_785(_loc2_);
         return _loc2_;
      }
      
      public function method_1152(param1:Array) : Array
      {
         var _loc11_:class_826 = null;
         var _loc2_:* = method_1149(param1);
         var _loc3_:int = int(_loc2_.var_340) - int(_loc2_.name_22);
         var _loc5_:class_844 = var_503;
         var _loc6_:Number = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         var _loc4_:int = int((int(_loc2_.var_341) - int(_loc2_.name_23)) * (0.1 + int((int(_loc6_) & 1073741823) % 10007) / 10007 * 0.5));
         _loc5_ = var_503;
         _loc6_ = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         var _loc7_:int = int((int(_loc6_) & 1073741823) % (_loc3_ - 1));
         _loc3_ = _loc3_ - _loc7_;
         _loc5_ = var_503;
         _loc6_ = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         var _loc8_:int = int((int(_loc6_) & 1073741823) % _loc7_);
         _loc2_.name_22 = int(_loc2_.name_22) + _loc8_;
         _loc2_.var_340 = int(_loc2_.var_340) - _loc7_;
         _loc2_.var_341 = int(_loc2_.name_23) + _loc4_;
         var _loc9_:Array = [];
         var _loc10_:int = 0;
         while(_loc10_ < int(param1.length))
         {
            _loc11_ = param1[_loc10_];
            _loc10_++;
            if(_loc11_.var_244 >= int(_loc2_.name_22) && _loc11_.var_244 <= int(_loc2_.var_340) && _loc11_.var_245 >= int(_loc2_.name_23) && _loc11_.var_245 <= int(_loc2_.var_341))
            {
               _loc9_.push(_loc11_);
            }
         }
         return _loc9_;
      }
      
      public function method_589() : _IslandStatus
      {
         return class_765.var_214.method_307(var_319,var_318);
      }
      
      public function method_1153(param1:int) : class_826
      {
         return var_455[param1];
      }
      
      public function method_1154() : int
      {
         return var_319 * class_911.var_214.var_307 + var_318;
      }
      
      public function method_141(param1:int) : Object
      {
         var _loc2_:Array = class_742.var_264[param1];
         var _loc3_:int = var_319 + int(_loc2_[0]);
         var _loc4_:int = var_318 + int(_loc2_[1]);
         _loc3_ = int(Number(class_663.method_113(_loc3_,class_911.var_214.var_307)));
         _loc4_ = int(Number(class_663.method_113(_loc4_,class_911.var_214.var_307)));
         return {
            "L\x01":_loc3_,
            "\x03\x01":_loc4_
         };
      }
      
      public function method_131() : String
      {
         return class_808.var_1037 + " [" + (int(var_319 - class_911.var_214.var_307 * 0.5)) + "," + (int(var_318 - class_911.var_214.var_307 * 0.5)) + "]";
      }
      
      public function method_319(param1:int) : BitmapData
      {
         var _loc5_:class_826 = null;
         var _loc6_:class_750 = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:Array = null;
         var _loc11_:class_826 = null;
         var _loc12_:class_738 = null;
         var _loc13_:class_750 = null;
         var _loc2_:BitmapData = new BitmapData(class_719.var_268 * class_719.var_332,class_719.var_269 * class_719.var_332,true,0);
         var _loc3_:int = 0;
         var _loc4_:Array = var_831;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            switch(_loc5_.var_252)
            {
               case 0:
                  continue;
               case 1:
                  _loc7_ = 0;
                  _loc8_ = 1;
                  _loc9_ = 0;
                  _loc10_ = _loc5_.var_899;
                  while(_loc9_ < int(_loc10_.length))
                  {
                     _loc11_ = _loc10_[_loc9_];
                     _loc9_++;
                     if(_loc11_ != null && (_loc11_.var_252 == 1 || _loc11_.var_252 == 2) && _loc11_.var_1089 >= _loc5_.var_1089)
                     {
                        _loc7_ = _loc7_ + int(Number(Math.pow(2,_loc8_)));
                     }
                     _loc8_ = int((_loc8_ + 1) % 4);
                  }
                  _loc6_ = class_702.package_13.method_98(_loc7_,param1 == 0?"dirt":"grass");
                  _loc6_.method_506(_loc2_,_loc5_.var_244 * class_719.var_332,_loc5_.var_245 * class_719.var_332);
                  _loc5_.var_895 = _loc7_;
                  continue;
               case 2:
               case 3:
                  if(param1 == 1)
                  {
                     _loc5_.var_895 = _loc5_.var_899[3].var_895;
                     _loc6_ = class_702.package_13.method_98(_loc5_.var_895,"cliff");
                     if(_loc5_.var_1089 == 0)
                     {
                        _loc7_ = 0;
                        while(_loc7_ < 2)
                        {
                           _loc7_++;
                           _loc8_ = _loc7_;
                           _loc12_ = new class_738();
                           _loc12_.method_128(_loc6_,0,0);
                           _loc12_.x = _loc5_.var_244 * 16;
                           _loc12_.y = (_loc5_.var_245 + _loc8_) * 16;
                           var_232.method_124(_loc12_,class_719.var_207);
                        }
                     }
                     else
                     {
                        _loc6_.method_506(_loc2_,_loc5_.var_244 * class_719.var_332,_loc5_.var_245 * class_719.var_332);
                        if(_loc5_.var_252 == 3)
                        {
                           _loc13_ = class_702.package_13.method_98(null,"ladder");
                           _loc13_.method_505(_loc2_,_loc5_.var_244 * class_719.var_332,_loc5_.var_245 * class_719.var_332);
                        }
                     }
                  }

            }
         }
         return _loc2_;
      }
      
      public function method_1155() : class_826
      {
         var _loc4_:class_826 = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         var _loc3_:Array = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(!(_loc4_.package_19 != null || _loc4_.var_252 != 1))
            {
               _loc1_.push(_loc4_);
            }
         }
         class_659.method_241(_loc1_,var_503);
         var _loc5_:Function = function(param1:class_826, param2:class_826):int
         {
            if(int(param1.var_541.length) < int(param2.var_541.length))
            {
               return -1;
            }
            return 1;
         };
         _loc1_.sort(_loc5_);
         return _loc1_[0];
      }
      
      public function method_98(param1:int, param2:int) : class_826
      {
         if(!method_222(param1,param2))
         {
            return null;
         }
         return var_496[param1][param2];
      }
      
      public function method_1156() : void
      {
         var _loc3_:Array = null;
         var _loc4_:class_826 = null;
         var _loc5_:int = 0;
         var _loc6_:class_826 = null;
         var _loc7_:class_844 = null;
         var _loc8_:Number = NaN;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:class_872 = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         _loc3_ = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.var_895 = 0;
            if(_loc4_.package_19 == null)
            {
               _loc1_.push(_loc4_);
            }
         }
         _loc2_ = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc4_ = _loc1_[_loc2_];
            _loc2_++;
            _loc5_ = 0;
            _loc3_ = _loc4_.var_899;
            while(_loc5_ < int(_loc3_.length))
            {
               _loc6_ = _loc3_[_loc5_];
               _loc5_++;
               _loc7_ = var_503;
               _loc8_ = _loc7_.var_503 * 16807 % 2147483647;
               _loc7_.var_503 = _loc8_;
               _loc4_.var_895 = _loc4_.var_895 + int((int(_loc8_) & 1073741823) % 3);
               if(_loc6_.package_19 != null)
               {
                  _loc4_.var_895 = _loc4_.var_895 + int(_loc6_.package_19.method_383());
               }
            }
         }
         class_659.method_241(_loc1_,var_503);
         _loc1_.sort(method_1142);
         _loc3_ = [];
         _loc2_ = 0;
         var _loc9_:Array = package_31.name_15.var_242;
         while(_loc2_ < int(_loc9_.length))
         {
            _loc5_ = int(_loc9_[_loc2_]);
            _loc2_++;
            _loc4_ = _loc1_.shift();
            _loc3_.push(_loc4_);
            _loc10_ = 0;
            _loc11_ = _loc4_.var_899;
            while(_loc10_ < int(_loc11_.length))
            {
               _loc6_ = _loc11_[_loc10_];
               _loc10_++;
               _loc6_.var_895 = _loc6_.var_895 - 2;
            }
            _loc1_.sort(method_1142);
            if(int(_loc1_.length) == 0)
            {
               break;
            }
         }
         _loc2_ = 0;
         _loc9_ = package_31.name_15.var_242;
         while(_loc2_ < int(_loc9_.length))
         {
            _loc5_ = int(_loc9_[_loc2_]);
            _loc2_++;
            _loc4_ = _loc3_.shift();
            if(!_loc4_.var_220)
            {
               _loc12_ = new class_872(this,_loc4_,_loc5_);
               if(int(_loc3_.length) == 0)
               {
                  break;
               }
            }
         }
         if(package_31.var_309 == _Reward.class_794 && class_794.var_214.var_260 == class_885.var_969)
         {
            new class_872(this,class_794.var_214.var_289.var_899[1],11);
         }
         method_1147(false);
      }
      
      public function method_440() : void
      {
         var _loc2_:int = 0;
         var _loc3_:* = null;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Array = null;
         var _loc8_:class_826 = null;
         var _loc9_:Boolean = false;
         var _loc10_:class_844 = null;
         var _loc11_:Number = NaN;
         var _loc12_:int = 0;
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            if(!package_31.var_480[_loc2_])
            {
               _loc3_ = method_1149(var_455);
               _loc4_ = [];
               _loc5_ = 0;
               while(int(_loc4_.length) == 0)
               {
                  _loc6_ = 0;
                  _loc7_ = var_455;
                  while(_loc6_ < int(_loc7_.length))
                  {
                     _loc8_ = _loc7_[_loc6_];
                     _loc6_++;
                     _loc9_ = false;
                     switch(_loc2_)
                     {
                        case 0:
                           _loc9_ = _loc8_.var_244 == int(_loc3_.var_340) - _loc5_;
                           break;
                        case 1:
                           _loc9_ = _loc8_.var_245 == int(_loc3_.var_341) - _loc5_;
                           break;
                        case 2:
                           _loc9_ = _loc8_.var_244 == int(_loc3_.name_22) + _loc5_;
                           break;
                        case 3:
                           _loc9_ = _loc8_.var_245 == int(_loc3_.name_23) + _loc5_;
                     }
                     if(_loc8_.package_19 != null)
                     {
                        _loc9_ = false;
                     }
                     if(_loc9_)
                     {
                        _loc4_.push(_loc8_);
                     }
                  }
                  _loc5_++;
               }
               _loc10_ = var_503;
               _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
               _loc10_.var_503 = _loc11_;
               _loc8_ = _loc4_[int((int(_loc11_) & 1073741823) % int(_loc4_.length))];
               new class_711(this,_loc8_,_loc2_);
            }
         }
         method_1157();
         _loc1_ = 0;
         _loc4_ = var_455;
         while(_loc1_ < int(_loc4_.length))
         {
            _loc8_ = _loc4_[_loc1_];
            _loc1_++;
            if(_loc8_.package_19 == null)
            {
               _loc2_ = 0;
               while(_loc2_ < 2)
               {
                  _loc2_++;
                  _loc5_ = _loc2_;
                  switch(_loc5_)
                  {
                     case 0:
                        _loc10_ = var_503;
                        _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
                        _loc10_.var_503 = _loc11_;
                        if(int((int(_loc11_) & 1073741823) % 24) == 0)
                        {
                           _loc10_ = var_503;
                           _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
                           _loc10_.var_503 = _loc11_;
                           _loc8_.method_482(_loc5_,"elements_dirt_medium",int((int(_loc11_) & 1073741823) % 5),4);
                        }
                        else
                        {
                           _loc10_ = var_503;
                           _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
                           _loc10_.var_503 = _loc11_;
                           if(int((int(_loc11_) & 1073741823) % 8) == 0)
                           {
                              _loc10_ = var_503;
                              _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
                              _loc10_.var_503 = _loc11_;
                              _loc8_.method_482(_loc5_,"elements_dirt_small",int((int(_loc11_) & 1073741823) % 8),8);
                           }
                        }
                        continue;
                     case 1:
                        _loc6_ = 0;
                        while(_loc6_ < 3)
                        {
                           _loc6_++;
                           _loc12_ = _loc6_;
                           _loc10_ = var_503;
                           _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
                           _loc10_.var_503 = _loc11_;
                           _loc8_.method_482(_loc5_,"elements_grass_medium",int((int(_loc11_) & 1073741823) % 5),7);
                        }

                  }
               }
            }
         }
      }
      
      public function method_1157() : void
      {
         var _loc1_:_Reward = package_31.var_309;
         if(_loc1_ == null)
         {
            return;
         }
         if(_loc1_ == _Reward.class_794)
         {
            new class_794(this,var_455[8]);
            return;
         }
         var _loc2_:class_826 = method_1155();
         new class_859(this,_loc2_,_loc1_);
         if(int(package_31.name_140) >= 0)
         {
            var_540 = method_1150();
         }
      }
      
      public function method_1159() : void
      {
         var _loc2_:Array = null;
         var _loc3_:class_826 = null;
         var _loc5_:int = 0;
         var _loc6_:class_826 = null;
         var _loc7_:int = 0;
         var _loc8_:Array = null;
         var _loc9_:int = 0;
         var _loc10_:class_844 = null;
         var _loc11_:Number = NaN;
         var_445 = [];
         var _loc1_:int = 0;
         _loc2_ = var_831;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_1089 = 0;
            _loc3_.var_252 = 0;
            _loc3_.var_895 = 0;
            _loc3_.var_65 = 0;
         }
         _loc3_ = var_496[int(class_719.var_268 * 0.5)][int(class_719.var_269 * 0.5)];
         method_1145(_loc3_);
         _loc1_ = 0;
         var _loc4_:int = int(package_31.name_15.var_307);
         while(_loc1_ < _loc4_)
         {
            _loc1_++;
            _loc5_ = _loc1_;
            method_328();
         }
         var_445 = [];
         _loc1_ = 0;
         _loc2_ = var_831;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc6_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc6_.var_252 == 1)
            {
               var_445.push(_loc6_);
            }
         }
         method_1144(1);
         _loc1_ = 2;
         _loc4_ = 0;
         while(_loc4_ < _loc1_)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc2_ = method_1152(var_445);
            _loc7_ = 0;
            _loc8_ = var_445;
            while(_loc7_ < int(_loc8_.length))
            {
               _loc6_ = _loc8_[_loc7_];
               _loc7_++;
               _loc6_.var_895 = 0;
            }
            _loc7_ = 0;
            while(_loc7_ < int(_loc2_.length))
            {
               _loc6_ = _loc2_[_loc7_];
               _loc7_++;
               _loc6_.var_895 = 1;
            }
            _loc8_ = method_1152(var_445);
            _loc7_ = 0;
            while(_loc7_ < int(_loc8_.length))
            {
               _loc6_ = _loc8_[_loc7_];
               _loc7_++;
               if(_loc6_.var_895 == 0)
               {
                  _loc2_.push(_loc6_);
               }
            }
            var_445 = _loc2_;
            method_1144(2 + _loc5_);
         }
         var_455 = [];
         _loc4_ = 0;
         _loc5_ = 0;
         _loc2_ = var_831;
         while(_loc5_ < int(_loc2_.length))
         {
            _loc6_ = _loc2_[_loc5_];
            _loc5_++;
            if(_loc6_.var_252 == 1)
            {
               var_455.push(_loc6_);
               _loc6_.var_215 = _loc4_;
               _loc7_ = 0;
               while(_loc7_ < 3)
               {
                  _loc7_++;
                  _loc9_ = _loc7_;
                  _loc10_ = var_503;
                  _loc11_ = _loc10_.var_503 * 16807 % 2147483647;
                  _loc10_.var_503 = _loc11_;
                  _loc6_.var_322.push(int((int(_loc11_) & 1073741823) % 10000));
               }
               _loc4_++;
            }
         }
         if(!method_1158())
         {
            method_1159();
         }
      }
      
      public function method_587() : void
      {
         var _loc4_:class_826 = null;
         var _loc1_:class_826 = var_445.shift();
         _loc1_.var_895 = 1;
         var _loc2_:int = 0;
         var _loc3_:Array = _loc1_.var_541;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.var_895 != 1)
            {
               var_445.push(_loc4_);
            }
         }
      }
      
      public function method_328() : void
      {
         var _loc3_:class_826 = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Array = null;
         var _loc7_:class_826 = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_445;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_895 = 0;
            _loc4_ = 0;
            _loc5_ = 0;
            _loc6_ = _loc3_.var_507;
            while(_loc5_ < int(_loc6_.length))
            {
               _loc7_ = _loc6_[_loc5_];
               _loc5_++;
               if(_loc7_.var_252 == 1)
               {
                  _loc3_.var_895 = _loc3_.var_895 + int([4,3,4,3][_loc4_]);
               }
               _loc4_++;
            }
         }
         var_445.sort(method_1142);
         var _loc8_:class_844 = var_503;
         var _loc9_:Number = _loc8_.var_503 * 16807 % 2147483647;
         _loc8_.var_503 = _loc9_;
         _loc1_ = int(Math.pow(int((int(_loc9_) & 1073741823) % 10007) / 10007,3) * int(var_445.length));
         _loc3_ = var_445[_loc1_];
         var_445.splice(_loc1_,1);
         method_1145(_loc3_);
      }
      
      public function method_1160() : void
      {
         var_532 = new Sprite();
         var_232.method_124(var_532,class_719.var_620);
         var_1309 = new Bitmap();
         var_1548 = new Bitmap();
         var _loc1_:Sprite = new Sprite();
         _loc1_.addChild(var_1309);
         var_532.addChild(var_1548);
         var_532.addChild(_loc1_);
         var_1309.bitmapData = method_319(0);
         var_1548.bitmapData = method_319(1);
         var_1311 = new Sprite();
         _loc1_.addChild(var_1311);
         var_1309.mask = var_1311;
         class_658.var_146(_loc1_,3,1,4456448,true);
      }
      
      public function method_1147(param1:Boolean) : void
      {
         if(int(var_242.length) > 0 || var_540 == null)
         {
            return;
         }
         var _loc2_:class_730 = new class_730(this,var_540,int(package_31.name_140));
         if(param1)
         {
            new class_835(_loc2_);
         }
      }
      
      public function method_1158() : Boolean
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_826 = null;
         var _loc6_:Array = null;
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = 0;
            _loc4_ = var_831;
            while(_loc3_ < int(_loc4_.length))
            {
               _loc5_ = _loc4_[_loc3_];
               _loc3_++;
               _loc5_.method_788();
            }
            _loc3_ = 0;
            _loc4_ = var_831;
            while(_loc3_ < int(_loc4_.length))
            {
               _loc5_ = _loc4_[_loc3_];
               _loc3_++;
               _loc5_.var_895 = 0;
            }
            var_445 = [var_455[0]];
            while(int(var_445.length) > 0)
            {
               method_587();
            }
            _loc4_ = [];
            _loc3_ = 0;
            _loc6_ = var_455;
            while(_loc3_ < int(_loc6_.length))
            {
               _loc5_ = _loc6_[_loc3_];
               _loc3_++;
               if(_loc5_.var_895 == 0)
               {
                  _loc4_.push(_loc5_);
               }
            }
            if(int(_loc4_.length) > 0)
            {
               if(_loc2_ == 0)
               {
                  method_1148(_loc4_);
               }
               if(_loc2_ == 1)
               {
                  return false;
               }
            }
         }
         return true;
      }
      
      public function method_142() : void
      {
         var _loc4_:class_826 = null;
         var _loc6_:Number = NaN;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Boolean = false;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:class_826 = null;
         var _loc19_:class_744 = null;
         var _loc20_:class_738 = null;
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = var_455;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.var_245 > _loc1_)
            {
               _loc1_ = _loc4_.var_245;
            }
         }
         var_1546 = [];
         var _loc5_:class_844 = new class_844(int(method_1154()));
         _loc6_ = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         int((int(_loc6_) & 1073741823) % 2000);
         _loc6_ = _loc5_.var_503 * 16807 % 2147483647;
         _loc5_.var_503 = _loc6_;
         _loc2_ = 12 + int((int(_loc6_) & 1073741823) % 50);
         var _loc7_:int = 0;
         while(_loc7_ < _loc2_)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc6_ = _loc5_.var_503 * 16807 % 2147483647;
            _loc5_.var_503 = _loc6_;
            _loc9_ = int((int(_loc6_) & 1073741823) % (class_719.var_268 - 1));
            _loc6_ = _loc5_.var_503 * 16807 % 2147483647;
            _loc5_.var_503 = _loc6_;
            _loc10_ = int((int(_loc6_) & 1073741823) % class_719.var_269);
            _loc4_ = var_496[_loc9_][_loc10_];
            if(_loc4_.var_252 == 0)
            {
               _loc11_ = true;
               _loc12_ = -2;
               while(_loc12_ < 2)
               {
                  _loc12_++;
                  _loc13_ = _loc12_;
                  _loc14_ = -4;
                  while(_loc14_ < 4)
                  {
                     _loc14_++;
                     _loc15_ = _loc14_;
                     _loc16_ = _loc9_ + _loc13_;
                     _loc17_ = _loc10_ + _loc15_;
                     if(method_222(_loc16_,_loc17_))
                     {
                        _loc18_ = var_496[_loc16_][_loc17_];
                        if(_loc18_.var_757 != null || _loc18_.var_252 != 0)
                        {
                           _loc11_ = false;
                           break;
                        }
                     }
                  }
               }
               if(_loc11_)
               {
                  _loc19_ = new class_744(this,_loc4_,_loc5_);
                  var_232.method_124(_loc19_,class_719.var_207);
                  var_1546.push(_loc19_);
               }
            }
         }
         _loc7_ = 0;
         _loc8_ = class_719.var_268;
         while(_loc7_ < _loc8_)
         {
            _loc7_++;
            _loc9_ = _loc7_;
            _loc10_ = 0;
            _loc12_ = class_719.var_269;
            while(_loc10_ < _loc12_)
            {
               _loc10_++;
               _loc13_ = _loc10_;
               _loc4_ = var_496[_loc9_][_loc13_];
               if(_loc4_.var_252 == 0)
               {
                  var_1545.push(_loc4_);
                  _loc20_ = method_1151(_loc4_);
                  _loc18_ = var_496[_loc9_][_loc13_ - 1];
                  var_232.method_124(_loc20_,class_719.var_207);
               }
            }
         }
         var_232.method_519(class_719.var_207);
         method_1146(true);
      }
   }
}
