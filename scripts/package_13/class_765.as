package package_13 {
import flash.display.Sprite;
import flash.text.TextField;

import package_24.class_656;
import package_24.class_878;

import package_32.App;
import package_32.class_899;

public class class_765 extends Sprite {

  public static var var_214:class_765;


  public var var_431:int;

  public var var_834:class_911;

  public var name_3:int;

  public var var_302:Boolean;

  public var var_836:TextField;

  public var var_432:int;

  public var name_78:Object;

  public var package_31:Object;

  public var var_41:class_656;

  public function class_765(param1:int = 0) {
    if (class_899.var_239) {
      return;
    }
    class_765.var_214 = this;
    super();
    var_431 = int(class_742.var_216 * 0.5);
    var_432 = int(class_742.var_323 * 0.5);
    App.rootMc.addChild(this);
    var_302 = false;
    graphics.beginFill(12613398);
    graphics.drawRect(0, 0, var_431, var_432);
    scaleX = 2;
    scaleY = 2;
    name_78 = method_596();
    name_78.var_41.y = var_432 * 0.5;
    var_834 = new class_911(param1);
    var_834.method_95();
    name_3 = 0;
    method_583(class_808.var_838);
  }

  public function method_79():void {
    switch (name_3) {
      case 0:
        var_834.method_79();
        if (var_834.var_302) {
          name_3 = name_3 + 1;
          method_583(class_808.var_835);
          break;
        }
        break;
      case 1:
        if (package_31 != null) {
          var_302 = true;
          name_3 = 2;
          break;
        }
    }
    name_78.var_41.method_79();
  }

  public function method_584():void {
    if (int(package_31._dailyPlays) > 0) {
      package_31._dailyPlays = int(package_31._dailyPlays) - 1;
    } else {
      package_31._plays = int(package_31._plays) - 1;
    }
  }

  public function method_583(param1:String):void {
    var _loc2_:* = name_78;
    _loc2_.GLWA.text = param1;
    _loc2_.GLWA.width = Number(_loc2_.GLWA.textWidth + 3);
    _loc2_.GLWA.x = -(int(_loc2_.GLWA.width * 0.5));
  }

  public function method_585(param1:int, param2:int, param3:_IslandStatus):void {
    package_31._islands[param1 * class_911.var_214.var_307 + param2] = param3;
  }

  public function method_209():void {
    MtCodec.apiAction(class_905.var_316 + "/game/start", package_31, method_586);
  }

  public function method_586(param1:Object):void {
    package_31 = param1;
  }

  public function method_185(param1:_PlayerAction, param2:class_719):void {
    var _loc5_:int = 0;
    var _loc6_:Boolean = false;
    var _loc7_:Array = null;
    var _loc8_:* = null;
    var _loc9_:_Reward = null;
    var _loc10_:_IslandStatus = null;
    var _loc11_:Array = null;
    var _loc12_:_Reward = null;
    var _loc13_:int = 0;
    var _loc14_:_IslandBonus = null;
    var _loc3_:class_719 = class_652.var_214.var_267;
    var _loc4_:Array = param1.var_296;
    switch (int(param1.var_295)) {
      case 0:
        _loc5_ = _loc4_[0];
        method_584();
        break;
      case 1:
        _loc6_ = _loc4_[0];
        _loc7_ = _loc4_[1];
        if (_loc6_) {
          method_587(class_652.var_214.var_321.var_215, class_652.var_214.var_267.var_319, class_652.var_214.var_267.var_318);
          break;
        }
        break;
      case 2:
        _loc5_ = _loc4_[0];
        _loc9_ = _loc4_[1];
        _loc10_ = _loc3_.method_589();
        _loc7_ = _loc10_.var_296;
        switch (int(_loc10_.var_295)) {
          default:
            break;
          case 1:
            _loc11_ = _loc7_[0];
            _loc12_ = _loc7_[1];
            method_585(_loc3_.var_319, _loc3_.var_318, _IslandStatus.method_272(_loc11_, null));
        }
        method_590(_loc9_);
        break;
      case 3:
        _loc5_ = _loc4_[0];
        _loc13_ = _loc4_[1];
        if (!method_591(_loc3_.var_319, _loc3_.var_318, true)) {
          package_31._rainbows = int(package_31._rainbows) - 1;
        }
        package_31._road.unshift({
          "_x": _loc5_,
          "_y": _loc13_
        });
        if (method_307(_loc5_, _loc13_) == _IslandStatus.var_392) {
          method_587(-1, _loc5_, _loc13_);
          break;
        }
        break;
      case 4:
        _loc8_ = package_31._savePoint;
        package_31._road.unshift(_loc8_);
        break;
      case 5:
        _loc7_ = _loc4_[0];
        _loc14_ = _loc4_[1];
        _loc5_ = 0;
        while (_loc5_ < int(_loc7_.length)) {
          _loc13_ = int(_loc7_[_loc5_]);
          _loc5_++;
          method_587(_loc13_, class_652.var_214.var_267.var_319, class_652.var_214.var_267.var_318);
        }
        method_592(_loc14_, -1);
        break;
      case 6:
        method_584();
        package_31._rainbows = int(package_31._rainbows) + 3;
        break;
      case 7:
        _loc6_ = _loc4_[0];
        package_31._rainbows = int(package_31._rainbows) - 1;
        break;
      case 8:
        if (method_588(_Item.var_424) && int(package_31._rainbows) > 0) {
          package_31._rainbows = int(package_31._rainbows) - 1;
          break;
        }
        method_584();
        break;
      case 9:
        _loc8_ = _loc4_[0];
        break;
      case 10:
        _loc5_ = _loc4_[0];
        package_31._god = _loc5_;
        package_31._savePoint = package_31._road[0];
        break;
      case 11:
        _loc5_ = _loc4_[0];
        if (_loc5_ == 1) {
          package_31._wid = int(package_31._wid) + 1;
          break;
        }
    }
    if (method_591(_loc3_.var_319, _loc3_.var_318)) {
      method_585(_loc3_.var_319, _loc3_.var_318, _IslandStatus.var_393);
    }
  }

  public function method_591(param1:int, param2:int, param3:Boolean = false):Boolean {
    var _loc7_:Array = null;
    var _loc8_:_Reward = null;
    var _loc4_:_IslandStatus = method_307(param1, param2);
    var _loc5_:* = var_834.method_203(var_834.method_175(param1, param2));
    var _loc6_:Array = _loc4_.var_296;
    switch (int(_loc4_.var_295)) {
      case 0:
        return false;
      case 1:
        _loc7_ = _loc6_[0];
        _loc8_ = _loc6_[1];
        return (_loc8_ == null || param3) && int(_loc7_.length) == int(_loc5_.name_79);
      case 2:
        return true;
    }
  }

  public function method_90(param1:int):Boolean {
    return int(package_31._god) == param1;
  }

  public function method_592(param1:_IslandBonus, param2:int):void {
    switch (int(param1.var_295)) {
      case 0:
        package_31._inv._volt = int(package_31._inv._volt) + param2;
        break;
      case 1:
        package_31._inv._fireball = int(package_31._inv._fireball) + param2;
        break;
      case 2:
        package_31._inv._tornado = int(package_31._inv._tornado) + param2;
    }
  }

  public function method_593(param1:_GameBonus, param2:int):void {
    switch (int(param1.var_295)) {
      case 0:
        package_31._inv._cheese = int(package_31._inv._cheese) + param2;
        break;
      case 1:
        package_31._inv._leaf = int(package_31._inv._leaf) + param2;
        break;
      case 2:
        package_31._inv._knife = int(package_31._inv._knife) + param2;
    }
  }

  public function method_172():void {
    var_41.method_89();
    parent.removeChild(this);
  }

  public function method_551():Boolean {
    return int(package_31._plays) + int(package_31._dailyPlays) > 0;
  }

  public function method_550():Boolean {
    return method_551() || int(package_31._rainbows) > 0 && Boolean(method_588(_Item.var_424));
  }

  public function method_594(param1:int):Boolean {
    var _loc4_:* = null;
    var _loc2_:int = 0;
    var _loc3_:Array = package_31._carts;
    while (_loc2_ < int(_loc3_.length)) {
      _loc4_ = _loc3_[_loc2_];
      _loc2_++;
      if (int(_loc4_._id) == param1) {
        return true;
      }
    }
    return false;
  }

  public function method_588(param1:_Item):Boolean {
    var _loc4_:_Item = null;
    var _loc2_:int = 0;
    var _loc3_:Array = package_31._items;
    while (_loc2_ < int(_loc3_.length)) {
      _loc4_ = _loc3_[_loc2_];
      _loc2_++;
      if (_loc4_ == param1) {
        return true;
      }
    }
    return false;
  }

  public function method_590(param1:_Reward):void {
    var _loc3_:_Item = null;
    var _loc4_:_IslandBonus = null;
    var _loc5_:int = 0;
    var _loc6_:_GameBonus = null;
    if (class_830.method_176(param1)) {
      package_31._inv._key = int(package_31._inv._key) - 1;
    }
    var _loc2_:Array = param1.var_296;
    switch (int(param1.var_295)) {
      case 0:
        _loc3_ = _loc2_[0];
        package_31._items.push(_loc3_);
        break;
      case 1:
        _loc4_ = _loc2_[0];
        method_592(_loc4_, 1);
        break;
      case 2:
        package_31._plays = int(package_31._plays) + class_885.var_311;
        break;
      case 3:
        package_31._hearts = int(package_31._hearts) + 1;
        break;
      case 4:
        _loc5_ = _loc2_[0];
        package_31._carts.push({
          "_id": _loc5_,
          "_lvl": 0
        });
        break;
      case 5:
        _loc5_ = _loc2_[0];
        package_31._carts.push({
          "_id": _loc5_,
          "_lvl": 0
        });
        break;
      case 6:
        package_31._plays = int(package_31._plays) + class_885.var_310;
        break;
      case 7:
        package_31._inv._key = int(package_31._inv._key) + 1;
        break;
      case 8:
        _loc6_ = _loc2_[0];
        method_593(_loc6_, 1);
    }
  }

  public function method_595():Array {
    var _loc4_:_Item = null;
    var _loc1_:Array = [];
    var _loc2_:int = 0;
    var _loc3_:Array = package_31._items;
    while (_loc2_ < int(_loc3_.length)) {
      _loc4_ = _loc3_[_loc2_];
      _loc2_++;
      switch (int(_loc4_.var_295)) {
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
        default:
          continue;
        case 21:
          _loc1_.push(0);
          continue;
        case 22:
          _loc1_.push(1);
          continue;
        case 23:
          _loc1_.push(2);
          continue;
        case 24:
          _loc1_.push(3);
          continue;
        case 25:
          _loc1_.push(4);
          continue;
        case 26:
          _loc1_.push(5);
          continue;
        case 27:
          _loc1_.push(6);

      }
    }
    return _loc1_;
  }

  public function method_596():Object {
    var_41 = new class_656();
    var_41.method_137(class_702.var_304.method_136("loading_bar"));
    var_41.x = var_431 * 0.5;
    addChild(var_41);
    var _loc2_:TextField = class_742.method_308(16772778, 8, -1, "nokia");
    _loc2_.y = -16;
    _loc2_.width = 80 * 2;
    var_41.addChild(_loc2_);
    return {
      "\x03\x1a!\x01": var_41,
      "GLWA": _loc2_
    };
  }

  public function method_597():int {
    return class_885.var_837 + int(int(package_31._hearts) / 4);
  }

  public function method_307(param1:int, param2:int):_IslandStatus {
    return package_31._islands[param1 * class_911.var_214.var_307 + param2];
  }

  public function method_587(param1:int, param2:int, param3:int):void {
    var _loc7_:Array = null;
    var _loc8_:_Reward = null;
    var _loc4_:int = param2 * class_911.var_214.var_307 + param3;
    var _loc5_:_IslandStatus = package_31._islands[_loc4_];
    var _loc6_:Array = _loc5_.var_296;
    switch (int(_loc5_.var_295)) {
      case 0:
        _loc7_ = [];
        if (param1 >= 0) {
          _loc7_ = [param1];
        }
        _loc5_ = _IslandStatus.method_272(_loc7_, var_834.method_175(param2, param3).var_309);
        break;
      case 1:
        _loc7_ = _loc6_[0];
        _loc8_ = _loc6_[1];
        _loc7_.push(param1);
        _loc5_ = _IslandStatus.method_272(_loc7_, _loc8_);
        break;
      case 2:
    }
    package_31._islands[_loc4_] = _loc5_;
  }
}
}
