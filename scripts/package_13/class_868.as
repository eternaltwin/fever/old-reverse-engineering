package package_13
{
   import flash.display.BlendMode;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.filters.GlowFilter;
   import flash.text.TextField;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_14.package_15.class_673;
   import package_14.package_15.class_680;
   import package_14.package_15.class_891;
   import package_14.package_15.class_920;
   import package_24.class_656;
   import package_24.class_738;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.App;
   import package_32.class_899;
   import package_8.package_9.class_814;
   import package_8.package_9.class_935;
   import package_9.class_676;
   import package_9.class_690;
   import package_9.class_847;
   
   public class class_868
   {
      
      public static var var_1289:int = 1056577;
      
      public static var var_320:int = 13;
      
      public static var var_1290:int = 74;
      
      public static var var_1291:int = 60;
      
      public static var var_214:class_868;
       
      
      public var var_371:int;
      
      public var var_1292:Object;
      
      public var var_1293:int;
      
      public var name_3:int;
      
      public var var_1297:Array;
      
      public var var_508:class_867;
      
      public var var_1300:Sprite;
      
      public var var_1199:Number;
      
      public var var_1305:class_738;
      
      public var var_1304:class_738;
      
      public var var_1299:class_755;
      
      public var var_1298:class_357;
      
      public var var_376:int;
      
      public var var_597:Boolean;
      
      public var var_1306:TextField;
      
      public var var_1308:TextField;
      
      public var GLWA:TextField;
      
      public var var_1294:Object;
      
      public var var_254:Number;
      
      public var var_1302:class_935;
      
      public var var_804:Array;
      
      public var var_1307:Sprite;
      
      public var var_1256:Array;
      
      public var var_1295:Boolean;
      
      public var var_1296:class_656;
      
      public function class_868()
      {
         var _loc2_:int = 0;
         var _loc3_:Sprite = null;
         if(class_899.var_239)
         {
            return;
         }
         class_868.var_214 = this;
         var_371 = int(class_742.var_216 * 0.5);
         var_1297 = [];
         var_1256 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = new Sprite();
            class_652.var_214.var_232.method_124(_loc3_,class_652.var_297);
            if(_loc2_ == 0)
            {
               _loc3_.graphics.beginFill(0,1);
               _loc3_.graphics.drawRect(0,0,class_742.var_216 * 0.5,class_868.var_320);
            }
            _loc3_.y = (class_742.var_323 * 0.5 - class_868.var_320) * _loc2_;
            var_1256.push({
               "U\\\x01":_loc3_,
               "\'~\x01":new class_755(_loc3_)
            });
         }
         var _loc4_:class_738 = new class_738();
         _loc4_.method_128(class_702.var_304.method_98(null,"inv"),0,0);
         _loc4_.y = 0;
         var_1256[1].var_232.method_124(_loc4_,0);
         var_1296 = new class_656();
         var_1296.method_137(class_702.var_304.method_136("bag"),false);
         var_1296.var_198.name_8(0);
         var_1296.x = 8;
         var_1296.y = 6;
         var_1256[1].var_232.method_124(var_1296,0);
         var _loc5_:class_738 = new class_738();
         _loc5_.method_128(class_702.var_304.method_98(0,"bonus_ground"));
         _loc5_.x = var_371 - 14;
         _loc5_.y = 6;
         var_1256[1].var_232.method_124(_loc5_,0);
         var_1306 = class_742.method_308(16777215,8,-1,"nokia");
         var_1306.x = Number(_loc5_.x + 5);
         var_1306.text = "0";
         var_1256[1].var_232.method_124(var_1306,0);
         GLWA = class_742.method_308(16777215,8,-1,"nokia");
         GLWA.filters = [new GlowFilter(0,1,2,2,40)];
         var_1256[1].var_232.method_124(GLWA,0);
         var_508 = class_867.var_1287;
         var_1295 = false;
         method_195();
         App.rootMc.stage.addEventListener(MouseEvent.CLICK,name_5);
      }
      
      public function method_975(param1:_Item) : void
      {
         var _loc6_:* = null;
         var _loc2_:* = class_765.var_214.package_31;
         var _loc3_:Boolean = int(_loc2_._plays) + int(_loc2_._dailyPlays) > 0;
         var _loc4_:Boolean = int(_loc2_._rainbows) > 0;
         switch(int(param1.var_295))
         {
            default:
               break;
            case 1:
               new class_920();
               break;
            case 2:
            default:
            default:
               method_467(class_808.var_1022);
               method_974(3);
               break;
            case 5:
            default:
            default:
               method_467(class_808.var_1022);
               method_974(5);
               break;
            case 8:
               new class_891();
               break;
            case 9:
               new class_680();
               break;
            case 10:
               if(int(_loc2_._rainbows) > 3)
               {
                  method_467(class_808.var_1026);
                  break;
               }
               if(!_loc3_)
               {
                  method_467(class_808.var_1023);
                  break;
               }
               class_652.var_214.method_186(_PlayerAction._Prism);
               new class_847();
               break;
            case 11:
               if(int(_loc2_._carts.length) == 0)
               {
                  method_467(class_808.var_1024);
                  break;
               }
               if(!class_765.var_214.method_550())
               {
                  method_467(class_808.var_1023);
                  break;
               }
               class_652.var_214.method_198();
               break;
            case 12:
               method_467(class_808.var_1022);
               method_974(4);
               break;
            case 13:
            default:
            default:
            default:
               if(int(_loc2_._rainbows) <= 0)
               {
                  method_467(class_808.var_619);
                  break;
               }
               new class_673();
               break;
            case 17:
            default:
            default:
               method_467(class_808.var_1022);
               break;
            case 20:
               _loc6_ = _loc2_._savePoint;
               if(_loc6_ == null)
               {
                  method_467(class_808.var_1025);
                  break;
               }
               method_973();
               class_652.var_214.method_186(_PlayerAction._Teleport);
               new class_690();
               method_195();
               break;
         }
      }
      
      public function method_979() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:* = null;
         switch(name_3)
         {
            case 0:
               var_254 = Number(Math.min(Number(var_254 + 0.1),1));
               _loc1_ = 0.5 - Math.cos(var_254 * 3.14) * 0.5;
               _loc2_ = class_742.var_323 * 0.5 - class_868.var_320;
               var_1256[1].class_319.y = _loc2_ - _loc1_ * class_868.var_1290;
               class_657.name_18(class_652.var_214.var_267,0,-(int(class_868.var_1291 * var_254)));
               if(var_254 == 1)
               {
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 1:
               _loc3_ = method_976();
               if(_loc3_ != var_1294)
               {
                  method_400(_loc3_);
               }
               if(var_1294 == null)
               {
                  if(var_1256[1].class_319.mouseY < 0)
                  {
                     method_977(class_808.var_1036);
                     break;
                  }
                  method_869();
                  break;
               }
               break;
            case 2:
               var_254 = Number(Math.min(Number(var_254 + 0.1),1));
               _loc1_ = Number(0.5 + Math.cos(var_254 * 3.14) * 0.5);
               _loc2_ = class_742.var_323 * 0.5 - class_868.var_320;
               var_1256[1].class_319.y = _loc2_ - _loc1_ * class_868.var_1290;
               class_657.name_18(class_652.var_214.var_267,0,-(int(class_868.var_1291 * (1 - var_254))));
               if(var_254 == 1)
               {
                  method_978();
                  break;
               }
         }
      }
      
      public function method_79() : void
      {
         var _loc2_:Boolean = false;
         var _loc3_:* = null;
         if(var_597)
         {
            return;
         }
         switch(int(var_508.var_295))
         {
            case 0:
               _loc2_ = var_1256[1].class_319.mouseY > 0;
               if(!class_652.var_214.var_300)
               {
                  _loc2_ = false;
               }
               if(var_1295 != _loc2_)
               {
                  var_1295 = _loc2_;
                  method_132(!!var_1295?class_808.var_1035:null);
                  var_1296.var_198.name_8(!!var_1295?1:-1);
                  break;
               }
               break;
            case 1:
               method_979();
         }
         if(var_1292 != null)
         {
            GLWA.visible = var_1292 % 10 < 7;
            _loc3_ = var_1292;
            var_1292 = var_1292 - 1;
            if(_loc3_ == 0)
            {
               var_1292 = null;
               GLWA.textColor = 16777215;
               method_869();
               GLWA.visible = true;
            }
         }
      }
      
      public function method_980() : void
      {
         method_684();
         method_869();
         if(var_1294 == null)
         {
            return;
         }
         var_1294.var_618.filters = [];
         var_1294 = null;
      }
      
      public function method_183() : void
      {
         if(var_1298 != null)
         {
            return;
         }
         var_1298 = new class_357();
         var_1298.x = class_742.var_216 - 12;
         var_1298.y = 36;
         App.rootMc.addChild(var_1298);
         var_1298.blendMode = BlendMode.ADD;
      }
      
      public function method_467(param1:String, param2:int = 16711680) : void
      {
         GLWA.textColor = param2;
         method_977(class_808.package_21(param1,class_657.method_244(param2)));
         var_1292 = 60;
      }
      
      public function method_977(param1:String) : void
      {
         GLWA.textColor = 16777215;
         GLWA.visible = true;
         var_1292 = null;
         method_132(param1);
      }
      
      public function method_191(param1:Boolean) : void
      {
         var_597 = param1;
         method_684();
      }
      
      public function method_400(param1:Object) : void
      {
         if(var_1294 != null)
         {
            method_980();
         }
         if(param1 == null || !param1.var_234)
         {
            return;
         }
         method_977(param1.var_884);
         var_1294 = param1;
         var_1299.method_110(var_1294.var_201);
         var _loc3_:GlowFilter = new GlowFilter(0,1,2,2,16);
         var _loc4_:GlowFilter = new GlowFilter(16777215,1,2,2,16);
         var_1294.var_618.filters = [_loc3_,_loc4_,_loc3_];
         method_386(param1.name_130);
      }
      
      public function method_984() : void
      {
         var _loc5_:* = null;
         var _loc7_:int = 0;
         var _loc8_:Boolean = false;
         var _loc10_:int = 0;
         var _loc12_:_Item = null;
         var _loc13_:* = null;
         var _loc17_:int = 0;
         var _loc18_:class_738 = null;
         method_684();
         name_3 = 0;
         var_254 = 0;
         var_508 = class_867.var_1288;
         class_652.var_214.method_135(false);
         method_869();
         var_1300 = new Sprite();
         var_1300.y = 24;
         var_1256[1].var_232.method_124(var_1300,1);
         var_1299 = new class_755(var_1300);
         var_804 = [];
         var _loc1_:* = class_765.var_214.package_31;
         var _loc2_:* = _loc1_._inv;
         var _loc3_:Array = [int(_loc2_._cheese),int(_loc2_._leaf),int(_loc2_._knife)];
         var _loc4_:int = 0;
         while(_loc4_ < 3)
         {
            _loc4_++;
            var var_343:Array = [_loc4_];
            _loc5_ = method_981(int(_loc3_[int(var_343[0])]));
            _loc5_.var_618.method_128(class_702.var_304.method_98(int(var_343[0]),"bonus_game"));
            _loc5_.var_201.x = 12;
            _loc5_.var_201.y = int(var_343[0]) * 16;
            _loc5_.var_884 = class_808.var_313[int(var_343[0])];
            _loc5_.name_130 = class_808.var_1019[int(var_343[0])];
            _loc5_.var_303 = function(param1:Array):Function
            {
               var var_343:Array = param1;
               return function():void
               {
                  class_868.var_214.method_974(int(var_343[0]));
               };
            }(var_343);
         }
         var _loc6_:Array = [int(_loc2_._volt),int(_loc2_._fireball),int(_loc2_._tornado)];
         _loc4_ = 0;
         while(_loc4_ < 3)
         {
            _loc4_++;
            var_343 = [_loc4_];
            _loc5_ = method_981(int(_loc6_[int(var_343[0])]));
            _loc5_.var_618.method_128(class_702.var_304.method_98(int(var_343[0]),"bonus_island"));
            _loc5_.var_201.x = 36;
            _loc5_.var_201.y = int(var_343[0]) * 16;
            _loc5_.var_884 = class_808.var_312[int(var_343[0])];
            _loc5_.name_130 = class_808.var_1018[int(var_343[0])];
            var var_214:Array = [this];
            _loc5_.var_303 = function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_343:Array = param2;
               return function():void
               {
                  var_214[0].method_982(int(var_343[0]));
               };
            }(var_214,var_343);
         }
         _loc4_ = 0;
         while(_loc4_ < 2)
         {
            _loc4_++;
            _loc7_ = _loc4_;
            _loc5_ = method_981(1);
            _loc5_.var_618.method_128(class_702.var_304.method_98(_loc7_,"bonus_daily"));
            _loc5_.var_201.x = 12 + _loc7_ * 24;
            _loc5_.var_201.y = 48;
            _loc5_.var_884 = class_808.var_1020[_loc7_];
            switch(_loc7_)
            {
               case 0:
                  §§push(false);
                  if(int(_loc2_._cheese) > 0 && int(_loc2_._leaf) > 0)
                  {
                     §§pop();
                     §§push(Boolean(int(_loc2_._knife) > 0));
                     break;
                  }
                  break;
               case 1:
                  §§push(false);
                  if(int(_loc2_._volt) > 0 && int(_loc2_._fireball) > 0)
                  {
                     §§pop();
                     §§push(Boolean(int(_loc2_._tornado) > 0));
                     break;
                  }
            }
            _loc8_ =; §§pop();
            if(_loc8_)
            {
               _loc5_.name_130 = class_808.var_1021[_loc7_ + 2];
            }
            else
            {
               class_658.method_248(_loc5_.var_201);
               _loc5_.name_130 = class_808.var_1021[_loc7_];
            }
         }
         var _loc9_:Array = [];
         _loc4_ = 28;
         _loc7_ = 0;
         while(_loc7_ < _loc4_)
         {
            _loc7_++;
            _loc10_ = _loc7_;
            _loc9_.push(false);
         }
         _loc7_ = 0;
         var _loc11_:Array = _loc1_._items;
         while(_loc7_ < int(_loc11_.length))
         {
            _loc12_ = _loc11_[_loc7_];
            _loc7_++;
            _loc9_[int(_loc12_.var_295)] = true;
         }
         _loc7_ = 7;
         var var_1301:Array = Type.method_253(_Item);
         _loc10_ = 0;
         while(_loc10_ < _loc4_)
         {
            _loc10_++;
            var_343 = [_loc10_];
            _loc5_ = method_981(Boolean(_loc9_[int(var_343[0])])?1:0);
            _loc5_.var_618.method_128(class_702.var_304.method_98(int(var_343[0]),"items"));
            _loc5_.var_201.x = 60 + int(int(var_343[0]) % _loc7_) * 16;
            _loc5_.var_201.y = int(int(var_343[0]) / _loc7_) * 16;
            _loc5_.var_884 = class_808.var_1016[int(var_343[0])];
            _loc5_.name_130 = class_808.var_1017[int(var_343[0])];
            _loc5_.var_303 = function(param1:Array):Function
            {
               var var_343:Array = param1;
               return function():void
               {
                  class_868.var_214.method_975(Type.method_254(_Item,var_1301[int(var_343[0])]));
               };
            }(var_343);
            if(int(var_343[0]) == int(_Item.var_423.var_295) && Boolean(_loc9_[int(var_343[0])]))
            {
               _loc13_ = method_983();
               if(_loc13_ != null)
               {
                  var_1302 = new class_935(_loc5_.var_618,Number([0.25,0.1,0.02][_loc13_]));
               }
            }
         }
         _loc5_ = method_981(1);
         _loc10_ = int(int(_loc1_._hearts) % 4);
         _loc5_.var_618.method_128(class_702.var_304.method_98(_loc10_,"big_heart"));
         _loc5_.var_201.x = 183;
         _loc5_.var_201.y = 6;
         _loc5_.var_884 = class_808.var_1029[0];
         _loc5_.name_130 = class_808.var_1029[1];
         if(_loc10_ > 0)
         {
            _loc5_.var_884 = _loc10_ + class_808.var_1029[2];
            _loc5_.name_130 = class_808.var_1029[3];
         }
         var _loc14_:int = int(class_765.var_214.method_597());
         var _loc15_:int = 0;
         var _loc16_:int = class_885.var_837 + class_885.var_1303;
         while(_loc15_ < _loc16_)
         {
            _loc15_++;
            _loc17_ = _loc15_;
            _loc18_ = new class_738();
            var_1299.method_124(_loc18_,1);
            _loc18_.x = Number(_loc5_.var_201.x - 9 + int(_loc17_ % 3) * 9);
            _loc18_.y = Number(Number(_loc5_.var_201.y + 21) + int(_loc17_ / 3) * 8);
            _loc18_.method_128(class_702.var_304.method_98(_loc17_ < _loc14_?0:2,"heart"));
         }
      }
      
      public function method_195() : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_738 = null;
         var _loc9_:TextField = null;
         var _loc10_:class_738 = null;
         var _loc11_:int = 0;
         var _loc1_:class_755 = var_1256[0].var_232;
         var _loc2_:* = class_765.var_214.package_31;
         while(int(var_1297.length) > 0)
         {
            var_1297.pop().method_89();
         }
         var_1304 = null;
         var_1305 = null;
         var _loc3_:int = 7;
         var _loc4_:Array = [];
         if(int(_loc2_._plays) <= 10)
         {
            _loc5_ = 0;
            _loc6_ = int(_loc2_._plays);
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc4_.push(1);
            }
         }
         else
         {
            _loc8_ = method_985(1);
            _loc8_.x = _loc3_;
            _loc9_ = class_742.method_308(16777215,8,-1,"nokia");
            _loc9_.text = class_691.method_97(int(_loc2_._plays));
            _loc9_.x = _loc3_ + 5;
            _loc9_.width = Number(_loc9_.textWidth + 3);
            _loc10_ = new class_738();
            _loc10_.addChild(_loc9_);
            _loc1_.method_124(_loc10_,0);
            var_1297.push(_loc10_);
            var_1305 = _loc8_;
            _loc3_ = int(Number(Number(_loc9_.x + _loc9_.width) + 7));
         }
         _loc5_ = 0;
         _loc6_ = int(_loc2_._dailyPlays);
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc7_ = _loc5_;
            _loc4_.push(0);
         }
         _loc5_ = 0;
         _loc6_ = 0;
         while(_loc6_ < int(_loc4_.length))
         {
            _loc7_ = int(_loc4_[_loc6_]);
            _loc6_++;
            _loc8_ = method_985(_loc7_);
            _loc8_.x = _loc3_ + _loc5_ * 11;
            _loc5_++;
            var_1304 = _loc8_;
            if(_loc7_ == 1)
            {
               var_1305 = _loc8_;
            }
         }
         _loc1_.method_519(0);
         var_1199 = 200;
         _loc6_ = 0;
         _loc7_ = int(_loc2_._rainbows);
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc11_ = _loc6_;
            _loc8_ = new class_738();
            _loc1_.method_124(_loc8_,0);
            _loc8_.method_128(class_702.var_304.method_98(null,"rainbow"));
            _loc8_.x = var_371 - (10 + _loc11_ * 11);
            _loc8_.y = 7;
            var_1199 = _loc8_.x;
            class_658.var_146(_loc8_,2,4,0);
            var_1297.push(_loc8_);
         }
         _loc6_ = int(_loc2_._inv._key);
         if(_loc6_ > 9)
         {
            _loc6_ = 9;
         }
         var_1306.text = class_691.method_97(_loc6_);
      }
      
      public function method_132(param1:String = undefined) : void
      {
         if(param1 == null)
         {
            param1 = class_652.var_214.var_288.var_267.method_131();
         }
         GLWA.htmlText = param1;
         GLWA.width = Number(GLWA.textWidth + 3);
         GLWA.x = int((var_371 - GLWA.width) * 0.5);
      }
      
      public function method_982(param1:int) : void
      {
         var _loc2_:class_676 = new class_676(param1);
         if(_loc2_.var_144)
         {
            method_973();
         }
      }
      
      public function method_974(param1:int) : void
      {
      }
      
      public function method_978() : void
      {
         var_508 = class_867.var_1287;
         var_1295 = true;
      }
      
      public function method_202() : void
      {
         if(var_1298 == null)
         {
            return;
         }
         new class_814(var_1298);
         var_1298 = null;
      }
      
      public function method_684() : void
      {
         if(var_1307 == null)
         {
            return;
         }
         var_1307.visible = false;
      }
      
      public function method_190() : Number
      {
         return var_1256[1].class_319.y - class_868.var_320;
      }
      
      public function method_283() : class_738
      {
         return var_1305;
      }
      
      public function method_870() : class_738
      {
         var _loc1_:class_738 = var_1304;
         if(_loc1_ == null)
         {
            _loc1_ = method_985(1);
            _loc1_.x = 7;
         }
         return _loc1_;
      }
      
      public function method_985(param1:Object) : class_738
      {
         var _loc2_:class_738 = new class_738();
         var_1256[0].var_232.method_124(_loc2_,0);
         _loc2_.method_128(class_702.var_304.method_98(param1,"icecube"));
         _loc2_.y = 7;
         var_1297.push(_loc2_);
         return _loc2_;
      }
      
      public function method_976() : Object
      {
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc1_:int = 0;
         var _loc2_:Array = var_804;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc4_ = Number(Math.max(Number(Math.abs(_loc3_.var_618.mouseX)),Number(Math.abs(_loc3_.var_618.mouseY))));
            if(_loc4_ < _loc3_.var_618.width * 0.5)
            {
               return _loc3_;
            }
         }
         return null;
      }
      
      public function method_983() : Object
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:* = null;
         var _loc12_:_IslandStatus = null;
         var _loc13_:* = null;
         var _loc14_:Array = null;
         var _loc15_:int = 0;
         var _loc2_:int = 2 * 2 + 1;
         var _loc3_:* = null;
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = 0;
            while(_loc6_ < _loc2_)
            {
               _loc6_++;
               _loc7_ = _loc6_;
               _loc8_ = _loc5_ - 2;
               _loc9_ = _loc7_ - 2;
               _loc10_ = int(Number(Math.max(Number(Math.abs(_loc8_)),Number(Math.abs(_loc9_)))));
               if(_loc3_ != null && _loc3_ <= _loc10_)
               {
                  continue;
               }
               _loc11_ = class_911.method_127(class_652.var_214.var_267.var_319 + _loc8_,class_652.var_214.var_267.var_318 + _loc9_);
               _loc12_ = class_765.var_214.method_307(int(_loc11_.var_244),int(_loc11_.var_245));
               _loc13_ = class_911.var_214.method_175(int(_loc11_.var_244),int(_loc11_.var_245));
               if(_loc13_.var_309 == null)
               {
                  continue;
               }
               _loc14_ = _loc13_.var_309.var_296;
               switch(int(_loc13_.var_309.var_295))
               {
                  default:
                  default:
                  default:
                  default:
                     continue;
                  case 4:
                     _loc15_ = _loc14_[0];
                     if(!class_765.var_214.method_594(_loc15_))
                     {
                        _loc3_ = _loc10_;
                     }

               }
            }
         }
         return _loc3_;
      }
      
      public function method_981(param1:int) : Object
      {
         var _loc5_:TextField = null;
         var _loc2_:Sprite = new Sprite();
         var _loc3_:class_738 = new class_738();
         var_1299.method_124(_loc2_,0);
         _loc2_.addChild(_loc3_);
         var _loc4_:* = {
            "2f7":null,
            "\x01^\x01":_loc3_,
            "eEY\x01":_loc2_,
            "\x1ba\x19A\x02":true,
            "\x06)$l\x01":"nom",
            "6J0m\x01":"description"
         };
         if(param1 == 0)
         {
            class_657.gfof(_loc3_,1,class_868.var_1289);
            _loc4_.var_234 = false;
         }
         else if(param1 > 1)
         {
            _loc5_ = class_742.method_308(16777215,8,-1,"nokia");
            _loc2_.addChild(_loc5_);
            _loc5_.x = 1;
            _loc5_.y = -1;
            _loc5_.text = class_691.method_97(param1);
            _loc5_.filters = [new GlowFilter(0,1,2,2,40)];
         }
         var_804.push(_loc4_);
         return _loc4_;
      }
      
      public function method_386(param1:String) : void
      {
         if(var_1307 == null)
         {
            var_1307 = new Sprite();
            var_1256[1].var_232.method_124(var_1307,1);
            var_1308 = class_742.method_308(16777215,8,-1,"nokia");
            var_1308.multiline = true;
            var_1308.wordWrap = true;
            var_1308.x = 4 + 1;
            var_1308.y = 4;
            var_1307.addChild(var_1308);
         }
         var_1307.visible = true;
         var_1308.width = 180;
         var_1308.htmlText = class_808.package_21(param1,"#CCCCCC");
         var_1308.width = Number(var_1308.textWidth + 5);
         var_1308.height = Number(var_1308.textHeight + 4);
         var_1307.graphics.clear();
         var_1307.graphics.beginFill(0);
         var_1307.graphics.drawRect(0,0,Number(var_1308.width + 4 * 2),Number(var_1308.height + 4 * 2));
         var_1307.x = int((class_742.var_216 * 0.5 - var_1307.width) * 0.5);
         var_1307.y = -(var_1307.height + 4);
      }
      
      public function method_973() : void
      {
         name_3 = 2;
         var_254 = 0;
         var_804 = [];
         method_980();
         var_1300.parent.removeChild(var_1300);
      }
      
      public function name_5(param1:*) : void
      {
         if(var_597)
         {
            return;
         }
         switch(int(var_508.var_295))
         {
            case 0:
               if(!!var_1295 && class_652.var_214.var_300 && Boolean(class_652.var_214.var_288.method_683()))
               {
                  method_984();
                  break;
               }
               break;
            case 1:
               if(name_3 == 1)
               {
                  if(var_1294 != null && var_1294.var_303 != null)
                  {
                     var_1294.var_303();
                     break;
                  }
                  if(var_1256[1].class_319.mouseY < 0)
                  {
                     class_652.var_214.method_135(true);
                     method_973();
                     break;
                  }
                  break;
               }
         }
      }
      
      public function method_869() : void
      {
         method_132("");
      }
   }
}
