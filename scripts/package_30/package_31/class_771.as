package package_30.package_31
{
   import package_32.class_899;
   
   public final class class_771
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["\'Ere\x03","MR\x10\x01\x02","\bG\x12\x0f\x01","\'\\;\x11\x02","Y\x13H\x0f\x03","g\x19I9\x03"];
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function class_771(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_637(param1:class_771) : class_771
      {
         return new class_771("\'\\;\x11\x02",3,[param1]);
      }
      
      public static function method_638(param1:class_771) : class_771
      {
         return new class_771("\bG\x12\x0f\x01",2,[param1]);
      }
      
      public static function method_639(param1:Array) : class_771
      {
         return new class_771("\'Ere\x03",0,[param1]);
      }
      
      public static function method_640(param1:Object) : class_771
      {
         return new class_771("MR\x10\x01\x02",1,[param1]);
      }
      
      public static function method_641(param1:Object, param2:class_771) : class_771
      {
         return new class_771("g\x19I9\x03",5,[param1,param2]);
      }
      
      public static function method_642(param1:Array) : class_771
      {
         return new class_771("Y\x13H\x0f\x03",4,[param1]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
