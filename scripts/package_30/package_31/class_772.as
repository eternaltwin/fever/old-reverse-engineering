package package_30.package_31
{
   import package_32.class_899;
   
   public final class class_772
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["\r}=\x02","\x17\x0f9-\x03","^\x14\x19c\x01","IX\x17Y\x02"];
      
      public static var var_873:class_772;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function class_772(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_643(param1:String, param2:Object, param3:class_770, param4:int, param5:int) : class_772
      {
         return new class_772("^\x14\x19c\x01",2,[param1,param2,param3,param4,param5]);
      }
      
      public static function method_644(param1:Object, param2:int) : class_772
      {
         return new class_772("\x17\x0f9-\x03",1,[param1,param2]);
      }
      
      public static function method_645(param1:String, param2:Object, param3:String, param4:int, param5:int) : class_772
      {
         return new class_772("IX\x17Y\x02",3,[param1,param2,param3,param4,param5]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
