package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_706 extends class_645
   {
      
      public static var var_588:int = 75;
      
      public static var var_589:int = 416;
      
      public static var var_590:int = 88;
      
      public static var var_434:int = 80;
       
      
      public var var_591:Number;
      
      public var var_599:Number;
      
      public var var_592:class_795;
      
      public var var_600:int;
      
      public var var_66:int;
      
      public var var_596:Number;
      
      public var var_595:Number;
      
      public var var_593:MovieClip;
      
      public var var_594:Object;
      
      public var var_597:int;
      
      public var var_598:Boolean;
      
      public function class_706()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_376() : void
      {
         var _loc2_:class_892 = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc1_:* = var_592.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            if(_loc2_.var_244 < class_706.var_588 + var_66 || _loc2_.var_244 > class_706.var_589 - var_66)
            {
               _loc2_.var_244 = Number(class_663.method_99(class_706.var_588 + var_66,_loc2_.var_244,class_706.var_589 - var_66));
               _loc2_.var_278 = _loc2_.var_278 * -1;
            }
            if(_loc2_.var_245 < var_66 || _loc2_.var_245 > class_706.var_590 - var_66)
            {
               _loc2_.var_245 = Number(class_663.method_99(var_66,_loc2_.var_245,class_706.var_590 - var_66));
               _loc2_.var_279 = _loc2_.var_279 * -1;
            }
            if(name_3 != 4 && name_3 != 6)
            {
               _loc3_ = _loc2_.var_244 - var_593.x;
               _loc4_ = _loc2_.var_245 - (var_593.y - 9);
               if(Number(Math.sqrt(Number(_loc3_ * _loc3_ + _loc4_ * _loc4_))) < var_66 + 6)
               {
                  name_3 = 5;
                  var_593.gotoAndPlay("lost");
                  method_81(false,40);
               }
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:MovieClip = null;
         var _loc6_:int = 0;
         var _loc8_:class_892 = null;
         if(var_594)
         {
            var_594 = false;
            _loc1_ = var_593.var_1;
            _loc1_ = var_593.var_1;
            _loc1_.gotoAndStop(int(class_691.method_114(_loc1_.totalFrames)) + 1);
         }
         var _loc2_:* = method_100();
         var _loc3_:Number = _loc2_.var_244 - var_595;
         var _loc4_:Number = _loc2_.var_245 - var_596;
         var _loc5_:Number = Number(Math.sqrt(Number(_loc3_ * _loc3_ + _loc4_ * _loc4_)));
         var_595 = Number(_loc2_.var_244);
         var_596 = Number(_loc2_.var_245);
         var_591 = Number(var_591 + _loc5_ * 0.15);
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(var_591 > 0)
               {
                  var_593.gotoAndStop("walk");
                  name_3 = 2;
               }
               method_376();
               break;
            case 2:
               if(var_591 < 0)
               {
                  var_597 = var_597 + 1;
               }
               else
               {
                  var_597 = 0;
               }
               while(var_591 > 0)
               {
                  var_591 = var_591 - 1;
                  var_593.nextFrame();
                  if(var_593.currentFrame == 30)
                  {
                     var_593.gotoAndStop("walk");
                  }
                  var_593.x = var_593.x + 1;
                  var_201.x = (50 - var_593.x) * 4;
                  var_598 = true;
               }
               if(var_593.x > class_706.var_589 - 10)
               {
                  var_593.x = class_706.var_589;
                  var_593.gotoAndPlay("win");
                  name_3 = 6;
                  method_81(true,20);
               }
               if(var_597 == 3 && var_598)
               {
                  name_3 = 3;
                  var_593.gotoAndPlay("jutsu");
                  var_597 = 0;
               }
               method_376();
               break;
            case 3:
               _loc6_ = var_597;
               var_597 = var_597 + 1;
               if(_loc6_ > 6)
               {
                  name_3 = 4;
                  var_593.gotoAndStop("object");
                  var_594 = true;
                  method_377();
               }
               method_376();
               break;
            case 4:
               if(_loc5_ > 0.5)
               {
                  var_593.gotoAndStop("walk");
                  name_3 = 2;
                  method_377();
                  var_591 = -1;
                  var_598 = false;
               }
               method_376();
               break;
            case 5:
               method_378();
               break;
            case 6:
               method_376();
         }
         super.method_79();
         var _loc7_:* = var_592.method_73();
         while(_loc7_.method_74())
         {
            _loc8_ = _loc7_.name_1();
            _loc8_.method_230(2);
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - 100 * param1];
         super.method_95(param1);
         var_600 = 1 + int(param1 * 10);
         var_599 = (0.5 + param1 * 0.5) * var_600;
         var_66 = 20;
         var_591 = 0;
         var_595 = 50;
         var_596 = 50;
         var_597 = 0;
         method_117();
         var_201.scaleY = Number(4);
         var_201.scaleX = 4;
      }
      
      public function method_377() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc1_:int = 0;
         while(_loc1_ < 12)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = new class_892(var_232.method_84(class_899.__unprotect__("B\x1d\x12\x02\x02"),2));
            _loc3_.var_278 = Math.random() * 2 - 1;
            _loc3_.var_244 = Number(var_593.x + _loc3_.var_278 * 5);
            _loc3_.var_245 = var_593.y - Math.random() * 18;
            _loc3_.var_250 = -(Math.random() * 0.1);
            _loc3_.var_243.gotoAndPlay(int(class_691.method_114(5)) + 1);
         }
      }
      
      public function method_378() : void
      {
         var _loc2_:class_892 = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc1_:* = var_592.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            _loc3_ = var_593.x - _loc2_.var_244;
            _loc4_ = var_593.y - 5 - _loc2_.var_245;
            _loc5_ = Number(Math.atan2(_loc4_,_loc3_));
            _loc2_.var_233 = 0.9;
            _loc6_ = 0.5;
            _loc2_.var_278 = Number(_loc2_.var_278 + Math.cos(_loc5_) * _loc6_);
            _loc2_.var_279 = Number(_loc2_.var_279 + Math.sin(_loc5_) * _loc6_);
         }
      }
      
      public function method_117() : void
      {
         var _loc7_:Boolean = false;
         var _loc8_:int = 0;
         var _loc9_:Number = NaN;
         var _loc10_:class_892 = null;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         class_319 = var_232.method_84(class_899.__unprotect__("\x1c[od"),0);
         var_593 = var_232.method_84(class_899.__unprotect__("WL\'\x14"),1);
         var_593.stop();
         var_593.y = class_706.var_434;
         var_593.x = 50;
         var_593.var_430 = 0;
         var_593.gotoAndStop("jutsu");
         var _loc1_:MovieClip = var_232.method_92(3);
         _loc1_.blendMode = BlendMode.ADD;
         _loc1_.alpha = 0.5;
         var _loc2_:MovieClip = new class_755(_loc1_).method_92(0);
         _loc2_.blendMode = BlendMode.LAYER;
         var _loc3_:class_755 = new class_755(_loc2_);
         var _loc4_:Array = [0];
         var _loc5_:Number = Number(Math.min(1 / (var_600 * 0.66),1));
         var _loc6_:Number = Number(Math.min(1 / (var_600 * 3),1));
         do
         {
            _loc4_ = class_742.method_379(var_600);
            _loc7_ = true;
            _loc8_ = 0;
            while(_loc8_ < int(_loc4_.length))
            {
               _loc9_ = Number(_loc4_[_loc8_]);
               _loc8_++;
               if(_loc9_ > _loc5_ || _loc9_ < _loc6_)
               {
                  _loc7_ = false;
               }
            }
         }
         while(!_loc7_);
         
         var_592 = new class_795();
         _loc8_ = 0;
         while(_loc8_ < int(_loc4_.length))
         {
            _loc9_ = Number(_loc4_[_loc8_]);
            _loc8_++;
            _loc10_ = new class_892(_loc3_.method_84(class_899.__unprotect__("9uWS\x01"),0));
            var_66 = 15;
            _loc10_.var_244 = Number(class_706.var_588 + var_66 + Math.random() * (class_706.var_589 - (class_706.var_588 + var_66 * 2)));
            _loc10_.var_245 = Number(var_66 + Math.random() * (class_706.var_590 - var_66 * 2));
            _loc11_ = Number(Number(0.77 + int(class_691.method_114(4)) * 1.57) + (Math.random() * 2 - 1) * 0.3);
            _loc12_ = var_599 * _loc9_;
            _loc10_.var_278 = Math.cos(_loc11_) * _loc12_;
            _loc10_.var_279 = Math.sin(_loc11_) * _loc12_;
            var_592.method_103(_loc10_);
         }
      }
   }
}
