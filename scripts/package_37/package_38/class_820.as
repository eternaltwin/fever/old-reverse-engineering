package package_37.package_38
{
   import package_32.class_899;
   import package_35.package_36._Fast.class_815;
   import package_35.package_36._Fast.class_816;
   import package_35.package_36._Fast.class_817;
   import package_35.package_36._Fast.class_818;
   import package_35.package_36._Fast.class_819;
   
   public class class_820
   {
       
      
      public var var_244:Xml;
      
      public var var_882:class_819;
      
      public var var_881:class_815;
      
      public var var_884:String;
      
      public var var_1071:String;
      
      public var var_1072:String;
      
      public var var_1073:class_818;
      
      public var name_107:class_817;
      
      public var name_78:Object;
      
      public var var_830:class_816;
      
      public function class_820(param1:Xml = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(param1.var_880 != Xml.class_771 && param1.var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "Invalid nodeType " + param1.var_880;
         }
         var_244 = param1;
         var_881 = new class_815(param1);
         var_882 = new class_819(param1);
         var_830 = new class_816(param1);
         name_107 = new class_817(param1);
         var_1073 = new class_818(param1);
      }
      
      public function method_131() : String
      {
         return var_244.var_880 == Xml.class_771?"Document":var_244.method_664();
      }
      
      public function method_771() : String
      {
         var _loc3_:Xml = null;
         var _loc1_:StringBuf = new StringBuf();
         var _loc2_:* = var_244.method_73();
         while(_loc2_.method_74())
         {
            _loc3_ = _loc2_.name_1();
            _loc1_.b = _loc1_.b + _loc3_.toString();
         }
         return _loc1_.b;
      }
      
      public function method_772() : String
      {
         var _loc1_:* = var_244.method_73();
         if(!_loc1_.method_74())
         {
            class_899.var_317 = new Error();
            throw method_131() + " does not have data";
         }
         var _loc2_:Xml = _loc1_.name_1();
         if(_loc1_.method_74())
         {
            class_899.var_317 = new Error();
            throw method_131() + " does not only have data";
         }
         if(_loc2_.var_880 != Xml.var_1062 && _loc2_.var_880 != Xml.var_1063)
         {
            class_899.var_317 = new Error();
            throw method_131() + " does not have data";
         }
         return _loc2_.method_766();
      }
      
      public function method_773() : Object
      {
         var var_331:Object = var_244.name_78();
         return {
            "\'*+x\x03":var_331.method_74,
            "?\x05\x10;\x01":function():class_820
            {
               var _loc1_:Xml = var_331.name_1();
               if(_loc1_ == null)
               {
                  return null;
               }
               return new class_820(_loc1_);
            }
         };
      }
   }
}
