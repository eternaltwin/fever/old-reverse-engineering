package
{
   import package_32.class_899;
   
   public final class class_670
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["(s\f\f\x03","$t %\x02"];
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function class_670(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_278(param1:int) : class_670
      {
         return new class_670("$t %\x02",1,[param1]);
      }
      
      public static function method_279(param1:Object, param2:int) : class_670
      {
         return new class_670("(s\f\f\x03",0,[param1,param2]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
