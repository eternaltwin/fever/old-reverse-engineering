package
{
   import flash.filters.GlowFilter;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_928 extends class_365
   {
      
      public static var var_1010:int = 88;
      
      public static var var_333:int = 90;
       
      
      public var var_318:int;
      
      public var var_319:int;
      
      public var var_439:int;
      
      public var var_215:int;
      
      public function class_928(param1:int = 0, param2:int = 0, param3:int = 0, param4:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_319 = param1;
         var_318 = param2;
         var_439 = param3;
         var_215 = param4;
         var _loc5_:Boolean = param4 == 4;
         gotoAndStop(!!_loc5_?2:1);
         GLWA.text = class_691.method_97(param3 + 1);
         GLWA.filters = [new GlowFilter(!!_loc5_?12131673:6761910,1,8,8,170)];
         method_1121();
      }
      
      public function method_824() : void
      {
         class_657.name_18(this,0,0);
      }
      
      public function method_1121() : void
      {
         x = class_928.var_1010 + var_319 * class_928.var_333;
         y = class_928.var_1010 + var_318 * class_928.var_333;
      }
      
      public function var_10() : void
      {
         class_657.name_18(this,0,40);
      }
   }
}
