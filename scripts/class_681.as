package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_681 extends class_645
   {
       
      
      public var var_481:Number;
      
      public var var_483:class_656;
      
      public var var_486:class_656;
      
      public var var_66:Number;
      
      public var var_487:Number;
      
      public var var_492:int;
      
      public var var_432:Number;
      
      public var var_490:Number;
      
      public var var_488:Array;
      
      public var var_484:MovieClip;
      
      public var var_485:MovieClip;
      
      public var var_482:Number;
      
      public var var_248:Number;
      
      public function class_681()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_309(param1:Number) : void
      {
         if(var_229[0])
         {
            var_482 = var_482 - var_248 * 0.2;
            var_482 = var_482 * 0.95;
            var_248 = Number(var_248 + var_482);
         }
         else
         {
            var_248 = Math.min(Number(Math.max(0,param1)),1) - 0.5;
         }
         var _loc2_:Number = Number(var_483.var_244 + Math.cos(var_248 - 1.57) * var_432);
         var _loc3_:Number = Number(var_483.var_245 + Math.sin(var_248 - 1.57) * var_432);
         var_484.graphics.clear();
         var_484.graphics.lineStyle(36,7557675,100);
         var_484.graphics.moveTo(var_483.var_244,var_483.var_245);
         var_484.graphics.curveTo(var_483.var_244,var_483.var_245 - var_432 * 0.5,_loc2_,_loc3_);
         var_485.graphics.clear();
         var_485.graphics.lineStyle(40,0,100);
         var_485.graphics.moveTo(var_483.var_244,var_483.var_245);
         var_485.graphics.curveTo(var_483.var_244,var_483.var_245 - var_432 * 0.5,_loc2_,_loc3_);
         var_486.var_243.rotation = var_248 / 0.0174;
         var_486.var_244 = _loc2_;
         var_486.var_245 = _loc3_;
      }
      
      public function method_310() : void
      {
         var _loc5_:class_892 = null;
         var _loc6_:MovieClip = null;
         var _loc1_:Number = var_487 - var_248;
         var _loc2_:Boolean = true;
         var _loc3_:int = 0;
         var _loc4_:Array = var_488;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(Number(_loc5_.var_489) > 0)
            {
               _loc2_ = false;
               _loc5_.var_244 = Number(var_486.var_244 + Math.cos(Number(var_248 + Number(_loc5_.var_65))) * _loc5_.var_8);
               _loc5_.var_245 = Number(var_486.var_245 + Math.sin(Number(var_248 + Number(_loc5_.var_65))) * _loc5_.var_8);
               _loc5_.var_489 = _loc5_.var_489 - Math.abs(_loc1_) * 10;
               if(Number(_loc5_.var_489) < 0)
               {
                  _loc5_.var_250 = 1;
                  _loc5_.var_278 = Number(_loc5_.var_278 + _loc1_ * 10);
               }
            }
            else if(_loc5_.var_245 > var_490 - var_66)
            {
               _loc5_.var_245 = var_490 - var_66;
               _loc5_.var_279 = _loc5_.var_279 * -0.5;
               _loc5_.var_278 = _loc5_.var_278 * 0.75;
               if(Boolean(_loc5_.var_491) != true)
               {
                  var_492 = var_492 - 1;
               }
               _loc5_.var_491 = true;
            }
            _loc5_.var_243.x = _loc5_.var_244;
            _loc5_.var_243.y = _loc5_.var_245;
         }
         if(!var_229[0] && _loc2_)
         {
            var_229 = [true];
            _loc6_ = class_319.var_1;
            _loc6_.play();
         }
         if(var_492 == 0)
         {
            method_81(true,10);
         }
      }
      
      override public function method_79() : void
      {
         super.method_79();
         method_309(method_100().var_244 / class_742.var_216);
         method_310();
         var_487 = var_248;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320 - param1 * 200];
         super.method_95(param1);
         var_481 = class_742.var_218 - 50;
         var_432 = 100;
         var_66 = 12;
         var_490 = class_742.var_218 - 12;
         var_248 = 0;
         var_487 = 0;
         var_482 = 0;
         var_492 = 8;
         method_117();
         method_309(0.5);
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Boolean = false;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:class_892 = null;
         var _loc16_:Number = NaN;
         var _loc17_:class_892 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x17u\x1dE\x01"),0);
         var _loc1_:MovieClip = class_319.var_1;
         _loc1_.stop();
         var_485 = var_232.method_92(class_645.var_209);
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("h\x19\x03-\x02"),class_645.var_209));
         var_483 = _loc2_;
         var_483.var_244 = class_742.var_216 * 0.5;
         var_483.var_245 = var_481;
         var_483.method_118();
         _loc2_ = new class_656(var_232.method_84(class_899.__unprotect__(" \'=\'\x02"),class_645.var_209));
         var_486 = _loc2_;
         var_486.var_244 = class_742.var_216 * 0.5;
         var_486.var_245 = var_481 - var_432;
         var_486.method_118();
         var_484 = var_232.method_92(class_645.var_209);
         var_488 = [];
         var _loc3_:int = 0;
         var _loc4_:int = var_492;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = 0;
            _loc7_ = 0;
            _loc8_ = 0;
            do
            {
               _loc8_++;
               _loc6_ = -Math.random() * 3.14;
               _loc7_ = Math.random() * 100;
               _loc9_ = Number(var_486.var_244 + Math.cos(Number(var_248 + _loc6_)) * _loc7_);
               _loc10_ = Number(var_486.var_245 + Math.sin(Number(var_248 + _loc6_)) * _loc7_);
               _loc11_ = true;
               _loc12_ = 0;
               _loc13_ = int(var_488.length);
               while(_loc12_ < _loc13_)
               {
                  _loc12_++;
                  _loc14_ = _loc12_;
                  _loc15_ = var_488[_loc14_];
                  _loc16_ = Number(_loc15_.method_115({
                     "L\x01":_loc9_,
                     "\x03\x01":_loc10_
                  }));
                  if(_loc16_ < 40 - _loc8_ * 0.1)
                  {
                     _loc11_ = false;
                     break;
                  }
               }
            }
            while(!_loc11_);
            
            _loc17_ = new class_892(var_232.method_84(class_899.__unprotect__("\x03X\x15\r"),class_645.var_209));
            _loc17_.var_233 = 0.99;
            _loc15_ = _loc17_;
            _loc15_.var_491 = false;
            _loc15_.var_65 = _loc6_;
            _loc15_.var_8 = _loc7_;
            _loc15_.var_244 = Number(var_486.var_244 + Math.cos(Number(var_248 + Number(_loc15_.var_65))) * _loc15_.var_8);
            _loc15_.var_245 = Number(var_486.var_245 + Math.sin(Number(var_248 + Number(_loc15_.var_65))) * _loc15_.var_8);
            _loc15_.var_489 = Number(100 + Math.random() * 200);
            _loc15_.method_118();
            var_488.push(_loc15_);
         }
         _loc1_ = var_232.method_84(class_899.__unprotect__("iif\x02\x03"),class_645.var_209);
         _loc1_.y = class_742.var_218;
      }
   }
}
