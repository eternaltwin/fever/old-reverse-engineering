package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.text.TextField;
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_24.class_738;
   import package_24.class_739;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_747;
   import package_8.package_9.class_814;
   import package_8.package_9.class_931;
   
   public class class_787 extends class_645
   {
      
      public static var var_268:int = 5;
      
      public static var var_269:int = 3;
      
      public static var var_934:int = 3;
      
      public static var var_935:int = 45;
       
      
      public var var_936:Array;
      
      public var var_251:int;
      
      public var name_55:Array;
      
      public var var_937:int;
      
      public var var_939:Array;
      
      public var GLWA:TextField;
      
      public var var_938:Array;
      
      public function class_787()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = var_251;
               var_251 = var_251 - 1;
               if(_loc1_ == 0)
               {
                  method_696();
                  break;
               }
               break;
            case 2:
               var_251 = var_251 - 1;
               class_657.gfof(class_319,(1 - var_251 / 30) * 0.5,0);
               if(var_251 == 0)
               {
                  method_697();
                  break;
               }
               break;
            case 3:
               var_251 = var_251 - 1;
               _loc1_ = int(Math.ceil(var_251 / 40));
               GLWA.text = class_691.method_97(_loc1_);
               GLWA.width = Number(GLWA.textWidth + 3);
               GLWA.height = Number(GLWA.textHeight + 3);
               GLWA.x = int((class_742.var_216 * 0.5 - GLWA.width) * 0.25) * 2;
               GLWA.y = int((class_742.var_218 * 0.5 - GLWA.height) * 0.25) * 2;
               if(var_251 == 0)
               {
                  method_698();
                  break;
               }
            case 4:
               var_251 = var_251 - 1;
               _loc1_ = int(Math.ceil(var_251 / 40));
               GLWA.text = class_691.method_97(_loc1_);
               GLWA.width = Number(GLWA.textWidth + 3);
               GLWA.height = Number(GLWA.textHeight + 3);
               GLWA.x = int((class_742.var_216 * 0.5 - GLWA.width) * 0.25) * 2;
               GLWA.y = int((class_742.var_218 * 0.5 - GLWA.height) * 0.25) * 2;
               if(var_251 == 0)
               {
                  method_698();
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_696() : void
      {
         var _loc1_:class_738 = null;
         while(int(var_938.length) > 0)
         {
            _loc1_ = var_938.pop();
            new class_814(_loc1_,int((_loc1_.x + _loc1_.y) * 0.2) - 20,5,true);
         }
         name_3 = name_3 + 1;
         var_251 = 30;
      }
      
      public function method_698() : void
      {
         var _loc5_:class_738 = null;
         name_3 = name_3 + 1;
         GLWA.visible = false;
         var_937 = int(var_939[name_55[0]]);
         var_939[name_55[0]] = var_936.pop();
         class_657.gfof(class_319,0,0);
         method_699();
         var _loc1_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = var_938;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(int(var_939[_loc1_]) >= 0)
            {
               _loc5_.addEventListener(MouseEvent.CLICK,function(param1:Function, param2:class_738, param3:int):Function
               {
                  var var_27:Function = param1;
                  var var_18:class_738 = param2;
                  var var_940:int = param3;
                  return function(param1:*):void
                  {
                     return var_27(var_18,var_940,param1);
                  };
               }(method_700,_loc5_,_loc1_));
               _loc5_.mouseEnabled = true;
               _loc5_.useHandCursor = true;
               _loc5_.buttonMode = true;
            }
            _loc1_++;
         }
      }
      
      public function method_697() : void
      {
         name_3 = name_3 + 1;
         GLWA = class_742.method_308(16777215,20,-1,"upheaval");
         GLWA.scaleY = Number(2);
         GLWA.scaleX = 2;
         var_251 = int(var_227[0] * 0.35);
         var_232.method_124(GLWA,2);
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc4_:int = 0;
         var _loc5_:class_738 = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc10_:* = null;
         var_227 = [500 - 100 * param1];
         super.method_95(param1);
         var_201.scaleY = Number(2);
         var_201.scaleX = 2;
         class_319 = new MovieClip();
         var _loc3_:int = 0;
         while(_loc3_ < 2)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = new class_738();
            _loc5_.x = _loc4_ * 200;
            _loc5_.method_128(class_702.var_571.method_98(null,"horde_bg"),0,0);
            class_319.addChild(_loc5_);
            if(_loc4_ == 0)
            {
               _loc5_.scaleY = _loc5_.scaleY * -1;
            }
            _loc5_.rotation = 90;
         }
         var_232.method_124(class_319,0);
         var_939 = [];
         _loc3_ = class_787.var_268 * class_787.var_269;
         var _loc6_:Array = [];
         var_936 = [];
         _loc4_ = 0;
         _loc7_ = class_787.var_935;
         while(_loc4_ < _loc7_)
         {
            _loc4_++;
            _loc8_ = _loc4_;
            var_936.push(_loc8_);
         }
         _loc4_ = 0;
         while(_loc4_ < _loc3_)
         {
            _loc4_++;
            _loc7_ = _loc4_;
            var_939.push(-1);
            _loc6_.push(_loc7_);
         }
         class_659.method_241(_loc6_);
         class_659.method_241(var_936);
         _loc4_ = class_787.var_934 + int(Math.round((_loc3_ - class_787.var_934) * Math.min(Number(Math.pow(param1,1.5)),1)));
         var _loc9_:Array = [];
         _loc7_ = 0;
         while(_loc7_ < _loc4_)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc10_ = _loc6_.pop();
            _loc9_.push(_loc10_);
            var_939[_loc10_] = var_936.pop();
         }
         name_55 = [_loc9_[int(class_691.method_114(int(_loc9_.length)))]];
         var_251 = int(var_227[0] * 0.3);
         method_699();
      }
      
      public function method_701(param1:int) : class_738
      {
         var _loc3_:class_738 = null;
         var _loc2_:class_738 = new class_738();
         _loc2_.method_128(class_702.var_571.method_98(null,"horde_card"));
         _loc2_.blendMode = BlendMode.LAYER;
         var_232.method_124(_loc2_,1);
         if(param1 >= 0)
         {
            _loc3_ = new class_738();
            _loc3_.method_128(class_702.var_571.method_98(param1,"horde_toys"));
            _loc2_.addChild(_loc3_);
         }
         return _loc2_;
      }
      
      public function method_699() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_738 = null;
         var_938 = [];
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = class_787.var_268;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = class_787.var_269;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = method_701(int(var_939[_loc1_]));
               var_938.push(_loc8_);
               _loc8_.x = 48 + _loc4_ * 26;
               _loc8_.y = 74 + _loc7_ * 28;
               _loc1_++;
            }
         }
      }
      
      public function method_700(param1:class_738, param2:int, param3:*) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:class_739 = null;
         var _loc11_:class_931 = null;
         var _loc12_:Array = null;
         var _loc13_:class_738 = null;
         var _loc14_:class_738 = null;
         var _loc15_:class_747 = null;
         if(var_220 != null)
         {
            return;
         }
         method_81(param2 == name_55[0],30);
         if(var_220)
         {
            var_939[name_55[0]] = var_937;
            _loc4_ = 12;
            _loc5_ = 8;
            _loc6_ = 0;
            while(_loc6_ < _loc4_)
            {
               _loc6_++;
               _loc7_ = _loc6_;
               _loc8_ = _loc7_ / _loc4_ * 6.28;
               _loc9_ = Number(0.1 + Math.random() * 3);
               _loc10_ = new class_739();
               _loc10_.method_137(class_702.package_9.method_136("spark_twinkle"));
               _loc10_.var_198.method_140();
               _loc10_.var_278 = Math.cos(_loc8_) * _loc9_;
               _loc10_.var_279 = Math.sin(_loc8_) * _loc9_;
               _loc10_.var_262 = Number(param1.x + _loc10_.var_278 * _loc5_ * 0.5);
               _loc10_.var_263 = Number(param1.y + _loc10_.var_279 * _loc5_);
               _loc10_.var_251 = 10 + int(class_691.method_114(20));
               _loc10_.var_233 = 0.94;
               _loc10_.var_250 = Number(0.05 + Math.random() * 0.1);
               _loc10_.method_118();
               var_232.method_124(_loc10_,3);
            }
            while(int(var_938.length) > 0)
            {
               var_938.pop().method_89();
            }
            method_699();
            _loc11_ = new class_931(var_938[name_55[0]]);
            _loc11_.var_146(2,4);
         }
         else
         {
            _loc4_ = 0;
            _loc5_ = 0;
            _loc12_ = var_938;
            while(_loc5_ < int(_loc12_.length))
            {
               _loc13_ = _loc12_[_loc5_];
               _loc5_++;
               _loc13_.alpha = 0.25;
               _loc13_.mouseEnabled = false;
               _loc13_.useHandCursor = false;
               if(_loc4_ == name_55[0])
               {
                  _loc13_.alpha = 1;
                  _loc14_ = method_701(var_937);
                  _loc14_.x = _loc13_.x;
                  _loc14_.y = _loc13_.y;
                  _loc15_ = new class_747(_loc14_,1000,6,6);
               }
               _loc4_++;
            }
         }
      }
   }
}
