package
{
   import flash.display.Sprite;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_864 extends class_645
   {
      
      public static var var_268:int = 7;
      
      public static var var_269:int = 6;
      
      public static var var_332:int = 50;
       
      
      public var var_1276:Array;
      
      public var var_1274:class_373;
      
      public var var_251:int;
      
      public var var_1275:int;
      
      public var var_318:Number;
      
      public var var_319:int;
      
      public var var_1214:Sprite;
      
      public var var_1277:int;
      
      public var name_36:int;
      
      public var var_496:Array;
      
      public var var_452:int;
      
      public function class_864()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_319 = int(class_663.method_256(0,int(Math.round((method_100().var_244 - 50) / class_864.var_332)),class_864.var_268 - 1));
               method_964();
               if(name_5)
               {
                  if(int(var_496[var_319][0]) == -1)
                  {
                     method_691();
                     break;
                  }
                  new class_931(class_319,0.1,16711680);
                  break;
               }
               break;
            case 2:
               var_318 = Number(var_318 + 0.5);
               _loc1_ = int(Math.ceil(var_318));
               var_1274.y = Number(80 + var_318 * class_864.var_332);
               if(int(var_496[var_319][_loc1_]) != -1)
               {
                  _loc2_ = _loc1_ - 1;
                  var_1274.y = Number(80 + _loc2_ * class_864.var_332);
                  var_1274.tabIndex = var_319 * 10 + _loc2_;
                  var_496[var_319][_loc2_] = var_452;
                  method_965();
                  break;
               }
               break;
            case 3:
               if(var_1275 != var_319 && int(class_691.method_114(8)) == 0)
               {
                  var_319 = var_1275;
                  if(var_251 < 6)
                  {
                     var_251 = 6;
                  }
               }
               method_964();
               _loc1_ = var_251;
               var_251 = var_251 - 1;
               if(_loc1_ < 0)
               {
                  method_691();
                  break;
               }
         }
      }
      
      public function method_966(param1:int, param2:int) : int
      {
         if(Number(Math.abs(param1 - name_36)) < Number(Math.abs(param2 - name_36)))
         {
            return -1;
         }
         return 1;
      }
      
      public function method_967() : void
      {
      }
      
      public function method_969() : void
      {
         var_1274 = new class_373();
         var_1276.push(var_1274);
         var_1214.addChild(var_1274);
         var_1274.x = Number(50 + int(class_864.var_268 * 0.5) * class_864.var_332);
         var_1274.y = -50;
         var_318 = -0.5;
         var_1274.gotoAndStop(var_452 + 1);
         if(var_452 == 0)
         {
            name_3 = 1;
         }
         else
         {
            var_319 = int(method_968());
            var_1275 = int(method_968());
            name_3 = 3;
            var_251 = 8;
         }
      }
      
      public function method_964() : void
      {
         var _loc1_:Boolean = int(var_496[var_319][0]) == -1;
         var_318 = !!_loc1_?-0.5:Number(-1);
         var_1274.x = Number(var_1274.x + (50 + var_319 * class_864.var_332 - var_1274.x) * 0.6);
         var _loc2_:Number = 80 + var_318 * class_864.var_332 - var_1274.y;
         var_1274.y = Number(var_1274.y + _loc2_ * 0.4);
         if(_loc2_ < 25 && var_319 != var_1277)
         {
            var_1274.y = Number(80 + -1 * class_864.var_332);
         }
         var_1277 = var_319;
      }
      
      public function method_249(param1:Array, param2:int, param3:int) : void
      {
         var _loc6_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = class_864.var_269;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            if(int(param1[param2][_loc6_ + 1]) != -1)
            {
               param1[param2][_loc6_] = param3;
               return;
            }
         }
      }
      
      public function method_691() : void
      {
         name_36 = var_319;
         var_1274.x = Number(50 + var_319 * class_864.var_332);
         name_3 = 2;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - 100 * param1];
         super.method_95(param1);
         var_496 = method_970();
         name_36 = 3;
         var_452 = param1 < 0.5?0:1;
         var_1276 = [];
         method_117();
         method_969();
      }
      
      public function method_395(param1:Number) : Number
      {
         return Number(80 + param1 * class_864.var_332);
      }
      
      public function method_394(param1:Number) : Number
      {
         return Number(50 + param1 * class_864.var_332);
      }
      
      public function method_971(param1:Number) : int
      {
         return int(Math.round((param1 - 50) / class_864.var_332));
      }
      
      public function method_662(param1:int, param2:Array) : Array
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Array = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:Array = null;
         var _loc13_:* = null;
         var _loc14_:Array = null;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         _loc5_ = class_864.var_268;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = [];
            _loc8_ = 0;
            _loc9_ = class_864.var_269;
            while(_loc8_ < _loc9_)
            {
               _loc8_++;
               _loc10_ = _loc8_;
               if(int(param2[_loc6_][_loc10_]) == param1)
               {
                  _loc7_.push({
                     "L\x01":_loc6_,
                     "\x03\x01":_loc10_
                  });
               }
               else
               {
                  if(int(_loc7_.length) > int(_loc3_.length))
                  {
                     _loc3_ = _loc7_;
                  }
                  _loc7_ = [];
               }
            }
            if(int(_loc7_.length) > int(_loc3_.length))
            {
               _loc3_ = _loc7_;
            }
         }
         _loc4_ = 0;
         _loc5_ = class_864.var_269;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = [];
            _loc8_ = 0;
            _loc9_ = class_864.var_268;
            while(_loc8_ < _loc9_)
            {
               _loc8_++;
               _loc10_ = _loc8_;
               if(int(param2[_loc10_][_loc6_]) == param1)
               {
                  _loc7_.push({
                     "L\x01":_loc10_,
                     "\x03\x01":_loc6_
                  });
               }
               else
               {
                  if(int(_loc7_.length) > int(_loc3_.length))
                  {
                     _loc3_ = _loc7_;
                  }
                  _loc7_ = [];
               }
            }
            if(int(_loc7_.length) > int(_loc3_.length))
            {
               _loc3_ = _loc7_;
            }
         }
         _loc7_ = [];
         _loc4_ = 0;
         _loc5_ = class_864.var_268 + class_864.var_269;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_[_loc6_] = [];
         }
         _loc4_ = 0;
         _loc5_ = class_864.var_268;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc8_ = 0;
            _loc9_ = class_864.var_269;
            while(_loc8_ < _loc9_)
            {
               _loc8_++;
               _loc10_ = _loc8_;
               _loc7_[_loc6_ + _loc10_].push({
                  "L\x01":_loc6_,
                  "\x03\x01":_loc10_,
                  "1*\x05\x02":int(param2[_loc6_][_loc10_])
               });
            }
         }
         _loc4_ = 0;
         while(_loc4_ < int(_loc7_.length))
         {
            _loc11_ = _loc7_[_loc4_];
            _loc4_++;
            _loc12_ = [];
            _loc5_ = 0;
            while(_loc5_ < int(_loc11_.length))
            {
               _loc13_ = _loc11_[_loc5_];
               _loc5_++;
               if(int(_loc13_.package_21) == param1)
               {
                  _loc12_.push({
                     "L\x01":int(_loc13_.var_244),
                     "\x03\x01":int(_loc13_.var_245)
                  });
               }
               else
               {
                  if(int(_loc12_.length) > int(_loc3_.length))
                  {
                     _loc3_ = _loc12_;
                  }
                  _loc12_ = [];
               }
            }
            if(int(_loc12_.length) > int(_loc3_.length))
            {
               _loc3_ = _loc12_;
            }
         }
         _loc11_ = [];
         _loc4_ = 0;
         _loc5_ = class_864.var_268 + class_864.var_269;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc11_[_loc6_] = [];
         }
         _loc4_ = 0;
         _loc5_ = class_864.var_268;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc8_ = 0;
            _loc9_ = class_864.var_269;
            while(_loc8_ < _loc9_)
            {
               _loc8_++;
               _loc10_ = _loc8_;
               _loc11_[class_864.var_269 - 1 + _loc6_ - _loc10_].push({
                  "L\x01":_loc6_,
                  "\x03\x01":_loc10_,
                  "1*\x05\x02":int(param2[_loc6_][_loc10_])
               });
            }
         }
         _loc4_ = 0;
         while(_loc4_ < int(_loc11_.length))
         {
            _loc12_ = _loc11_[_loc4_];
            _loc4_++;
            _loc14_ = [];
            _loc5_ = 0;
            while(_loc5_ < int(_loc12_.length))
            {
               _loc13_ = _loc12_[_loc5_];
               _loc5_++;
               if(int(_loc13_.package_21) == param1)
               {
                  _loc14_.push({
                     "L\x01":int(_loc13_.var_244),
                     "\x03\x01":int(_loc13_.var_245)
                  });
               }
               else
               {
                  if(int(_loc14_.length) > int(_loc3_.length))
                  {
                     _loc3_ = _loc14_;
                  }
                  _loc14_ = [];
               }
            }
            if(int(_loc14_.length) > int(_loc3_.length))
            {
               _loc3_ = _loc14_;
            }
         }
         return _loc3_;
      }
      
      public function method_968() : int
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:Array = null;
         var _loc1_:Array = [];
         _loc2_ = 0;
         _loc3_ = class_864.var_268;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            if(int(var_496[_loc4_][0]) == -1)
            {
               _loc1_.push(_loc4_);
            }
         }
         _loc1_.sort(method_966);
         if(var_237[0] > 0.2)
         {
            _loc2_ = 0;
            while(_loc2_ < int(_loc1_.length))
            {
               _loc3_ = int(_loc1_[_loc2_]);
               _loc2_++;
               _loc5_ = method_972(var_496);
               method_249(_loc5_,_loc3_,var_452);
               _loc6_ = method_662(var_452,_loc5_);
               if(int(_loc6_.length) == 4)
               {
                  return _loc3_;
               }
            }
         }
         if(Number(Math.random()) < var_237[0])
         {
            _loc2_ = 0;
            while(_loc2_ < int(_loc1_.length))
            {
               _loc3_ = int(_loc1_[_loc2_]);
               _loc2_++;
               _loc5_ = method_972(var_496);
               method_249(_loc5_,_loc3_,1 - var_452);
               _loc6_ = method_662(1 - var_452,_loc5_);
               if(int(_loc6_.length) == 4)
               {
                  return _loc3_;
               }
            }
         }
         _loc2_ = 3;
         if(_loc2_ > int(_loc1_.length))
         {
            _loc2_ == int(_loc1_.length);
         }
         return int(_loc1_[int(class_691.method_114(_loc2_))]);
      }
      
      public function method_970() : Array
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         var _loc3_:int = class_864.var_268;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc1_[_loc4_] = [];
            _loc5_ = 0;
            _loc6_ = class_864.var_269;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc1_[_loc4_][_loc7_] = -1;
            }
         }
         return _loc1_;
      }
      
      public function method_965() : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_373 = null;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc1_:Array = method_662(var_452,var_496);
         if(int(_loc1_.length) >= 4)
         {
            name_3 = 4;
            method_81(var_452 == 0,20);
            _loc2_ = [];
            _loc3_ = 0;
            _loc4_ = var_1276;
            while(_loc3_ < int(_loc4_.length))
            {
               _loc5_ = _loc4_[_loc3_];
               _loc3_++;
               _loc6_ = 0;
               while(_loc6_ < int(_loc1_.length))
               {
                  _loc7_ = _loc1_[_loc6_];
                  _loc6_++;
                  if(_loc5_.tabIndex == int(_loc7_.var_244) * 10 + int(_loc7_.var_245))
                  {
                     _loc2_.push(_loc5_);
                  }
               }
            }
            _loc3_ = 0;
            while(_loc3_ < int(_loc2_.length))
            {
               _loc5_ = _loc2_[_loc3_];
               _loc3_++;
               new class_931(_loc5_,0.1,16777215);
            }
            return;
         }
         var_452 = 1 - var_452;
         method_969();
      }
      
      public function method_972(param1:Array) : Array
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc2_:Array = method_970();
         var _loc3_:int = 0;
         var _loc4_:int = class_864.var_268;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_[_loc5_] = [];
            _loc6_ = 0;
            _loc7_ = class_864.var_269;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc2_[_loc5_][_loc8_] = int(param1[_loc5_][_loc8_]);
            }
         }
         return _loc2_;
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_372 = null;
         class_319 = new class_371();
         addChild(class_319);
         var_1214 = new Sprite();
         addChild(var_1214);
         var _loc1_:Sprite = new Sprite();
         addChild(_loc1_);
         var _loc2_:int = 0;
         var _loc3_:int = class_864.var_268;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = class_864.var_269;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = new class_372();
               _loc1_.addChild(_loc8_);
               _loc8_.x = Number(50 + _loc4_ * class_864.var_332);
               _loc8_.y = Number(80 + _loc7_ * class_864.var_332);
            }
         }
         var _loc9_:class_370 = new class_370();
         addChild(_loc9_);
         _loc9_.x = 50 + 0 * class_864.var_332 - class_864.var_332 * 0.5;
         _loc9_.y = Number(Number(80 + (class_864.var_269 - 1) * class_864.var_332) + class_864.var_332 * 0.5);
      }
   }
}
