package package_24
{
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_739 extends class_656
   {
       
      
      public var var_263:Number;
      
      public var var_262:Number;
      
      public var var_250:Number;
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_251:Object;
      
      public var name_20:Object;
      
      public var var_274:Function;
      
      public var var_532:Object;
      
      public var var_233:Number;
      
      public var var_272:int;
      
      public var var_273:int;
      
      public function class_739()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_262 = 0;
         var_263 = 0;
         var_278 = 0;
         var_279 = 0;
         var_233 = 1;
         var_250 = 0;
         var_273 = 10;
         var_272 = 0;
      }
      
      public function method_118() : void
      {
         x = var_262;
         y = var_263;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         if(name_20 != null)
         {
            name_20 = name_20 - 1;
            if(name_20 <= 0)
            {
               name_20 = null;
               visible = true;
            }
            return;
         }
         var_279 = Number(var_279 + var_250);
         var_278 = var_278 * var_233;
         var_279 = var_279 * var_233;
         var_262 = Number(var_262 + var_278);
         var_263 = Number(var_263 + var_279);
         if(var_532 != null && var_263 >= Number(var_532.var_245))
         {
            var_263 = Number(var_532.var_245);
            var_278 = var_278 * var_532.name_73;
            var_279 = var_279 * -var_532.name_74;
         }
         method_118();
         if(var_251 != null)
         {
            var_251 = var_251 - 1;
            if(var_251 < var_273)
            {
               _loc1_ = var_251 / var_273;
               switch(var_272)
               {
                  case 0:
                     break;
                  case 1:
                     alpha = _loc1_;
                     break;
                  case 2:
                     _loc2_ = _loc1_;
                     scaleY = _loc2_;
                     scaleX = _loc2_;
                     break;
                  case 3:
                     visible = var_251 % 4 < 2;
                     break;
                  case 4:
                     class_657.name_18(this,0,int(255 * (1 - _loc1_)));
               }
            }
            if(var_251 <= 0)
            {
               method_89();
            }
         }
         super.method_79();
      }
      
      public function method_129(param1:Number, param2:Number) : void
      {
         var_262 = param1;
         var_263 = param2;
         method_118();
      }
      
      public function method_488(param1:Number, param2:Number = 1.0, param3:Number = 0.5) : void
      {
         var_532 = {
            "\x03\x01":param1,
            "n,A\x01":param2,
            "vA;\x01":param3
         };
      }
      
      override public function method_89() : void
      {
         if(var_274 != null)
         {
            var_274();
         }
         super.method_89();
      }
   }
}
