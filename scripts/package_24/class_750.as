package package_24
{
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import package_32.class_899;
   
   public class class_750
   {
       
      
      public var var_245:int;
      
      public var var_244:int;
      
      public var var_371:int;
      
      public var var_741:BitmapData;
      
      public var var_739:Boolean;
      
      public var var_738:Boolean;
      
      public var var_740:Object;
      
      public var var_769:Rectangle;
      
      public var var_376:int;
      
      public var var_737:int;
      
      public var var_736:int;
      
      public function class_750(param1:BitmapData = undefined, param2:Object = undefined, param3:Object = undefined, param4:Object = undefined, param5:Object = undefined, param6:Boolean = false, param7:Boolean = false, param8:Object = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_741 = param1;
         var_244 = param2;
         var_245 = param3;
         var_740 = param8;
         var_371 = param4;
         var_376 = param5;
         var_769 = new Rectangle(param2,param3,param4,param5);
         var_738 = param6;
         var_739 = param7;
         var_736 = 0;
         var_737 = 0;
      }
      
      public function method_366(param1:int, param2:int, param3:Object, param4:Object) : class_750
      {
         return new class_750(var_741,var_244 + param1,var_245 + param2,param3,param4,var_738,var_739,var_740);
      }
      
      public function method_505(param1:BitmapData, param2:int, param3:int) : void
      {
         var _loc4_:class_738 = new class_738();
         _loc4_.method_128(this,0,0);
         var _loc5_:Matrix = new Matrix();
         _loc5_.translate(param2,param3);
         param1.draw(_loc4_,_loc5_);
      }
      
      public function method_506(param1:BitmapData, param2:int, param3:int) : void
      {
         var _loc4_:Point = new Point(param2,param3);
         param1.copyPixels(var_741,var_769,_loc4_);
      }
   }
}
