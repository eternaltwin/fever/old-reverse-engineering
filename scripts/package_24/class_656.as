package package_24
{
   import package_32.class_899;
   
   public class class_656 extends class_738
   {
      
      public static var var_299:Array = [];
       
      
      public var var_271:Boolean;
      
      public var var_744:Array;
      
      public var var_198:class_878;
      
      public function class_656()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         class_656.var_299.push(this);
         var_271 = true;
      }
      
      public function method_79() : void
      {
         var _loc1_:class_750 = null;
         if(var_198 != null)
         {
            var_198.method_79();
            if(!!visible && var_198 != null)
            {
               _loc1_ = var_198.method_485();
               if(_loc1_ != var_742)
               {
                  graphics.clear();
                  method_128(_loc1_);
               }
            }
         }
         if(var_271)
         {
            method_293();
         }
      }
      
      public function method_486(param1:class_878) : void
      {
         if(var_198 != null)
         {
            param1.var_205 = var_198.var_205;
            param1.var_743 = var_198.var_743;
         }
         var_198 = param1;
      }
      
      public function method_290(param1:Array) : void
      {
         var_744 = param1;
         name_1();
      }
      
      public function method_137(param1:class_878, param2:Boolean = true) : void
      {
         var_198 = param1;
         if(var_198 == null)
         {
            return;
         }
         var_198.var_743 = param2;
         method_128(var_198.method_485());
      }
      
      public function name_1() : void
      {
         method_137(var_744.shift());
         if(int(var_744.length) > 0)
         {
            var_198.var_258 = name_1;
         }
      }
      
      override public function method_89() : void
      {
         class_656.var_299.method_116(this);
         super.method_89();
      }
      
      public function method_487(param1:class_878, param2:Boolean = true) : void
      {
         if(param1 != null && var_198 != null && var_198.var_745 == param1.var_745)
         {
            return;
         }
         method_137(param1);
      }
      
      override public function name_72() : void
      {
         super.name_72();
         var_198 = null;
         var_271 = true;
         var_744 = null;
      }
   }
}
