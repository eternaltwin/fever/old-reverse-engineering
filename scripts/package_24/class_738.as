package package_24
{
   import flash.display.BitmapData;
   import flash.display.BlendMode;
   import flash.display.DisplayObjectContainer;
   import flash.display.Sprite;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import package_32.class_899;
   
   public class class_738 extends Sprite
   {
      
      public static var var_370:Boolean;
      
      public static var var_731:ColorTransform;
      
      public static var var_732:Matrix;
       
      
      public var var_733:class_874;
      
      public var var_735:Number;
      
      public var var_734:Number;
      
      public var var_742:class_750;
      
      public function class_738()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_734 = 0.5;
         var_735 = 0.5;
      }
      
      public function method_483(param1:Number, param2:Number) : void
      {
         var_734 = param1;
         var_735 = param2;
      }
      
      public function method_293() : void
      {
         x = int(Math.round(x));
         y = int(Math.round(y));
      }
      
      public function method_89() : void
      {
         if(parent != null)
         {
            parent.removeChild(this);
         }
      }
      
      public function method_484(param1:BitmapData, param2:Number, param3:Number) : void
      {
         graphics.clear();
         graphics.lineStyle();
         graphics.beginBitmapFill(param1,new Matrix());
         graphics.moveTo(0,0);
         graphics.lineTo(param2,0);
         graphics.lineTo(param2,param3);
         graphics.lineTo(0,param3);
         graphics.lineTo(0,0);
         graphics.endFill();
      }
      
      public function method_128(param1:class_750, param2:Object = undefined, param3:Object = undefined) : void
      {
         if(param2 != null)
         {
            var_734 = param2;
         }
         if(param3 != null)
         {
            var_735 = param3;
         }
         var _loc4_:int = -(int(param1.var_371 * var_734));
         var _loc5_:int = -(int(param1.var_376 * var_735));
         _loc4_ = _loc4_ + param1.var_736;
         _loc5_ = _loc5_ + param1.var_737;
         var _loc6_:Matrix = class_738.var_732;
         _loc6_.identity();
         var _loc7_:int = -(param1.var_244 - _loc4_);
         var _loc8_:int = -(param1.var_245 - _loc5_);
         _loc6_.translate(_loc7_,_loc8_);
         if(param1.var_738)
         {
            _loc6_.scale(-1,1);
         }
         if(param1.var_739)
         {
            _loc6_.scale(1,-1);
         }
         if(param1.var_740 != null)
         {
            _loc6_.rotate(param1.var_740);
         }
         graphics.clear();
         graphics.lineStyle();
         graphics.beginBitmapFill(param1.var_741,_loc6_);
         graphics.drawRect(_loc4_,_loc5_,param1.var_371,param1.var_376);
         graphics.endFill();
         var_742 = param1;
      }
      
      public function name_72() : void
      {
         var_734 = 0.5;
         var_735 = 0.5;
         graphics.clear();
         blendMode = BlendMode.NORMAL;
         transform.colorTransform = class_738.var_731;
         scaleX = 1;
         scaleY = 1;
         rotation = 0;
         alpha = 1;
         x = -100;
         y = -100;
         visible = true;
         filters = [];
      }
   }
}
